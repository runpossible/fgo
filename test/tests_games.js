/* global */
'use strict'
var expect = require('chai').expect,
    request = require('superagent');

describe('Games', function() {
    it('Can get empty list of games', function (done) {
        global.getAuthenticatedRequest('get', global.serverUrl + 'game', function (err, authenticatedRequest, cookies, user) {
            if (err) return done(err);

            authenticatedRequest.end(function (err, res3) {
                var data = JSON.parse(res3.text);
                expect(data).to.have.length(0);

                done(err);
            });
        });
    })
});