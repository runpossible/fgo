/* global */
'use strict';

var expect = require('chai').expect,
    request = require('superagent'),
    async = require('async');

describe('Game Stats', function () {
    var currentUser, currentLocation, currentGame;
    before(function (done) {
        async.waterfall([
            function getNewUser(callback) {
                global.getNewUser().then(function (regUser) {
                    currentUser = regUser;
                    return callback(null, currentUser);
                });
            },
            function getNewLocation(user, callback) {
                var locData = {
                    name: 'test_loc_' + global.guid(),
                    coordinates: [25.623979568481445, 43.0801731251127],
                    formattedAddress: 'ul. \"Elin Pelin\" 15, 5000 Veliko Tarnovo, Bulgaria'
                };

                global.newAuthRequest('post', 'location', currentUser, locData)
                    .end(function (err, res) {
                        expect(res.status).to.equal(200);
                        currentLocation = res.body;
                        return callback(null, currentLocation);
                    });
            },
            function getNewGame(location, callback){
                var game = {
                    name: 'test_game_' + global.guid(),
                    location: currentLocation
                };

                global.newAuthRequest('post', 'game', currentUser, game)
                    .end(function (err, res) {
                        expect(res.status).to.equal(200);
                        currentGame = res.body;
                        return callback(null, currentGame);
                    });
            }
        ], function (err, url) {
            return done();
        });
    });

    // GET Player Stats
    it('Can get player stats when no stats are present', function (done) {
        global.newAuthRequest('get', 'game/' + currentGame._id + '/player_stats', currentUser)
            .end(function (err, res) {
                expect(res.status).to.equal(200);
                expect(res.body).not.to.be.empty;
                expect(res.body.gameFacts).not.to.be.an('undefined');
                expect(res.body.playerRatings).not.to.be.an('undefined');
                expect(res.body.averageRatings).not.to.be.an('undefined');
                expect(res.body.gameFacts).to.be.empty;
                expect(res.body.playerRatings).to.be.empty;
                expect(res.body.averageRatings).to.be.empty;
                return done();
            });
    });

    it('Can get player stats when player has no access to game', function (done) {
        global.getNewUser().then(function (newUser) {
            global.newAuthRequest('get', 'game/' + currentGame._id + '/player_stats', newUser)
                .end(function (err, res) {
                    expect(res.status).to.equal(400);
                    return done();
                });
        });
    });

    it('Can get player stats when there are goals entered', function (done) {
        async.waterfall([
            function addGoals(callback) {
                var fact = new db.GameFact();
                fact.game = currentGame;
                fact.player = currentUser.id;
                fact.createdBy = currentUser.id;
                fact.value = 3;
                fact.type = 'GoalScored';

                fact.save(callback);
            }
        ], function (err, gameFact) {
            expect(err).to.be.null;
            global.newAuthRequest('get', 'game/' + currentGame._id + '/player_stats', currentUser)
                .end(function (err, res) {
                    expect(res.status).to.equal(200);
                    expect(res.body).not.to.be.empty;
                    expect(res.body.gameFacts).not.to.be.an('undefined');
                    expect(res.body.playerRatings).not.to.be.an('undefined');
                    expect(res.body.averageRatings).not.to.be.an('undefined');
                    expect(res.body.gameFacts.length).to.equal(1);
                    expect(res.body.playerRatings).to.be.empty;
                    expect(res.body.averageRatings).to.be.empty;

                    expect(res.body.gameFacts[0].player).to.equal(currentUser.id);
                    expect(res.body.gameFacts[0].value).to.equal(gameFact.value);
                    expect(res.body.gameFacts[0]._id).to.equal(gameFact.id);

                    return done();
                });
        });
    });
});