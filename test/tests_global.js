/* global */
'use strict'
var expect = require('chai').expect,
    request = require('superagent');

describe('tests common', function () {
    it('is server url loaded', function () {
        expect(global.serverUrl).to.equal('http://localhost:3000/api/v1/');
    });
});