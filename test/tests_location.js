/* global */
'use strict';

var expect = require('chai').expect,
    request = require('superagent'),
    _ = require('lodash');

var getFullLocData = function() {
    var locData = {
        name: 'test_loc_' + global.guid(),
        coordinates: [25.623979568481445, 43.0801731251127],
        formattedAddress: 'ul. \"Elin Pelin\" 15, 5000 Veliko Tarnovo, Bulgaria'
    };
    return locData;
};

describe('Groups', function () {
    var currentUser;
    before(function (done) {
        global.getNewUser().then(function (regUser) {
            currentUser = regUser;
            return done();
        });
    });

    // CREATE LOCATION
    it('Can create a new location - Invalid coordinates length = 1', function (done) {
        var locData = {
            name: 'test_loc_' + global.guid(),
            coordinates: [25.623979568481445]
        };
        global.newAuthRequest('post', 'location', currentUser, locData)
            .end(function (err, res) {
                expect(res.status).to.equal(400);
                return done();
            });
    });

    it('Can create a new location - Invalid coordinates length = 3', function (done) {
        var locData = {
            name: 'test_loc_' + global.guid(),
            coordinates: [25.623979568481445, 25.623979568481445, 25.623979568481445]
        };
        global.newAuthRequest('post', 'location', currentUser, locData)
            .end(function (err, res) {
                expect(res.status).to.equal(400);
                return done();
            });
    });

    it('Can create a new location - Invalid no coordinates', function (done) {
        var locData = {
            name: 'test_loc_' + global.guid()
        };
        global.newAuthRequest('post', 'location', currentUser, locData)
            .end(function (err, res) {
                expect(res.status).to.equal(400);
                return done();
            });
    });

    it('Can create a new location - Invalid name', function (done) {
        var locData = {
            coordinates: [25.623979568481445]
        };
        global.newAuthRequest('post', 'location', currentUser, locData)
            .end(function (err, res) {
                expect(res.status).to.equal(400);
                return done();
            });
    });

    it('Can create a new location', function (done) {
        var locData = getFullLocData();

        global.newAuthRequest('post', 'location', currentUser, locData)
            .end(function (err, res) {
                expect(res.status).to.equal(200);

                global.db.Location.find({name: locData.name})
                    .exec(function (err, locations) {
                        expect(locations).to.have.length(1);


                        var aLoc = locations[0];
                        expect(aLoc.name).to.equal(locData.name);
                        expect(aLoc.loc[0]).to.equal(locData.coordinates[0]);
                        expect(aLoc.loc[1]).to.equal(locData.coordinates[1]);
                        expect(aLoc.formattedAddress).to.equal(locData.formattedAddress);

                        return done();
                    });
            });
    });

    // GET LOCATION
    it('Can get location', function (done) {
        var locData = getFullLocData();
        global.newAuthRequest('post', 'location', currentUser, locData)
            .end(function (err, res) {
                expect(res.status).to.equal(200);

                global.newAuthRequest('get', 'location', currentUser)
                    .end(function (err, res) {
                        var locations = JSON.parse(res.text);
                        var loc1 = _.find(locations, 'name', locData.name);

                        expect(loc1.name).to.be.equal(locData.name);
                        expect(loc1.loc).to.have.length(2);
                        expect(loc1.loc[0]).to.be.equal(locData.coordinates[0]);
                        expect(loc1.loc[1]).to.be.equal(locData.coordinates[1]);
                        expect(loc1.formattedAddress).to.be.equal(locData.formattedAddress);

                        return done();
                    });
            });
    });

    // GET LOCATION BY ID
    it('Can get location - By ID', function (done) {
        var locData = getFullLocData();
        global.newAuthRequest('post', 'location', currentUser, locData)
            .end(function (err, res) {
                expect(res.status).to.equal(200);
                var id = JSON.parse(res.text)._id;

                global.newAuthRequest('get', 'location/' + id, currentUser)
                    .end(function (err, res) {
                        var loc1 = JSON.parse(res.text);

                        expect(loc1.name).to.be.equal(locData.name);
                        expect(loc1.loc).to.have.length(2);
                        expect(loc1.loc[0]).to.be.equal(locData.coordinates[0]);
                        expect(loc1.loc[1]).to.be.equal(locData.coordinates[1]);
                        expect(loc1.formattedAddress).to.be.equal(locData.formattedAddress);

                        return done();
                    });
            });
    });

    it('Can get location - By ID, Not found', function (done) {
        global.newAuthRequest('get', 'location/567b9fe138763276023480eb', currentUser)
            .end(function (err, res) {
                expect(res.status).to.be.equal(404);
                return done();
            });
    });

    // CREATE Favourite Locations
    it('Can add a new favourite location', function (done) {
        var locData = getFullLocData();

        global.newAuthRequest('post', 'location', currentUser, locData)
            .end(function (err, res) {
                expect(res.status).to.equal(200);
                var id = JSON.parse(res.text)._id;

                global.newAuthRequest('post', 'favourite/location/' + id, currentUser)
                    .end(function (err, res) {
                        expect(res.status).to.equal(200);

                        global.db.User.find({'name': currentUser.name})
                            .exec(function (err, usrList) {
                                var usr = usrList[0];
                                global.db.FavouriteLocation
                                    .find({user: usr._id})
                                    .populate('user location')
                                    .exec(function (err, favLocations) {
                                        //console.log(favLocations);
                                        expect(favLocations).to.have.length(1);

                                        var fLoc = favLocations[0];
                                        expect(fLoc.location._id + '').to.be.equal(id + '');
                                        expect(fLoc.user._id + '').to.be.equal(usr._id + '');
                                        return done();
                                    });
                            });

                    });
            });
    });

    it('Cannot add a DUPLICATE favourite location', function (done) {
        var locData = getFullLocData();

        global.getNewUser().then(function (regUser) {
            var usr = regUser;
            global.newAuthRequest('post', 'location', usr, locData)
                .end(function (err, res) {
                    expect(res.status).to.equal(200);
                    var id = JSON.parse(res.text)._id;

                    global.newAuthRequest('post', 'favourite/location/' + id, usr)
                        .end(function (err, res) {
                            expect(res.status).to.equal(200);

                            // ensure the second request fails to add the same favourite location
                            global.newAuthRequest('post', 'favourite/location/' + id, usr)
                                .end(function (err, res) {
                                    expect(res.status).to.equal(400);
                                    global.db.User.find({'name': usr.name})
                                        .exec(function (err, usrList) {
                                            var fullUsr = usrList[0];
                                            // ensure it's not entered twice
                                            global.db.FavouriteLocation
                                                .find({user: fullUsr._id})
                                                .populate('user location')
                                                .exec(function (err, favLocations) {
                                                    expect(favLocations).to.have.length(1);
                                                    return done();
                                                });
                                        });
                                });
                        });
                });
        });
    });

    // GET Favourite Locations
    it('Can GET user`s favourite locations', function (done) {
        var locData = getFullLocData();

        global.getNewUser().then(function (regUser) {
            var usr = regUser;
            global.newAuthRequest('post', 'location', usr, locData)
                .end(function (err, res) {
                    var id = JSON.parse(res.text)._id;

                    global.newAuthRequest('post', 'favourite/location/' + id, usr)
                        .end(function (err, res) {

                            global.newAuthRequest('get', 'favourite/location', usr)
                                .end(function (err, res2) {
                                    expect(res2.status).to.be.equal(200);
                                    var favLocations = JSON.parse(res2.text);
                                    expect(favLocations).to.have.length(1);

                                    var fLoc = favLocations[0];
                                    expect(fLoc.location._id + '').to.be.equal(id + '');
                                    expect(fLoc.user.name + '').to.be.equal(usr.name + '');
                                    return done();
                                });
                        });
                });
        });
    });
});