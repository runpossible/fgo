// tutorials: https://semaphoreci.com/community/tutorials/getting-started-with-node-js-and-mocha
var request = require('superagent'),
    expect = require('chai').expect,
    ok = require('okay'),
    Q = require('Q'),
    async = require('async');

// require the utilities.
require('../server/Utilities');

var env = global.getEnvironment();
var config = rootRequire('/config/config')[env];

before(function(done) {
    console.log('global before');

    global.serverUrl = "http://localhost:3000/api/v1/";

    global.guid = function guid() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    };

    global.registerUser = function(data, done) {
        var body = data || {
            email: 'test_user_' + global.guid(),
            password: '123',
            name:' test_user_' + global.guid()
        };

        var registerUrl = global.serverUrl + 'auth/register';
        request.post(registerUrl)
            .timeout(30000)
            .send(body)
            .set('Accept', 'application/json')
            .end(function(err, res) {
                if(err) return done(err, res);

                body.id = res.body.user.id;

                return done(err, res, body);
            });
    };

    global.login = function(data, cookies, done) {
        var loginUrl = global.serverUrl + 'auth/login';

        var req = request.post(loginUrl);
        req.cookies = cookies;

        return req.set('Accept', 'application/json')
            .send(data)
            .end(done);
    };

    global.getAuthenticatedRequest = function(method, url, done) {
        // register a user
        global.registerUser(null, function(err, res, user) {
            expect(res.status).to.equal(201);

            // get the session cookie
            var cookies = res.headers['set-cookie'].pop().split(';')[0];

            // login using the session cookie
            global.login({
                username: user.email,
                password: user.password
            }, cookies, function(err, loginResponse) {
                var loginData = JSON.parse(loginResponse.text);
                if(!loginData.success) {
                    return done(new Error('could not log in - success: false'));
                }

                if(err) {
                    return done(new Error('could not log in'));
                }

                var req = request[method](url);
                req.cookies = cookies;

                req.set('Accept', 'application/json');
                req.set('Content-Type', 'application/json');
                return done(null, req, cookies, loginData.user);
            });
        });
    };

    global.newAuthRequest = function(method, url, currentUser, data) {
        var fullUrl = global.serverUrl + url;

        var req = request[method](fullUrl);
        req.cookies = currentUser.cookies;
        req.set('Accept', 'application/json');
        req.set('Content-Type', 'application/json');

        if(data) {
            req.send(data);
        }

        return req;
    };

    global.getNewUser = function(userData) {
        var deferred = Q.defer();

        global.registerUser(userData, function(err, res, user) {
            expect(res.status).to.equal(201);

            // get the session cookie
            var cookies = res.headers['set-cookie'].pop().split(';')[0];

            // login using the session cookie
            global.login({
                username: user.email,
                password: user.password
            }, cookies, function(err, loginResponse) {
                var loginData = JSON.parse(loginResponse.text);
                if (!loginData.success) {
                    return deferred.reject(new Error('could not log in - success: false'));
                }

                if (err) {
                    return deferred.reject(new Error('could not log in'));
                }

                return deferred.resolve({
                    email: user.email,
                    name: user.name,
                    id: user.id,
                    cookies: cookies
                });
            });
        });

        return deferred.promise;
    };

    rootRequire('/db/mongoose')(config, function d(err, db) {
        // set DB in the global
        global.db = db;

        return done();
    });
});

after(function(done) {
    console.log('global after');

    async.waterfall([
        function rmUsers(cb) {
            db.User.remove({
                email: {
                    $regex: /test_user_.*/i
                }
            }, cb);
        },
        function rmGroups(rmUsersCount, cb) {
            db.Group.remove({
               name: {
                   $regex: /test_group_.*/i
               }
            }, cb);
        },
        function rmLocations(a, cb) {
            db.Location.remove({
                name: {
                    $regex: /test_loc_.*/i
                }
            }, cb);
        },
        function rmGames(a, cb) {
            db.Game.remove({
                name: {
                    $regex: /test_game_.*/i
                }
            }, cb);
        },
        function rmGameFacts(a, cb){
            // delete all GameFacts with broken game references
            db.GameFact.find().exec(ok(cb, function(gameFacts)
            {
                async.each(gameFacts, function(gameFact, callback) {
                   db.Game.findOne({'_id': gameFact.game}).exec(function(findErr, foundGame) {
                       if (!foundGame) {
                           db.GameFact.remove({_id: gameFact._id}).exec(callback);
                       } else {
                           callback(null, gameFact);
                       }
                   });
               }, function(err){
                    cb(null, gameFacts);
                });
            }));
        },
        function rmPlayerRatings(a, cb){
            // delete all PlayerRatings with broken game references
            db.PlayerRating.find().exec(ok(cb, function(playerRatings)
            {
                async.each(playerRatings, function(playerRating, callback) {
                    db.Game.findOne({'_id': playerRating.game}).exec(function(findErr, foundGame) {
                        if (!foundGame) {
                            db.PlayerRating.remove({_id: playerRating._id}).exec(callback);
                        } else {
                            callback();
                        }
                    });
                }, function(err){
                    cb(null, playerRatings);
                });
            }));
        }
    ], function final(err) {
        return done(err);
    });
});