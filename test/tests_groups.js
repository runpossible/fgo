/* global */
'use strict';

var expect = require('chai').expect,
    request = require('superagent');

describe('Groups', function () {
    var currentUser;
    before(function (done) {
        global.getNewUser().then(function (regUser) {
            currentUser = regUser;
            return done();
        });
    });

    // GET Groups
    it('Can get empty list of groups', function (done) {
        global.newAuthRequest('get', 'group', currentUser)
            .end(function (err, res) {
                var data = JSON.parse(res.text);
                expect(data).to.be.empty;
                return done();
            });
    });

    it('Can get all user groups', function (done) {
        global.getNewUser().then(function (regUser) {
            var newUsr = regUser;

            var g = {
                name: 'test_group_' + global.guid()
            };
            global.newAuthRequest('post', 'group', newUsr, g)
                .end(function (err, res) {
                    expect(res.status).to.equal(200);

                    global.newAuthRequest('get', 'group', newUsr)
                        .end(function (err, gRes) {
                            var groups = JSON.parse(gRes.text);
                            expect(groups).to.have.length(1);
                            expect(groups[0].name).to.equal(g.name);

                            return done();
                        });
                });
        });
    });


    it('Can get all user groups - EMPTY list', function (done) {
        global.getNewUser().then(function (regUser) {
            var newUsr = regUser;

            global.newAuthRequest('get', 'group', newUsr)
                .end(function (err, res) {
                    expect(res.status).to.equal(200);
                    var groups = JSON.parse(res.text);
                    expect(groups).to.have.length(0);

                    return done();
                });
        });
    });

    // CREATE a group
    it('Can create a new group', function (done) {
        var g = {
            name: 'test_group_' + global.guid()
        };
        global.newAuthRequest('post', 'group', currentUser, g)
            .end(function (err, res) {
                expect(res.status).to.equal(200);

                global.db.Group.find({name: g.name})
                    .exec(function (err, groups) {
                        if (err) return done(err);

                        expect(groups).to.have.length(1);
                        var actualG = groups[0];
                        expect(actualG.name).to.equal(g.name);

                        return done();
                    });
            });
    });

    it('Cannot create a new group without a Name', function (done) {
        var g = {
            customData: 'test_group_' + global.guid()
        };
        global.newAuthRequest('post', 'group', currentUser, g)
            .end(function (err, res) {
                expect(res.status).to.equal(400);
                return done();
            });
    });

    // DELETE a group
    it('OWNER can delete a group', function (done) {
        var g = {
            name: 'test_group_' + global.guid()
        };
        global.newAuthRequest('post', 'group', currentUser, g)
            .end(function (err, res) {
                expect(res.status).to.equal(200);
                var id = JSON.parse(res.text)._id;

                global.newAuthRequest('del', 'group/' + id, currentUser)
                    .end(function (err, res) {
                        expect(res.status).to.equal(200);

                        global.db.Group.find({name: g.name})
                            .exec(function (err, groups) {
                                if (err) return done(err);

                                expect(groups).to.have.length(0);

                                return done();
                            });
                    });

            });
    });

    it('Non-owner cannot delete a group', function (done) {
        var g = {
            name: 'test_group_' + global.guid()
        };
        global.newAuthRequest('post', 'group', currentUser, g)
            .end(function (err, res) {
                expect(res.status).to.equal(200);
                var id = JSON.parse(res.text)._id;

                global.getNewUser().then(function (regUser) {
                    var newUsr = regUser;

                    global.newAuthRequest('del', 'group/' + id, newUsr)
                        .end(function (err, res) {
                            expect(res.status).to.equal(401);

                            var obj = JSON.parse(res.text);
                            expect(obj.errorMessage).to.equal('Access Denied');
                            return done();
                        });

                });
            });
    });
});