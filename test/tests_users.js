/* global */
'use strict';

var expect = require('chai').expect,
    request = require('superagent');

describe('Users', function() {
    var currentUser;
    before(function(done) {
        global.getNewUser().then(function(regUser) {
            currentUser = regUser;
            return done();
        });
    });

    it('Can register a new local user', function (done) {
        var expectedUser = {
            email: 'test_user_' + global.guid(),
            password: '123',
            name:' test_user_' + global.guid()
        };

        // when returning a promise, done is not needed in the IT and all
        // errors are caught and returned to Mocha from the expect.
        global.getNewUser(expectedUser).then(function(registeredUser) {
            expect(registeredUser.cookies).to.not.be.empty;

            // check the database
            global.db.User.find({ name: expectedUser.name })
                .exec(function(err, usersList) {
                    var dbUser = usersList[0];
                    expect(dbUser.email).to.equal(expectedUser.email);
                    expect(dbUser.name).to.equal(expectedUser.name);
                    return done();
                });
        });
    });

    it('Search user without query - results in BAD REQUEST', function(done) {
        var req = global.newAuthRequest('get', 'search-users', currentUser);

        req.end(function(err, res) {
            expect(res.status).to.equal(400);
            return done();
        });
    });

    it('Search user - unexisting user', function(done) {
        global.newAuthRequest('get', 'search-users?email=unexisting', currentUser)
            .end(function(err, res) {
                expect(res.status).to.equal(200);
                var usr = JSON.parse(res.text);
                expect(usr).to.be.empty;
                return done();
            });
    });

    it('Search user - can find user by EMAIL', function(done) {
        global.newAuthRequest('get', 'search-users?email=' + currentUser.email, currentUser)
            .end(function(err, res) {
                expect(res.status).to.equal(200);

                var foundUser = JSON.parse(res.text)[0];
                expect(foundUser.email).to.equal(currentUser.email);
                expect(foundUser.name).to.equal(currentUser.name);

                return done();
            });
    });

    it('Search user - can find user by NAME', function(done) {
        global.newAuthRequest('get', 'search-users?name=' + currentUser.name, currentUser)
            .end(function(err, res) {
                expect(res.status).to.equal(200);

                var foundUser = JSON.parse(res.text)[0];
                expect(foundUser.email).to.equal(currentUser.email);
                expect(foundUser.name).to.equal(currentUser.name);

                return done();
            });
    });
});