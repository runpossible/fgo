VERSION=1.0.7

make-image:
	s2i build . registry.access.redhat.com/rhscl/nodejs-4-rhel7 registry.starter-us-east-1.openshift.com/fgo/fgo:$(VERSION)

push-image:
	docker push registry.starter-us-east-1.openshift.com/fgo/fgo

make-and-push: make-image push-image

test: make-image
	docker run -i -t registry.starter-us-east-1.openshift.com/fgo/fgo:$(VERSION) /bin/bash