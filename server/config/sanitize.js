var escape = require('escape-html');

module.exports = function(obj) {
    if (obj && obj instanceof Object) {
        for (var key in obj) {
            var value = obj[key];
            // if the passed value is a string
            if(value && typeof value === 'string') {
                // Protect for Mongo Injection
                // http://blog.websecurify.com/2014/08/hacking-nodejs-and-mongodb.html
                if (/^\$/.test(key)) {
                    delete obj[key];
                } else {
                    // escape html
                    obj[key] = escape(obj[key]);
                }
            }
        }
    }
    return obj;
};