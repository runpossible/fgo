'use strict';
var winston = require('winston'),
	base64url = require('base64url');

var addRequestInfo = function(logEntry, req) {
	var req = req || {};
	logEntry.request = {
		originalUrl: req.originalUrl
	};
	logEntry.request.body = req.body ? JSON.stringify(req.body) : '';
	logEntry.request.params = req.params ? JSON.stringify(req.params) : '';
	logEntry.request.user = req.user ? req.user._id.toString() : '';
    logEntry.request.method = req.method;
    logEntry.request.sessionID = req.sessionID || '';
	logEntry.createdAt = new Date(); // utc
};

exports = module.exports = {
	sanitizeUser: function(user, includeEmail) {
		var id = '';
		if(user.id) {
			id = user.id.toString();
		}
		if(user._id) {
			id = user._id.toString();
		}

	    var result = {
			id: id,
			_id: user._id,
			//email: user.email,
			//facebook: user.facebook,
			//google: user.google,
			hasFacebook: user.facebook && user.facebook.id,
			hasGoogle: user.google && user.google.id,
			preferredAvatar: user.preferredAvatar || 3,
			preferredPosition: user.preferredPosition,
			firstName: user.firstName,
			lastName: user.lastName,
			language: user.language,
			birthDate: user.birthDate,
			city: user.city,
			height: user.height,
			weight: user.weight,
			countryId: user.countryId,
			strongFoot: user.strongFoot,
			name: user.name,
			timezone: user.timezone,
			public: user.public,
			hasLocal: (user.local != undefined) && (user.local.hashed_pwd != undefined),
			friendsCount: (user.friends || []).length,
			shouldReceiveGameInviteMails: user.shouldReceiveGameInviteMails,
			master: user.master
		};

		if(includeEmail){
			result.email = user.email;
		}

		if(user.birthDay){
			result.age = new Date(new Date - user.birthDay).getFullYear()-1970;
		}

		if(config.cacheAvatar){
			result.avatarUrl = user.avatarUrl;
		}

		if(config.profileActivationRequired && user.local && user.local.activationCode){
			result.activationCode = user.local.activationCode;
		}

		return result;
	},

	logError: function(err, req) {
		try {
			err = err || '';
			var message = err.message || '';
			var logEntry = {
				stack: err.stack || '',
				statusCode: err.statusCode || '',
				data: err.data || ''
			};

			if(!logEntry.stack && logEntry.data && logEntry.data.errorMessage && logEntry.data.errorMessage.stack){
				logEntry.stack = logEntry.data.errorMessage.stack;
				delete logEntry.data.errorMessage.stack;
			}

			if(!message && logEntry.data && logEntry.data.errorMessage && logEntry.data.errorMessage.message){
				message = logEntry.data.errorMessage.message;
				delete logEntry.data.errorMessage.message;
			}

			addRequestInfo(logEntry, req);

			winston.loggers.get('error-logger').error(message, logEntry);
		}
		catch(e) {
			// should not happen - like never.
			console.log('Log error has failed: ' + e);
		}
	},

	logInfo: function(data, req) {
		try {
            data = data || {};
			var message = data.message || '';
			var logEntry = {
                statusCode: data.statusCode
			};

			addRequestInfo(logEntry, req);

			winston.loggers.get('info-logger').info(message, logEntry);
		}
		catch(e) {
			// should not happen - like never.
			console.log('Log error has failed: ' + e);
		}
	},

	addStateParam: function(req, requestData){
		if(req.query.redirect_uri){
			var state = {
				redirect_uri: req._parsedUrl.query
			};
			requestData.state = base64url(JSON.stringify(state));
		}
		return requestData;
	},

	addTargetEmailParam: function(req, requestData){
		if(req.query.targetEmail){
			var state = {
				targetEmail: req.query.targetEmail
			};
			requestData.state = base64url(JSON.stringify(state));
		}
		return requestData;
	},

	getRedirectUriFromState: function(req, defaultSuccessRedirect){
		if(req.query.state){
			var decodedState = base64url.decode(req.query.state);
			var state = JSON.parse(decodedState);
			if(state.redirect_uri){
				return decodeURIComponent(state.redirect_uri).replace('redirect_uri=', '');
			}
		}
		return defaultSuccessRedirect;
	},

	getTargetEmailFromState: function(req){
		if(req.query.state){
			var decodedState = base64url.decode(req.query.state);
			var state = JSON.parse(decodedState);
			if(state.targetEmail){
				return decodeURIComponent(state.targetEmail);
			}
		}
		return null;
	},

	getGoogleScopes: function(){
		return ['https://www.googleapis.com/auth/plus.login',
			'https://www.googleapis.com/auth/plus.me',
			'https://www.googleapis.com/auth/plus.profile.emails.read',
			'email',
			'profile'];
	},

	getFacebookScopes: function(){
		return ['email',
			'user_friends',
			'user_posts',
			'publish_actions'];
	}
};