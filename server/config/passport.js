var passport = require('passport'),
	LocalStrategy = require('passport-local').Strategy,
	FacebookStrategy = require('passport-facebook').Strategy,
	GoogleStrategy = require('passport-google-oauth').OAuth2Strategy,
	BearerStrategy = require('passport-http-bearer').Strategy,
	AvatarService = require('../services/AvatarService'),
	MailService = require('../services/MailService'),
	AccessTokenService = require('../services/AccessTokenService'),
	common = require('./common');

exports = module.exports = function(_db, _config) {
	var db = _db;
	var config = _config;


	// Local Strategy
	passport.use(new LocalStrategy(
		function(email, password, done) {
			db.User.findOne({
				"email": email.toLowerCase()
			}).exec(function(err, user) {
				if (err) {
					return done(err);
				}

				if (user && user.authenticateLocal(password)) {
					return done(null, common.sanitizeUser(user, true));
				}
				return done(null, false);
			});
		}
	));

	passport.serializeUser(function(user, done) {
		done(null, user._id);
	});

	passport.deserializeUser(function(id, done) {
		db.User.findOne({
			_id: id
		}).exec(function(err, user) {
			if (err) return done(err);

			if (user) return done(null, user);
			return done(null, false);
		});
	});

	// TODO: Refactor
	var getUserByEmail = function(email, done) {
		db.User.findOne({
			'email': email
		}, function(err, user) {
			if (err) return done(err);
			return done(null, user);
		});
	};

	// Facebook - https://scotch.io/tutorials/easy-node-authentication-facebook
	passport.use(new FacebookStrategy({
			clientID: config.auth.facebook.client_id,
			clientSecret: config.auth.facebook.client_secret,
			callbackURL: config.auth.facebook.callbackURL,
			profileFields: ['id', 'displayName', 'link', 'photos', 'emails'],
			passReqToCallback: true
		},
		function(req, accessToken, refreshToken, profile, done) {
			var targetEmail = common.getTargetEmailFromState(req);
			// asynchronous
			process.nextTick(function() {

				// find the user in the database based on their facebook id
				db.User.findOne({
					'facebook.id': profile.id
				}, function(err, user) {

					// if there is an error, stop everything and return that
					// ie an error connecting to the database
					if (err)
						return done(err);

					// if the user is found, then log them in
					if (user) {
						if(targetEmail && (targetEmail != user.email))
						{
							return done(null, null, {
									message : 'FACEBOOK ACCOUNT ALREADY CONNECTED WITH ANOTHER USER',
									errorCode: 1
								});
						}
						else {
							if(((refreshToken || user.facebook.refreshToken) != user.facebook.refreshToken)
								|| (user.facebook.token != accessToken)) {
								if (refreshToken) {
									user.facebook.refreshToken = refreshToken;
								}
								user.facebook.token = accessToken;
								user.save(done);
							}
							else{
								return done(null, user); // user found, return that user
							}
						}
					} else {
						if(!targetEmail && (!profile.emails || profile.emails.length == 0)){
							return done("No email address returned!", null)
						}
						getUserByEmail(targetEmail || profile.emails[0].value, function(err, user) {
							if (err) return done(err);

							var newUser = false;
							if (!user) {
								user = new db.User();
								// user doesn't exist, so set the primary e-mail
								user.email = profile.emails[0].value.toLowerCase();
								newUser = true;
							}

							// set all of the facebook information in our user model
							user.facebook.id = profile.id; // set the users facebook id                   
							user.facebook.token = accessToken; // we will save the token that facebook provides to the user
							user.facebook.refreshToken = refreshToken;
							user.facebook.name = profile.displayName;
							//user.facebook.email = profile.emails[0].value; // facebook can return multiple emails so we'll take the first

							// set user.name if there is none.
							if(!user.name) {
								user.name = profile.displayName;
							}

							// save our user to the database
							user.save(function(err) {
								if (err)
									throw err;

								// if successful, try to get facebook photo and return the user
								AvatarService.getFacebookPhoto(user.facebook.id, 'large', function(avatarErr, avatarUrl){
									if(!avatarErr){
										user.preferredAvatar = 1;
										user.avatarUrl = avatarUrl;

										user.save(function(avatarUpdateErr){
											if(avatarUpdateErr){
												common.logError(avatarUpdateErr, req);
											}

											done(null, user);
											MailService.sendRegistrationMail(user, profile.displayName, newUser, 'Facebook');
										});
									} else {
										done(null, user);
										MailService.sendRegistrationMail(user, profile.displayName, newUser, 'Facebook');
									}
								});
							});
						});
					}

				});
			});
			//console.log('successfully logged in with FB! Profile = ' + JSON.stringify(profile));
		}
	));

	// =========================================================================
	// GOOGLE ==================================================================
	// =========================================================================
	passport.use(new GoogleStrategy({
			clientID: config.auth.google.client_id,
			clientSecret: config.auth.google.client_secret,
			callbackURL: config.auth.google.callbackURL,
			passReqToCallback: true
		},
		function(req, token, refreshToken, profile, done) {
			var targetEmail = common.getTargetEmailFromState(req);
			// make the code asynchronous
			// User.findOne won't fire until we have all our data back from Google
			process.nextTick(function() {

				// try to find the user based on their google id
				db.User.findOne({
					'google.id': profile.id
				}, function(err, user) {
					if (err)
						return done(err);

					if (user) {
						// if a user is found, log them in
						if(targetEmail && (targetEmail != user.email))
						{
							return done(null, null, {
								message : 'GOOGLE ACCOUNT ALREADY CONNECTED WITH ANOTHER USER',
								errorCode: 2
							});
						}
						else {
							if(((refreshToken || user.google.refreshToken) != user.google.refreshToken)
								|| (user.google.token != token)) {
								if(refreshToken) {
									user.google.refreshToken = refreshToken;
								}
								user.google.token = token;
								user.save(done);
							}
							else{
								return done(null, user); // user found, return that user
							}
						}
					} else {
						if(!targetEmail && (!profile.emails || profile.emails.length == 0)){
							return done("No email address returned!")
						}
						getUserByEmail(targetEmail || profile.emails[0].value, function(err, user) {
							if (err) return done(err);

							var newUser = false;
							if (!user) {
								user = new db.User();
								user.email = profile.emails[0].value.toLowerCase();
								newUser = true;
							}

							// set all of the relevant information
							user.google.id = profile.id;
							user.google.token = token;
							user.google.refreshToken = refreshToken;

							// set user.name if there is none.
							if(!user.name) {
								user.name = profile.displayName;
							}

							user.google.name = profile.displayName;

							// save the user
							user.save(function(err) {
								if (err)
									throw err;

								// if successful, try to get facebook photo and return the user
								AvatarService.getGooglePhoto(user.google.id, 200, function(avatarErr, avatarUrl){
									if(!avatarErr){
										user.preferredAvatar = 2;
										user.avatarUrl = avatarUrl;

										user.save(function(avatarUpdateErr){
											if(avatarUpdateErr){
												common.logError(avatarUpdateErr, req);
											}

											done(null, user);
											MailService.sendRegistrationMail(user, profile.displayName, newUser, 'Google');
										});
									} else {
										done(null, user);
										MailService.sendRegistrationMail(user, profile.displayName, newUser, 'Google');
									}
								});
							});
						});
					}
				});
			});
		}
	));

	// Bearer strategy
	passport.use(new BearerStrategy(
	function(token, done) {
			AccessTokenService.getAccessToken(token, function(err, token) {
				if(err) {
					return done(err);
				}

				if(!token || !token.user) {
					return done(null, false);
				}

				var user = common.sanitizeUser(token.user, true)
				return done(null, user);
			})
		}
	));
};