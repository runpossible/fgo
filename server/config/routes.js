var RouteHandlers = require('../http/RouteHandlers'),
    AuthenticationHandlers = require('../http/AuthenticationHandlers'),
	passport = require('passport'),
	base = '/api/v1',
	admin = base + '/admin',
	common = require('./common');

module.exports = function(app, config, db) {
	var oauthCallback = function(req, res, next, provider){
		if(common.getTargetEmailFromState(req)) {
			passport.authenticate(provider, function (err, user, info) {
				if(err){
					next(err);
				}
				if(!user){
					return res.redirect('/profile/edit?provider=' + provider + '&linkError=' + info.errorCode);
				}else{
					return res.redirect('/profile/edit?provider=' + provider + '&linkSuccess=true');
				}
			})(req, res, next);
		}else{
			passport.authenticate(provider, {
				successRedirect: common.getRedirectUriFromState(req, '/'),
				failureRedirect: '/login'
			})(req, res, next);
		}
	};

	// Unauthorized requests
	app.post(base + '/auth/access-token-from-fb', AuthenticationHandlers.getAccessTokenFromFacebookAccessToken);
	app.post(base + '/auth/access-token-from-google', AuthenticationHandlers.getAccessTokenFromGoogleAccessToken);

	// Auth
	app.all('/api*', AuthenticationHandlers.authenticateWithBearerToken);
		
	app.all('/api*', RouteHandlers.checkAuthenticated);
	app.all(admin + '*', RouteHandlers.checkMaster);

	app.post(admin + '/send-mail', RouteHandlers.sendMail);

	// Authentication endpoints
	app.post(base + '/auth/login-access-token', AuthenticationHandlers.getAccessTokenFromLogin);
	app.post(base + '/auth/login', RouteHandlers.postLogin);
	app.post(base + '/auth/logout', RouteHandlers.postLogout);
	app.get(base + '/auth/logout', RouteHandlers.getLogout);
	app.post(base + '/auth/register', RouteHandlers.register);
	app.post(base + '/auth/register-from-mobile', RouteHandlers.registerFromMobile);

	// GeoIp
	app.get(base + '/current-country', RouteHandlers.getCurrentCountry);

	// Redirect the user to Facebook for authentication.  When complete,
	// Facebook will redirect the user back to the application at
	//     /auth/facebook/callback
	app.get(base + '/auth/facebook', function(req, res, next){
		passport.authenticate('facebook', common.addStateParam(req, {scope: common.getFacebookScopes(), accessType: 'offline'}))(req, res, next);
	});

	app.get(base + '/auth/facebook/link', function(req, res, next){
		passport.authenticate('facebook', common.addTargetEmailParam(req, {scope: common.getFacebookScopes(), accessType: 'offline'}))(req, res, next);
	});

	// Facebook will redirect the user to this URL after approval.  Finish the
	// authentication process by attempting to obtain an access token.  If
	// access was granted, the user will be logged in.  Otherwise,
	// authentication has failed.
	app.get(base + '/auth/facebook/callback', function(req, res, next){
		return oauthCallback(req, res, next, 'facebook');
	});

	app.get(base + '/auth/google', function(req, res, next){
		passport.authenticate('google', common.addStateParam(req, {scope: common.getGoogleScopes(), accessType: 'offline', approvalPrompt: 'auto'}))(req, res, next);
	});

	app.get(base + '/auth/google/link', function(req, res, next){
		passport.authenticate('google', common.addTargetEmailParam(req, {scope: common.getGoogleScopes(), accessType: 'offline', approvalPrompt: 'auto'}))(req, res, next);
	});

	// the callback after google has authenticated the user
	app.get(base + '/auth/google/callback', function(req, res, next){
		return oauthCallback(req, res, next, 'google');
	});

	app.delete(base + '/user/facebook', RouteHandlers.disconnectFacebok);
	app.delete(base + '/user/google', RouteHandlers.disconnectGoogle);
    
    app.post(base + '/user/reset-password', RouteHandlers.resetPasswordHandler);
	app.post(base + '/user/password', RouteHandlers.setPasswordHandler);
	app.post(base + '/user/activate', RouteHandlers.activateHandler);

	app.post(base + '/user/contact', RouteHandlers.contactUsHandler);

	// Avatar
	app.get(base + '/user/avatar/:type', RouteHandlers.getCurrentUserAvatar);
	app.get(base + '/avatar/:userId/:type', RouteHandlers.getUserAvatar);

	// Friends
	app.get(base + '/user/friends/facebook', RouteHandlers.getCurrentUserFacebookFriends);
	app.get(base + '/user/friends/google', RouteHandlers.getCurrentUserGoogleFriends);
	app.get(base + '/user/friends/facebook/all', RouteHandlers.getCurrentUserFacebookFriendsToInvite);
	app.get(base + '/user/friends/google/all', RouteHandlers.getCurrentUserGoogleFriendsToInvite);
	app.post(base + '/user/search', RouteHandlers.searchForUsers);
	app.post(base + '/user/friends/search', RouteHandlers.searchForFriends);
	app.get(base + '/user/facebook-image', RouteHandlers.getUserFacebookImage);


	app.post(base + '/user/friends/facebook/post', RouteHandlers.postToFacebookFeed);
	
	app.get(base + '/user/friends/facebook/debug', RouteHandlers.getCurrentUserFacebookFriendsDebug);
	app.get(base + '/user/friends/google/debug', RouteHandlers.getCurrentUserGoogleFriendsDebug);

	app.get(base + '/user/:userId', RouteHandlers.getUserProfile);
    app.post(base + '/user/image', RouteHandlers.uploadUserImage);
	app.get(base + '/user/:id/image', RouteHandlers.getUserImage);
	app.get(base + '/user/:id/image/:imageId/image.png', RouteHandlers.getUserImage);

	// Games
	app.post(base + '/game', RouteHandlers.createGame);
	app.put(base + '/game/:gameId', RouteHandlers.updateGame);
	app.delete(base + '/game/:gameId', RouteHandlers.deleteGame);
	app.get(base + '/game', RouteHandlers.getAllUserGames);
	app.get(base + '/game/upcomming', RouteHandlers.getUpcommingUserGame);
	app.get(base + '/game/upcomming/all', RouteHandlers.getAllUpcommingUserGames);
	app.get(base + '/game/paginated', RouteHandlers.getAllPastUserGamesPaginated);
	app.get(base + '/game/last', RouteHandlers.getLastUserGame);
	app.get(base + '/game/:gameId', RouteHandlers.getGameByUrlNameOrId);
	app.post(base + '/game/:gameId/players/:playerId', RouteHandlers.addPlayerToGame);
	app.delete(base + '/game/:gameId/players/:playerId', RouteHandlers.removePlayerFromGame);
	app.post(base + '/game/:gameId/invitation_code', RouteHandlers.generateGameInvitationCode);
	app.delete(base + '/game/:gameId/invitation_code', RouteHandlers.deleteGameInvitationCode);
	app.get(base + '/game/:gameId/player_stats', RouteHandlers.getGamePlayerStats);
	app.post(base + '/game/:gameId/player_stats/rating', RouteHandlers.updatePlayerRating);
	app.post(base + '/game/:gameId/player_stats/goals', RouteHandlers.updateGamePlayerGoals);
	app.post(base + '/game/:gameId/statistics', RouteHandlers.updateGameStatistics);
	app.get(base + '/public/upcoming_games', RouteHandlers.getAllUpcomingPublicGames);

	// Groups
	app.post(base + '/group', RouteHandlers.createGroup); // tests: ok
	app.get(base + '/group', RouteHandlers.getAllUserGroups); // tests: ok
	app.get(base + '/group/:groupId', RouteHandlers.getByUrlNameOrId);
	app.delete(base + '/group/:groupId', RouteHandlers.deleteGroupById); // tests: ok
	app.post(base + '/group/:groupId/invitation_code', RouteHandlers.generateGroupInvitationCode);
	app.delete(base + '/group/:groupId/invitation_code', RouteHandlers.deleteGroupInvitationCode);
	app.put(base + '/group/:groupId/invite/:inviteId', RouteHandlers.inviteAction);
	app.put(base + '/game/:gameId/invite/:inviteId', RouteHandlers.inviteAction);
	app.delete(base + '/group/:groupId/members/:userId', RouteHandlers.removeGroupMember);
	app.post(base + '/group/:id/image', RouteHandlers.uploadGroupImage);
	app.get(base + '/group/:id/image', RouteHandlers.getGroupImage);
	app.get(base + '/group/:id/image/:imageId/image.png', RouteHandlers.getGroupImage);
	app.post(base + '/game/:gameId/guest', RouteHandlers.addGuestToGame);
	app.delete(base + '/game/:gameId/guest/:guestId', RouteHandlers.removeGuestFromGame);
	app.put(base + '/group/:groupId/join', RouteHandlers.joinGroup);
	app.delete(base + '/group/:groupId/join', RouteHandlers.leaveGroup);


	// Comments
	app.post(base + '/comments/game/:gameId', RouteHandlers.addGameComment);
	app.get(base + '/comments/game/:gameId', RouteHandlers.getAllGameComments);
	app.post(base + '/comments/group/:groupId', RouteHandlers.addGroupComment);
	app.get(base + '/comments/group/:groupId', RouteHandlers.getAllGroupComments);
	app.post(base + '/comments/user/:userId', RouteHandlers.addUserComment);
	app.get(base + '/comments/user/:userId', RouteHandlers.getAllUserComments);

	// Locations
	app.post(base + '/location', RouteHandlers.createLocation); // tests: ok
	app.get(base + '/location', RouteHandlers.getLocationsHandler); // tests: ok
	app.get(base + '/location/search', RouteHandlers.searchLocationsHandler);
	app.get(base + '/location/:id', RouteHandlers.getLocationByIdHandler); // tests: ok
	app.get(base + '/favourite/location', RouteHandlers.getFavouriteLocations);
	app.post(base + '/favourite/location/:id', RouteHandlers.addFavouriteLocation);
	app.delete(base + '/favourite/location/:id', RouteHandlers.removeFavouriteLocation);
	app.post(base + '/location/:id/image', RouteHandlers.uploadLocationImage);
	app.get(base + '/location/:id/image', RouteHandlers.getLocationImage);
	app.get(base + '/location/:id/image/:imageId/image.png', RouteHandlers.getLocationImage);

	// Notifications
	app.post(base + '/notifications', RouteHandlers.addGameNotification);

	// Profile
	app.post(base + '/profile', RouteHandlers.updateProfile);
	app.put(base + '/profile/language', RouteHandlers.updateProfileLanguage);
	app.get(base + '/profile/friends', RouteHandlers.getUserFriends);
	app.post(base + '/profile/friends', RouteHandlers.addUserFriends);
	app.delete(base + '/profile/friends/:userId', RouteHandlers.removeUserFriend);
	app.put(base + '/profile/timezone', RouteHandlers.updateProfileTimezone);

	// Invitations
	// generic create/get of invitations
	app.post(base + '/invite', RouteHandlers.createInvite);
	app.get(base + '/invite', RouteHandlers.getInvitations);
	app.get(base + '/invite/:gameId', RouteHandlers.getAllGameInvitations);

	// Invitations - get for groups
	app.get(base + '/invite/:groupId', RouteHandlers.getActiveGroupInvitations);
	app.post(base + '/invite/:groupId/:invitation_code', RouteHandlers.acceptGroupInvitation);
	app.get(base + '/invite/game/:gameId', RouteHandlers.getActiveGameInvitations);
	app.post(base + '/invite/game/:gameId/:invitation_code', RouteHandlers.acceptGameInvitation);

	// Invitations - get for games
	app.get(base + '/invite/game/:gameId', RouteHandlers.getActiveGameInvitations);

	// Users
	app.get(base + '/search-users', RouteHandlers.searchUsers); // tests: ok
	app.get(base + '/users', RouteHandlers.getAllUsers);

	app.get(base + '/signup-mail', RouteHandlers.sendSignupMailHandler);

	// Errors
	app.get(base + '/errors', RouteHandlers.getAllErrors);

	// Not Found
	app.get(base + '*', RouteHandlers.notFound);
};