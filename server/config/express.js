var express = require('express'),
    cookieParser = require('cookie-parser'),
    bodyParser = require('body-parser'),
    session = require('express-session'),
    passport = require('passport'),
    fs = require('fs'),
    common = require('./common'),
    MongoStore = require('connect-mongo')(session),
    _ = require('lodash'),
    async = require('async'),
    RouteHandlers = require('../http/RouteHandlers'),
    favicon = require('serve-favicon'),
    compression = require('compression'),
    winston = require('winston'),
    helmet = require('helmet'),
    sanitize = require('./sanitize');

/**
 * Requiring `winston-mongodb` will expose
 * `winston.transports.MongoDB`
 */
require('winston-mongodb').MongoDB;

module.exports = function (app, config, db) {
    app.use(compression({threshold: 0}));

    app.set('port', config.port);
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended: true}));
    app.use(cookieParser());
    app.use(helmet()); // http://expressjs.com/en/advanced/best-practice-security.html


    var cookie = {
        maxAge: 2592000000,
        secure: false
    };

    // if(config.sslEnabled)
    // {
    //     cookie.secure = true;
    // }

    // Set MongoDB persistent session - user receives a cookie named connect.sid
    // Authentication is based on this session.
    app.use(session({
        secret: 'fgo secret.',
        resave: true,
        saveUninitialized: true,
        cookie: cookie,
        store: new MongoStore({mongooseConnection: db.Mongoose.connection}),
    }));

    app.use(passport.initialize());
    app.use(passport.session());

    // https://www.npmjs.com/package/express-domain-middleware
    // Set up a Domain per express request, for any express request.
    app.use(require('express-domain-middleware'));

    // Add logger
    // set the custom levels
    winston.setLevels({
            'info': 0,
            'error': 2
        });

    winston.loggers.add('error-logger', {
        transports: [
            new winston.transports.MongoDB({
               name: 'mongo-error-log',
               db: config.dbConnString,
               level: 'error',
               collection: 'errorsLog',
               capped: true,
               cappedSize: 50000000 // 50 MB
            }),
            new (winston.transports.Console)()
       ]
    });

    winston.loggers.add('info-logger', {
        transports: [
            new winston.transports.MongoDB({
                name: 'mongo-info-log',
                db: config.dbConnString,
                level: 'info',
                collection: 'infoLog',
                capped: true,
                cappedSize: 50000000 // 50 MB
            })
        ]
    });

    if(config.sslEnabled) {
        app.use(function(req, res, next) {
            if((!req.secure)  && (req.protocol !== 'https') && (req.get('X-Forwarded-Proto') !== 'https')) {
                var secureUrl = ['https://', req.get('Host'), req.url].join('');
                res.writeHead(301, { "Location":  secureUrl });
                res.end();
            }
            else
                next();
        });
    }

    // if we don't have this route here, when you open the root of the site you are always taken to login page
    app.get('/', RouteHandlers.getWebApp);

    app.use(function (req, res, next) {
        // Sanitize all input from the user
        req.body = sanitize(req.body);
        req.params = sanitize(req.params);
        req.query = sanitize(req.query);
        next();
    });

    app.use(function (req, res, next) {
        if(req.url.indexOf('/api') == 0 && !(req.url.lastIndexOf('/image.png') == req.url.length - '/image.png'.length))
        {
            res.setHeader('Cache-Control', "no-cache, no-store, must-revalidate");
            res.setHeader("Pragma", "no-cache");
            res.setHeader("Expires", 0);
        } else if (config.env == "production") {
            if ((req.url.indexOf('.css') > -1) || (req.url.indexOf('.js') > -1)
               || (req.url.indexOf('.png') > -1) || (req.url.indexOf('.jpg') > -1)
               || (req.url.indexOf('.jpeg') > -1) || (req.url.indexOf('.woff2') > -1)
               || (req.url.indexOf('?t=') > -1)) {
                res.setHeader('Cache-Control', 'public, max-age=345600'); // 4 days
                res.setHeader('Expires', new Date(Date.now() + 345600000).toUTCString());
            }
        }
        return next();
    });

    app.use(function (req, res, next) {
        res.appContext = {
            req: req
        };
        return next();
    });

    app.use(favicon(__dirname + './../../app/assets/img/favicon.ico', {maxAge: 2592000000}));

    // When getting the /index.html, inject the user in the window.currentUser variable.
    app.use(express.static(config.rootPath + '/app'));

    app.use('/design', express.static(config.rootPath + '/design'));

    app.use('/languages', express.static(config.rootPath + '/app/languages'));

    // add auth routes
    require('./routes')(app, config, db);

    // the asterisk route should be on the bottom, so that all the routes to be checked and if none of them applies
    // then serve the web app, this is needed because of the html mode which does not have # in the url
    app.get('*', RouteHandlers.getWebApp);

    app.use(function onError(err, req, res, next) {
        common.logError(err, req);

        if (err == "No email address returned!"){
            res.redirect('/welcome?login&missingEmail');
        }
        else {
            res.status(500).send({
                message: err.message
            });
        }
    });
};