var path = require('path');
var rootPath = path.normalize(__dirname + './../../');
exports = module.exports = {
	development: {
		env: "development",
		port: process.env.PORT || 3000,
        ip_address: process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1',
		site_address: "http://localhost:3000",
		rootPath: rootPath,
		profileActivationRequired: false,
		dbConnString: 'mongodb://localhost/fgo',
		cacheAvatar: true,
		sslEnabled: false,
		auth: {
			facebook: {
				name: 'Facebook',
				client_id: 882557195159647,
				client_secret: 'e1a6f3e91561a80546949108df814ec7',
				callbackURL: "http://localhost:3000/api/v1/auth/facebook/callback",
			},
			google: {
				name: 'Google',
				client_id: '150797401432-o8fe23f4ak1o3oq56eev3idjrug59nnm.apps.googleusercontent.com',
				client_secret: 'HeNfX9qM-VMOFeSk0S1lcHXT',
				callbackURL: 'http://localhost:3000/api/v1/auth/google/callback',
				API_key: 'AIzaSyDMsN_6oAVNYMZxxd7n230dN006XgBOWg0'
			}
		},
		mail: {
			enabled: false,
			useMailGun: true,
			mailGun: {
				API_key: 'key-448437ce08edf98d205666bd8982aaa8',
				API_baseURL: 'https://api.mailgun.net/v3/sandbox7f6782500e2a4b8ab90a7b26e4bf4386.mailgun.org',
				smtp_hostName: 'smtp.mailgun.org',
				domain: 'sandbox7f6782500e2a4b8ab90a7b26e4bf4386.mailgun.org',
				from: 'Football Game Organizer <postmaster@sandbox7f6782500e2a4b8ab90a7b26e4bf4386.mailgun.org>',
				user: 'postmaster@sandbox7f6782500e2a4b8ab90a7b26e4bf4386.mailgun.org',
				pass: 'f4695704ba6c4ed704aa68f9a4e45138'
			},
			host: 'mail.privateemail.com',
			port: '25',
			user: 'contact@fgo.io',
			pass: 'fgoDumDeeDum123',
			bcc: 'cypressx@gmail.com'
		},
		everlive: {
			apiKey: 'vmsektgoxu7w5ih9',
			masterKey: '4gejiwuSzekrJS6TRdc0utn6tjRdFhWd',
			notificationsUrl: 'https://api.everlive.com/v1/vmsektgoxu7w5ih9/Push/Notifications',
			notificationsEnabled: true
		}
	},
	production: {
		env: "production",
        port: process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 3000,
       ip_address: process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1',
		site_address: "https://www.fgo.io",
		cacheAvatar: true,
		sslEnabled: false,
		rootPath: rootPath,
		profileActivationRequired: true,
		dbConnString: process.env.OPENSHIFT_MONGODB_DB_CONNECTION_STRING, // openshift mongo
		auth: {
			facebook: {
				name: 'Facebook',
				client_id: 877406112341422,
				client_secret: '683be750c1855beff585d4a326ee4c1c',
				callbackURL: "https://www.fgo.io/api/v1/auth/facebook/callback",
			},
			google: {
				name: 'Google',
				client_id: '150797401432-o8fe23f4ak1o3oq56eev3idjrug59nnm.apps.googleusercontent.com',
				client_secret: 'HeNfX9qM-VMOFeSk0S1lcHXT',
				callbackURL: 'https://www.fgo.io/api/v1/auth/google/callback',
				API_key: 'AIzaSyDMsN_6oAVNYMZxxd7n230dN006XgBOWg0'
			}
		},
		mail: {
			enabled: true,
			useMailGun: true,
			mailGun: {
				API_key: 'key-448437ce08edf98d205666bd8982aaa8',
				API_baseURL: 'https://api.mailgun.net/v3/fgo.io',
				smtp_hostName: 'smtp.mailgun.org',
				domain: 'fgo.io',
				from: 'Football Game Organizer <system@fgo.io>',
				user: 'system@fgo.io',
				pass: 'fgoDumDeeDum123'
			},
			host: 'mail.privateemail.com',
			port: '25',
			user: 'contact@fgo.io',
			pass: 'fgoDumDeeDum123',
			bcc: 'cypressx@gmail.com,v.trifonov@gmail.com,contact@asensokolov.com'
		},
		everlive: {
			apiKey: 'vmsektgoxu7w5ih9',
			masterKey: '4gejiwuSzekrJS6TRdc0utn6tjRdFhWd',
			notificationsUrl: 'https://api.everlive.com/v1/vmsektgoxu7w5ih9/Push/Notifications',
			notificationsEnabled: true
		}
	}
};