/* global db, config */
'use strict';

var fs = require('fs'),
    cheerio = require('cheerio'),
    common = global.rootRequire('/config/common'),
    passport = require('passport'),
    _ = require('lodash'),
    async = require('async'),
    db = global.db,
    config = global.config,
    UserService = rootRequire('/services/UserService'),
    AvatarService = rootRequire('/services/AvatarService'),
    GroupService = rootRequire('/services/GroupService'),
    GameService = rootRequire('/services/GameService'),
    InviteService = rootRequire('/services/InviteService'),
    CommentService = rootRequire('/services/CommentService'),
    LocationService = rootRequire('/services/LocationService'),
    NotificationService = rootRequire('/services/NotificationService'),
    MailService = rootRequire('/services/MailService'),
    ObjectID = require('mongoose').Types.ObjectId,
    ok = require('okay'),
    FriendsService = rootRequire('/services/FriendsService'),
    responder = rootRequire('/http/HttpResponder'),
    randomstring = require("randomstring"),
    maxmind = require('maxmind'),
    AccessTokenService = rootRequire('/services/AccessTokenService');

maxmind.init(config.rootPath + '/geoip/GeoIP.dat');

var RouteHandlers = {};

// Not Found
RouteHandlers.notFound = function notFound(req, res) {
    responder.notFound(res, _.template('Cannot <%= method %> <%= originalUrl %>')(req));
};

// Web
RouteHandlers.getWebApp = function getWebApp(req, res) {
    var html = fs.readFileSync(config.rootPath + '/app/index.html', 'utf8');
    html = html.replace(/jsSuffix/g, global.jsSuffix.toString());
    var $ = cheerio.load(html);

    var inLandingPage = false;
    if((req.originalUrl.toString() == '/' && !req.user) || req.originalUrl.indexOf('/welcome') == 0){
        $('body').addClass('landing-page');
        inLandingPage = true;
    }

    if (process.env.OPENSHIFT_NODEJS_PORT) {
        // use r.js optimized file
        $('script[data-main="/main"]').attr('data-main', '/main-built.js?bust=' + global.jsSuffix);

        var ga = fs.readFileSync(config.rootPath + '/app/js/google/google-tracking.js', 'utf8');
        var analyticsScript = '<script>' + ga + '</script>';
        $('body').append(analyticsScript);
    }

    // we don't need google plus for now
    //var googlePlus = fs.readFileSync(config.rootPath + '/app/js/google/google-plus.html', 'utf8');
    //$('body').append(googlePlus);

    $('body').prepend('<script>window.sslEnabled = ' + config.sslEnabled + ';window.requestSuffix=' + global.jsSuffix + ';</script>');

    if (!req.user) {
        return res.send($.html({decodeEntities: false}));
    }

    NotificationService.checkUser(req.user);
    var user = common.sanitizeUser(req.user, true);
    UserService.fillUserStats(user, function(err){
        var userString = JSON.stringify(user);
        var userScript = '<script>window.currentUser = JSON.parse(\'' + userString + '\'); window.facebookAppId = \'' + config.auth.facebook.client_id + '\'; window.googleClientId = \'' + config.auth.google.client_id + '\'</script>';
        $('body').prepend(userScript);

        res.send($.html({decodeEntities: false}));
    });
};

// Auth
RouteHandlers.postLogin = function postLogin(req, res, next) {
    var auth = passport.authenticate('local', function (err, user) {
        if (err) return next(err);

        if (user && user.activationCode) return res.send({
            message: 'USER NOT ACTIVATED',
            success: false
        });

        if (!user) return res.send({
            success: false
        });

        req.logIn(user, function (err) {
            if (err) return next(err);

            UserService.fillUserStats(user, function(err){
                res.send({
                    success: true,
                    user: user
                });
            });
        });
    });
    auth(req, res, next);
};

RouteHandlers.register = function register(req, res, next) {
    var email = req.body.email,
        pwd = req.body.password,
        name = req.body.name,
        language = req.body.language || 'en',
        countryId = req.body.countryId,
        preferredPosition = req.body.preferredPosition;

    if (!email || !pwd) {
        return next(new Error('EMAIL AND PASSWORD ARE REQUIRED'));
    }

    var salt = db.User.createSalt(),
        hash = db.User.hashPwd(salt, pwd);
    var user = {
        email: email.toLowerCase(),
        local: {
            salt: salt,
            hashed_pwd: hash
        },
        name: name,
        countryId: countryId,
        preferredPosition: preferredPosition,
        language: language
    };

    if(config.profileActivationRequired){
        user.local.activationCode = randomstring.generate({
            length: 64,
            charset: 'alphanumeric'
        });
    }

    db.User.create(user, function (err, createdUser) {
        // duplicate key error.
        if (db.isDuplicateError(err)) {
            return responder.respond(res, {
                success: false,
                errorMessage: 'USER ALREADY EXISTS'
            }, 409);
        }

        if (err) return next(err);

        user.id = createdUser._id;

        if(config.profileActivationRequired) {
            MailService.sendActivationMail(user, function (err) {
                if (err) return next(err);
                return responder.respond(res, {success: true}, 201);
            });
        } else {
            return responder.respond(res, {success: true, user: user}, 201);
        }
    });
};

RouteHandlers.registerFromMobile = function register(req, res, next) {
    if(req.query.secret !== 'darivaskoyosko') {
         return responder.respond(res, {
                success: false,
                errorMessage: 'Not Found'
            }, 404);
    }
    var email = req.body.email,
        pwd = req.body.password,
        name = req.body.fullName,
        language = req.body.language || 'en',
        countryId = req.body.countryId,
        preferredPosition = req.body.preferredPosition;

    if (!email || !pwd) {
        return next(new Error('EMAIL AND PASSWORD ARE REQUIRED'));
    }

    var salt = db.User.createSalt(),
        hash = db.User.hashPwd(salt, pwd);
    var user = {
        email: email.toLowerCase(),
        local: {
            salt: salt,
            hashed_pwd: hash
        },
        name: name,
        countryId: countryId,
        preferredPosition: preferredPosition,
        language: language
    };

    db.User.create(user, function (err, createdUser) {
        // duplicate key error.
        if (db.isDuplicateError(err)) {
            return responder.respond(res, {
                success: false,
                errorMessage: 'USER ALREADY EXISTS'
            }, 409);
        }

        if (err) return next(err);

        user.id = createdUser._id;
        user._id = createdUser._id;

        // For Mobile - Get the access token for the user.
        var sanitizedUser = common.sanitizeUser(user, true);
        return AccessTokenService.createAccessToken(user._id, function (err, token) {
            if (err) {
                return responder.respond(res, {
                    success: false,
                    errorMessage: 'Could not create an access token: ' + err
                }, 400);
            }

            return responder.respond(res, {
                success: true,
                accessToken: token,
                user: sanitizedUser
            }, 201);
        });
    });
};

RouteHandlers.postLogout = function postLogout(req, res) {
    req.logout();

    res.end();
};

RouteHandlers.getLogout = function postLogout(req, res) {
    req.logout();

    res.redirect('/');
};

RouteHandlers.getUserProfile = function getUserProfile(req, res) {
    if(!req.params.userId){
        return responder.badRequest(res);
    }
    UserService.getUserProfile(req.params.userId, RouteHandlers.handleCommonCrudResponse.bind(null, res));
};

RouteHandlers.getUserFacebookImage = function getUserFacebookImage(req, res) {
    if(!req.user.facebook || !req.user.facebook.id){
        return responder.badRequest(res);
    }

    UserService.getFacebookPhoto(req.user._id, RouteHandlers.handleCommonCrudResponse.bind(null, res));
};

// route middleware to make sure a user is logged in
RouteHandlers.checkAuthenticated = function postLogout(req, res, next) {
    res.context = req;

    // if user is authenticated in the session, carry on
    if (req.isAuthenticated() || req.originalUrl.toLowerCase().indexOf("/api/v1/auth") === 0)
        return next();

    // reset password endpoints
    if(req.originalUrl.toLowerCase().indexOf('/api/v1/user/reset-password') === 0
        || req.originalUrl.toLowerCase().indexOf('/api/v1/user/password') === 0
        || req.originalUrl.toLowerCase().indexOf('/api/v1/user/activate') === 0
        || req.originalUrl.toLowerCase().indexOf('/api/v1/user/contact') === 0

        //get current country endpoint
        || req.originalUrl.toLowerCase().indexOf('/api/v1/current-country') === 0
        // bearer authentication
        || req.originalUrl.toLowerCase().indexOf('/api/v1/bearer') === 0)
    {
        return next();
    }

    // if they aren't redirect them to the home page
    res.redirect('/');
};

// route middleware to make sure a user is master
RouteHandlers.checkMaster = function checkMaster(req, res, next) {
    db.User.findOne({ _id: req.user._id}).exec(function(err, user) {
        if (err) {
            return responder.badRequest(res);
        }
        if(user.master){
            return next();
        };

        // if the user is not master redirect to the home page
        res.redirect('/');
    });
};

RouteHandlers.sendMail = function sendMail(req, res) {
    MailService.sendMassMail(req.body, function (err) {
        if (err) {
            return responder.badRequest(res, {}, err);
        }
        responder.respond(res, { success: true});
    });
};

RouteHandlers.getCurrentCountry = function getCurrentCountry(req, res) {
    var ip = req.query.ip || (req.headers['x-forwarded-for'] ||
        req.connection.remoteAddress ||
        req.socket.remoteAddress ||
        req.connection.socket.remoteAddress);

    var country = maxmind.getCountry(ip);

    var code = country.code || 'bg';
    if(code == '--'){
        code = 'bg';
    }

    var data = {
        country_code: code.toLowerCase()
    };

    return responder.respond(res, data);
};

RouteHandlers.disconnectFacebok = function disconnectFacebok(req, res) {
    db.User.findOne({ _id: req.user._id}).exec(function(err, user){
        if (err) {
            return responder.badRequest(res);
        }
        user.facebook = undefined;
        if(user.preferredAvatar == 1){
            user.preferredAvatar = undefined;
        }
        user.save(function(err, updatedUser){
            if (err) {
                return responder.badRequest(res);
            }
            var resultUser = common.sanitizeUser(updatedUser, true);
            responder.respond(res, resultUser);
        });
    });
};

RouteHandlers.disconnectGoogle = function disconnectGoogle(req, res) {
    db.User.findOne({ _id: req.user._id}).exec(function(err, user){
        if (err) {
            return responder.badRequest(res);
        }
        user.google = undefined;
        if(user.preferredAvatar == 2){
            user.preferredAvatar = undefined;
        }
        user.save(function(err, updatedUser){
            if (err) {
                return responder.badRequest(res);
            }
            var resultUser = common.sanitizeUser(updatedUser, true);
            responder.respond(res, resultUser);
        });
    });
};

RouteHandlers.resetPasswordHandler = function(req, res){
  if(!req.body.email){
      return responder.badRequest(res);
  }
  UserService.requestPasswordReset(req.body.email, function (err) {
            if (err) {
                return responder.badRequest(res, {
                    success: true,
                    message: err
                });
            }
            return responder.respond(res, {success: true});
        });
};

RouteHandlers.setPasswordHandler = function(req, res){
    if(!req.body.secret || !req.body.password){
        return responder.badRequest(res);
    }
    UserService.setPassword(req.body.secret, req.body.password, RouteHandlers.handleCommonCrudResponse.bind(null, res));
};

RouteHandlers.activateHandler = function(req, res){
    if(!req.body.code){
        return responder.badRequest(res);
    }
    UserService.activate(req.body.code, RouteHandlers.handleCommonCrudResponse.bind(null, res));
};

RouteHandlers.contactUsHandler = function contactUsHandler(req, res) {
    if(!req.body.name || !req.body.email || !req.body.message) {
        return responder.badRequest(res);
    }

    UserService.contactUs(req.body, req.user, RouteHandlers.handleCommonCrudResponse.bind(null, res));
};

RouteHandlers.sendSignupMailHandler = function sendSignupMailHandler(req, res) {
    if(!req.query.email || !req.query.name || req.query.secret != 'yosko') {
        return responder.badRequest(res);
    }

    MailService.sendSignedUpUserMail(req.query, function(err) {
        if (err) {
            return responder.respond(res, null);
        }

        return responder.respond(res,{
            success: true
        });
    });
};

// Avatar
RouteHandlers.getCurrentUserAvatar = function getCurrentUserAvatar(req, res) {
    var preferredAvatar = req.query.preferredAvatar;
    var user = req.user ? common.sanitizeUser(req.user) : null;
    AvatarService.getUserAvatar(user, req.params.type, preferredAvatar, function (err, avatarUrl) {
        if (err) {
            responder.respond(res, 'http://www.gravatar.com/avatar/00?d=mm&s=80');
        }
        else {
            responder.respond(res, avatarUrl);
        }
    });
};

RouteHandlers.getUserAvatar = function getUserAvatar(req, res) {
    var preferredAvatar = req.query.preferredAvatar;
    var searchQuery = {};
    if (req.params.userId.indexOf('@') > 0) {
        searchQuery = {email: req.params.userId.toLowerCase()};
    }
    else {
        searchQuery = {_id: req.params.userId};
    }
    db.User.findOne(searchQuery).exec(function (err, user) {
        if (err) {
            responder.respond(res, null);
            return;
        }
        AvatarService.getUserAvatar(user, req.params.type, preferredAvatar, function (err, avatarUrl) {
            if (err) {
                responder.respond(res, null);
            }
            else {
                responder.respond(res, avatarUrl);
            }
        });
    });
};

// Friends
RouteHandlers.getCurrentUserFacebookFriends = function getCurrentUserFacebookFriends(req, res) {
    if(!req.user.facebook || !req.user.facebook.id){
        return responder.badRequest(res);
    }
    FriendsService.getFacebookFriends(req.user.facebook.id, RouteHandlers.handleCommonCrudResponse.bind(null, res));
};

RouteHandlers.getCurrentUserFacebookFriendsDebug = function getCurrentUserFacebookFriendsDebug(req, res) {
    if(!req.user.facebook || !req.user.facebook.id){
        return responder.badRequest(res);
    }
    FriendsService.getFacebookFriendsDebug(req.user.facebook.id, RouteHandlers.handleCommonCrudResponse.bind(null, res));
};

RouteHandlers.getCurrentUserGoogleFriends = function getCurrentUserGoogleFriends(req, res) {
    if(!req.user.google || !req.user.google.id){
        return responder.badRequest(res);
    }
    FriendsService.getGoogleFriends(req.user._id, req.user.google.id, RouteHandlers.handleCommonCrudResponse.bind(null, res));
};

RouteHandlers.getCurrentUserFacebookFriendsToInvite = function getCurrentUserFacebookFriendsToInvite(req, res) {
    if(!req.user.facebook || !req.user.facebook.id){
        return responder.badRequest(res);
    }
    FriendsService.getFacebookFriendsToInvite(req.user._id, req.user.facebook.id, function(err, friends){
        if(err){
            return responder.badRequest(res, err);
        }
        UserService.getPotentialFriendsFromFB(friends, function(err, result){
            RouteHandlers.handleCommonCrudResponse(res, err, _.sortBy(result, 'name'));
        });
    });
};

RouteHandlers.postToFacebookFeed = function postToFacebookFeed(req, res) {
    var message = req.body.message;
    var tags = req.body.tags;
    if(!req.user.facebook || !req.user.facebook.id || !message || !tags){
        return responder.badRequest(res);
    }
    FriendsService.postToFacebookFeed(req.user._id, req.user.facebook.id, message, tags, RouteHandlers.handleCommonCrudResponse.bind(null, res));
};

RouteHandlers.getCurrentUserGoogleFriendsToInvite = function getCurrentUserGoogleFriendsToInvite(req, res) {
    if(!req.user.google || !req.user.google.id){
        return responder.badRequest(res);
    }
    FriendsService.getGoogleFriendsToInvite(req.user._id, req.user.google.id, function(err, friends){
        if(err){
            return responder.badRequest(res, err);
        }
        UserService.getPotentialFriendsFromGoogle(friends, function(err, result){
            RouteHandlers.handleCommonCrudResponse(res, err, _.sortBy(result, 'name'));
        });
    });
};

RouteHandlers.searchForFriends = function searchForFriends(req, res) {
    FriendsService.searchForFriends(req.user._id, req.body.search || '', RouteHandlers.handleCommonCrudResponse.bind(null, res));
};

RouteHandlers.searchForUsers = function searchForUsers(req, res) {
    FriendsService.searchForUsers(req.body.search || '', RouteHandlers.handleCommonCrudResponse.bind(null, res));
};

RouteHandlers.getCurrentUserGoogleFriendsDebug = function getCurrentUserGoogleFriendsDebug(req, res) {
    if(!req.user.google || !req.user.google.id){
        return responder.badRequest(res);
    }
    FriendsService.getGoogleFriendsDebug(req.user._id, req.user.google.id, RouteHandlers.handleCommonCrudResponse.bind(null, res));
};

var executeCheckingGameAccess = function(req, res, operation){
    var user = req.user;
    if (!user) {
        return responder.badRequest(res);
    }

    if(req.body.groupId) {
        db.Group.findOne({_id: req.body.groupId}).populate('admin_users users createdBy').exec(function (err, group) {
            if (err || !group) {
                return responder.badRequest(res, null, err);
            } else {
                // TODO: Yosif removed this, as currently users cannot be made admins...
                //var userRole = GroupService.getUserRole(user.id, group);
                //if (userRole == GroupService.Constants.UserRole.Admin || userRole == GroupService.Constants.UserRole.Creator) {
                    operation();
                //} else {
                //    return responder.badRequest(res, null, 'No permissions to edit games in that group');
                //}
            }
        });
    } else {
        process.nextTick(operation);
    }
};

// Games
RouteHandlers.createGame = function createGame(req, res) {
    executeCheckingGameAccess(req, res, function () {
        GameService.createGame(req.user, req.body, RouteHandlers.handleCommonCrudResponse.bind(null, res));
    });
};

RouteHandlers.updateGame = function updateGame(req, res) {
    if(!req.params.gameId)
    {
        return responder.badRequest(res);
    }

    if(req.body.group) {
        req.body.groupId = req.body.group._id;
    }

    executeCheckingGameAccess(req, res, function () {
        GameService.updateGame(req.params.gameId, req.body, RouteHandlers.handleCommonCrudResponse.bind(null, res));
    });
};

RouteHandlers.deleteGame = function createGame(req, res) {
    if(!req.params.gameId)
    {
        return responder.badRequest(res);
    }

    GameService.getById(req.params.gameId, function(err, game){
        if(err || !game){
            return responder.badRequest(res);
        }

        var user = req.user;
        if (!user) {
            return responder.badRequest(res);
        }

        // Vasko - only game creator can delte game now
        if(req.user.id != game.createdBy.toHexString()){
            return responder.badRequest(res, null, 'No permissions to delete the game');
        }
        
        if(game.group) {
            req.body = {
                groupId: game.group.id
            };
        }

        executeCheckingGameAccess(req, res, function () {
            GameService.deleteGame(req.params.gameId, function(err) {
                if(err) {
                    return RouteHandlers.handleCommonCrudResponse(res, err);
                }

                InviteService.deleteByGame(game._id, RouteHandlers.handleCommonCrudResponse.bind(null, res));
            });
        });
    });
};

RouteHandlers.getAllUserGames = function getAllUserGames(req, res) {
    GameService.getAll(req.user._id, true, RouteHandlers.handleCommonCrudResponse.bind(null, res));
};

RouteHandlers.getUpcommingUserGame = function getUpcommingUserGame(req, res) {
    GameService.getAll(req.user._id, false, function(err, games){
        if (err) {
            return responder.badRequest(res);
        }

        for(var index in games) {
            games[index].gamePassed = new Date() > games[index].date_time;
            games[index].isOwner = req.user._id === games[index].createdBy;
        }

        games = _.sortBy(_.filter(games, { gamePassed: false }), 'date_time');
        if(games.length > 0) {
            return RouteHandlers.handleCommonCrudResponse(res, null, games[0]);
        } else {
            return RouteHandlers.handleCommonCrudResponse(res, null, {});
        }
    });
};

RouteHandlers.getAllUpcommingUserGames = function getAllUpcommingUserGames(req, res) {
    GameService.getAll(req.user._id, true, function(err, games){
        if (err) {
            return responder.badRequest(res);
        }

        for(var index in games) {
            games[index].gamePassed = new Date() > games[index].date_time;
            games[index].isOwner = req.user._id === games[index].createdBy;
        }

        games = _.sortBy(_.filter(games, { gamePassed: false }), 'date_time');
        return RouteHandlers.handleCommonCrudResponse(res, null, games);
    });
};

RouteHandlers.getAllUpcomingPublicGames = function getAllUpcomingPublicGames(req, res) {
    GameService.getAllUpcomingPublicGames(RouteHandlers.handleCommonCrudResponse.bind(null, res));
};

RouteHandlers.getAllPastUserGamesPaginated = function getAllPastUserGamesPaginated(req, res) {
    GameService.getAll(req.user._id, true, function(err, games) {
        if (err) {
            return responder.badRequest(res);
        }

        var nowDate = new Date();

        games = _.filter(games, function(game){ return game.date_time < nowDate; });

        return RouteHandlers.paginatedResponse(res, null, games);
    });
};

RouteHandlers.getLastUserGame = function getLastUserGame(req, res) {
    GameService.getLastUserGame(req.user._id, function (err, game) {
        if (err) {
            return responder.badRequest(res, null, err);
        }
        else {
            return responder.respond(res, game);
        }
    });
};

RouteHandlers.getGameByUrlNameOrId = function getGameByUrlNameOrId (req, res) {
    var gameId = req.params.gameId;
    if (!gameId || !req.user) {
        return responder.badRequest(res);
    }

    GameService.checkAccess(req.user.id, gameId, function (err, game, isAdmin) {
        if (err || !game) {
            return responder.badRequest(res, null, err);
        }
        else {
            return responder.respond(res, game);
        }
    });
};

RouteHandlers.getGamePlayerStats = function getGamePlayerStats (req, res) {
    var gameId = req.params.gameId;
    if (!gameId || !req.user) {
        return responder.badRequest(res);
    }

    GameService.checkAccess(req.user.id, gameId, function (err, game, isAdmin, isParticipant) {
        if (err || !game) {
            return responder.badRequest(res, null, err);
        }
        else {
            return GameService.getPlayerStats(game.id, req.user.id, function (err, result) {
                if (err) {
                    return responder.badRequest(res, null, err);
                }
                return responder.respond(res, result);
            });
        }
    });
};

RouteHandlers.updatePlayerRating = function updatePlayerRating (req, res) {
    var gameId = req.params.gameId,
        playerId = req.body.playerId,
        rating = req.body.rating;
    if (!gameId || !playerId || (!rating && rating != 0)) {
        return responder.badRequest(res);
    }

    GameService.checkAccess(req.user.id, gameId, function (err, game, isAdmin, isParticipant) {
        if (err || !game || !isParticipant) {
            return responder.badRequest(res, null, err);
        }
        else {
            return GameService.updatePlayerRating(gameId, playerId, req.user.id, rating, function (err, result) {
                if (err) {
                    return responder.badRequest(res, null, err);
                }
                return responder.respond(res, result);
            });
        }
    });
};

RouteHandlers.updateGameStatistics = function updateGameStatistics (req, res) {
    var gameId = req.params.gameId;
    if (!gameId) {
        return responder.badRequest(res);
    }

    GameService.checkAccess(req.user.id, gameId, function (err, game, isAdmin, isParticipant) {
        if (err || !game || !isParticipant) {
            return responder.badRequest(res, null, err);
        }
        else {
            return GameService.updateGameStatistics(gameId, req.body, req.user.id, function (err, result) {
                if (err) {
                    return responder.badRequest(res, null, err);
                }
                return responder.respond(res, result);
            });
        }
    });
};

RouteHandlers.updateGamePlayerGoals = function updateGamePlayerGoals (req, res) {
    var gameId = req.params.gameId,
        playerId = req.body.playerId,
        value = req.body.value;
    if (!gameId || !playerId || (!value && value != 0)) {
        return responder.badRequest(res);
    }

    GameService.checkAccess(req.user.id, gameId, function (err, game, isAdmin, isParticipant) {
        if (err || !game || !isParticipant) {
            return responder.badRequest(res, null, err);
        }
        else {
            return GameService.updateGameFact(gameId, playerId, req.user.id, value, GameService.Constants.GameFactType.GoalScored, function (err, result) {
                if (err) {
                    return responder.badRequest(res, null, err);
                }
                return responder.respond(res, result);
            });
        }
    });
};

RouteHandlers.addPlayerToGame = function addPlayerToGame(req, res) {
    var gameId = req.params.gameId;
    var playerId = req.params.playerId;
    if (!gameId || !playerId || !req.user) {
        return responder.badRequest(res);
    }

    GameService.checkAccess(req.user.id, gameId, function (err, game, isAdmin) {
        if (err || !game) {
            return responder.badRequest(res, null, err);
        }
        else {
            if (playerId == req.user.id || isAdmin) {
                GameService.addPlayer(gameId, playerId, RouteHandlers.handleCommonCrudResponse.bind(null, res));
            } else {
                return responder.badRequest(res, null, 'No permissions to modify game');
            }
        }
    });
};

RouteHandlers.removePlayerFromGame = function removePlayerFromGame(req, res) {
    var gameId = req.params.gameId;
    var playerId = req.params.playerId;
    if (!gameId || !playerId || !req.user) {
        return responder.badRequest(res);
    }

    GameService.checkAccess(req.user.id, gameId, function (err, game, isAdmin) {
        if (err || !game) {
            return responder.badRequest(res, null, err);
        }
        else {
            if (playerId == req.user.id || isAdmin) {
                GameService.removePlayer(gameId, playerId, RouteHandlers.handleCommonCrudResponse.bind(null, res));
            } else {
                return responder.badRequest(res, null, 'No permissions to modify game');
            }
        }
    });
};

// Authentication

// Invitations
RouteHandlers.createInvite = function createInvite(req, res) {
    var createdBy = req.user._id,
        player = req.body.playerId,
        groupId = req.body.groupId,
        gameId = req.body.gameId;

    // if missing player, or the type of invitation - game or group cannot be determined
    if (!player || (!groupId && !gameId)) {
        return responder.badRequest(res);
    }

    if(groupId) {
        GroupService.getById(groupId, function (err, group) {
            if (err || !group) {
                return responder.badRequest(res, null, err || "group not found");
            }

            var allUsers = (group.admin_users || []).concat(group.users || []);
            var isMember = _.some(allUsers, {id: req.user.id});
            if (isMember) {
                return InviteService.createGroupInvite(createdBy, player, group, RouteHandlers.handleCommonCrudResponse.bind(null, res));
            }

            return responder.accessDenied(req, res);
        });
    } else if(gameId) {
        GameService.getById(gameId, function (err, game) {
            if (err || !game) {
                return responder.badRequest(res, null, err || "game not found");
            }

            return InviteService.createGameInvite(createdBy, player, game, RouteHandlers.handleCommonCrudResponse.bind(null, res));
        });
    }
};

RouteHandlers.getInvitations = function getInvitations(req, res) {
    var userId = req.user._id;
    var status = req.query.status;

    InviteService.find(userId, status, RouteHandlers.handleCommonCrudResponse.bind(null, res));
};

RouteHandlers.getAllGameInvitations = function getAllGameInvitations(req, res) {
    var gameId = req.params.gameId;

    InviteService.getAllForGame(gameId, RouteHandlers.handleCommonCrudResponse.bind(null, res));
};

RouteHandlers.getActiveGroupInvitations = function getActiveGroupInvitations(req, res) {
    var groupId = req.params.groupId;

    InviteService.findActiveByGroup(groupId, RouteHandlers.handleCommonCrudResponse.bind(null, res));
};

RouteHandlers.getActiveGameInvitations = function getActiveGameInvitations(req, res) {
    var gameId = req.params.gameId;

    InviteService.findActiveByGame(gameId, RouteHandlers.handleCommonCrudResponse.bind(null, res));
};

RouteHandlers.addGuestToGame = function addGuestToGame(req, res) {
    var data = {
        createdById: req.user._id,
        createdByName: req.user.name,
        name: req.body.name
    };

    GameService.addGuestToGame(req.params.gameId, data, RouteHandlers.handleCommonCrudResponse.bind(null, res));
};

RouteHandlers.removeGuestFromGame = function removeGuestFromGame(req, res) {
    GameService.removeGuestFromGame(req.params.gameId, req.params.guestId, RouteHandlers.handleCommonCrudResponse.bind(null, res));
};

RouteHandlers.joinGroup = function joinGroup(req, res) {
    GroupService.join(req.params.groupId, req.user._id, RouteHandlers.handleCommonCrudResponse.bind(null, res));
};

RouteHandlers.leaveGroup = function leaveGroup(req, res) {
    GroupService.leave(req.params.groupId, req.user._id, RouteHandlers.handleCommonCrudResponse.bind(null, res));
};

RouteHandlers.inviteAction = function acceptInvite(req, res) {
    var groupId = req.params.groupId,
        inviteId = req.params.inviteId,
        status = req.body.status,
        gameId = req.params.gameId;

    if (status !== InviteService.Constants.Status.Accepted &&
        status != InviteService.Constants.Status.Rejected) {
        return responder.badRequest(res);
    }

    if(groupId) {
        return GroupService.inviteAction(groupId, inviteId, status, function (err) {
            if (err) {
                return responder.badRequest(res);
            }
            return responder.respond(res, {success: true});
        });
    }

    if(gameId) {
        return GameService.inviteAction(gameId, inviteId, status, function (err) {
            if (err) {
                return responder.badRequest(res);
            }
            return responder.respond(res, {success: true});
        });
    }

    return responder.badRequest(res);
};

RouteHandlers.acceptGroupInvitation = function acceptGroupInvitation(req, res) {
    var groupId = req.params.groupId;
    var invitation_code = req.params.invitation_code;

    GroupService.createGroupInvite(groupId, req.user._id, invitation_code, function (err) {
        if (err) {
            if(err == 'USER ALREADY PART OF THE GROUP'){
                return responder.respond(res, {error: err}, 202);
            }
            else {
                return responder.badRequest(res, {error: err});
            }
        }
        return responder.respond(res, {success: true});
    })
};

RouteHandlers.acceptGameInvitation = function acceptGameInvitation(req, res) {
    var gameId = req.params.gameId;
    var invitation_code = req.params.invitation_code;

    GameService.createGameInvite(gameId, req.user._id, invitation_code, function (err) {
        if (err) {
            if(err == 'USER ALREADY PART OF THE GAME'){
                return responder.respond(res, {error: err}, 202);
            }
            else {
                return responder.badRequest(res, {error: err});
            }
        }

        responder.respond(res, {success: true});
    })
};

RouteHandlers.removeGroupMember = function removeGroupMember(req, res) {
    var groupId = ObjectID(req.params.groupId),
        userId = ObjectID(req.params.userId);

    async.waterfall([
        function updateUsers(callback) {
            db.Group.update({_id: groupId},
                {
                    $pull: {
                        users: userId
                    }
                }, callback);
        },
        function updateAdminUsers(res, callback) {
            db.Group.update({_id: groupId},
                {
                    $pull: {
                        admin_users: userId
                    }
                }, callback);
        }
    ], function (err) {
        if (err) {
            return responder.badRequest(res, null, err);
        }
        return responder.respond(res, {
            success: true
        });
    });
};

// Comments
RouteHandlers.addGameComment = function addGameComment(req, res) {
    var user = req.user;
    var gameId = req.params.gameId;
    var userId = req.user.id;
    var text = req.body.text;
    if (!gameId || !user || !text) {
        return responder.badRequest(res);
    }

    GameService.checkAccess(userId, gameId, function (err, game, isAdmin) {
        if (err || !game) {
            return responder.badRequest(res, null, err);
        }
        else {
            CommentService.addGameComment(game.id, userId, text, RouteHandlers.handleCommonCrudResponse.bind(null, res));
        }
    });
};

RouteHandlers.getAllGameComments = function getAllGameComments(req, res) {
    var user = req.user;
    var gameId = req.params.gameId;
    var userId = req.user.id;
    if (!gameId || !user) {
        return responder.badRequest(res);
    }

    GameService.checkAccess(userId, gameId, function (err, game, isAdmin) {
        if (err || !game) {
            return responder.badRequest(res, null, err);
        }
        else {
            CommentService.getByGameId(game.id, RouteHandlers.handleCommonCrudResponse.bind(null, res));
        }
    });
};

RouteHandlers.addGroupComment = function addGroupComment(req, res) {
    var user = req.user;
    var groupId = req.params.groupId;
    var userId = req.user.id;
    var text = req.body.text;
    if (!groupId || !user || !text) {
        return responder.badRequest(res);
    }

    GroupService.getByUrlNameOrId(groupId, function(err, group){
        if(err){
            return responder.badRequest(res);
        }
        if(GroupService.userCanAccess(userId, group))
        {
            CommentService.addGroupComment(group.id, userId, text, RouteHandlers.handleCommonCrudResponse.bind(null, res));
        }
    });
};

RouteHandlers.getAllGroupComments = function getAllGroupComments(req, res) {
    var user = req.user;
    var groupId = req.params.groupId;
    var userId = req.user.id;
    if (!groupId || !user) {
        return responder.badRequest(res);
    }

    GroupService.getByUrlNameOrId(groupId, function(err, group){
        if(err){
            return responder.badRequest(res);
        }
        if(GroupService.userCanAccess(userId, group))
        {
            CommentService.getByGroupId(group.id, RouteHandlers.handleCommonCrudResponse.bind(null, res));
        }
    });
};

RouteHandlers.addUserComment = function addUserComment(req, res) {
    var createdBy = req.user;
    var userId = req.params.userId;
    var text = req.body.text;
    if (!userId || !text) {
        return responder.badRequest(res);
    }

    CommentService.addUserComment(userId, createdBy._id, text, RouteHandlers.handleCommonCrudResponse.bind(null, res));
};

RouteHandlers.getAllUserComments = function getAllGroupComments(req, res) {
    var createdBy = req.user;
    var userId = req.params.userId;
    if (!userId) {
        return responder.badRequest(res);
    }

    CommentService.getByUserId(userId, RouteHandlers.handleCommonCrudResponse.bind(null, res));
};

RouteHandlers.uploadGroupImage = function uploadGroupImage(req, res) {
    var id = req.params.id;
    if(!id) {
        return responder.badRequest(res, null, 'invalid body');
    }

    return GroupService.uploadGroupImage(req, id, RouteHandlers.handleCommonCrudResponse.bind(null, res));
};

RouteHandlers.getGroupImage = function getGroupImage(req, res) {
    var id = req.params.id;
    if(!id) {
        return responder.badRequest(res, null, 'invalid request');
    }

    return GroupService.getGroupImage(res, id);
};

RouteHandlers.uploadUserImage = function uploadUserImage(req, res) {
    return UserService.uploadUserImage(req, req.user._id, RouteHandlers.handleCommonCrudResponse.bind(null, res));
};

RouteHandlers.getUserImage = function getUserImage(req, res) {
    var id = req.params.id;
    if(!id) {
        return responder.badRequest(res, null, 'invalid request');
    }

    return UserService.getUserImage(res, id);
};

// Notifications
RouteHandlers.addGameNotification = function addGameNotification(req, res) {
    var gameId = req.body.gameId;
    var userId = req.user.id;
    if (!gameId || !userId) {
        return responder.badRequest(res);
    }

    GameService.checkAccess(userId, gameId, function (err, game, isAdmin) {
        if (err || !game) {
            return responder.badRequest(res, null, err);
        }
        else {
            NotificationService.addGameNotification(game, new db.Mongoose.Types.ObjectId(userId), req.body.type, {}, RouteHandlers.handleCommonCrudResponse.bind(null, res));
        }
    });
};

// Profile
RouteHandlers.updateProfileTimezone = function updateProfileTimezone(req, res) {
    var user = req.user;
    if (!user || !req.body.timezone) {
        return responder.badRequest(res);
    }

    UserService.updateProfileTimezone(req.user.id, req.body.timezone, function (err, user) {
        if (err || !user) {
            return responder.badRequest(res, null, err);
        }

        var sanitizedUser = common.sanitizeUser(user, true);
        return responder.respond(res, sanitizedUser);
    });
};

RouteHandlers.updateProfile = function updateProfile(req, res) {
    var user = req.user;
    if (!user || !req.body.name) {
        return responder.badRequest(res);
    }

    UserService.updateProfile(req.user.id, req.body, function (err, user) {
        if (err || !user) {
            return responder.badRequest(res, null, err);
        }

        var sanitizedUser = common.sanitizeUser(user, true);
        UserService.fillUserStats(sanitizedUser, function(err){
            if (err) {
                return responder.badRequest(res);
            } else {
                return responder.respond(res, sanitizedUser);
            }
        });
    });
};

RouteHandlers.updateProfileLanguage = function updateProfileLanguage(req, res) {
    var user = req.user;
    if (!user || !req.body.language) {
        return responder.badRequest(res);
    }

    UserService.updateProfileLanguage(req.user.id, req.body.language, function (err, user) {
        if (err || !user) {
            return responder.badRequest(res, null, err);
        }

        return responder.respond(res, common.sanitizeUser(user, true));
    });
};

RouteHandlers.getUserFriends = function updateProfile(req, res) {
    UserService.getFriends(req.user.id, RouteHandlers.addGameStatsToUsers.bind(null, res));
};

RouteHandlers.addUserFriends = function addUserFriends(req, res) {
    if (!req.body.users){
        return responder.badRequest(res);
    }

    UserService.addFriends(req.user.id, req.body.users, RouteHandlers.handleCommonCrudResponse.bind(null, res));
};

RouteHandlers.removeUserFriend = function removeUserFriend(req, res) {
    if (!req.params.userId){
        return responder.badRequest(res);
    }

    UserService.removeFriends(req.user.id, [req.params.userId], RouteHandlers.handleCommonCrudResponse.bind(null, res));
};

// Groups
RouteHandlers.createGroup = function createGroup(req, res) {
    var user = req.user;
    var name = req.body.name;
    var description = req.body.description;
    if (!name || !user) {
        return responder.badRequest(res);
    }

    GroupService.createGroup(user._id, name, description, RouteHandlers.handleCommonCrudResponse.bind(null, res));
};

RouteHandlers.getAllUserGroups = function getAllUserGroups(req, res) {
    GroupService.getAll(req.user._id, function (err, groups) {
        if (err) {
            return responder.badRequest(res, null, err);
        }
        GroupService.addGames(groups, RouteHandlers.handleCommonCrudResponse.bind(null, res));
    });
};

RouteHandlers.getByUrlNameOrId = function getByUrlNameOrId(req, res) {
    var groupId = req.params.groupId;
    if (!groupId) {
        return responder.badRequest(res);
    }
    GroupService.getByUrlNameOrId(groupId, function (err, group) {
        if (err) {
            return responder.badRequest(res, null, err);
        }

        //if (GroupService.userCanAccess(req.user.id, group)) {
            return responder.respond(res, group);
        //}
        // let everyone inside...?
        //return responder.accessDenied(req, res);
    });
};

RouteHandlers.deleteGroupById = function deleteGroupById(req, res) {
    var groupId = req.params.groupId;
    if (!groupId) {
        return responder.badRequest(res);
    }
    GroupService.getById(groupId, function (err, group) {
        if (err) {
            return responder.badRequest(res, null, err);
        }

        var userRole = GroupService.getUserRole(req.user.id, group);

        if (userRole == GroupService.Constants.UserRole.Admin || userRole == GroupService.Constants.UserRole.Creator) {
            GroupService.deleteById(groupId, function done(err) {
                if(err) {
                    return RouteHandlers.handleCommonCrudResponse(res, err, null);
                }

                InviteService.deleteByGroup(groupId, RouteHandlers.handleCommonCrudResponse.bind(null, res));
            });
        } else {
            responder.accessDenied(req, res);
        }
    });
};

RouteHandlers.generateGroupInvitationCode = function generateGroupInvitationCode(req, res) {
    var groupId = req.params.groupId;

    db.Group.findOne({_id: groupId}).exec(function (err, group) {
        if (err || !group) {
            return responder.badRequest(res, null, err);
        } else {
            // Yo: Removed this, so that all groups have an invitation code for now.
            //if (group.createdBy == req.user.id || (group.admin_users || []).indexOf(req.user.id) > -1) {
                GroupService.generateInvitationCode(group, function (err, groupWithCode) {
                    if (err || !groupWithCode) {
                        return responder.badRequest(res, null, err);
                    }
                    return responder.respond(res, groupWithCode);
                });
            //} else {
            //    return responder.badRequest(res, null, 'No permissions to generate invitation code in that group');
            //}
        }
    });
};

RouteHandlers.deleteGroupInvitationCode = function deleteGroupInvitationCode(req, res) {
    var groupId = req.params.groupId;

    db.Group.findOne({_id: groupId}).exec(function (err, group) {
        if (err || !group) {
            return responder.badRequest(res, null, err);
        } else {
            if (group.admin_users && group.admin_users.indexOf(req.user.id) > -1) {
                GroupService.deleteInvitationCode(group, function (err, groupWithoutCode) {
                    if (err || !groupWithoutCode) {
                        return responder.badRequest(res, null, err);
                    }
                    return responder.respond(res, {success: true});
                });
            } else {
                return responder.badRequest(res, null, 'No permissions to delete invitation code in that group');
            }
        }
    });
};

RouteHandlers.generateGameInvitationCode = function generateGameInvitationCode(req, res) {
    var gameId = req.params.gameId;

    GameService.checkAccess(req.user.id, gameId, function (err, game, isAdmin) {
        if (err || !game || !isAdmin) {
            return responder.badRequest(res, null, err);
        }
        else {
            GameService.generateInvitationCode(game, function (err, gameWithCode) {
                if (err || !gameWithCode) {
                    return responder.badRequest(res, null, err);
                }
                return responder.respond(res, gameWithCode);
            });
        }
    });
};

RouteHandlers.deleteGameInvitationCode = function deleteGameInvitationCode(req, res) {
    var gameId = req.params.gameId;

    GameService.checkAccess(req.user.id, gameId, function (err, game, isAdmin) {
        if (err || !game || !isAdmin) {
            return responder.badRequest(res, null, err);
        }
        else {
            GameService.deleteInvitationCode(game, function (err, groupWithoutCode) {
                if (err || !groupWithoutCode) {
                    return responder.badRequest(res, null, err);
                }
                return responder.respond(res, {success: true});
            });
        }
    });
};

RouteHandlers.addGameStatsToUsers = function addGameStatsToUsers(res, err, users){
    if(err){
        return responder.badRequest(res, null, err);
    }
    return UserService.addUserStats(users, RouteHandlers.handleCommonCrudResponse.bind(null, res));
};

// Users
RouteHandlers.searchUsers = function searchUsers(req, res) {
    var email = req.query.email,
        name = req.query.name;
    if (!email && !name) {
        return responder.badRequest(res);
    }

    UserService.searchUsers(email, name, RouteHandlers.addGameStatsToUsers.bind(null, res));
};

RouteHandlers.getAllUsers = function getAllUsers(req, res) {
    var email = req.user.email;
    UserService.getAllUsers(email, RouteHandlers.addGameStatsToUsers.bind(null, res));
};

RouteHandlers.getAllErrors = function getAllErrors(req, res) {
    var email = req.user.email;
    UserService.getAllErrors(email, RouteHandlers.addGameStatsToUsers.bind(null, res));
};

RouteHandlers.addFavouriteLocation = function addFavouriteLocation(req, res) {
    var favLoc = req.params.id;
    var userId = req.user.id;

    if(!favLoc) {
        return responder.badRequest(res, null, 'invalid body');
    }

    return LocationService.addFavouriteLocation(userId, favLoc,
        function(err, data) {
            return RouteHandlers.handleCommonCrudResponse(res, err, data);
        });
};

RouteHandlers.removeFavouriteLocation = function removeFavouriteLocation(req, res) {
    var favLoc = req.params.id;
    var userId = req.user.id;

    if(!favLoc) {
        return responder.badRequest(res, null, 'invalid body');
    }

    return LocationService.removeFavouriteLocation(userId, favLoc,
        function(err, data) {
            return RouteHandlers.handleCommonCrudResponse(res, err, data);
        });
};

RouteHandlers.uploadLocationImage = function uploadLocationImage(req, res) {
    var id = req.params.id;
    if(!id) {
        return responder.badRequest(res, null, 'invalid body');
    }

    return LocationService.uploadLocationImage(req, id, RouteHandlers.handleCommonCrudResponse.bind(null, res));
};

RouteHandlers.getLocationImage = function getLocationImage(req, res) {
    var id = req.params.id;
    if(!id) {
        return responder.badRequest(res, null, 'invalid request');
    }

    return LocationService.getLocationImage(res, id);
};

RouteHandlers.getFavouriteLocations = function getFavouriteLocations(req, res) {
    return LocationService.getFavouriteLocations(req.user.id, RouteHandlers.handleCommonCrudResponse.bind(null, res));
};

RouteHandlers.createLocation = function createLocation(req, res) {
    var location = req.body;

    if(!location.name || !location.coordinates || location.coordinates.length !== 2) {
        return responder.badRequest(res, null, 'invalid body');
    }

    return LocationService.createLocation(req.user.id, location, RouteHandlers.handleCommonCrudResponse.bind(null, res));
};

RouteHandlers.getLocationsHandler = function getLocationsHandler(req, res) {
    return LocationService.getLocations(RouteHandlers.handleCommonCrudResponse.bind(null, res));
};

RouteHandlers.getLocationByIdHandler = function(req, res) {
    var id = req.params.id;
    if(!id) {
        return responder.badRequest(res, null, 'invalid request - missing identifier');
    }

    return LocationService.getLocationById(id, RouteHandlers.handleCommonCrudResponse.bind(null, res));
};

RouteHandlers.searchLocationsHandler = function searchLocationsHandler(req, res) {
    var criteria = {
        type: req.query.type,
        name: req.query.name,
        address: req.query.address,
        lat: req.query.lat,
        long: req.query.long,
        maxDist: req.query.maxDist,
        txt: req.query.txt
    };

    if(!criteria.type) {
        return responder.badRequest(res, null, 'invalid request - missing type');
    }

    return LocationService.searchLocations(criteria, RouteHandlers.handleCommonCrudResponse.bind(null, res));
};

// Common
RouteHandlers.handleCommonCrudResponse = function (res, err, data) {
    if (err) {
        return responder.badRequest(res, null, err);
    }

    // set empty response, for not found items.
    if(!data && res.appContext.req.method !== 'DELETE') {
        return responder.notFound(res);
    }

    return responder.respond(res, data);
};

RouteHandlers.paginatedResponse = function (res, err, data) {
    if (err) {
        return RouteHandlers.handleCommonCrudResponse(res, err, data);
    }
    else {
        var page = 0, count = Number.MAX_VALUE;
        if(res.appContext.req.query.page){
            page = parseInt(res.appContext.req.query.page);
        }
        if(res.appContext.req.query.count){
            count = parseInt(res.appContext.req.query.count);
        }

        var result = {
            total: data.length,
            items: _.slice(data, page * count, page * count + count)
        };

        return responder.respond(res, result);
    }
};

exports = module.exports = RouteHandlers;