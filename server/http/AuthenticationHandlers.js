/* global db, config */
'use strict';

var responder = rootRequire('/http/HttpResponder'),
    request = require('superagent'),
    AvatarService = require('../services/AvatarService'),
    MailService = require('../services/MailService'),
    AccessTokenService = require('../services/AccessTokenService'),
    common = require('../config/common'),
    passport = require('passport');

var AuthenticationHandlers = {};

var getUserByEmail = function (email, done) {
    db.User.findOne({
        'email': email.toLowerCase()
    }, function (err, user) {
        if (err) return done(err);
        return done(null, user);
    });
};

// data: {"id":"10153071715953207","name":"Yosif Yosifov","email":"cypressx\u0040gmail.com"}
var handleFacebookUserLoggedIn = function (res, data, accessToken, done) {
    // find the user in the database based on their facebook id
    db.User.findOne({
        'facebook.id': data.id
    }, function (err, user) {
        if (err) {
            return done(err);
        }

        // if the user is found, then log them in
        if (user) {
            user.facebook.token = accessToken;
            user.save();
            return done(null, user);
        } else {
            getUserByEmail(data.email, function (err, user) {
                if (err) {
                    return done(err);
                }

                var newUser = false;
                if (!user) {
                    user = new db.User();
                    // user doesn't exist, so set the primary e-mail
                    user.email = data.email.toLowerCase();
                    newUser = true;
                }

                // set all of the facebook information in our user model
                user.facebook.id = data.id; // set the users facebook id
                user.facebook.token = accessToken; // we will save the token that facebook provides to the user
                user.facebook.name = data.name;
                //user.facebook.email = data.email;

                // set user.name if there is none.
                if (!user.name) {
                    user.name = data.name;
                }

                // save our user to the database
                user.save(function (err) {
                    if (err)
                        throw err;

                    // if successful, try to get facebook photo and return the user
                    AvatarService.getFacebookPhoto(user.facebook.id, 'large', function (avatarErr, avatarUrl) {
                        if (!avatarErr) {
                            user.preferredAvatar = 1;
                            user.avatarUrl = avatarUrl;

                            user.save(function (avatarUpdateErr) {
                                if (avatarUpdateErr) {
                                    common.logError(avatarUpdateErr, req);
                                }

                                done(null, user);
                                MailService.sendRegistrationMail(user, data.name, newUser, 'Facebook');
                            });
                        } else {
                            done(null, user);
                            MailService.sendRegistrationMail(user, data.name, newUser, 'Facebook');
                        }
                    });
                });
            });
        }
    });
};

// data: {"id":"10153071715953207","name":"Yosif Yosifov","email":"cypressx\u0040gmail.com"}
var handleGoogleUserLoggedIn = function (res, data, accessToken, done) {
    // find the user in the database based on their facebook id
    db.User.findOne({
        'google.id': data.sub
    }, function (err, user) {
        if (err) {
            return done(err);
        }

        // if the user is found, then log them in
        if (user) {
            user.google.token = accessToken;
            user.save();
            return done(null, user);
        } else {
            getUserByEmail(data.email, function (err, user) {
                if (err) {
                    return done(err);
                }

                var newUser = false;
                if (!user) {
                    user = new db.User();
                    // user doesn't exist, so set the primary e-mail
                    user.email = data.email.toLowerCase();
                    newUser = true;
                }

                // set all of the google information in our user model
                user.google.id = data.sub; // set the users google id
                user.google.token = accessToken; // we will save the token that google provides to the user
                user.google.name = data.name;
                //user.google.email = data.email;

                // set user.name if there is none.
                if (!user.name) {
                    user.name = data.name;
                }

                user.preferredAvatar = 1;
                user.avatarUrl = data.picture;

                // save our user to the database
                user.save(function (err) {
                    if (err)
                        throw err;

                    done(null, user);
                    MailService.sendRegistrationMail(user, data.name, newUser, 'Google');
                });
            });
        }
    });
};

AuthenticationHandlers.getAccessTokenFromLogin = function getAccessTokenFromLogin(req, res) {
    var email = req.body.username,
        password = req.body.password;

    db.User.findOne({
        "email": email.toLowerCase()
    }).exec(function (err, user) {
        if (err) {
            return responder.respond(res, {
                success: false,
                errorMessage: 'Login has failed!'
            }, 400);
        }

        if (user && user.authenticateLocal(password)) {
            var sanitizedUser = common.sanitizeUser(user, true);
            return AccessTokenService.createAccessToken(user._id, function (err, token) {
                if (err) {
                    return responder.respond(res, {
                        success: false,
                        errorMessage: 'Could not create an access token: ' + err
                    }, 400);
                }

                return responder.respond(res, {
                    success: true,
                    accessToken: token,
                    user: sanitizedUser
                }, 200);
            });
        } else {
            return responder.respond(res, {
                success: false,
                errorMessage: 'Could not create an access token: ' + err
            }, 400);
        }
    });
};

AuthenticationHandlers.getAccessTokenFromFacebookAccessToken = function getAccessTokenFromFacebookAccessToken(req, res) {
    // validates the access token - basically calls the /me endpoint
    var fbAccessToken = req.body.fb_access_token;
    if (!fbAccessToken) {
        return responder.respond(res, {
            success: false,
            errorMessage: 'Invalid request - expected fb_access_token inside'
        }, 400);
    }

    var fields = 'fields=id,name,email';
    var url = 'https://graph.facebook.com/me?access_token=' + fbAccessToken + '&' + fields;
    request.get(url)
        .end(function (err, fbResult) {
            if (err) {
                return responder.respond(res, {
                    success: false,
                    errorMessage: 'Invalid facebook access_token!'
                }, 400);
            }

            var data = JSON.parse(fbResult.text);

            if(!data.email){
                return responder.respond(res, {
                    success: false,
                    errorMessage: "No email address returned!"
                }, 501);
            }

            // handle user logged in
            handleFacebookUserLoggedIn(res, data, fbAccessToken, function (err, user) {
                if (err) {
                    return responder.respond(res, {
                        success: false,
                        errorMessage: 'Error occurred: ' + err
                    }, 400);
                }

                AccessTokenService.createAccessToken(user._id, function (err, token) {
                    if (err) {
                        return responder.respond(res, {
                            success: false,
                            errorMessage: 'Could not create an access token: ' + err
                        }, 400);
                    }

                    var sanitizedUser = common.sanitizeUser(user, true);
                    return responder.respond(res, {
                        success: true,
                        accessToken: token,
                        user: sanitizedUser
                    }, 200);
                });
            });
        });
};

AuthenticationHandlers.getAccessTokenFromGoogleAccessToken = function getAccessTokenFromGoogleAccessToken(req, res) {
    // validates the access token - basically calls the /me endpoint
    var googleAccessToken = req.body.google_access_token;
    if (!googleAccessToken) {
        return responder.respond(res, {
            success: false,
            errorMessage: 'Invalid request - expected google_access_token inside'
        }, 400);
    }

    var url = 'https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=' + googleAccessToken;
    request.get(url)
        .end(function (err, googleResult) {
            if (err) {
                return responder.respond(res, {
                    success: false,
                    errorMessage: 'Invalid google access_token!'
                }, 400);
            }

            var data = JSON.parse(googleResult.text);

            if(!data.email){
                return responder.respond(res, {
                    success: false,
                    errorMessage: "No email address returned!"
                }, 501);
            }

            // handle user logged in
            handleGoogleUserLoggedIn(res, data, googleAccessToken, function (err, user) {
                if (err) {
                    return responder.respond(res, {
                        success: false,
                        errorMessage: 'Error occurred: ' + err
                    }, 400);
                }

                AccessTokenService.createAccessToken(user._id, function (err, token) {
                    if (err) {
                        return responder.respond(res, {
                            success: false,
                            errorMessage: 'Could not create an access token: ' + err
                        }, 400);
                    }

                    var sanitizedUser = common.sanitizeUser(user, true);
                    return responder.respond(res, {
                        success: true,
                        accessToken: token,
                        user: sanitizedUser
                    }, 200);
                });
            });
        });
};

AuthenticationHandlers.authenticateWithBearerToken = function authenticateWithBearerToken(req, res, next) {
    var accessToken = req.get('Authorization');
    if (accessToken && accessToken.match(/^Bearer (.+)/i)) {
        return passport.authenticate('bearer', { session: true })(req, res, next);
    }

    return next();
}

exports = module.exports = AuthenticationHandlers;