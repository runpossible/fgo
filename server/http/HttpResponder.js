'use strict';

var common = global.rootRequire('/config/common');

var _respond = function respond(res, data, httpStatusCode, contentType) {
    httpStatusCode = httpStatusCode || 200;

    var logMessage = {
        statusCode: httpStatusCode
    };

    // if it's not a successful response, log the error.
    if (httpStatusCode != 200 && httpStatusCode != 201) {
        logMessage.data = data;
        common.logError(logMessage, res.appContext.req);
    } else {
        logMessage.message = 'Response is OK.';
        common.logInfo(logMessage, res.appContext.req);
    }

    contentType = contentType || 'application/json';

    res.writeHead(httpStatusCode, {'Content-Type': contentType});

    res.end(JSON.stringify(data));
};

exports = module.exports = {
    badRequest: function (res, data, errorMessage) {
        var d = data || {success: false};
        if (errorMessage) {
            d.errorMessage = errorMessage;
        }

        return _respond(res, d, 400);
    },

    notFound: function (res, errorMessage) {
        var d = {
            success: false,
            errorMessage: errorMessage || 'Not found'
        };

        return _respond(res, d, 404);
    },

    respond: _respond,

    respondPlain: function (res, data, httpStatusCode, contentType) {
        httpStatusCode = httpStatusCode || 200;
        contentType = contentType || 'application/json';

        res.writeHead(httpStatusCode, {'Content-Type': contentType});

        res.end(data);
    },

    accessDenied: function accessDenied(req, res) {
        _respond(res, {
            success: false,
            errorMessage: "Access Denied"
        }, 401);
    }
};