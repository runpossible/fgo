/*global db*/
'use strict';

var constants = {
    Status: {
        Active: 'Active',
        Accepted: 'Accepted',
        Rejected: 'Rejected',
        Expired: 'Expired'
    },
    PopulateString: 'createdBy player group game'
};

var async = require('async'),
    ok = require('okay'),
    _ = require('lodash'),
    common = global.rootRequire('/config/common'),
    MailService = rootRequire('/services/MailService'),
    PushService = rootRequire('/services/PushService');

exports = module.exports = {
    Constants: constants,

    /**
     * Create a new Invitation for a Player to join a Group
     * @param createdBy
     * @param player
     * @param group
     */
    createGroupInvite: function createGroupInvite(createdBy, player, group, done) {
        // todo: can check if the player is already a member of the group...
        async.waterfall([
            function findExistingInvite(callback) {
                db.Invite.findOne({
                    $and: [
                        {
                            player: player
                        },
                        {
                            group: group
                        },
                        {
                            status: constants.Status.Active
                        }
                    ]
                }).exec(callback);
            },
            function createInvite(existingInvite, callback) {
                if(existingInvite) {
                    // this user  is already invited
                    return callback(null, existingInvite);
                }

                var invite = new db.Invite();
                invite.group = group;
                invite.status = constants.Status.Active;
                invite.player = player;
                invite.createdBy = createdBy;

                invite.save(callback);
            }
        ], function final(err, invite) {
            if(err) {
                return done(err);
            }
            return done(null, invite);
        });
    },

    createGameInvite: function createGameInvite(createdBy, player, game, done) {
        async.waterfall([
            function findExistingInvite(callback) {
                db.Invite.findOne({
                    $and: [
                        {
                            player: player
                        },
                        {
                            game: game
                        },
                        {
                            status: constants.Status.Active
                        }
                    ]
                }).exec(callback);
            },
            function createInvite(existingInvite, callback) {
                if(existingInvite) {
                    // this user  is already invited
                    return callback(null, existingInvite);
                }

                var invite = new db.Invite();
                invite.game = game;
                invite.status = constants.Status.Active;
                invite.player = player;
                invite.createdBy = createdBy;
                invite.mailSent = true;

                PushService.send({
                    text: "You're invited to a game",
                    game: {
                        id: game.id,
                        date_time: game.date_time,
                        locationName: game.locationName,
                        location: game.location
                    },
                    invite: {
                        id: invite.id
                    }
                }, invite.player, createdBy, function(err) {
                    if(err) {
                        common.logError("Couldn't send push notificatoin to: " + player.email + ". With error: " + err);
                    }
                });

                MailService.sendInviteToGameMail(invite, function(err) {
                    if(err) {
                        common.logError("couldn't send invite game mail to" + player.email + ". With error: " + err);
                    }
                });

                invite.save(callback);
            }
        ], function final(err, invite) {
            if(err) {
                return done(err);
            }

            return done(null, invite);
        });
    },

    /**
     * Find all Invitations for a Player, filtered by status
     * @param userId
     * @param status
     */
    find: function find(userId, status, done) {
        status = status || constants.Status.Active; // by default get Active invites.
        db.Invite.find({
            $and: [
                {
                    player: userId
                },
                {
                    status: status
                }
            ]
        }).populate(constants.PopulateString).exec(function(err, invites){
            if(err){
                return done(err);
            }
            if(status == constants.Status.Active){
                invites = _.filter(invites, function(invite){ return !invite.game || (invite.game && invite.game.date_time > new Date())});
            }
            done(err, invites);
        });
    },


    /**
     * Checks if the User is invited to the game or not
     * @param userId
     * @param gameId
     * @param done
     */
    isInvited: function isInvited(userId, gameId, done) {
        return db.Invite.find({
            $and: [
                {
                    player: userId
                },
                {
                    game: gameId
                }
            ]
        }).exec(function(err, invite) {
            var isInvited = invite && invite.length > 0;
            return done(isInvited); // if no invite is found => the user's not invited.
        });
    },

    findAllPendingGameInvitations: function findAllPendingGameInvitations(userId, done) {
        return db.Invite.find({
            $and: [
                {
                    player: userId
                },
                {
                    status: {
                        $nin: [ constants.Status.Accepted, constants.Status.Expired ]
                    }
                }
            ]
        }).exec(done);
    },

    findActiveByGroup: function findActiveByGroup(groupId, done) {
        db.Invite.find({
            $and: [
                {
                    group: groupId
                },
                {
                    status: constants.Status.Active
                }
            ]
        }).populate(constants.PopulateString).exec(done);
    },

    getAllForGame: function getAllForGame(gameId, done) {
        return db.Invite.find({
            $and: [
                {
                    game: gameId
                },
                {
                    status: {
                        $nin: [
                            constants.Status.Expired
                        ]
                    }
                }
            ]
        }).populate(constants.PopulateString).exec(done);
    },

    findActiveByGame: function findActiveByGame(gameId, done) {
        db.Invite.find({
            $and: [
                {
                    game: gameId
                },
                {
                    status: constants.Status.Active
                }
            ]
        }).populate(constants.PopulateString).exec(done);
    },

    deleteByGame: function(gameId, done) {
        db.Invite.find({ game: gameId }).exec(ok(function(invites){
            async.each(invites, function(invite, callback){ invite.remove(callback);}, done);
        }));
    },

    deleteByGroup: function(groupId, done) {
        db.Invite.find({ group: groupId }).exec(ok(function(invites){
            async.each(invites, function(invite, callback){ invite.remove(callback);}, done);
        }));
    }
};