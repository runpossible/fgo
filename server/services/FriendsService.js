/* global db */

'use strict';

var _ = require('lodash'),
    async = require('async'),
    google = require('googleapis'),
    plus = google.plus('v1'),
    googleAuth = require('google-auth-library'),
    FB = require('fb'),
    ok = require('okay');

var FriendsService = function () {
};

var getFBAccessToken = function(callback) {
    FB.api('oauth/access_token', {
        client_id: global.config.auth.facebook.client_id,
        client_secret: global.config.auth.facebook.client_secret,
        grant_type: 'client_credentials'
    }, function (res) {
        if(!res || res.error) {
            console.log(!res ? 'error occurred' : res.error);
            return callback(!res ? 'error occurred' : res.error);
        }

        FB.setAccessToken(res.access_token);
        callback();
    });
};

var callGoogleAuthorized = function(userId, callback) {
    db.User.findOne({ _id: userId}).exec(ok(callback, function(user){
        var clientSecret = global.config.auth.google.client_secret;
        var clientId = global.config.auth.google.client_id;
        var redirectUrl = global.config.auth.google.callbackURL;
        var auth = new googleAuth();
        var oauth2Client = new auth.OAuth2(clientId, clientSecret, redirectUrl);
        oauth2Client.setCredentials({
            access_token: user.google.token,
            refresh_token: user.google.refreshToken
            });
        callback(oauth2Client);
    }));

};

var getFacebookUserToken = function(userId, callback) {
    db.User.findOne({ _id: userId}).exec(ok(callback, function(user){
        callback(null, {
            access_token: user.facebook.token,
            refresh_token: user.facebook.refreshToken
        });
    }));
};

var callFBEndpoint = function(url, method, callback) {
    getFBAccessToken(function(err){
        if(err){
            return callback(err);
        }
        FB.api('', 'post', {
            batch: [
                { method: method, relative_url: url }
            ]
        }, function (res) {
            if(!res || res.error) {
                return callback(!res ? 'error occurred' : res.error);
            }

            var actualResult = JSON.parse(res[0].body);

            if(actualResult.error) {
                return callback(actualResult.error);
            } else {
                callback(null, actualResult);
            }
        });
    });
};

FriendsService.getFacebookFriends = function getFacebookFriends(fbUserId, callback) {
    callFBEndpoint('/' + fbUserId + '/friends', 'get', ok(callback, function (response){
        var friendIds = _.pluck(response.data, 'id');
        db.User.find({ 'facebook.id': {$in: friendIds } }).sort('name').exec(callback);
    }));
};

FriendsService.getFacebookFriendsDebug = function getFacebookFriends(fbUserId, callback) {
    callFBEndpoint('/' + fbUserId + '/friends?limit=5000', 'get', ok(callback, function (response){
        callback(null, {
            reponse: response,
            token: FB.getAccessToken()
        });
    }));
};

FriendsService.getGoogleFriends = function getGoogleFriends(userId, googleUserId, done) {
    var resultIds = [];
    var addFriendsToList = function(nextPageToken, auth, collection, callback){
        plus.people.list({userId: googleUserId, collection: collection, pageToken: nextPageToken, key: global.config.auth.google.API_key, auth: auth}, ok(callback, function (response){
            var friendIds = _.pluck(response.items, 'id');
            resultIds = resultIds.concat(friendIds);
            if(response.nextPageToken){
                addFriendsToList(response.nextPageToken, auth, collection, callback)
            }
            else {
                callback(null, resultIds);
            }
        }));
    };
    var getUsers = ok(done, function(resultIds){
        db.User.find({'google.id': {$in: resultIds}}).sort('name').exec(done);
    });

    callGoogleAuthorized(userId, function(auth){
        addFriendsToList(undefined, auth, 'connected', ok(done, function(resultIds){
            if(resultIds.length == 0) {
                addFriendsToList(undefined, auth, 'visible', getUsers);
            }
            else{
                getUsers(null, resultIds);
            }
        }));
    });
};

FriendsService.getFacebookFriendsToInvite = function getFacebookFriendsToInvite(userId, facebookUserId, done) {
    var results = [];
    var addFriendsToList = function(nextPageUrl, callback){
        getFacebookUserToken(userId, ok(callback, function( tokens){
            var relative_url = nextPageUrl || '/' + facebookUserId + '/taggable_friends?limit=100&fields=name,picture.width(100),id&access_token=' + tokens.access_token;
            if(nextPageUrl){
                var relativeStart = nextPageUrl.indexOf('/' + facebookUserId + '/');
                relative_url = nextPageUrl.substring(relativeStart);
            }

            FB.api('', 'post', {
                batch: [
                    { method: 'get', relative_url: relative_url}
                ]
            }, function (res) {
                if(!res || res.error) {
                    return callback(!res ? 'error occurred' : res.error);
                }

                var actualResult = JSON.parse(res[0].body);

                if(actualResult.error) {
                    return callback(actualResult.error);
                } else {
                    results = results.concat(actualResult.data);
                    if(actualResult.paging && actualResult.paging.next){
                        addFriendsToList(actualResult.paging.next, callback)
                    }
                    else {
                        callback(null, results);
                    }
                }
            });
        }));
    };

    getFBAccessToken(function(){
        addFriendsToList(undefined, done);
    });
};

FriendsService.postToFacebookFeed = function postToFacebookFeed(userId, facebookUserId, message, tags, done) {
    getFBAccessToken(function() {
       getFacebookUserToken(userId, ok(done, function (tokens) {
           var siteAddress = global.config.site_address;
           if(siteAddress.indexOf('localhost') > -1){
               siteAddress = 'https://www.fgo.io';
           }
           FB.api('me/feed', 'post', {
                    access_token: tokens.access_token,
                    tags: tags,
                    link: siteAddress,
                    description: message,
                    picture: siteAddress + '/assets/img/fgo-fb-post-image.jpg'
           }, function (res) {
               if(!res || res.error) {
                   console.log(!res ? 'error occurred' : res.error);
                   return done(res.error);
               }
               return done(null, res);
           });

        }));
    });
};

FriendsService.getGoogleFriendsToInvite = function getGoogleFriendsToInvite(userId, googleUserId, done) {
    var results = [];
    var addFriendsToList = function(nextPageToken, auth, collection, callback){
        plus.people.list({userId: googleUserId, collection: collection, pageToken: nextPageToken, key: global.config.auth.google.API_key, auth: auth}, ok(callback, function (response){
            results = results.concat(response.items);
            if(response.nextPageToken){
                addFriendsToList(response.nextPageToken, auth, collection, callback)
            }
            else {
                callback(null, results);
            }
        }));
    };

    callGoogleAuthorized(userId, function(auth){
        addFriendsToList(undefined, auth, 'visible', done);
    });
};

FriendsService.getGoogleFriendsDebug = function getGoogleFriendsDebug(userId, googleUserId, done) {
    var resultIds = [];
    var addFriendsToList = function(nextPageToken, auth, collection, callback){
        plus.people.list({userId: googleUserId, collection: collection, pageToken: nextPageToken, key: global.config.auth.google.API_key, auth: auth}, ok(callback, function (response){
            var friendIds = response.items;
            resultIds = resultIds.concat(friendIds);
            if(response.nextPageToken){
                addFriendsToList(response.nextPageToken, auth, collection, callback)
            }
            else {
                callback(null, auth, resultIds);
            }
        }));
    };
    var getUsers = ok(done, function(auth, resultIds){
        done(null, {
            result: resultIds,
            auth: auth
        });
    });

    callGoogleAuthorized(userId, function(auth){
        addFriendsToList(undefined, auth, 'connected', ok(done, function(auth, resultIds){
            if(resultIds.length == 0) {
                addFriendsToList(undefined, auth, 'visible', getUsers);
            }
            else{
                getUsers(null, auth, resultIds);
            }
        }));
    });
};

FriendsService.searchForFriends = function searchForFriends(userId, search, done) {
    search = (search || '').trim();
    var nameExpr = new RegExp(search, 'i');
    async.waterfall([
        function getUserFriends(cb){
            db.User.findOne({ _id: userId}).exec(cb);
        },
        function getPlayersByCityAndLocationName(user, cb){
            var friends = user.friends;
            var friendIds =  _.map(friends, function (p) {
                return p.toString();
            });
            if(search) {
                db.Game.find({
                    $and: [
                        {
                            'players': {$in: friends}
                        },
                        {
                            $or: [{'city.name': nameExpr}, {'name': nameExpr}]
                        }
                    ]
                }, {players: 1}).exec(ok(done, function (results) {
                    var players = _.pluck(results, 'players');
                    players = _.flatten(players);
                    var playerIds = _.map(players, function (p) {
                        return p.toString();
                    });
                    playerIds = _.uniq(playerIds);

                    cb(null, {
                        friends: friendIds,
                        playerIds: playerIds
                    });
                }));
            } else {
                cb(null, {
                    friends: friendIds,
                    playerIds: friendIds
                });
            }
        }, function getPlayerDetails(result, cb){
            db.User.find({
                $and: [
                    {
                        '_id': {$in: result.friends}
                    },
                    {
                        $or: [{ _id: {$in: result.playerIds }}, { 'name': nameExpr }]
                    }
                ]
            }).sort('name').exec(cb);
        }], done);
};

FriendsService.searchForUsers = function searchForUsers(search, done) {
    search = (search || '').trim();
    var nameExpr = new RegExp(search, 'i');
    async.waterfall([
        function getPlayersByCityAndLocationName(cb){
            db.Game.find({ $or: [{ 'city.name': nameExpr }, { 'locationName': nameExpr }]}, { players: 1}).exec(ok(done, function(results){
                var players = _.pluck(results, 'players');
                players = _.flatten(players);
                players = _.filter(players, function(p) 
                    { 
                        if(p) {
                            return true;
                        }
                        
                        return false;
                    });
                var playerIds = _.map(players, function(p){ return p.toString(); });
                playerIds = _.uniq(playerIds);
                cb(null, playerIds);
            }));
        }, function getPlayerDetails(locationPlayers, cb){
            // the user should have active local profile or facebook or google profile
            db.User.find({
                $and: [
                {
                    'public': true
                },
                {
                    $or: [ { 'local.activationCode': null }, { 'facebook.id' : {$ne: null}}, {'google.id': {$ne: null}}]
                },
                {
                    $or: [{ _id: {$in: locationPlayers }}, { 'name': nameExpr }, { 'email': nameExpr }]
                }
            ]}).sort('name').exec(cb);
        }], done);
};

exports = module.exports = FriendsService;