/*global db, gfs, utilities*/
'use strict';

var async = require('async'),
    multiparty = require('multiparty'),
    FileUtility = global.rootRequire('./FileUtility');

var SearchTypes = {
    ByNearest: 'by-nearest',
    ByName: 'by-name',
    ByAddress: 'by-address',
    All: 'all'
};

exports = module.exports = {
    addFavouriteLocation: function addFavouriteLocation(userId, locationId, done) {
        var favLoc = new db.FavouriteLocation();
        favLoc.location = locationId;
        favLoc.user = userId;

        favLoc.save(function(err, location){
            // duplicate key error.
            if (db.isDuplicateError(err)) {
                done('LOCATION ALREADY ADDED');
            } else {
                done(err, location);
            }
        });
    },

    removeFavouriteLocation: function removeFavouriteLocation(userId, locationId, done) {
        db.FavouriteLocation.find({ user: userId, location: locationId}).remove(done);
    },

    getFavouriteLocations: function getFavouriteLocations(userId, done) {
        return db.FavouriteLocation.find({user: userId}).populate('user location').exec(done);
    },

    createLocation: function createLocation(owner, location, done) {
        var newLoc = new db.Location();
        newLoc.name = location.name;
        newLoc.loc = location.coordinates;
        newLoc.formattedAddress = location.formattedAddress;
        newLoc.price = location.price;
        newLoc.playersCount = location.playersCount;
        newLoc.phone = location.phone;
        newLoc.totalFieldsCount = location.totalFieldsCount;
        newLoc.city = location.city;

        newLoc.createdBy = owner;

        newLoc.save(done);
    },

    uploadLocationImage: function uploadLocationImage(req, id, done) {
        var form = new multiparty.Form();
        form.parse(req, function (err, fields, files) {
            if (files.file.length) {
                var file = files.file[0];
                db.Location.findOne({_id: id}, function (err, loc) {
                    if (loc) {
                        FileUtility.ensureFileSize(file, null, function(err, isOk) {
                            if(err) return done(err);

                            FileUtility.saveFileInGridFS(file, function(err, fileName) {
                                if(err) return done(err);

                                loc.primaryImage = fileName;
                                return loc.save(done);
                            })
                        });
                    }
                });
            }
        });
    },

    getLocationImage: function getLocationImage(res, id) {
        db.Location.findOne({_id: id}, function (err, loc) {
            if (loc) {
                var readStream = global.gfs.createReadStream({
                    filename: loc.primaryImage
                }).on('error', function (err) {
                    console.log('cannot read image. error: ' + err);
                }).on('close', function() {
                    //return done();
                });

                return readStream.pipe(res);
            }
        });
    },

    getLocations: function getLocationcs(done) {
        return db.Location.find({}).exec(done);
    },

    getLocationById: function getLocationById(id, done) {
        return db.Location.findOne({_id: id}, done);
    },

    searchLocations: function searchLocations(criteria, done) {
        var limit = 1000;

        switch (criteria.type) {
            case SearchTypes.ByNearest:
                // http://blog.robertonodi.me/how-to-use-geospatial-indexing-in-mongodb-using-express-and-mongoose/
                var dist = criteria.maxDist || 10000; // 10 km
                //dist /= 6371;
                var coords = [
                    criteria.long,
                    criteria.lat
                ];

                //query.where('loc').near({ center: { coordinates: [10, 10], type: 'Point' }, maxDistance: 5 });
                return db.Location.find({}).where('loc')
                    .near({center: {coordinates: coords, type: 'Point'}, maxDistance: dist})
                    .limit(limit)
                    .exec(done);
            /*return db.Location.find({
             loc: {
             $near: coords,
             $maxDistance: dist
             }
             }).limit(limit).exec(done);*/
            case SearchTypes.All:
                var nameExpr = new RegExp(criteria.txt, 'i');
                var addrExpr = new RegExp(criteria.txt, 'i');

                return db.Location.find({
                    $or: [
                        {
                            formattedAddress: addrExpr
                        },
                        {
                            name: nameExpr
                        }
                    ]
                }).limit(limit).exec(done);

            case SearchTypes.ByName:
                var nameExpr = new RegExp(criteria.name, 'i');
                return db.Location.find({name: nameExpr})
                    .limit(limit).exec(done);

            case SearchTypes.ByAddress:
                var addrExpr = new RegExp(criteria.address, 'i');
                return db.Location.find({formattedAddress: addrExpr})
                    .limit(limit).exec(done);
        }

        return done('Unsupported type: ' + criteria.type);
    }
};