/*global db, gfs, utilities*/
'use strict';

var async = require('async'),
    ok = require('okay'),
    fs = require('fs'),
    cheerio = require('cheerio'),
    nodemailer = require('nodemailer'),
    _ = require('lodash'),
    moment = require('moment-timezone'),
    mailgun = require('mailgun-js')({apiKey: config.mail.mailGun.API_key, domain: config.mail.mailGun.domain}),
    Entities = require('html-entities').XmlEntities;

var entities = new Entities();

var transporter = nodemailer.createTransport("SMTP", {
    host: config.mail.host,
    port: config.mail.port,
    auth: {
        user: config.mail.user,
        pass: config.mail.pass
    }
});

var mailContents = {
    passwordReset: {
        bg: fs.readFileSync(config.rootPath + '/app/mails/password-reset.bg.html', 'utf8'),
        en: fs.readFileSync(config.rootPath + '/app/mails/password-reset.en.html', 'utf8')
    },
    activation: {
        bg: fs.readFileSync(config.rootPath + '/app/mails/activation.bg.html', 'utf8'),
        en: fs.readFileSync(config.rootPath + '/app/mails/activation.en.html', 'utf8')
    },
    inviteToGame: {
        bg: fs.readFileSync(config.rootPath + '/app/mails/invite-to-game.bg.html', 'utf8'),
        en: fs.readFileSync(config.rootPath + '/app/mails/invite-to-game.en.html', 'utf8')
    },
    contactUs: {
        bg: fs.readFileSync(config.rootPath + '/app/mails/contactus.bg.html', 'utf8')
    },
    signedUp: {
        bg: fs.readFileSync(config.rootPath + '/app/mails/signed-up-users-mail.html', 'utf8')
    },
    userRegistered: {
        bg: fs.readFileSync(config.rootPath + '/app/mails/user-registered.html', 'utf8')
    }
};

var mailTextContents = {
    passwordReset: {
        bg: fs.readFileSync(config.rootPath + '/app/mails/password-reset.bg.txt.html', 'utf8'),
        en: fs.readFileSync(config.rootPath + '/app/mails/password-reset.en.txt.html', 'utf8')
    },
    activation: {
        bg: fs.readFileSync(config.rootPath + '/app/mails/activation.bg.txt.html', 'utf8'),
        en: fs.readFileSync(config.rootPath + '/app/mails/activation.en.txt.html', 'utf8')
    },
    inviteToGame: {
        bg: fs.readFileSync(config.rootPath + '/app/mails/invite-to-game.bg.txt.html', 'utf8'),
        en: fs.readFileSync(config.rootPath + '/app/mails/invite-to-game.en.txt.html', 'utf8')
    }
};

var sendWithRetry = function(mailOptions, done) {
    if(!mailOptions.bcc){
         mailOptions.bcc = config.mail.bcc;
    }
    var count = 1;

    var send = function (mail) {
        if(config.mail.enabled) {
            if (config.mail.useMailGun) {
                mail.from = mailOptions.overridenSender || config.mail.mailGun.from;
                mailgun.messages().send(mail, function (err, res) {
                    if (err) {
                        console.log('Error trying to send mail for ' + count + ' time! Err:' + err);
                        count++;
                        if (count < 4) {
                            send(mail);
                        }
                        else {
                            done(err);
                        }
                    } else {
                        done(null, res);
                    }
                });
            }
            else {
                transporter.sendMail(mail, function handler(err, res) {
                    if (err) {
                        console.log('Error trying to send mail for ' + count + ' time! Err:' + err);
                        count++;
                        if (count < 4) {
                            send(mail);
                        }
                        else {
                            done(err);
                        }
                    } else {
                        done(null, res);
                    }
                });
            }
        } else {
            done(null);
        }
    };

    send(mailOptions);
};

exports = module.exports = {
    sendPasswordReset: function(passwordReset, done){
        // mail sending may not work in some networks, but works on openshift
        
        var userLanguage = (passwordReset.user.language || 'en').toLowerCase();
        var mailContent = mailContents.passwordReset[userLanguage];
        var mailTextContent = mailTextContents.passwordReset[userLanguage];
        var $ = cheerio.load(mailContent);
        var navigateUrl = global.config.site_address + '/welcome?secret=' + passwordReset.secret;
        $("a[href='password-reset']").attr('href', navigateUrl);
        $('#player-name').text(passwordReset.user.name);

        mailTextContent = _.template(mailTextContent)(
            {
                name: passwordReset.user.name,
                link: navigateUrl
            });

        var mailOptions = {
            from: 'contact@fgo.io', // sender address
            to: passwordReset.user.email, // list of receivers
            subject: userLanguage == 'bg' ? 'Смяна на парола' : 'Password reset', // Subject line
            text: mailTextContent, // text body
            html: $.html() // html body
        };

        // send mail with defined transport object
        sendWithRetry(mailOptions, function(err, result){
            if(err){
                console.log(err);
            }
            done(err, result);
        });
    },

    sendActivationMail: function(user, done){
        // mail sending may not work in some networks, but works on openshift

        var userLanguage = (user.language || 'en').toLowerCase();
        var mailContent = mailContents.activation[userLanguage];
        var mailTextContent = mailTextContents.activation[userLanguage];
        var $ = cheerio.load(mailContent);
        var navigateUrl = global.config.site_address + '/welcome?code=' + user.local.activationCode;
        $("a[href='activation']").attr('href', navigateUrl);
        $('#player-name').text(user.name);

        mailTextContent = _.template(mailTextContent)(
            {
                name: user.name,
                link: navigateUrl
            });

        var mailOptions = {
            from: 'contact@fgo.io', // sender address
            to: user.email, // list of receivers
            subject: userLanguage == 'bg' ? 'Активация' : 'Activation mail', // Subject line
            text: mailTextContent, // text body
            html: $.html() // html body

        };

        // send mail with defined transport object
        sendWithRetry(mailOptions, function(err, result){
            if(err){
                console.log(err);
            }
            done(err, result);
        });
    },

    sendRegistrationMail: function(user, displayName, newUser, comesFrom, done){
        // mail sending may not work in some networks, but works on openshift

        var userLanguage = 'bg';
        var mailContent = mailContents.userRegistered[userLanguage];
        var $ = cheerio.load(mailContent);
        var avatarUrl = user.avatarUrl;
        $("#avatar").attr('src', avatarUrl);
        $(".comes-from").text(comesFrom);
        $("#name").text(user.name);
        $("#displayName").text(displayName);

        if(newUser){
            $("#existingUser").html('');
        } else {
            $("#newUser").html('');
        }

        var mailOptions = {
            from: 'contact@fgo.io', // sender address
            to: 'contact@fgo.io', // list of receivers
            subject: 'FGO: Регистрация',// Subject line
            html: $.html() // html body
        };

        // send mail with defined transport object
        sendWithRetry(mailOptions, function(err, result){
            if(err){
                console.log(err);
            }
            if(done) {
                done(err, result);
            }
        });
    },

    sendSignedUpUserMail: function(data, done) {
        var mailContent = mailContents.signedUp.bg;
        var $ = cheerio.load(mailContent);

        $('#name').text(data.name);
        $('#email').text(data.email);
        var now = new Date();
        $('#date').text(now.toString());

        var mailOptions = {
            from: 'Football Game Oranizer <contact@fgo.io>', // sender address
            to: data.email, // list of receivers
            subject: 'Първа версия', // Subject line
            html: $.html() // html body
        };

        // send mail with defined transport object
        sendWithRetry(mailOptions, function(err, result){
            if(err){
                console.log(err);
            }
            done(err, result);
        });
    },

    sendContactUsMail: function(data, user, done){
        var mailContent = mailContents.contactUs.bg;
        var $ = cheerio.load(mailContent);

        $('#name').text(data.name);
        $('#email').text(data.email);
        $('#message').text(data.message);
        var now = new Date();
        $('#date').text(now.toString());
        if(user) {
            $('#userId').text(user._id + ''); // cast to string
            $('#userEmail').text(user.email);
            $('#userName').text(user.name);
        }

        var mailOptions = {
            from: 'Football Game Oranizer <contact@fgo.io>', // sender address
            to: 'contact@fgo.io', // list of receivers
            subject: 'FGO: Контакт', // Subject line
            html: $.html() // html body
        };

        // send mail with defined transport object
        sendWithRetry(mailOptions, function(err, result){
            if(err){
                console.log(err);
            }
            done(err, result);
        });
    },

    sendInviteToGameMail: function(data, done){
        var game,
            player,
            createdBy;
        async.waterfall([
            function getGame(cb) {
                db.Game.findOne({_id: data.game}).populate('location').exec(function(err, g) {
                    if (err) return cb(err);
                    game = g;
                    cb();
                });
            },
            function getPlayer(cb) {
                db.User.findOne({ _id: data.player }).exec(function(err, p) {
                    if (err) return cb(err);
                    player = p;
                    cb();
                })
            },
            function getCreatedBy(cb) {
                db.User.findOne({ _id: data.createdBy }).exec(function(err, c) {
                    if (err) return cb(err);
                    createdBy = c;
                    cb();
                })
            }
        ], function(err) {
            if(err) return done(err);

            // user doesn't want to receive any emails.
            if(player.shouldReceiveGameInviteMails === false) {
                return done();
            }

            var userLanguage = (player.language || 'en').toLowerCase();
            var mailContent = mailContents.inviteToGame[userLanguage];
            var mailTextContent = mailTextContents.inviteToGame[userLanguage];
            var $ = cheerio.load(mailContent);

            var inviteId = data._id;
            var gameUrl = global.config.site_address + '/game-details/' + game._id;
            var acceptInviteUrl = gameUrl + '?action=Accepted&inviteId=' + inviteId;
            var rejectInviteUrl = gameUrl + '?action=Rejected&inviteId=' + inviteId;

            var updateProfileUrl = global.config.site_address + '/profile/edit';
            $("a[href='update-profile']").attr('href', updateProfileUrl);
            $("a[href='game-url']").attr('href', game.invitation_shorturl);
            $("a[href='accept-invite']").attr('href', acceptInviteUrl);
            $("a[href='reject-invite']").attr('href', rejectInviteUrl);
            $('#player-name').text(player.name);
            $('#created-by-name').text(createdBy.name);
            $('#location-name').text(game.location.name);

            // setting user locale
            moment.locale(userLanguage);

            // format date according to the timezone
            var dateTime = moment(game.date_time);
            var dateTimeString = dateTime.tz(player.timezone).format('MMMM Do YYYY, H:mm:ss');
            $('#date-time-text').text(dateTimeString);

            mailTextContent = _.template(mailTextContent)(
                {
                    name: player.name,
                    invitedBy: createdBy.name,
                    gameOn: dateTimeString,
                    gameAt: game.location.name,
                    acceptLink: acceptInviteUrl,
                    rejectLink: rejectInviteUrl,
                    unsubscribe: updateProfileUrl
                });

            var mailOptions = {
                from: 'contact@fgo.io',
                to: player.email,
                subject: userLanguage == 'bg' ? 'Покана за мач' : 'Game invitation',
                text: mailTextContent,
                html: $.html()
            };

            // send mail with defined transport object
            sendWithRetry(mailOptions, function(err, result){
                if(err){
                    console.log(err);
                }
                done(err, result);
            });
        });

    },

    sendMassMail: function(data, done){
        var mails = [];
        mails.push(data.emails);

        var sender = entities.decode(data.sender);
        var htmlMessage = null;
        if(data.htmlMessage) {
            htmlMessage = entities.decode(data.htmlMessage);
            htmlMessage = htmlMessage.replace(/(?:\r\n|\r|\n)/g, '<br />');
        }

        if(data.oneByOne) {
            mails = data.emails.split(",");
        }

        async.forEachSeries(mails, function(mail, callback){
            mail = mail.replace(/^\s+|\s+$/g, '');
            var mailOptions = {
                from: sender || 'Football Game Oranizer <contact@fgo.io>', // sender address
                overridenSender: sender || 'Football Game Oranizer <contact@fgo.io>', // sender address
                to: mail, // list of receivers
                subject: data.subject,
                text: data.message,
                html: htmlMessage
            };

            // send mail with defined transport object
            sendWithRetry(mailOptions, function(err, result){
                if(err){
                    console.log(err.message || err);
                    callback(err.message || err, result);
                } else
                {
                    callback(null, result);
                }
            });
        }, function(err, other){
            done(err, other);
        });
    }
};