/*global db, utilities*/
'use strict';
var _ = require('lodash'),
    async = require('async'),
    InviteService = global.rootRequire('/services/InviteService'),
    rand = require("generate-key"),
    google = require('googleapis'),
    ok = require('okay'),
    CommentService = global.rootRequire('/services/CommentService'),
    InviteService = global.rootRequire('/services/InviteService'),
    slug = require('slug'),
    multiparty = require('multiparty'),
    FileUtility = global.rootRequire('./FileUtility');

var constants = {
    UserRole: {
        Admin: 'Admin',
        Creator: 'Creator',
        User: 'User',
        Invited: 'Invited'
    }
};

exports = module.exports = {
    Constants: constants,
    createGroup: function createGroup(ownerId, name, description, done) {
        var self = this;

        var group = new db.Group();

        group.name = name;
        group.description = description;
        group.createdBy = ownerId;
        group.admin_users.push(ownerId);
        group.urlName = slug(name);

        var baseName = group.urlName.toLowerCase();
        var index = 0;

        var trySave = function(savedCallback){
            group.save(function(err, saved){
                if(err && err.code === 11000)
                {
                    if(err.message.indexOf('.groups.$urlName') > -1)                    {
                        group.urlName = baseName + '-' + ++index;
                        trySave(savedCallback);
                    }
                    else {
                        savedCallback(err, saved);
                    }
                }
                else savedCallback(err, saved);
            });
        };

        trySave(ok(done, function(savedGroup){
            self.generateInvitationCode(savedGroup, done);
        }));
    },

    // TODO: Add notification - a new user has joined.
    join: function join(groupId, playerId, done) {
        async.waterfall([
            function getGroup(callback) {
                db.Group.findOne({_id: groupId}).populate('users').exec(callback);
            },
            function join(group, callback) {
                if (!group) {
                    return callback('Group not found');
                }

                group.users = group.users || [];
                var alreadyIn = _.some(group.users, {_id: playerId});
                if (!alreadyIn) {
                    group.users.push(playerId);
                    return group.save(callback);
                } else {
                    return callback(null, group);
                }
            }
        ], function (err, group) {
            return done(err, group);
        });
    },

    // TODO: Add a notification - a user has left.
    leave: function leave(groupId, playerId, done) {
        async.waterfall([
            function getGroup(callback) {
                db.Group.findOne({_id: groupId}).populate('users').exec(callback);
            },
            function join(group, callback) {
                if(!group) {
                    return callback('Group not found');
                }

                group.users = group.users || [];
                var alreadyIn = _.some(group.users, { _id: playerId } );
                if(alreadyIn) {
                    group.users = _.remove(group.users, { _id: playerId });
                    group.users.remove(playerId);
                    return group.save(callback);
                } else {
                    return callback();
                }
            }], function (err) {
            return done(err);
        });
    },

    inviteAction: function inviteAction(groupId, inviteId, status, done) {
        var group,
            invite;

        async.waterfall([
            function getGroup(callback) {
                db.Group.findOne({_id: groupId}).populate('users admin_users createdBy').exec(callback);
            },
            function getInvite(_group, callback) {
                if (!_group) {
                    return callback('Group not found');
                }

                // store in the global scope variable
                group = _group;

                db.Invite.findOne({_id: inviteId}).populate('player').exec(callback);
            },
            function updateInviteStatus(_invite, callback) {
                if (!_invite) {
                    return callback('Invalid invitation.');
                }

                // store in the global scope variable
                invite = _invite;

                if (invite.status !== InviteService.Constants.Status.Active) {
                    return callback('Invite status is invalid: ' + invite.status);
                }

                invite.status = status;
                return invite.save(callback);
            },
            function addToGroup(saved, count, callback) {
                if (status === InviteService.Constants.Status.Accepted) {
                    group.users = group.users || [];
                    group.users.push(invite.player._id);

                    return group.save(callback);
                } else {
                    return callback();
                }
            }
        ], function (err) {
            return done(err);
        });
    },

    createGroupInvite: function createGroupInvite(groupId, userId, invitation_code, done) {
        var group,
            invite,
            self = this;

        async.waterfall([
            function getGroup(callback) {
                self.getByUrlNameOrId(groupId, callback);
            },
            function getInvite(_group, callback) {
                if (!_group) {
                    return callback('Group not found');
                }

                // store in the global scope variable
                group = _group;

                if (group.invitation_code !== invitation_code)
                {
                    return callback('INVALID INVITATION CODE');
                } else
                if(self.getUserRole(userId, group)){
                    return callback('USER ALREADY PART OF THE GROUP');
                } else {
                    db.Invite.findOne({group: groupId, player: userId}).populate('player').exec(callback);
                }
            },
            function updateInviteStatus(_invite, callback) {
                if (_invite) {
                    invite = _invite;
                    invite.status = InviteService.Constants.Status.Active;
                    invite.save(callback);
                }
                else
                {
                    InviteService.createGroupInvite(null, userId, groupId, callback)
                }
            }
        ], function (err) {
            return done(err);
        });
    },

    getByUrlNameOrId: function getByUrlNameOrId(search, done) {
        var self = this;
        self.getByUrlName(search, function(err, group) {
            if (group){
                done(err, group);
            }
            else self.getById(search, done);
        });
    },

    getByUrlName: function getByUrlName(urlName, done) {
        db.Group.findOne({urlName: urlName}).populate('users admin_users createdBy').exec(ok(done, function (group) {
            if(group) {
                db.Invite.find({
                    group: group.id,
                    status: InviteService.Constants.Status.Active
                }).populate('player').exec(ok(done, function (invites) {
                    group._doc.invitedUsers = _.map(invites, function (invite) {
                        return invite.player.id;
                    });

                    done(null, group);
                }));
            }
            else{
                done(null, group);
            }
        }));
    },

    getById: function getById(groupId, done) {
        db.Group.findOne({_id: groupId}).populate('users admin_users createdBy').exec(ok(done, function (group) {
            db.Invite.find({group: groupId, status: InviteService.Constants.Status.Active}).populate('player').exec(ok(done, function (invites) {
                group._doc.invitedUsers = _.map(invites, function (invite) {
                    return invite.player.id;
                });

                done(null, group);
            }));
        }));
    },

    deleteById: function deleteById(groupId, done) {
        async.parallel([
            function(callback){
                InviteService.deleteByGroup(groupId, callback);
            },
            function(callback){
                CommentService.deleteByGroup(groupId, callback);
            }
        ], ok(done, function(){
            db.Group.findOne({_id: groupId}).exec(ok(done, function (group) {
                group.remove(done);
            }));
        }));
    },

    getAll: function getAll(userId, done) {
        db.Group.find({
                $or: [
                    {
                        users: userId
                    },
                    {
                        admin_users: userId
                    }
                ]
            }
        ).populate('users admin_users createdBy').exec(done);
    },

    generateInvitationCode: function generateInvitationCode(group, done){
        group.invitation_code = rand.generateKey(20);

        var params = {
            resource: {
                longUrl: global.config.site_address + '/invitations?groupId=' + group._id + '&activationCode=' + group.invitation_code
            }
        };

        var urlshortener = google.urlshortener({ version: 'v1', auth: global.config.auth.google.API_key });
        urlshortener.url.insert(params, function (err, response) {
            if (err) {
                console.log('Encountered error', err);
            } else {
                group.invitation_shorturl = response.id;
            }
            group.save(done);
        });
    },

    deleteInvitationCode: function deleteInvitationCode(group, done){
        if(group.invitation_code) {
            group.invitation_code = undefined;
        }
        if(group.invitation_shorturl) {
            group.invitation_code = undefined;
        }
        group.save(done);
    },

    addGames: function addGames(groups, done) {
        if (!groups) {
            done('no groups passed');
        }

        var now = new Date();

        var resultGroups = [];

        async.each(groups, function (group, callback) {
            db.Game.find({group: group.id}).exec(ok(callback, function (games) {

                group._doc.games = games;
                if (games && games.length > 0) {
                    var passedGames = _(games).filter(function (game) {
                        return now >= game.date_time;
                    }).value();
                    var newGames = _(games).filter(function (game) {
                        return now < game.date_time;
                    }).value();
                    if (passedGames.length > 0) {
                        group._doc.lastGame = _(passedGames).sortBy(function (game) {
                            return -game.date_time;
                        }).value()[0];
                    }
                    if (newGames.length > 0) {
                        group._doc.nextGame = _(newGames).sortBy(function (game) {
                            return game.date_time;
                        }).value()[0];
                    }
                }
                resultGroups.push(group);
                callback();
            }))
        }, function (err) {
            done(err, resultGroups);
        });
    },

    userCanAccess: function userCanAccess(userId, group) {
        if(this.getUserRole(userId, group)){
            return true;
        }
        else {
            return (group._doc && (group._doc.invitedUsers || []).indexOf(userId) > -1);
        }
    },

    getUserRole: function getUserRole(userId, group){
        if (_.some(group.admin_users || [], function (user) {
                return user.id == userId;
            })) {
            return constants.UserRole.Admin;
        }
        if(group.createdBy && (group.createdBy.id == userId))
        {
            return constants.UserRole.Creator;
        }
        if (_.some(group.users || [], function (user) {
                return user.id == userId;
            })) {
            return constants.UserRole.User;
        }
        return null;
    },

    inviteGroupForGame: function(groupId, game, ownerId, done) {
        async.waterfall([
            function getGroup(callback) {
                db.Group.findOne({_id: groupId}).populate('users admin_users createdBy').exec(callback);
            },
            function inviteAll(group, callback) {
                var allUsers = (group.users || []).concat(group.admin_users || []);
                async.each(allUsers, function(user, cb) {
                    // no need to invite the owner.
                    if(user._id.toString() === ownerId.toString()) {
                        return cb();
                    }

                    InviteService.createGameInvite(ownerId, user._id, game, cb);
                }, callback);
            }
        ], function final(err) {
            return done(err);
        });
    },

    uploadGroupImage: function uploadGroupImage(req, id, done) {
        var form = new multiparty.Form();
        form.parse(req, function (err, fielos, files) {
            if (files.file.length) {
                var file = files.file[0];
                db.Group.findOne({_id: id}, function (err, group) {
                    if (group) {

                        FileUtility.ensureFileSize(file, null, function(err, isOk) {
                            if(err) return done(err);

                            FileUtility.saveFileInGridFS(file, function(err, fileName) {
                                if(err) return done(err);

                                group.groupImage = fileName;
                                return group.save(done);
                            })
                        });
                    }
                });
            }
        });
    },

    getGroupImage: function getGroupImage(res, id) {
        db.Group.findOne({_id: id}, function (err, group) {
            if (group) {
                var readStream = global.gfs.createReadStream({
                    filename: group.groupImage
                }).on('error', function (err) {
                    console.log('cannot read image. error: ' + err);
                }).on('close', function() {
                    //return done();
                });

                return readStream.pipe(res);
            }
        });
    },

    incGroupGamesCount: function incGroupGamesCount(id, count, done) {
        db.Group.update({ _id: id }, {
            $inc: {
                gamesCount: count
            }
        }, done);
    },

    updateGroupGoals: function updateGroupGoals(groupId, done) {
        //db.Game.aggregate(([{$match: { group: db.Mongoose.Types.ObjectId(groupId),
        //    type: 'GoalScored'}}, { $group: { _id: null, sum: { $sum: "$value" } } }])).exec(callback);
        db.Game.aggregate([{$match: { group: db.Mongoose.Types.ObjectId(groupId)}},
            { $group: { _id: null, team1goals: { $sum: "$team1.goals" }, team2goals: { $sum: "$team2.goals" } } }])
            .exec(ok(done, function(result){
                var goalsCount = 0;
                if(result.length > 0){
                    goalsCount = (result[0].team1goals || 0) + (result[0].team2goals || 0);
                }
                db.Group.update({ _id: groupId }, { $set: { goalsCount: goalsCount }}, done);
            }));
    }
};