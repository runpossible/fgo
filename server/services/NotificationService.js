/* global db */

'use strict';

var _ = require('lodash'),
    async = require('async'),
    GroupService = global.rootRequire('/services/GroupService'),
    GameService = global.rootRequire('/services/GameService'),
    InviteService = global.rootRequire('/services/InviteService'),
    ok = require('okay');

var constants = {
    NotificationType: {
        GameCreated: 'GameCreated',
        GameTimeChanged: 'GameTimeChanged',
        GameLocationChanged: 'GameLocationChanged',
        GameTimeAndLocationChanged: 'GameTimeAndLocationChanged',
        GameDeleted: 'GameDeleted',
        GroupDeleted: 'GroupDeleted',
        GameCommentAdded: 'GameCommentAdded',
        GroupCommentAdded: 'GroupCommentAdded',
        UserCommentAdded: 'UserCommentAdded',
        InviteSend: 'InviteSend',
        InviteAccepted: 'InviteAccepted',
        InviteRejected: 'InviteRejected',
        RateGame: 'RateGame',
        RatePlayers: 'RatePlayers',
        AddedAsFriend: 'AddedAsFriend',
        RemovedAsFriend: 'RemovedAsFriend'
    },
    PopulateString: 'group game fromPlayer toPlayer'
};

exports = module.exports = {
    Constants: constants,

    addNotification: function addNotification(data, done) {
        var notification = new db.Notification();
        notification.group = data.group;
        notification.groupName = data.groupName;
        notification.game = data.game;
        notification.gameName = data.gameName;
        notification.toPlayer = data.toPlayer;
        notification.fromPlayer = data.fromPlayer;
        notification.type = data.type;
        notification.targetId = data.targetId;

        notification.save(function (err, saved) {
            if (err) {
                console.log(err);
            }
            if (done) {
                done(err, saved);
            }
        });
    },

    addGroupNotification: function(groupId, fromUser, type, game, done){
        var self = this;
        // notify all group members for the new game
        GroupService.getById(groupId, function (err, group) {
            if (err){
                return console.log(err);
            }
            var allGroupUsers = (group.users || []).concat(group.admin_users || []);
            var userIds = _.pluck(allGroupUsers, "id");
            userIds = _.uniq(userIds);
            async.each(userIds, function(userId, callback) {
                // no need to notify the owner.
                if((new db.Mongoose.Types.ObjectId(userId)).id === fromUser.id) {
                    return callback();
                }

                self.addNotification({
                    group: group,
                    groupName: group.name,
                    game: game,
                    gameName: game ? game.name : undefined,
                    toPlayer: userId,
                    fromPlayer: fromUser,
                    type: type,
                    targetId: game ? game._id : groupId
                }, callback);

            }, function(err) {
                if (err) {
                    console.log(err);
                }
                if (done) {
                    done(err);
                }
            });
        });
    },

    addGameNotification: function(game, fromUser, type, notifications, done){
        var self = this;
        async.parallel([
            function(callback){
                return callback(null, game.players || []);
            },
            function(callback){
                if((!game.group) || (!notifications.notifyGroupUsers)){
                    return callback(null, []);
                }else
                {
                    GroupService.getById(game.group, ok(function (group) {
                        var allGroupUsers = (group.users || []).concat(group.admin_users || []);
                        var userIds = _.pluck(allGroupUsers, "id");
                        callback(null, _.map(userIds, function(userId){ return new db.Mongoose.Types.ObjectId(userId);}));
                    }));
                }
            }
        ], function(err, result){
            if (err){
                return console.log(err);
            }
            var allUserIds = _.pluck((result[0] || []).concat(result[1] || []), "id");
            var userIds = _.uniq(allUserIds);

            var notifyFromUser = notifications.notifyFromUser;

            async.each(userIds, function (userId, callback) {
                // no need to notify the owner.
                if (!notifyFromUser && (new db.Mongoose.Types.ObjectId(userId).id === fromUser.id)) {
                    return callback();
                }

                self.addNotification({
                    group: game.group,
                    groupName: game.group ? game.group.name : undefined,
                    game: game,
                    gameName: game.name,
                    toPlayer: new db.Mongoose.Types.ObjectId(userId),
                    fromPlayer: fromUser,
                    type: type
                }, callback);

            }, function(err) {
                if (done) {
                    done(err, {});
                }
            });
        });
    },

    gameSaved: function gameSaved(wasNew, game) {
        if(wasNew && game.group) {
            this.addGroupNotification(game.group, game.createdBy, constants.NotificationType.GameCreated, game);
        }
    },

    // NOTE: In order remove callback to work you will need to call the .remove method of the entity but not query
    gameRemoved: function gameRemoved(game) {
        this.addGameNotification(game, game.createdBy, constants.NotificationType.GameDeleted,
            {notifyFromUser: true, notifyGroupUsers: true});
    },

    addedGameComment: function addedGameComment(comment){
        var self = this;
        GameService.getById(comment.game, function (err, game) {
            if (err){
                return console.log(err);
            }
            var users = (game.players || []);
            users.push(game.createdBy);
            var allUserIds = _.pluck(users, "id");
            var userIds = _.uniq(allUserIds);

            async.each(userIds, function (userId, callback) {
                // no need to notify the owner.
                if ((new db.Mongoose.Types.ObjectId(userId)).id === comment.createdBy.id) {
                    return callback();
                }

                self.addNotification({
                    group: game.group,
                    groupName: game.group ? game.group.name : undefined,
                    game: game,
                    gameName: game.name,
                    toPlayer: new db.Mongoose.Types.ObjectId(userId),
                    fromPlayer: comment.createdBy,
                    type: constants.NotificationType.GameCommentAdded,
                    targetId: comment._id
                }, callback);

            }, function(err){
                    if (err){
                        return console.log(err);
                    }
            });
        });
    },

    addedUserComment: function addedUserComment(comment){
        if(comment.user._id != comment.createdBy._id){
            this.addNotification({
                user: comment.user,
                toPlayer: comment.user,
                fromPlayer: comment.createdBy,
                type: constants.NotificationType.UserCommentAdded,
                targetId: comment._id
            });
        }
    },

    addedGroupComment: function addedGroupComment(comment){
        var self = this;
        GroupService.getById(comment.group, function (err, group) {
            if (err){
                return console.log(err);
            }
            var allGroupUsers = (group.users || []).concat(group.admin_users || []);
            allGroupUsers.push(group.createdBy);
            var userIds = _.pluck(allGroupUsers, "id");
            userIds = _.uniq(userIds);
            async.each(userIds, function(userId, callback) {
                // no need to notify the owner.
                if((new db.Mongoose.Types.ObjectId(userId)).id === comment.createdBy.id) {
                    return callback();
                }

                self.addNotification({
                    group: group,
                    groupName: group.name,
                    toPlayer: userId,
                    fromPlayer: comment.createdBy,
                    type: constants.NotificationType.GroupCommentAdded,
                    targetId: comment._id
                }, callback);

            });
        });
    },

    commentSaved: function commentSaved(wasNew, comment){
        if(wasNew){
            if(comment.group){
                this.addedGroupComment(comment);
            } else if(comment.game){
                this.addedGameComment(comment);
            } else if(comment.user){
                this.addedUserComment(comment);
            }
        }
    },

    // NOTE: In order remove callback to work you will need to call the .remove method of the entity but not query
    commentRemoved: function commentRemoved(comment){
    },

    groupSaved: function groupSaved(wasNew, group){
    },

    // NOTE: In order remove callback to work you will need to call the .remove method of the entity but not query
    groupRemoved: function groupRemoved(group){
        var self = this;
        var allGroupUsers = (group.users || []).concat(group.admin_users || []);
        allGroupUsers.push(group.createdBy);
        var userIds = _.pluck(allGroupUsers, "id");
        userIds = _.uniq(userIds);
        async.each(userIds, function(userId, callback) {
            // no need to notify the owner.
            self.addNotification({
                group: group,
                groupName: group.name,
                toPlayer: new db.Mongoose.Types.ObjectId(userId),
                type: constants.NotificationType.GroupDeleted,
            }, callback);

        });
    },

    inviteSaved: function inviteSaved(wasNew, invite) {
        var type = null;
        var self = this;
        if (wasNew) {
            type = constants.NotificationType.InviteSend;
        } else if (invite.status === InviteService.Constants.Status.Accepted){
            type = constants.NotificationType.InviteAccepted;
        } else if( invite.status === InviteService.Constants.Status.Rejected){
            type = constants.NotificationType.InviteRejected;
        }

        if(type){
            var fromPlayer = wasNew ? invite.createdBy : invite.player;
            var toPlayer = wasNew? invite.player : invite.createdBy;
        }

        setTimeout(function(){
            self.addNotification({
                group: invite.group,
                game: invite.game,
                toPlayer: toPlayer,
                fromPlayer: fromPlayer,
                type: type,
                targetId: invite._id
            });
        }, 100);
    },

    // NOTE: In order remove callback to work you will need to call the .remove method of the entity but not query
    inviteRemoved: function inviteRemoved(invite){
    },

    checkUser: function checkUser(user){
        var self = this;
        var lastCheck = (user.lastNotificationCheck || new Date(0));
        var now = new Date();
        var difference = now - lastCheck;
        if(difference > 5 * 60 * 1000)
        {
            var searchTime = new Date();
            // we are searching for one hour back as the games in most of the cases are 1 hour long
            searchTime.setHours(searchTime.getHours() - 1);

            db.Game.find({ date_time: { $lt: searchTime} }).exec(function (err, games){
                if(err){
                    return console.log(err);
                }
                var gameIds = _.map(games, function(g) { return g._id.toString(); });

                async.parallel([
                    function(callback){
                        db.PlayerRating.find({ game: { $in: gameIds}}).populate("game").select("game -_id").exec(callback);
                    },
                    function(callback){
                        db.Notification.find({ game: { $in: gameIds}, type: self.Constants.NotificationType.RatePlayers}).populate("game").select("game -_id").exec(callback);
                    }
                ], function(err, results){
                    if(err){
                        return console.log(err);
                    }

                    var allGameIds = _.pluck(games, "id");
                    var gamesWithRatingIds = _.uniq(_.pluck(results[0], "game.id"));
                    var gamesWithNotificationIds = _.uniq(_.pluck(results[1], "game.id"));
                    var gamesNeedingNotificationIds = _.difference(allGameIds, _.union(gamesWithRatingIds, gamesWithNotificationIds));

                    async.each(gamesNeedingNotificationIds, function(gameId, callback) {
                        var game = _.findLast(games, function(searched){
                            return searched.id == gameId;
                        })

                        self.addNotification({
                            game: game,
                            gameName: game ? game.name : undefined,
                            toPlayer: user,
                            fromPlayer: user,
                            type: self.Constants.NotificationType.RatePlayers,
                            targetId: game.id
                        }, callback);
                    }, function(err){
                        if(err){
                            return console.log(err);
                        }

                        user.lastNotificationCheck = now;
                        user.save();
                    });


                });
            });
        }
    }
};