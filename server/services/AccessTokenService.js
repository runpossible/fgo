/* global db */
'use strict';

var rand = require("generate-key"),
    async = require('async');

var AccessTokenService = function () {
};

AccessTokenService.getAccessToken = function getAccessToken(token, done) {
    return db.AccessToken.findOne({value: token}).then(function(dbToken) {
        return db.User.findOne({
				"_id": dbToken.user
			}).exec(function(err, user) {
                var pureToken = {
                    user: user,
                    value: dbToken.value,
                    updatedAt: dbToken.updatedAt
                };
                return done(err, pureToken);
            });
    });
};

AccessTokenService.createAccessToken = function createAccessToken(userId, callback) {
    async.waterfall([
        function removeCurrentUserAccessTokens(cb) {
            db.AccessToken.find({user: userId}).remove(cb);
        },
        function saveNewAccessToken(count, cb) {
            var token = rand.generateKey(20);

            var accToken = new db.AccessToken();
            accToken.user = userId;
            accToken.value = token;
            accToken.save(cb);
        }
    ], function final(err, accToken) {
        if(err) {
            return callback(err);
        }

        return callback(null, accToken.value);
    });
};

exports = module.exports = AccessTokenService;