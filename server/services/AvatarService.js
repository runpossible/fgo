/* global db */

'use strict';

var _ = require('lodash'),
    async = require('async'),
    google = require('googleapis'),
    plus = google.plus('v1'),
    md5 = require('md5'),
    ok = require('okay');

var AvatarService = function () {
};

AvatarService.getGooglePhoto = function getGooglePhoto(userId, size, callback) {
    if (!size) {
        size = 80;
    }
    plus.people.get({userId: userId, auth: global.config.auth.google.API_key}, ok(callback, function (response) {
        if (response && response.image && response.image.url) {
            var url = response.image.url;
            callback(null, response.image.url.replace('sz=50', 'sz=' + size));
        }
        else {
            callback('There is no image for the current user');
        }
    }));
};

AvatarService.getFacebookPhoto = function getFacebookPhoto(userId, type, callback) {
    if (!type) {
        type = "normal";
    }

    // type can be "large", "normal", "small" or "square"
    callback(null, 'https://graph.facebook.com/' + userId + '/picture?type=' + type);
};

AvatarService.getGravatarPhoto = function getGravatarPhoto(email, size, callback) {
    if (!size) {
        size = "80";
    }
    var hash = md5(email.toLowerCase());
    callback(null, 'http://www.gravatar.com/avatar/' + hash + '?d=mm&s=' + size);
    // we can change default avatar by passing a url to the "d" query string parameter
};

AvatarService.getUserAvatar = function getUserAvatar(user, type, preferredAvatar, globalCallback) {
    type = type.toLowerCase();
    var size = 50;
    if (type == 'large') {
        size = 200;
    }
    else if (type == 'normal') {
        size = 100;
        if(config.cacheAvatar && user.avatarUrl && (!preferredAvatar || preferredAvatar == user.preferredAvatar)){
            return globalCallback(null, user.avatarUrl);
        }
    }
    else {
        type = 'small';
    }

    if (!user) {
        return globalCallback(null, 'http://www.gravatar.com/avatar/00?d=mm&s=' + size);
    }

    var doneCallback = function(err, url){
        if(config.cacheAvatar && type == 'normal'){
            db.User.update({_id: user._id}, {
                $set: {avatarUrl: url}
            }).exec(ok(globalCallback, function(){
                globalCallback(err, url);
            }));
        } else {
            globalCallback(err, url);
        }
    };

    async.waterfall([
        function getFacebook(callback) {
            if (user.facebook && user.facebook.id && (preferredAvatar == "1" || (!preferredAvatar && (user.preferredAvatar == 1 || !user.preferredAvatar)))) {
                return AvatarService.getFacebookPhoto(user.facebook.id, type, callback)
            }

            return callback(null, null);
        },
        function getGoogle(facebookUrl, callback) {
            if (facebookUrl) {
                return doneCallback(null, facebookUrl);
            }

            if (user.google && user.google.id && (preferredAvatar == "2" || (!preferredAvatar && (user.preferredAvatar == 2 || !user.preferredAvatar)))) {
                return AvatarService.getGooglePhoto(user.google.id, size, callback)
            }

            return callback(null, null);
        },
        //function getGravatar(googleUrl, callback) {
        //    if (googleUrl) {
        //        return doneCallback(null, googleUrl);
        //    }
        //
        //    return AvatarService.getGravatarPhoto(user.email, size, callback);
        //},
        function getDefaultPhoto(googleUrl, callback) {
            if (googleUrl) {
                return doneCallback(null, googleUrl);
            }

            return callback(null, config.site_address + '/assets/img/profile-default.png');
        }
    ], function (err, url) {
        doneCallback(err, url);
    });
};

exports = module.exports = AvatarService;