/* global db */

'use strict';

var _ = require('lodash'),
    async = require('async'),
    ok = require('okay'),
    db = global.db,
    NotificationService = global.rootRequire('/services/NotificationService'),
    GameService = global.rootRequire('/services/GameService'),
    MailService = global.rootRequire('/services/MailService'),
    AvatarService = global.rootRequire('/services/AvatarService'),
    randomstring = require("randomstring"),
    multiparty = require('multiparty'),
    common = global.rootRequire('/config/common'),
    FileUtility = global.rootRequire('./FileUtility');

var UserService = function() {
};

var adminEmails = ['v.trifonov@gmail.com', 'cypressx@gmail.com', 'asoulxnrg@gmail.com'];

UserService.Constants = {
    StrongFoot: {
        Left: 'STRONG FOOT LEFT',
        Right: 'STRONG FOOT RIGHT',
        Both: 'STRONG FOOT BOTH'
    }
};

UserService.getUserVenues = function getUserVenues(userId, globalCallback){
    async.waterfall([
        function getUserGroups(callback){
            // Yosif: should check for admin_users too
            db.Group.find({
                $or: [
                {
                    users: { $in: [userId]}
                },
                {
                    admin_users: { $in: [userId]}
                }]
            }).exec(callback);
        },
        function getUserGames(userGroups, callback){
            var userGroupIds = _.map(userGroups, function(g) { return g._id.toString(); });
            db.Game.find({ group: { $in: userGroupIds}}).populate('location').exec(callback);
        },
        function getResult(games, callback){
            var locations = _.uniq(_.pluck(_.sortBy(games, function (game) {
                return -game.createdOn;
            }), 'location'));
            callback(null, _(locations).filter(function(location) { return location}).value());
        }
    ], function (err, userVenues){
        globalCallback(err, userVenues);
    });
};

UserService.searchUsers = function searchUsers(email, name, callback) {
    if(email){
        email = email.toLowerCase();
    }
    var emailFilter = {
        email: email
    };
    var nameFilter = {
        name: {
            $regex: name
        }
    };

    if(email && name) {
        return db.User.find().and([emailFilter, nameFilter]).exec(callback);
    }

    if(email) {
        return db.User.find(emailFilter).exec(callback);
    }

    if(name) {
        return db.User.find(nameFilter).exec(callback);
    }

    return callback('Cannot search without email or name');
};

UserService.getAllUsers = function getAllUsers(email, callback) {
    if(adminEmails.indexOf(email.toLowerCase()) > -1){
        return db.User.find({}).exec(callback);
    }
    else {
        return callback('not authorized');
    }
};

UserService.getByEmail = function (email, callback) {
    if(email){
        email = email.toLowerCase();
    }
    db.User.findOne({ email: email}).exec(callback);
};

UserService.getAllErrors = function getAllErrors(email, callback) {
    if(adminEmails.indexOf(email.toLowerCase()) > -1){
        return db.Error.find({}).exec(callback);
    }
    else {
        return callback('not authorized');
    }
};

UserService.updateProfileTimezone = function updateProfileTimezone(userId, timezone, done) {
    db.User.findOne({ _id: userId }).exec(ok(done, function (user) {
        user.timezone = timezone;
        user.save(done);
    }));
};

UserService.updateProfile = function updateProfile(userId, data, done) {
    db.User.findOne({ _id: userId}).exec(ok(done, function (user) {
        user.name = data.name;
        if(data.preferredAvatar != user.preferredAvatar) {
            user.preferredAvatar = data.preferredAvatar;
        }
        if(data.avatarUrl){
            user.avatarUrl = data.avatarUrl;
        }
        user.preferredPosition = data.preferredPosition;
        user.birthDate = data.birthDate;
        user.city = data.city;
        user.height = data.height;
        user.weight = data.weight;
        user.strongFoot = data.strongFoot;
        user.countryId = data.countryId;
        user.public = data.public;
        user.shouldReceiveGameInviteMails = data.shouldReceiveGameInviteMails;

        if(data.currentPassword && data.newPassword){
            if(!data.newPassword || data.newPassword === '')
            {
                return done('MISSING PASSWORD');
            }else if (user.local && user.local.hashed_pwd && (!user.authenticateLocal(data.currentPassword))) {
                return done('WRONG PASSWORD');
            }
            else {
                var salt = db.User.createSalt();
                var hash = db.User.hashPwd(salt, data.newPassword);
                user.local = {
                    salt : salt,
                    hashed_pwd: hash
                };
            }
        } else if(data.newPassword && (!(user.local && user.local.hashed_pwd))){
            var salt = db.User.createSalt();
            var hash = db.User.hashPwd(salt, data.newPassword);
            user.local = {
                salt : salt,
                hashed_pwd: hash
            };
        }
        user.save(done);
    }));
};

UserService.updateProfileLanguage = function updateProfileLanguage(userId, language, done) {
    db.User.update({ _id: userId}, { $set: { language: language}}).exec(done);
};

UserService.getFriends = function getFriends(userId, done) {
    db.User.findOne({ _id: userId}).populate('friends').exec(ok(done, function (user) {
        var friends = _.sortBy(user.friends || [], function (friend) {
            return friend.name;
        });
        done(null, friends);
    }));
};

UserService.addFriends = function getFriends(currentUserId, userIds, done) {
    db.User.findOne({ _id: currentUserId}).populate('friends').exec(ok(done, function (user) {
        userIds = _.uniq(userIds);
        async.each(userIds, function(userId, callback){
            if(!_.some(user.friends, { 'id': userId })) {
                db.User.update({_id: currentUserId}, {
                    $addToSet: {friends: userId}
                }).exec(function(err){
                    if(!err){
                        NotificationService.addNotification(
                            {
                                toPlayer: userId,
                                fromPlayer: currentUserId,
                                type: NotificationService.Constants.NotificationType.AddedAsFriend
                            }, callback);
                    } else {
                        return callback(err);
                    }
                });
            } else {
                return callback();
            }
        },ok(done, function(){
            UserService.getFriends(currentUserId, done);
        }));
    }));
};

UserService.removeFriends = function getFriends(currentUserId, userIds, done) {
    db.User.findOne({ _id: currentUserId}).populate('friends').exec(ok(done, function (user) {
        userIds = _.uniq(userIds);
        async.each(userIds, function(userId, callback){
            if(_.some(user.friends, { 'id': userId })) {
                db.User.update({_id: currentUserId}, {
                    $pull: {friends: userId}
                }).exec(function(err){
                    if(!err){
                        NotificationService.addNotification(
                            {
                                toPlayer: userId,
                                fromPlayer: currentUserId,
                                type: NotificationService.Constants.NotificationType.RemovedAsFriend
                            }, callback);
                    } else {
                        return callback(err);
                    }
                });
            } else {
                callback();
            }
        },ok(done, function(){
            UserService.getFriends(currentUserId, done);
        }));
    }));
};

UserService.getPotentialFriendsFromFB = function getPotentialFriendsFromFB(fbFriends, callback){
    var fbFriendIs = _.pluck(fbFriends, "id");
    db.User.find({ "facebook.id" : { $in: [fbFriendIs]}}).exec(ok(callback, function(existingUsers){
        var existingFBUserIds = _.pluck(existingUsers, "facebook.id");
        var nonExisting = _.filter(fbFriends, function(friend){
            return !_.any(existingFBUserIds, function(id){
                return friend.id == id;
            })
        });
        var mapped = _.map(nonExisting, function(item){
            var result = {
                facebookId: item.id,
                name: item.name
            };
            if(item.picture && item.picture.data && item.picture.data.url){
                result.avatar_url = item.picture.data.url
            }
            return result;
        });
        callback(null, mapped);
    }));
};

UserService.getPotentialFriendsFromGoogle = function getPotentialFriendsFromGoogle(googleFriends, callback){
    var googleFriendIs = _.pluck(googleFriends, "id");
    db.User.find({ "facebook.id" : { $in: [googleFriendIs]}}).exec(ok(callback, function(existingUsers){
        var existingGoogleUserIds = _.pluck(existingUsers, "google.id");
        var nonExisting = _.filter(googleFriends, function(friend){
            return !_.any(existingGoogleUserIds, function(id){
                return friend.id == id;
            })
        });
        var mapped = _.map(nonExisting, function(item){
            var result = {
                googleId: item.id,
                googleUrl: item.url,
                name: item.displayName,
            };
            if(item.image && item.image.url){
                result.avatar_url = item.image.url
            }
            return result;
        });
        callback(null, mapped);
    }));
};

UserService.fillUserStats = function fillUserStats(user, callback) {
        this.getUserStats(user._id, function(err, userStats) {
            if (!err) {
                user.goals = userStats.goals;
                user.rating = userStats.rating;
                user.games = userStats.games;
                user.manOfTheMatch = userStats.manOfTheMatch;
            }
            callback(err);
        });
};

UserService.getUserStats = function getUserStats(userId, globalCallback){
    async.parallel([
        function(callback){
            db.GameFact.aggregate([{$match: { player: db.Mongoose.Types.ObjectId(userId), type: GameService.Constants.GameFactType.GoalScored}}, { $group: {_id: {player: "$player"}, goals: { $sum: "$value"}}}]).exec(callback);
        },
        function(callback){
            db.PlayerRating.aggregate([{$match: { player: db.Mongoose.Types.ObjectId(userId)}}, { $group: {_id: {player: "$player"}, rating: { $avg: "$rating"}}}]).exec(callback);
        },
        function(callback){
            db.Game.find({players:db.Mongoose.Types.ObjectId(userId), date_time: { $lt: new Date() }}).count(callback);
        },
        function(callback){
            db.Game.find({ men_of_the_match: db.Mongoose.Types.ObjectId(userId) }).count(callback);
        }
    ], function(err, result) {
        if (err){
            globalCallback(err);
        }
        var goals = 0,
            games = 0,
            manOfTheMatch = 0,
            rating = undefined;
        if(result[0].length > 0){
            goals = result[0][0].goals;
        }
        if(result[1].length > 0){
            rating = Number((result[1][0].rating).toFixed(1));
        }
        if(result[2]){
            games = Number(result[2]);
        }
        if(result[3]){
            manOfTheMatch = Number(result[3]);
        }
        globalCallback(err, {
            goals: goals,
            rating: rating,
            games: games,
            manOfTheMatch: manOfTheMatch
        });
    });
};

UserService.addUserStats = function addUserStats(users, globalCallback){
    var userIds = _.map(users, function(user){
        return db.Mongoose.Types.ObjectId(user.id);
    });
    async.parallel([
        function(callback){
            db.GameFact.aggregate([{$match: { player: { $in: userIds}, type: GameService.Constants.GameFactType.GoalScored}}, { $group: {_id: {player: "$player"}, goals: { $sum: "$value"}}}]).exec(callback);
        },
        function(callback){
            db.PlayerRating.aggregate([{$match: { player: { $in: userIds}}}, { $group: {_id: {player: "$player"}, rating: { $avg: "$rating"}}}]).exec(callback);
        }
    ], function(err, results) {
        var goalsScored = results[0];
        var playerRatings = results[1];

        async.each(users, function(user, callback){
            var goalStat = _.findLast(goalsScored, function(aggregated){
                return aggregated._id.player.equals(user._id);
            });
            var playerRating = _.findLast(playerRatings, function(aggregated){
                return aggregated._id.player.equals(user._id);
            });
            if(goalStat){
                user._doc.goals = goalStat.goals;
            }
            if(playerRating){
                user._doc.rating = playerRating.rating;
            }
            callback();
        }, ok(function(result) {
            globalCallback(null, users);
        }));
    });
};

UserService.deletePasswordRequests = function(user, done){
    db.PasswordReset.remove({ user: user, used: false}).exec(done);
};

UserService.requestPasswordReset = function(userEmail, done) {
    var self = this;
    this.getByEmail(userEmail, ok(done, function(user){
        if(!user){
            return done();
        }
        if(!user.local || !user.local.hashed_pwd){
            return done('NO LOCAL USER');
        }

        self.deletePasswordRequests(user, ok(done, function(){
            var passwordReset = new db.PasswordReset();
            passwordReset.user = user;
            passwordReset.requestedOn = new Date();
            passwordReset.used = false;
            passwordReset.secret = randomstring.generate({
                length: 64,
                charset: 'alphanumeric'
            }).toUpperCase();

            MailService.sendPasswordReset(passwordReset, ok(done, function(){
                passwordReset.save(ok(done, function(passwordReset){
                    user.local.passwordReset = passwordReset;
                    user.save(ok(done, function(user){
                        done();
                    }))
                }));
            }));
        }));
    }));
};

UserService.setPassword = function(secret, password, done){
    db.PasswordReset.findOne({ secret: secret}).populate('user').exec(ok(done, function(passwordReset){
        if(!passwordReset || !passwordReset.user){
            return done();
        }
        if(passwordReset.used){
            return done('Code already used');
        }

        var salt = db.User.createSalt();
        var hash = db.User.hashPwd(salt, password);
        passwordReset.user.local = {
            salt : salt,
            hashed_pwd: hash
        };
        passwordReset.user.save(ok(done, function(user){
            passwordReset.used = true;
            passwordReset.save(ok(done, function(pr){
                return done(null, {
                    success: true
                });
            }));
        }));
    }));
};

UserService.activate = function(code, done){
    db.User.findOne({ 'local.activationCode': code}).exec(ok(done, function(user){
        if(!user){
            return done();
        }

        user.local.activationCode = undefined;

        user.save(ok(done, function(user){
            done(null, {success: true});
        }));
    }));
};

UserService.contactUs = function(data, user, done) {
    MailService.sendContactUsMail(data, user, function(err) {
        return done(err, {success: true});
    });
};

UserService.getUserProfile = function(userId, done){
    var self = this;
    db.User.findOne({ '_id': userId}).exec(ok(done, function(user){
        if(!user){
            return done();
        }
        var sanitizedUser = common.sanitizeUser(user);
        self.fillUserStats(sanitizedUser, ok(done, function(){
            done(null, sanitizedUser);
        }));
    }));
};

UserService.getFacebookPhoto = function(userId, done){
    var self = this;
    db.User.findOne({ '_id': userId}).exec(ok(done, function(user){
        if(!user){
            return done();
        }
        if(user.facebook && user.facebook.id){
            AvatarService.getFacebookPhoto(user.facebook.id, 'large', done)
        } else {
            done('User does not have facebook profile');
        }
    }));
};

UserService.uploadUserImage = function uploadUserImage(req, id, done) {
    var form = new multiparty.Form();
    form.parse(req, function (err, fielos, files) {
        if (files.file.length) {
            var file = files.file[0];
            db.User.findOne({_id: id}, function (err, user) {
                if (user) {

                    FileUtility.ensureFileSize(file, null, function(err, isOk) {
                        if(err) return done(err);

                        FileUtility.saveFileInGridFS(file, function(err, fileName) {
                            if(err) return done(err);

                            user.userImage = fileName;
                            user.avatarUrl = '/api/v1/user/' + user._id + '/image/' + user.userImage + '/image.png';
                            return user.save(done);
                        })
                    });
                }
            });
        }
    });
};

UserService.getUserImage = function getUserImage(res, id) {
    db.User.findOne({_id: id}, function (err, user) {
        if (user) {
            var readStream = global.gfs.createReadStream({
                filename: user.userImage
            }).on('error', function (err) {
                console.log('cannot read image. error: ' + err);
            }).on('close', function() {
                //return done();
            });

            return readStream.pipe(res);
        }
    });
};


exports = module.exports = UserService;