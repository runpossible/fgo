/*global db, utilities*/
'use strict';

var async = require('async'),
    _ = require('lodash'),
    ok = require('okay');

var populateString = 'group game user createdBy';

exports = module.exports = {
    getById: function getById(commentId, done) {
        db.Comment.findOne({_id: commentId}).populate(populateString).exec(done);
    },

    addGroupComment: function addGroupComment(groupId, userId, text, done) {
        var comment = new db.Comment();

        comment.text = text;
        comment.createdBy = userId;
        comment.group = groupId;

        comment.save(done);
    },

    addGameComment: function addGameComment(gameId, userId, text, done) {
        var comment = new db.Comment();

        comment.text = text;
        comment.createdBy = userId;
        comment.game = gameId;

        comment.save(done);
    },

    addUserComment: function addUserComment(userId, createdByUserId, text, done) {
        var comment = new db.Comment();

        comment.text = text;
        comment.createdBy = createdByUserId;
        comment.user = userId;

        comment.save(done);
    },

    getByGroupId: function getByGroupId(groupId, done) {
        db.Comment.find({group: groupId}).populate(populateString).exec(done);
    },

    getByUserId: function getByUserId(userId, done) {
        db.Comment.find({user: userId}).populate(populateString).exec(done);
    },

    getByGameId: function getByGameId(gameId, done) {
        db.Comment.find({game: gameId}).populate(populateString).exec(done);
    },

    deleteByGame: function(gameId, done) {
        db.Comment.find({ game: gameId }).exec(ok(function(comments){
            async.each(comments, function(comment, callback){ comment.remove(callback);}, done);
        }));
    },

    deleteByUser: function(userId, done) {
        db.Comment.find({ user: userId }).exec(ok(function(comments){
            async.each(comments, function(comment, callback){ comment.remove(callback);}, done);
        }));
    },

    deleteByGroup: function(groupId, done) {
        db.Comment.find({ group: groupId }).exec(ok(function(comments){
            async.each(comments, function(comment, callback){ comment.remove(callback);}, done);
        }));
    }
};