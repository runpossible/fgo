/*global db, utilities*/
'use strict';

var async = require('async'),
    GroupService = global.rootRequire('/services/GroupService'),
    _ = require('lodash'),
    ok = require('okay'),
    rand = require("generate-key"),
    InviteService = global.rootRequire('/services/InviteService'),
    CommentService = global.rootRequire('/services/CommentService'),
    slug = require('slug'),
    google = require('googleapis'),
    uuid = require('node-uuid');

var constants = {
    GameFactType: {
        GoalScored: 'GoalScored'
    }
};

function combineDateTime(date, time) {
    var date = new Date(date.getFullYear(), date.getMonth(), date.getDate(), time.getHours(), time.getMinutes(), 0);
    return date;
};

var updateGameCity = function(game, location, done){
    // update the city in game
    db.Location.findOne({ _id: location}).exec(ok(done, function(location){
        game.city = location.city;
        game.locationName = location.name;
        game.save(done);
    }));
};

exports = module.exports = {
    Constants: constants,

    createGame: function createGame(owner, game, done) {
        var self = this;

        var getBaseUrlName = function (currentGame) {
            var urlName = '';
            if (game.group) {
                urlName = game.group.urlName || game.group.name;
            } else {
                urlName = owner.name;
            }
            urlName += " " + currentGame.date_time.getFullYear() + "-" + currentGame.date_time.getMonth() + "-" + currentGame.date_time.getDate();
            urlName += " " + currentGame.name;
            return slug(urlName).toLowerCase();
        };

        var newGame = new db.Game();

        newGame.name = game.name;
        newGame.date_time = combineDateTime(new Date(game.date), new Date(game.time));
        newGame.group = game.groupId ? game.groupId : null; // if it comes as empty string, make it null.
        newGame.players.push(owner.id);
        newGame.public = game.public;
        newGame.createdBy = owner.id;
        newGame.numberOfPlayers = game.numberOfPlayers;
        newGame.location = game.location.id;
        newGame.urlName = getBaseUrlName(newGame);
        newGame.price = game.price;
        newGame.team1 = game.team1;
        newGame.team2 = game.team2;

        var now = new Date();
        if(newGame.date_time < now) {
            return done('Cannot create a game in the past.');
        }

        var baseName = newGame.urlName;
        var index = 0;

        var trySave = function (savedCallback) {
            newGame.save(function (err, saved) {
                if (err && err.code === 11000) {
                    if (err.message.indexOf('.games.$urlName') > -1) {
                        newGame.urlName = baseName + '-' + ++index;
                        trySave(savedCallback);
                    }
                    else {
                        savedCallback(err, saved);
                    }
                }
                else savedCallback(err, saved);
            });
        };

        trySave(function (err, savedGame) {
            if (err) {
                return done(err);
            }

            self.generateInvitationCode(savedGame, ok(done, function(gameWithInvitaionCode){
                // If we create a game for a group, invite everyone from the group.
                if (game.groupId) {
                    return GroupService.inviteGroupForGame(game.groupId, savedGame, owner.id, function (err) {
                        return GroupService.incGroupGamesCount(game.groupId, 1, function() {
                            updateGameCity(savedGame, game.location.id, done);
                        });
                    });
                }

                updateGameCity(savedGame, game.location.id, done);
            }));
        });
    },

    updateGame: function updateGame(gameId, body, done) {
        var updateGame = function (oldGame, body, location) {
            oldGame.name = body.name;
            oldGame.date_time = combineDateTime(new Date(body.date), new Date(body.time));
            oldGame.public = body.public;
            oldGame.price = body.price || oldGame.price;
            oldGame.numberOfPlayers = body.numberOfPlayers;

            oldGame.location = location.id;

            updateGameCity(oldGame, location.id, done);
        };

        db.Game.findOne({_id: gameId}).populate('group location').exec(ok(done, function (oldGame) {
            if (oldGame.group && oldGame.group.id != body.groupId) {
                done('wrong group passed');
            }
            updateGame(oldGame, body, body.location);
        }));
    },

    deleteGame: function deleteGame(gameId, done) {
        async.parallel([
            function (callback) {
                InviteService.deleteByGame(gameId, callback);
            },
            function (callback) {
                CommentService.deleteByGame(gameId, callback);
            }
        ], ok(done, function () {
            db.Game.findOne({_id: gameId}).populate('group').exec(ok(done, function (game) {
                // decrease group games
                if(game.group) {
                    return GroupService.incGroupGamesCount(game.group._id, -1, function() {
                        return game.remove(done);
                    });
                }
                return game.remove(done);
            }));
        }));
    },

    getById: function getById(gameId, done) {
        db.Game.findOne({_id: gameId}).populate('players group').exec(done);
    },

    addPlayer: function addPlayer(gameId, playerId, done) {
        db.Game.update({_id: gameId}, {
            $addToSet: {players: playerId}
        }).exec(done);
    },

    /**
     * Remove the player from the game
     *  If the player was invited, set the invitation as Rejected
     *  If the player wasn't invited - create new invitation, with rejected status
     *  Remove the player from the list of players for the Game.
     * @param gameId
     * @param playerId
     * @param done
     */
    removePlayer: function removePlayer(gameId, playerId, done) {
        async.waterfall([
            function getInvite(callback) {
                db.Invite.findOne({
                    $and: [
                        {
                            player: playerId
                        },
                        {
                            game: gameId
                        }
                    ]
                }).exec(callback);
            },

            function setInviteStatusAsRejected(invite, callback) {
                if(invite) {
                    // there's an existing invite, update the status.
                    invite.status = InviteService.Constants.Status.Rejected;
                    return invite.save(function(err) {
                        return callback(err);
                    });
                } else {
                    // Create a new invite, rejected
                    invite = new db.Invite();
                    invite.game = gameId;
                    invite.status = InviteService.Constants.Status.Rejected;
                    invite.player = playerId;
                    //invite.createdBy = 'system';
                    invite.mailSent = true;
                    return invite.save(function(err) {
                        return callback(err);
                    });
                }
            },

            function removePlayer(callback) {
                db.Game.update(
                    {
                        _id: gameId
                    },
                    {
                        $pull: { players: playerId }
                    }).exec(callback);
            }
        ], function end(err) {
            return done(err);
        });
    },

    /**
     * Checks if the User can access the game
     * - If the game is Public everyone can access
     * - If the game is Private and the user is part of the Group - can access
     * - If the game is Private, but the user is invited - can access
     * Executes callback(err, game, isAdmin, isParticipant)
     * @param userId
     * @param gameId
     * @param done
     */
    checkAccess: function checkAccess(userId, gameId, done) {
        this.getAll(userId, true, ok(done, function (allUserGames) {
            var items = _(allUserGames).filter(function (game) {
                return game.urlName == gameId || game._id == gameId;
            }).value();

            if (items.length == 0) {
                // no user games, found. If the game is a public one - it should be accessible.
                var isObjectId = gameId.match(/^[0-9a-fA-F]{24}$/);
                var query = [ { urlName: gameId }];
                if(isObjectId) {
                    query.push({ _id: gameId });
                }

                return db.Game.find({
                    $or: query
                }).populate('players group location createdBy').exec(ok(done, function(game) {
                    // if the game is public, everyone can access it.
                    if (game && game.length > 0 && game[0].public) {
                        return done(null, game[0], false, false);
                    }

                    // If the game is private, but the user has an invitation, it's accessible
                    if(game && game.length > 0) {
                        var g = game[0];
                        return InviteService.isInvited(userId, g._id, function (isInvited) {
                            if (isInvited) {
                                return done(null, g, false, false);
                            }

                            return done(null, null);
                        });
                    } else {
                        // the game doesn't exist
                        return done(null, null);
                    }
                }));
            }

            var playerIds = _.map(items[0].players, function (player) {
                return player.id;
            });

            return done(null, items[0],
                (items[0].group && items[0].group.admin_users && items[0].group.admin_users.indexOf(userId) > -1) || (items[0].createdBy && items[0].createdBy.id == userId),
                (playerIds.indexOf(userId) > -1));
        }));
    },

    getAll: function getAll(userId, populateDepencencies, globalCallback) {
        // getting all the games created by the user, with the user as a player inside or the games for all the groups
        // in which the user participates
        var games = [],
            pendingInvitationGames = [];

        async.waterfall([
            function getUserGroups(callback) {
                GroupService.getAll(userId, callback);
            },
            function getUserGames(groups, callback) {
                var groupIds = _(groups).pluck("id").value();

                var populateString = populateDepencencies ? 'players group location createdBy' : '';

                db.Game.find({
                        $or: [
                            {
                                players: userId
                            },
                            {
                                createdBy: userId
                            },
                            {
                                group: {$in: groupIds}
                            }
                        ]
                    }
                ).populate(populateString).sort('-date_time').exec(callback);
            },
            function persistGames(userGames, callback) {
                games = userGames;
                callback(null);
            },
            function getGamesInvitedTo(callback) {
                InviteService.findAllPendingGameInvitations(userId, function(err, pendingInvites) {
                    if(err) {
                        return callback(null, null);
                    }

                    if(pendingInvites && pendingInvites.length > 0) {
                        // some invites are for groups, not for games -> skip these.
                        pendingInvites = _.filter(pendingInvites, function(pi) {
                            return pi.game;
                        });

                        pendingInvites = _.filter(pendingInvites, function(pi) {
                            var alreadyLoaded = _.some(games, function(g) {
                                return g._id.toString() === pi.game.toString();;
                            });
                            return !alreadyLoaded;
                        });

                        return callback(null, pendingInvites);
                    } else {
                        return callback(null, null);
                    }
                });
            },
            function loadPendingInviteGames(pendingInvites, callback) {
                if(pendingInvites && pendingInvites.length > 0) {
                    var populateString = populateDepencencies ? 'players group location createdBy' : '';
                    var gameIds = _.filter(pendingInvites, function(p) {
                        return p.game;
                    });
                    gameIds = _.pluck(gameIds, 'game');

                    db.Game.find({
                            _id: {
                                $in: gameIds
                            }
                        }
                    ).populate(populateString).sort('-date_time').exec(function(err, newGames) {
                            pendingInvitationGames = newGames || [];
                            return callback(null);
                        });
                } else {
                    return callback(null);
                }
            }
        ], function (err) {
            var allGames = games.concat(pendingInvitationGames);
            allGames = _.sortBy(allGames, 'date_time');
            allGames.reverse(); // _.orderBy is not supported, cannot pass the order inside.
            globalCallback(err, allGames);
        });
    },

    getAllUpcomingPublicGames: function getAllUpcomingPublicGames(done) {
        var now = new Date();
        db.Game.find({
           $and: [
               {
                   date_time: { $gte: now }
               },
               {
                   'public': true
               }
           ]
        }).populate('players group location createdBy').sort('-date_time').exec(done);
    },

    getLastUserGame: function getLastUserGame(userId, globalCallback) {
        // getting all the games created by the user, with the user as a player inside or the games for all the groups
        // in which the user participates
        async.waterfall([
            function getUserGroups(callback) {
                GroupService.getAll(userId, callback);
            },
            function getUserGames(groups, callback) {
                var groupIds = _(groups).pluck("id").value();
                db.Game.find({
                        $or: [
                            {
                                players: userId
                            },
                            {
                                createdBy: userId
                            },
                            {
                                group: {$in: groupIds}
                            }
                        ]
                    }
                ).populate('players group location createdBy').sort('-date_time').limit(1).exec(callback);
            }
        ], function (err, games) {
            globalCallback(err, games.length > 0 ? games[0] : null);
        });
    },

    generateInvitationCode: function generateInvitationCode(game, done) {
        game.invitation_code = rand.generateKey(20);

        var params = {
            resource: {
                longUrl: global.config.site_address + '/invitations?gameId=' + game._id + '&activationCode=' + game.invitation_code
            }
        };

        var urlshortener = google.urlshortener({version: 'v1', auth: global.config.auth.google.API_key});
        urlshortener.url.insert(params, function (err, response) {
            if (err) {
                console.log('Encountered error', err);
            } else {
                game.invitation_shorturl = response.id;
            }
            game.save(done);
        });
    },

    deleteInvitationCode: function deleteInvitationCode(game, done) {
        if (game.invitation_code) {
            game.invitation_code = undefined;
        }
        if (game.invitation_shorturl) {
            game.invitation_shorturl = undefined;
        }
        game.save(done);
    },

    createGameInvite: function createGameInvite(gameId, userId, invitation_code, done) {
        var game,
            invite,
            self = this;

        async.waterfall([
            function getGame(callback) {
                db.Game.findOne({_id: gameId}).populate('players').exec(callback);
            },
            function getInvite(_game, callback) {
                if (!_game) {
                    return callback('Game not found');
                }

                // store in the global scope variable
                game = _game;

                if (game.invitation_code !== invitation_code) {
                    return callback('INVALID INVITATION CODE');
                } else if (_.some(game.players || [], function (user) {
                        return user.id == userId;
                    })) {
                    return callback('USER ALREADY PART OF THE GAME');
                } else {
                    db.Invite.findOne({game: gameId, player: userId}).populate('player').exec(callback);
                }
            },
            function updateInviteStatus(_invite, callback) {
                if (_invite) {
                    invite = _invite;
                    invite.createdBy = game.createdBy;
                    invite.status = InviteService.Constants.Status.Active;
                    invite.save(callback);
                }
                else {
                    InviteService.createGameInvite(game.createdBy, userId, game, callback)
                }
            }
        ], function (err) {
            return done(err);
        });
    },

    inviteAction: function inviteAction(gameId, inviteId, status, done) {
        var self = this;

        var game,
            invite;

        async.waterfall([
            function getGame(callback) {
                db.Game.findOne({_id: gameId}).populate('players group location').exec(callback);
            },
            function getInvite(_game, callback) {
                if (!_game) {
                    return callback('Game not found');
                }

                // store in the global scope variable
                game = _game;

                db.Invite.findOne({_id: inviteId}).populate('player').exec(callback);
            },
            function updateInviteStatus(_invite, callback) {
                if (!_invite) {
                    return callback('Invalid invitation.');
                }

                // store in the global scope variable
                invite = _invite;

                /* No longer true - you can accept a Rejected invite...
                if (invite.status !== InviteService.Constants.Status.Active) {
                    return callback('Invite status is invalid: ' + invite.status);
                }*/

                invite.status = status;
                return invite.save(callback);
            },
            function addToGame(saved, count, callback) {
                if (status === InviteService.Constants.Status.Accepted) {
                    return self.addPlayer(gameId, invite.player._id, function (err) {
                        return callback(err);
                    });
                }

                return callback();
            }
        ], function (err) {
            return done(err);
        });
    },

    addGuestToGame: function(gameId, data, done) {
        var _game;
        async.waterfall([
            function getGame(callback) {
                db.Game.findOne({_id: gameId}).exec(callback);
            },

            function addGuest(game, callback) {
                game.guests = game.guests || '[]';
                var guestsArr = JSON.parse(game.guests);
                data._id = uuid.v1();
                guestsArr.push(data);
                game.guests = JSON.stringify(guestsArr);
                game.save(callback);

                _game = game;
            }
        ], function cb(err) {
            return done(err, _game);
        });
    },

    removeGuestFromGame: function(gameId, guestId, done) {
        async.waterfall([
            function getGame(callback) {
                db.Game.findOne({_id: gameId}).exec(callback);
            },

            function addGuest(game, callback) {
                game.guests = game.guests || '[]';

                // remove the guest
                var guestsArr = JSON.parse(game.guests);
                _.remove(guestsArr, function(g) {
                    return g._id == guestId;
                });

                game.guests = JSON.stringify(guestsArr);
                game.save(callback);
            }
        ], function cb(err) {
            return done(err);
        });
    },

    getPlayerStats: function getPlayerStats(gameId, userId, globalCallback) {
        async.parallel([
            function (callback) {
                db.GameFact.find({game: gameId}).exec(callback);
            },
            function (callback) {
                db.PlayerRating.find({game: gameId, createdBy: userId}).exec(callback);
            },
            function(callback){
                db.PlayerRating.aggregate([
                    {
                        $match: {
                            game: db.Mongoose.Types.ObjectId(gameId)
                        }
                    },
                    {
                        $group: {
                            _id: {
                                player: "$player",
                            },
                            rating: {
                                $avg: "$rating"
                            }
                        }
                    }]).exec(callback);
            }
        ], ok(globalCallback, function (results) {
            var result = {
                gameFacts: results[0],
                playerRatings: results[1],
                averageRatings: _.map(results[2], function (aggregation) {
                   return {
                       player: aggregation._id.player,
                       rating: aggregation.rating
                   };
                })
            };
            return globalCallback(null, result);
        }));
    },

    updateManOfTheMatch: function updateManOfTheMatch(gameId, done){
        db.PlayerRating.aggregate([{$match: { game: db.Mongoose.Types.ObjectId(gameId)}},
            { $group: {_id: {player: "$player"}, rating: { $avg: "$rating"}}}]).exec(ok(done, function(results){
                var men_of_the_match = undefined;
                if(results.length > 0){
                    var manOfTheMatch = _.sortBy(results, function(r){ return - r.rating; })[0];
                    var menOfTheMatch = _.filter(results, function(r){ return r.rating == manOfTheMatch.rating});
                    men_of_the_match = _.map(menOfTheMatch, function(r){ return r._id.player; });
                }
                db.Game.update({ _id: gameId }, {$set: { men_of_the_match : men_of_the_match}}).exec(done);
        }));
    },

    updatePlayerRating: function updatePlayerRating(gameId, playerId, userId, rating, callback) {
        var self = this;
        db.PlayerRating.findOne({
            game: gameId,
            player: playerId,
            createdBy: userId
        }).exec(ok(callback, function (playerRating) {
            if (playerRating) {
                // disable update of player you've already rated
                //playerRating.rating = rating;
            } else {
                playerRating = new db.PlayerRating();

                playerRating.rating = rating;
                playerRating.game = gameId;
                playerRating.player = playerId;
                playerRating.createdBy = userId;
            }

            playerRating.save(callback);
            //playerRating.save(ok(callback, function(playerRating){
            //    self.updateManOfTheMatch(gameId, ok(callback, function(game){
            //        callback(null, playerRating);
            //    }))
            //}));
        }));
    },

    updateGameStatistics: function updateGameStatistics(gameId, game, userId, callback) {
        var setBody = {};
        var self = this;
        var updateGameDetails = function(done){
            if(game.team1 && game.team1.goals){
                setBody.team1 = game.team1;
            }
            if(game.team2 && game.team2.goals != undefined){
                setBody.team2 = game.team2;
            }
            for (var hasProperties in setBody) break;
            if(hasProperties){
                db.Game.update({ _id: gameId}, { $set: setBody}).exec(ok(done, function(){
                    if(game.group)
                    {
                        GroupService.updateGroupGoals(game.group._id, done);
                    } else {
                        done(null);
                    }
                }));
            } else {
                done(null);
            }
        };

        updateGameDetails(ok(callback, function(){
            async.forEachSeries(game.players, function(player, done){
                async.waterfall([function updateRating(cb){
                    if(player._id != userId && player.rating){
                        self.updatePlayerRating(gameId, player._id, userId, player.rating, ok(cb, function(){
                            cb();
                        }));
                    } else {
                        cb();
                    }
                }, function updateGoals(cb){
                    if(player.goals != undefined){
                        self.updateGameFact(gameId, player._id, userId, player.goals, self.Constants.GameFactType.GoalScored, ok(cb, function(){
                            cb();
                        }));
                    } else {
                        cb();
                    }
                }], function(err){
                    done(err);
                })
            }, ok(callback, function(){
                self.updateManOfTheMatch(gameId, callback);
            }));
        }));
    },

    updateGameFact: function updateGameFacts(gameId, playerId, userId, value, type, callback) {
        db.GameFact.findOne({game: gameId, player: playerId, type: type}).exec(ok(callback, function (gameFact) {
            if (gameFact) {
                gameFact.value = value;
                gameFact.updatedBy = userId;
            } else {
                gameFact = new db.GameFact();

                gameFact.value = value;
                gameFact.game = gameId;
                gameFact.player = playerId;
                gameFact.createdBy = userId;
                gameFact.type = type;
            }

            gameFact.save(callback);
        }));
    }

};