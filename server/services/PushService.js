/*global db*/
'use strict';

var request = require('superagent'),
    moment = require('moment-timezone'),
    async = require('async');

exports = module.exports = {
    send: function send(message, userId, invitedByUserId, done) {
        if (config.notificationsEnabled) {
            return done();
        }

        if (!message) {
            return done('No push message provided!');
        }
        if (!userId) {
            return done('No user id provided!');
        }

        async.series([
            function getUser(callback) {
                return db.User.findOne({ _id: userId }).exec(callback);
            },
            function getInvitedByUser(callback) {
                db.User.findOne({ _id: invitedByUserId }).exec(callback);
            },
            function getLocation(callback) {
                db.Location.findOne({ _id: message.game.location }).exec(callback);
            }
        ], function (err, results) {
            if (err) {
                return done(err);
            }

            var user = results[0],
                invitedByUser = results[1],
                location = results[2];

            message.invitedBy = {
                email: invitedByUser.email,
                name: invitedByUser.name,
                id: invitedByUser.id.toString()
            };

            var userLanguage = (user.language || 'bg').toLowerCase();
            var text = "New Game at {{game_time}} at '{{game_location}}' by {{user_name}}";
            if (userLanguage == 'bg') {
                text = "Нов Мач на {{game_time}} на '{{game_location}}' от {{user_name}}";
            }

            // setting user locale
            moment.locale(userLanguage);

            // format date according to the timezone
            var dateTime = moment(message.game.date_time);
            var dateTimeString = dateTime.tz(user.timezone).format('Do MMMM YYYY, H:mm:ss');

            // {{ game.date_time | date:'HH:mm'}} / {{ game.date_time | date: 'dd MMM yyyy' }}
            text = text.replace('{{game_time}}', dateTimeString)
                .replace('{{game_location}}', location.name)
                .replace('{{user_name}}', invitedByUser.name);

            var data = {
                Message: text,
                Filter: {
                    'Parameters.userId': userId
                },
                IOS: {
                    aps: {
                        alert: text,
                        category: "NEW_INVITE"
                    },
                    customData: JSON.stringify(message)
                }//,
                //Android: {
                //     customData: message
                //}
            };

            var body = JSON.stringify(data);

            var url = config.everlive.notificationsUrl;
            request.post(url)
                .send(body)
                .set('Content-Type', 'application/json')
                .set('Accept', 'application/json')
                .set('Authorization', 'Masterkey ' + config.everlive.masterKey)
                .end(function (err, res) {
                    done(err, res);
                });
        });
    }
};