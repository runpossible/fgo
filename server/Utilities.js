'use strict';
var path = require('path');
var projectDir = __dirname;

global.rootRequire = function (modulePath) {
    return require(path.join(projectDir, modulePath));
};

global.getEnvironment = function() {
    if (process.env.OPENSHIFT_NODEJS_PORT) {
        process.env.NODE_ENV = process.env.NODE_ENV || 'production';
    }
    else{
        process.env.NODE_ENV = process.env.NODE_ENV || 'development';
    }
    return process.env.NODE_ENV;
};

global.jsSuffix = new Date().getTime();

exports = module.exports = {};