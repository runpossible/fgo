var crypto = require('crypto');

exports = module.exports = function(mongoose) {
  var createSalt = function() {
    return crypto.randomBytes(128).toString('base64');
  };

  var hashPwd = function(salt, pwd) {
    var hmac = crypto.createHmac('sha1', salt);
    return hmac.update(pwd).digest('hex');
  };

  var userSchema = new mongoose.Schema({
    email: {
        type: String,
        index: { unique: true }
    },
    name: {
      type: String
    },
    public: { type: Boolean, default: true },
    master: Boolean,
    preferredAvatar: Number, // 1 - Facebook, 2 - Google, 3 - Gravatar,
    avatarUrl: String,
    preferredPosition: Number, //0 - None, 1 - Keeper, 2 - Defender, 3 - Midfielder, 4 - Attacker
    friends: [{ type: mongoose.Schema.ObjectId, ref: 'User' }],
    countryId: Number,
    city: String,
    height: Number,
    weight: Number,
    strongFoot: String,
    language: String,
    timezone: { type: String, default: 'Europe/Sofia' },
    birthDate: Date,
    shouldReceiveGameInviteMails: { type: Boolean, default: true },
    userImage: {
            type: String
    },
    local: {
      salt: String,
      activationCode: String,
      hashed_pwd: String,
      passwordReset: { type: mongoose.Schema.ObjectId, ref: 'PasswordReset' }
    },
    facebook: {
      id: String,
      token: String,
      refreshToken: String,
      email: String,
      name: String
    },
    google: {
      id: String,
      token: String,
      refreshToken: String,
      email: String,
      name: String
    },
    runkeeper: {
      profileId: Number,
      token: String,
      profile: String
    },
    strava: {
      profileId: Number,
      token: String
    },
    updatedAt: Date,
    createdAt: Date,
    lastNotificationCheck: Date
  });

  // Automatically set the updated at
  userSchema.pre('save', function(next) {
    this.wasNew = this.isNew;
    this.updatedAt = new Date();
    if(!this.createdAt) {
      this.createdAt = new Date();
    }
    next();
  });

  userSchema.methods = {
    authenticateLocal: function(pwd) {
      return this.local && this.local.salt && this.local.hashed_pwd && hashPwd(this.local.salt, pwd) === this.local.hashed_pwd;
    }
  };

  userSchema.statics.createSalt = createSalt;
  userSchema.statics.hashPwd = hashPwd;

  var User = mongoose.model('User', userSchema);

  return User;
};