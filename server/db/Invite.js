var NotificationService = rootRequire('/services/NotificationService');

exports = module.exports = function(mongoose) {

    var inviteSchema = new mongoose.Schema({
        group: { type: mongoose.Schema.ObjectId, ref: 'Group' },
        game: { type: mongoose.Schema.ObjectId, ref: 'Game' },
        status: { type: String, default: 'Active' }, // 'active', 'canceled'
        player: {type: mongoose.Schema.ObjectId, ref: 'User'},
        createdBy: {type: mongoose.Schema.ObjectId, ref: 'User'},
        updatedAt: Date,
        mailSent: Boolean
    });

    var Invite = mongoose.model('Invite', inviteSchema);

    // Automatically set the updated at
    inviteSchema.pre('save', function(next) {
        this.wasNew = this.isNew;
        this.updatedAt = new Date();
        next();
    });

    inviteSchema.post('save', function (doc) {
        NotificationService.inviteSaved(this.wasNew, doc);
    });

    inviteSchema.post('remove', function (doc) {
        NotificationService.inviteRemoved(doc);
    });

    return Invite;
};