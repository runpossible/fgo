var NotificationService = rootRequire('/services/NotificationService');

exports = module.exports = function(mongoose) {

    var groupSchema = new mongoose.Schema({
        name: {
            type: String,
            index: { unique: true }
        },// Name of the group
        description: {
            type: String,

        },
        groupImage: {
            type: String
        },
        gamesCount: {
            type: Number
        },
        goalsCount: {
            type: Number
        },
        urlName: {
            type: String,
            index: { unique: true }
        },// Url of the group
        createdBy: { type: mongoose.Schema.ObjectId, ref: 'User' },
        users: [{ type: mongoose.Schema.ObjectId, ref: 'User' }],
        admin_users: [{ type: mongoose.Schema.ObjectId, ref: 'User' }],
        default_location: { type: mongoose.Schema.ObjectId, ref: 'Location' },
        invitation_code: String,
        invitation_shorturl: String,
        updatedAt: Date
    });

    var Group = mongoose.model('Group', groupSchema);

    // Automatically set the updated at
    groupSchema.pre('save', function(next) {
        this.wasNew = this.isNew;
        this.updatedAt = new Date();
        next();
    });

    groupSchema.post('save', function (doc) {
        NotificationService.groupSaved(this.wasNew, doc);
    });

    groupSchema.post('remove', function (doc) {
        NotificationService.groupRemoved(doc);
    });

    return Group;
};