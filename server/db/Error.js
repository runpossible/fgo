exports = module.exports = function(mongoose) {

    var errorSchema = new mongoose.Schema({
        request: {
            originalUrl: String,
            params: String,
            body: String
        },
        statusCode: Number,
        data: String,
        error: String,
        stack: String,
        user: {type: mongoose.Schema.ObjectId, ref: 'User'},
        updatedAt: Date
    });

    var Error = mongoose.model('Error', errorSchema);

    // Automatically set the updated at
    errorSchema.pre('save', function(next) {
        this.wasNew = this.isNew;
        this.updatedAt = new Date();
        next();
    });

    return Error;
};