exports = module.exports = function(mongoose) {

    var notificationSchema = new mongoose.Schema({
        group: { type: mongoose.Schema.ObjectId, ref: 'Group' },
        groupName: String,
        game: { type: mongoose.Schema.ObjectId, ref: 'Game' },
        gameName: String,
        fromPlayer: {type: mongoose.Schema.ObjectId, ref: 'User'},
        toPlayer: {type: mongoose.Schema.ObjectId, ref: 'User'},
        targetId: String,
        type: String,
        updatedAt: Date
    });

    var Notification = mongoose.model('Notification', notificationSchema);

    // Automatically set the updated at
    notificationSchema.pre('save', function(next) {
        this.wasNew = this.isNew;
        this.updatedAt = new Date();
        next();
    });

    return Notification;
};