exports = module.exports = function(mongoose) {

    var gameFactSchema = new mongoose.Schema({
        game: { type: mongoose.Schema.ObjectId, ref: 'Game' },
        group: { type: mongoose.Schema.ObjectId, ref: 'Group' },
        player: {type: mongoose.Schema.ObjectId, ref: 'User'},
        createdBy: {type: mongoose.Schema.ObjectId, ref: 'User'},
        updatedBy: {type: mongoose.Schema.ObjectId, ref: 'User'},
        value: Number,
        type: String,
        updatedAt: Date
    });

    var GameFact = mongoose.model('GameFact', gameFactSchema);

    // Automatically set the updated at
    gameFactSchema.pre('save', function(next) {
        this.wasNew = this.isNew;
        this.updatedAt = new Date();
        next();
    });

    return GameFact;
};