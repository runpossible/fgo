var NotificationService = rootRequire('/services/NotificationService');

exports = module.exports = function(mongoose) {

    var playerRatingSchema = new mongoose.Schema({
        player: { type: mongoose.Schema.ObjectId, ref: 'User' },
        game: { type: mongoose.Schema.ObjectId, ref: 'Game' },
        group: { type: mongoose.Schema.ObjectId, ref: 'Group' },
        createdBy: { type: mongoose.Schema.ObjectId, ref: 'User' },
        rating: Number,
        updatedAt: Date
    });

    var PlayerRating = mongoose.model('PlayerRating', playerRatingSchema);

    // Automatically set the updated at
    playerRatingSchema.pre('save', function(next) {
        this.wasNew = this.isNew;
        this.updatedAt = new Date();
        next();
    });

    playerRatingSchema.post('save', function (doc) {
        //NotificationService.groupSaved(this.wasNew, doc);
    });

    playerRatingSchema.post('remove', function (doc) {
        //NotificationService.groupRemoved(doc);
    });

    return PlayerRating;
};