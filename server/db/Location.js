exports = module.exports = function(mongoose) {

    var locationSchema = new mongoose.Schema({
        name: { type: String },
        loc: {
            type: [Number],
            index: '2dsphere'
        },
        formattedAddress: { type: String },
        city: {
            name: { type: String},
            municipality: { type: String},
            district: { type: String},
            country: { type: String},
            postalCode: { type: String }
        },
        price: { type: String },
        playersCount: { type: Number },
        createdBy: {type: mongoose.Schema.ObjectId, ref: 'User'},
        updatedAt: Date,
        primaryImage: { type: String },
        phone: { type: String },
        totalFieldsCount: { type: Number }
    });

    var Location = mongoose.model('Location', locationSchema);

    // Automatically set the updated at
    locationSchema.pre('save', function(next) {
        this.wasNew = this.isNew;
        this.updatedAt = new Date();

        //http://stackoverflow.com/questions/16388836/does-applying-a-2dsphere-index-on-a-mongoose-schema-force-the-location-field-to
        if (this.isNew && Array.isArray(this.location) && 0 === this.location.length) {
            this.location = undefined;
        }

        next();
    });

    // Create a geospatial index.
    //locationSchema.index({ loc: '2dsphere' });

    return Location;
};