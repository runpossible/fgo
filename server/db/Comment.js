var NotificationService = rootRequire('/services/NotificationService');

exports = module.exports = function(mongoose) {

    var commentSchema = new mongoose.Schema({
        group: { type: mongoose.Schema.ObjectId, ref: 'Group' },
        game: { type: mongoose.Schema.ObjectId, ref: 'Game' },
        user: { type: mongoose.Schema.ObjectId, ref: 'User' },
        createdBy: {type: mongoose.Schema.ObjectId, ref: 'User'},
        text:  String,
        updatedAt: Date
    });

    var Comment = mongoose.model('Comment', commentSchema);

    // Automatically set the updated at
    commentSchema.pre('save', function(next) {
        this.wasNew = this.isNew;
        this.updatedAt = new Date();
        next();
    });

    commentSchema.post('save', function (doc) {
        NotificationService.commentSaved(this.wasNew, doc);
    });

    commentSchema.post('remove', function (doc) {
        NotificationService.commentRemoved(doc);
    });

    return Comment;
};