exports = module.exports = function(mongoose) {

    var accessTokenSchema = new mongoose.Schema({
        user: { type: mongoose.Schema.ObjectId, ref: 'User', index: { unique: true } },
        value:  String,
        updatedAt: Date
    });

    var AccessToken = mongoose.model('AccessToken', accessTokenSchema);

    // Automatically set the updated at
    accessTokenSchema.pre('save', function(next) {
        this.wasNew = this.isNew;
        this.updatedAt = new Date();
        next();
    });

    return AccessToken;
};