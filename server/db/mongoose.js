var mongoose = require('mongoose');
require('mongoose-double')(mongoose);

var Grid = require('gridfs-stream');
Grid.mongo = mongoose.mongo;

exports = module.exports = function (config, done) {
    // http://stackoverflow.com/questions/30909492/mongoerror-topology-was-destroyed
    var options = {
        server: {socketOptions: {keepAlive: 1, connectTimeoutMS: 30000}},
        replset: {socketOptions: {keepAlive: 1, connectTimeoutMS: 30000}}
    };
    mongoose.connect(config.dbConnString, options);

    var User = require('./User')(mongoose);
    var Group = require('./Group')(mongoose);
    var Game = require('./Game')(mongoose);
    var Invite = require('./Invite')(mongoose);
    var Error = require('./Error')(mongoose);
    var Comment = require('./Comment')(mongoose);
    var Notification = require('./Notification')(mongoose);
    var Location = require('./Location')(mongoose);
    var PlayerRating = require('./PlayerRating')(mongoose);
    var GameFact = require('./GameFact')(mongoose);
    var FavouriteLocation = require('./FavouriteLocation')(mongoose);
    var PasswordReset = require('./PasswordReset')(mongoose);
    var AccessToken = require('./AccessToken')(mongoose);

    var dbObject = {
        User: User,
        Group: Group,
        Game: Game,
        Invite: Invite,
        Error: Error,
        Comment: Comment,
        Notification: Notification,
        Location: Location,
        PlayerRating: PlayerRating,
        GameFact: GameFact,
        FavouriteLocation: FavouriteLocation,
        PasswordReset: PasswordReset,
        AccessToken: AccessToken,
        Mongoose: mongoose,
        Schema: mongoose.Schema,
        isDuplicateError: function (err) {
            return err && err.code === 11000;
        }
    };

    var db = mongoose.connection;

    db.on('error', function callback(err) {
        console.error.bind(console, 'connection error: ' + err);

        done(err);
    });

    db.once('open', function callback() {
        console.log('Connected to DB');

        var gfs = Grid(mongoose.connection.db, mongoose.mongo);
        global.gfs = gfs;

        done(null, dbObject); // ok
    });
};