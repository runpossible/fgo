exports = module.exports = function (mongoose) {

    var favLocSchema = new mongoose.Schema({
        location: {type: mongoose.Schema.ObjectId, ref: 'Location'},
        user: {type: mongoose.Schema.ObjectId, ref: 'User'},
        updatedAt: Date
    });

    favLocSchema.index({location: 1, user: 1}, {unique: true});

    var favLoc = mongoose.model('FavouriteLocation', favLocSchema);

    // Automatically set the updated at
    favLocSchema.pre('save', function (next) {
        this.wasNew = this.isNew;
        this.updatedAt = new Date();

        next();
    });

    return favLoc;
};