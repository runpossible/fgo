var NotificationService = rootRequire('/services/NotificationService');

exports = module.exports = function(mongoose) {

    var passwordResetSchema = new mongoose.Schema({
        user: { type: mongoose.Schema.ObjectId, ref: 'User' },
        secret: String,
        used: Boolean,
        requestedOn: Date,
        updatedAt: Date
    });

    var PasswordReset = mongoose.model('PasswordReset', passwordResetSchema);

    // Automatically set the updated at
    passwordResetSchema.pre('save', function(next) {
        this.wasNew = this.isNew;
        this.updatedAt = new Date();
        next();
    });

    passwordResetSchema.post('save', function (doc) {
        //NotificationService.groupSaved(this.wasNew, doc);
    });

    passwordResetSchema.post('remove', function (doc) {
        //NotificationService.groupRemoved(doc);
    });

    return PasswordReset;
};