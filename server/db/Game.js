var NotificationService = rootRequire('/services/NotificationService');

exports = module.exports = function(mongoose) {

    var gameSchema = new mongoose.Schema({
        name: String,
        urlName: {
            type: String,
            index: { unique: true }
        },// Url of the group
        date_time: Date,
        location: { type: mongoose.Schema.ObjectId, ref: 'Location' },
        locationName: { type: String},
        group: { type: mongoose.Schema.ObjectId, ref: 'Group' },
        players: [{ type: mongoose.Schema.ObjectId, ref: 'User' }],
        guests: { type: String, default: '[]' },
        men_of_the_match: [{ type: mongoose.Schema.ObjectId, ref: 'User' }],
        status: { type: String, default: 'active' }, // 'active', 'canceled'
        public: { type: Boolean, default: false },
        price: { type: String },
        numberOfPlayers: Number,
        invitation_code: String,
        invitation_shorturl: String,
        createdBy: {type: mongoose.Schema.ObjectId, ref: 'User'},
        updatedAt: Date,
        city: {
            name: { type: String},
            municipality: { type: String},
            district: { type: String},
            country: { type: String},
            postalCode: { type: String }
        },
        team1: {
            name: { type: String },
            goals: { type: Number }
        },
        team2: {
            name: { type: String },
            goals: { type: Number }
        }
    });

    var Game = mongoose.model('Game', gameSchema);

    // Automatically set the updated at
    gameSchema.pre('save', function(next) {
        this.wasNew = this.isNew;
        this.updatedAt = new Date();
        next();
    });

    gameSchema.post('save', function (doc) {
        NotificationService.gameSaved(this.wasNew, doc);
    });

    gameSchema.post('remove', function (doc) {
        NotificationService.gameRemoved(doc);
    });

    return Game;
};