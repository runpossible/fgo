var fs = require('fs'),
    uuid = require('node-uuid');

exports = module.exports = {
    ensureFileSize: function ensureFileSize(file, limitInMbs, done) {
        limitInMbs = limitInMbs || 5;

        var stats = fs.statSync(file.path);
        var mbsSize = stats.size / (1024.0 * 1024.0);

        if (mbsSize < limitInMbs) {
            return done(null, true);
        }

        return done('File too large - ' + limitInMbs +
            'mb is the max size allowed. File size: ' + mbsSize, false);
    },

    saveFileInGridFS: function saveFileInGridFS(file, done) {
        var fileName = uuid.v1();
        var gfs = global.gfs;

        var fileStream = fs.createReadStream(file.path);
        fileStream.pipe(gfs.createWriteStream({
            filename: fileName
        }).on('close', function (savedFile) {
            // file saved.

            return done(null, fileName);
        }).on('error', done));
    }
};
