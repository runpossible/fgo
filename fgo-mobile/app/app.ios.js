var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var application = require("application");
;
;
;
;
var MyDelegate = (function (_super) {
    __extends(MyDelegate, _super);
    function MyDelegate() {
        _super.apply(this, arguments);
    }
    MyDelegate.prototype.applicationDidFinishLaunchingWithOptions = function (application, launchOptions) {
        return FBSDKApplicationDelegate.sharedInstance().applicationDidFinishLaunchingWithOptions(application, launchOptions);
    };
    MyDelegate.prototype.applicationOpenURLSourceApplicationAnnotation = function (application, url, sourceApplication, annotation) {
        return FBSDKApplicationDelegate.sharedInstance().applicationOpenURLSourceApplicationAnnotation(application, url, sourceApplication, annotation);
    };
    MyDelegate.prototype.applicationDidBecomeActive = function (application) {
        FBSDKAppEvents.activateApp();
    };
    MyDelegate.prototype.applicationWillTerminate = function (application) {
        //Do something you want here
    };
    MyDelegate.prototype.applicationDidEnterBackground = function (application) {
        //Do something you want here
    };
    MyDelegate.ObjCProtocols = [UIApplicationDelegate];
    return MyDelegate;
}(UIResponder));
application.ios.delegate = MyDelegate;
application.start();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLmlvcy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImFwcC5pb3MudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxJQUFJLFdBQVcsR0FBRyxPQUFPLENBQUMsYUFBYSxDQUFDLENBQUM7QUFFYixDQUFDO0FBSUMsQ0FBQztBQUNXLENBQUM7QUFDZCxDQUFDO0FBRTlCO0lBQXlCLDhCQUFXO0lBQXBDO1FBQXlCLDhCQUFXO0lBc0JwQyxDQUFDO0lBbkJDLDZEQUF3QyxHQUF4QyxVQUF5QyxXQUEwQixFQUFFLGFBQTJCO1FBQy9GLE1BQU0sQ0FBQyx3QkFBd0IsQ0FBQyxjQUFjLEVBQUUsQ0FBQyx3Q0FBd0MsQ0FBQyxXQUFXLEVBQUUsYUFBYSxDQUFDLENBQUM7SUFDdkgsQ0FBQztJQUVELGtFQUE2QyxHQUE3QyxVQUE4QyxXQUFXLEVBQUUsR0FBRyxFQUFFLGlCQUFpQixFQUFFLFVBQVU7UUFDM0YsTUFBTSxDQUFDLHdCQUF3QixDQUFDLGNBQWMsRUFBRSxDQUFDLDZDQUE2QyxDQUFDLFdBQVcsRUFBRSxHQUFHLEVBQUUsaUJBQWlCLEVBQUUsVUFBVSxDQUFDLENBQUM7SUFDbEosQ0FBQztJQUVELCtDQUEwQixHQUExQixVQUEyQixXQUEwQjtRQUNqRCxjQUFjLENBQUMsV0FBVyxFQUFFLENBQUM7SUFDakMsQ0FBQztJQUVELDZDQUF3QixHQUF4QixVQUF5QixXQUEwQjtRQUNqRCw0QkFBNEI7SUFDOUIsQ0FBQztJQUVELGtEQUE2QixHQUE3QixVQUE4QixXQUEwQjtRQUN0RCw0QkFBNEI7SUFDOUIsQ0FBQztJQXBCYSx3QkFBYSxHQUFHLENBQUMscUJBQXFCLENBQUMsQ0FBQztJQXFCeEQsaUJBQUM7QUFBRCxDQUFDLEFBdEJELENBQXlCLFdBQVcsR0FzQm5DO0FBRUQsV0FBVyxDQUFDLEdBQUcsQ0FBQyxRQUFRLEdBQUcsVUFBVSxDQUFDO0FBQ3RDLFdBQVcsQ0FBQyxLQUFLLEVBQUUsQ0FBQyJ9