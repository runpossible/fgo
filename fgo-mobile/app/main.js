"use strict";
var application_1 = require("nativescript-angular/application");
var app_routes_1 = require("./app.routes");
var app_component_1 = require("./app.component");
var status_bar_util_1 = require("./utils/status-bar-util");
// register a third party drop down - there's no builtin
var element_registry_1 = require("nativescript-angular/element-registry");
element_registry_1.registerElement("DropDown", function () { return require("nativescript-drop-down/drop-down").DropDown; });
status_bar_util_1.setStatusBarColors();
application_1.nativeScriptBootstrap(app_component_1.AppComponent, [
    app_routes_1.APP_ROUTER_PROVIDERS
]);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFpbi5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIm1haW4udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLDRCQUFvQyxrQ0FBa0MsQ0FBQyxDQUFBO0FBQ3ZFLDJCQUFtQyxjQUFjLENBQUMsQ0FBQTtBQUNsRCw4QkFBMkIsaUJBQWlCLENBQUMsQ0FBQTtBQUM3QyxnQ0FBaUMseUJBQXlCLENBQUMsQ0FBQTtBQUUzRCx3REFBd0Q7QUFDeEQsaUNBQThCLHVDQUF1QyxDQUFDLENBQUE7QUFDdEUsa0NBQWUsQ0FBQyxVQUFVLEVBQUUsY0FBTSxPQUFBLE9BQU8sQ0FBQyxrQ0FBa0MsQ0FBQyxDQUFDLFFBQVEsRUFBcEQsQ0FBb0QsQ0FBQyxDQUFDO0FBRXhGLG9DQUFrQixFQUFFLENBQUM7QUFDckIsbUNBQXFCLENBQUMsNEJBQVksRUFBRTtJQUNsQyxpQ0FBb0I7Q0FDckIsQ0FBQyxDQUFDIn0=