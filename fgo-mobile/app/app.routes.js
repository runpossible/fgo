"use strict";
var router_1 = require("nativescript-angular/router");
var login_component_1 = require("./pages/login/login.component");
var upcoming_component_1 = require("./pages/games/upcoming.component");
var auth_guard_1 = require("./auth.guard");
exports.routes = [
    { path: "", component: upcoming_component_1.UpcomingComponent, canActivate: [auth_guard_1.AuthGuard] },
    { path: "login", component: login_component_1.LoginPageComponent }
];
exports.APP_ROUTER_PROVIDERS = [
    router_1.nsProvideRouter(exports.routes, { enableTracing: false }),
    auth_guard_1.AuthGuard
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLnJvdXRlcy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImFwcC5yb3V0ZXMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUNBLHVCQUE4Qiw2QkFDOUIsQ0FBQyxDQUQwRDtBQUMzRCxnQ0FBaUMsK0JBQStCLENBQUMsQ0FBQTtBQUVqRSxtQ0FBZ0Msa0NBQWtDLENBQUMsQ0FBQTtBQUNuRSwyQkFBd0IsY0FBYyxDQUFDLENBQUE7QUFFMUIsY0FBTSxHQUFpQjtJQUNsQyxFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsU0FBUyxFQUFFLHNDQUFpQixFQUFFLFdBQVcsRUFBRSxDQUFDLHNCQUFTLENBQUMsRUFBRTtJQUNwRSxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUUsU0FBUyxFQUFFLG9DQUFrQixFQUFFO0NBQ2pELENBQUM7QUFFVyw0QkFBb0IsR0FBRztJQUNsQyx3QkFBZSxDQUFDLGNBQU0sRUFBRSxFQUFFLGFBQWEsRUFBRSxLQUFLLEVBQUUsQ0FBQztJQUNqRCxzQkFBUztDQUNWLENBQUMifQ==