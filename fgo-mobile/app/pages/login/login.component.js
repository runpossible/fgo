"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var page_1 = require("ui/page");
var sign_in_1 = require('../../shared/auth/sign-in');
var register_1 = require('../../shared/auth/register');
var authentication_service_1 = require("../../shared/auth/authentication.service");
var dialog_util_1 = require("../../utils/dialog-util");
var FacebookLoginHandler = require("nativescript-facebook-login");
var frame_1 = require("ui/frame");
var translate_pipe_1 = require('../../shared/pipes/translate.pipe');
var translation_1 = require('../../shared/translation');
var LoginPageComponent = (function () {
    function LoginPageComponent(_router, _authService, page, _translation) {
        var _this = this;
        this._router = _router;
        this._authService = _authService;
        this.page = page;
        this._translation = _translation;
        this.isLoggingIn = true;
        this.isAuthenticating = false;
        this.selectedState = 'main';
        this.selectedIndex = 0;
        this.signInInfo = new sign_in_1.SignInInfo("yo@yo.com", "123");
        this.registerInfo = new register_1.RegisterInfo('', '', '', '', 0);
        this.translation = _translation;
        // initialize the swipe event - cast swipe to string
        page.on('swipe', zonedCallback(function (args) {
            if (args.direction === 1) {
                // right
                // if we swipe right, go back to the main if we're in register/signin pages
                if (_this.selectedState == 'register' || _this.selectedState == 'signin') {
                    _this.selectedState = 'main';
                }
            }
        }));
        this.initPositions();
    }
    LoginPageComponent.prototype.initPositions = function () {
        this.positions = [
            this.translation.translate('None'),
            this.translation.translate('Keeper'),
            this.translation.translate('Defender'),
            this.translation.translate('Midfielder'),
            this.translation.translate('Attacker')
        ];
    };
    LoginPageComponent.prototype.ngOnInit = function () {
        this.page.actionBarHidden = true;
    };
    LoginPageComponent.prototype.onchange = function (selected) {
        console.log("selected index " + selected);
    };
    LoginPageComponent.prototype.setLang = function (lang) {
        //console.log('lang = ' + lang);
        this._translation.setLang(lang);
        // update the values of the positions dropdown
        this.initPositions();
    };
    LoginPageComponent.prototype.showContainer = function (selection) {
        this.selectedState = selection;
    };
    LoginPageComponent.prototype.joinNow = function () {
        //alert('join now');
        this.showContainer('register');
    };
    LoginPageComponent.prototype.signIn = function () {
        this.showContainer('signin');
    };
    LoginPageComponent.prototype.onSignUp = function () {
        console.log('signing up with: ' + JSON.stringify(this.registerInfo));
        this._authService.register(this.registerInfo)
            .then(function (user) {
            // navigate
            dialog_util_1.alert('hello after register');
        }).catch(function (e) {
            dialog_util_1.alert('error occured.' + e);
        });
        ;
    };
    LoginPageComponent.prototype.onSignIn = function () {
        var _this = this;
        // login here...
        //alert('sign in: ' + JSON.stringify(this.signInInfo));
        this._authService.login(this.signInInfo)
            .then(function () {
            _this._router.navigate([""]);
        }).catch(function (e) {
            dialog_util_1.alert('error occured. ' + e);
        });
        ;
    };
    LoginPageComponent.prototype.onFacebookLogin = function () {
        console.log('on facebook login');
        //Here we select the login behaviour
        //Recomended system account with native fallback for iOS
        if (frame_1.topmost().ios) {
            console.log('is ios');
            FacebookLoginHandler.init(2);
        }
        else if (frame_1.topmost().android) {
            FacebookLoginHandler.init();
        }
        var successCallback = function (result) {
            //Do something with the result, for example get the AccessToken
            var token;
            if (frame_1.topmost().android) {
                token = result.getAccessToken().getToken();
            }
            else if (frame_1.topmost().ios) {
                token = result.token.tokenString;
                console.log('full result: ' + JSON.stringify(result));
            }
            console.log('Received token: ' + token);
        };
        var cancelCallback = function () {
            dialog_util_1.alert("Login was cancelled");
        };
        var failCallback = function (error) {
            var errorMessage = "Error with Facebook";
            //Try to get as much information as possible from error
            if (error) {
                if (frame_1.topmost().ios) {
                    if (error.localizedDescription) {
                        errorMessage += ": " + error.localizedDescription;
                    }
                    else if (error.code) {
                        errorMessage += ": Code " + error.code;
                    }
                    else {
                        errorMessage += ": " + error;
                    }
                }
                else if (frame_1.topmost().android) {
                    if (error.getErrorMessage) {
                        errorMessage += ": " + error.getErrorMessage();
                    }
                    else if (error.getErrorCode) {
                        errorMessage += ": Code " + error.getErrorCode();
                    }
                    else {
                        errorMessage += ": " + error;
                    }
                }
            }
            dialog_util_1.alert(errorMessage);
        };
        //Register our callbacks
        FacebookLoginHandler.registerCallback(successCallback, cancelCallback, failCallback);
        //Start the login process
        FacebookLoginHandler.logInWithPublishPermissions(["publish_actions"]);
    };
    LoginPageComponent.prototype.onGoogleLogin = function () {
        dialog_util_1.alert('google login');
    };
    __decorate([
        core_1.ViewChild("joinContainer"), 
        __metadata('design:type', core_1.ElementRef)
    ], LoginPageComponent.prototype, "joinContainer", void 0);
    __decorate([
        core_1.ViewChild("mainContainer"), 
        __metadata('design:type', core_1.ElementRef)
    ], LoginPageComponent.prototype, "mainContainer", void 0);
    LoginPageComponent = __decorate([
        core_1.Component({
            selector: "my-app",
            providers: [authentication_service_1.AuthenticationService],
            templateUrl: "pages/login/login.html",
            styleUrls: ["pages/login/login-common.css", "pages/login/login.css"],
            pipes: [translate_pipe_1.default]
        }), 
        __metadata('design:paramtypes', [router_1.Router, authentication_service_1.AuthenticationService, page_1.Page, translation_1.default])
    ], LoginPageComponent);
    return LoginPageComponent;
}());
exports.LoginPageComponent = LoginPageComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsibG9naW4uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBdUQsZUFBZSxDQUFDLENBQUE7QUFDdkUsdUJBQXFCLGlCQUFpQixDQUFDLENBQUE7QUFNdkMscUJBQW1CLFNBQVMsQ0FBQyxDQUFBO0FBRTdCLHdCQUF5QiwyQkFDekIsQ0FBQyxDQURtRDtBQUNwRCx5QkFBMkIsNEJBQzNCLENBQUMsQ0FEc0Q7QUFDdkQsdUNBQW9DLDBDQUEwQyxDQUFDLENBQUE7QUFFL0UsNEJBQW9CLHlCQUF5QixDQUFDLENBQUE7QUFDOUMsSUFBSSxvQkFBb0IsR0FBRyxPQUFPLENBQUMsNkJBQTZCLENBQUMsQ0FBQztBQUNsRSxzQkFBc0IsVUFBVSxDQUFDLENBQUE7QUFFakMsK0JBQTRCLG1DQUFtQyxDQUFDLENBQUE7QUFDaEUsNEJBQXdCLDBCQUEwQixDQUFDLENBQUE7QUFZbkQ7SUFjRSw0QkFBb0IsT0FBZSxFQUN6QixZQUFtQyxFQUNuQyxJQUFVLEVBQ1YsWUFBeUI7UUFqQnJDLGlCQXVLQztRQXpKcUIsWUFBTyxHQUFQLE9BQU8sQ0FBUTtRQUN6QixpQkFBWSxHQUFaLFlBQVksQ0FBdUI7UUFDbkMsU0FBSSxHQUFKLElBQUksQ0FBTTtRQUNWLGlCQUFZLEdBQVosWUFBWSxDQUFhO1FBaEJuQyxnQkFBVyxHQUFHLElBQUksQ0FBQztRQUNuQixxQkFBZ0IsR0FBRyxLQUFLLENBQUM7UUFDekIsa0JBQWEsR0FBRyxNQUFNLENBQUM7UUFHdkIsa0JBQWEsR0FBRyxDQUFDLENBQUM7UUFZaEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLG9CQUFVLENBQUMsV0FBVyxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQ3JELElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSx1QkFBWSxDQUFDLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUN4RCxJQUFJLENBQUMsV0FBVyxHQUFHLFlBQVksQ0FBQztRQUVoQyxvREFBb0Q7UUFDcEQsSUFBSSxDQUFDLEVBQUUsQ0FBQyxPQUFPLEVBQUUsYUFBYSxDQUFDLFVBQUMsSUFBSTtZQUNsQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3pCLFFBQVE7Z0JBQ1IsMkVBQTJFO2dCQUMzRSxFQUFFLENBQUMsQ0FBQyxLQUFJLENBQUMsYUFBYSxJQUFJLFVBQVUsSUFBSSxLQUFJLENBQUMsYUFBYSxJQUFJLFFBQVEsQ0FBQyxDQUFDLENBQUM7b0JBQ3ZFLEtBQUksQ0FBQyxhQUFhLEdBQUcsTUFBTSxDQUFDO2dCQUM5QixDQUFDO1lBQ0gsQ0FBQztRQUNILENBQUMsQ0FBQyxDQUFDLENBQUM7UUFFSixJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7SUFDdkIsQ0FBQztJQUVELDBDQUFhLEdBQWI7UUFDRSxJQUFJLENBQUMsU0FBUyxHQUFHO1lBQ2YsSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDO1lBQ2xDLElBQUksQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQztZQUNwQyxJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUM7WUFDdEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDO1lBQ3hDLElBQUksQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQztTQUN2QyxDQUFDO0lBQ0osQ0FBQztJQUVELHFDQUFRLEdBQVI7UUFDRSxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7SUFDbkMsQ0FBQztJQUVNLHFDQUFRLEdBQWYsVUFBZ0IsUUFBUTtRQUN0QixPQUFPLENBQUMsR0FBRyxDQUFDLGlCQUFpQixHQUFHLFFBQVEsQ0FBQyxDQUFDO0lBQzVDLENBQUM7SUFFRCxvQ0FBTyxHQUFQLFVBQVEsSUFBSTtRQUNWLGdDQUFnQztRQUNoQyxJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNoQyw4Q0FBOEM7UUFDOUMsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO0lBQ3ZCLENBQUM7SUFFRCwwQ0FBYSxHQUFiLFVBQWMsU0FBUztRQUNyQixJQUFJLENBQUMsYUFBYSxHQUFHLFNBQVMsQ0FBQztJQUNqQyxDQUFDO0lBRUQsb0NBQU8sR0FBUDtRQUNFLG9CQUFvQjtRQUNwQixJQUFJLENBQUMsYUFBYSxDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQ2pDLENBQUM7SUFFRCxtQ0FBTSxHQUFOO1FBQ0UsSUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUMvQixDQUFDO0lBRUQscUNBQVEsR0FBUjtRQUNFLE9BQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQztRQUNyRSxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDO2FBQzFDLElBQUksQ0FBQyxVQUFDLElBQUk7WUFDVCxXQUFXO1lBQ1gsbUJBQUssQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO1FBQ2hDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFDLENBQUM7WUFDVCxtQkFBSyxDQUFDLGdCQUFnQixHQUFHLENBQUMsQ0FBQyxDQUFDO1FBQzlCLENBQUMsQ0FBQyxDQUFDO1FBQUEsQ0FBQztJQUNSLENBQUM7SUFFRCxxQ0FBUSxHQUFSO1FBQUEsaUJBU0M7UUFSQyxnQkFBZ0I7UUFDaEIsdURBQXVEO1FBQ3ZELElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUM7YUFDdkMsSUFBSSxDQUFDO1lBQ0osS0FBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQzlCLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFDLENBQUM7WUFDVCxtQkFBSyxDQUFDLGlCQUFpQixHQUFHLENBQUMsQ0FBQyxDQUFDO1FBQy9CLENBQUMsQ0FBQyxDQUFDO1FBQUEsQ0FBQztJQUNOLENBQUM7SUFFRCw0Q0FBZSxHQUFmO1FBQ0UsT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO1FBQ2pDLG9DQUFvQztRQUVwQyx3REFBd0Q7UUFDeEQsRUFBRSxDQUFDLENBQUMsZUFBTyxFQUFFLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUNsQixPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQ3RCLG9CQUFvQixDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUMvQixDQUFDO1FBRUQsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLGVBQU8sRUFBRSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFDM0Isb0JBQW9CLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDOUIsQ0FBQztRQUVELElBQUksZUFBZSxHQUFHLFVBQVUsTUFBTTtZQUNwQywrREFBK0Q7WUFDL0QsSUFBSSxLQUFLLENBQUM7WUFDVixFQUFFLENBQUMsQ0FBQyxlQUFPLEVBQUUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO2dCQUN0QixLQUFLLEdBQUcsTUFBTSxDQUFDLGNBQWMsRUFBRSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQzdDLENBQUM7WUFDRCxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsZUFBTyxFQUFFLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDdkIsS0FBSyxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDO2dCQUNqQyxPQUFPLENBQUMsR0FBRyxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDeEQsQ0FBQztZQUVELE9BQU8sQ0FBQyxHQUFHLENBQUMsa0JBQWtCLEdBQUcsS0FBSyxDQUFDLENBQUM7UUFDMUMsQ0FBQyxDQUFBO1FBRUQsSUFBSSxjQUFjLEdBQUc7WUFDbkIsbUJBQUssQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO1FBQy9CLENBQUMsQ0FBQTtRQUVELElBQUksWUFBWSxHQUFHLFVBQVUsS0FBSztZQUNoQyxJQUFJLFlBQVksR0FBRyxxQkFBcUIsQ0FBQztZQUN6Qyx1REFBdUQ7WUFDdkQsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztnQkFDVixFQUFFLENBQUMsQ0FBQyxlQUFPLEVBQUUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO29CQUNsQixFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDO3dCQUMvQixZQUFZLElBQUksSUFBSSxHQUFHLEtBQUssQ0FBQyxvQkFBb0IsQ0FBQztvQkFDcEQsQ0FBQztvQkFDRCxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7d0JBQ3BCLFlBQVksSUFBSSxTQUFTLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQztvQkFDekMsQ0FBQztvQkFDRCxJQUFJLENBQUMsQ0FBQzt3QkFDSixZQUFZLElBQUksSUFBSSxHQUFHLEtBQUssQ0FBQztvQkFDL0IsQ0FBQztnQkFDSCxDQUFDO2dCQUNELElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxlQUFPLEVBQUUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO29CQUMzQixFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQzt3QkFDMUIsWUFBWSxJQUFJLElBQUksR0FBRyxLQUFLLENBQUMsZUFBZSxFQUFFLENBQUM7b0JBQ2pELENBQUM7b0JBQ0QsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO3dCQUM1QixZQUFZLElBQUksU0FBUyxHQUFHLEtBQUssQ0FBQyxZQUFZLEVBQUUsQ0FBQztvQkFDbkQsQ0FBQztvQkFDRCxJQUFJLENBQUMsQ0FBQzt3QkFDSixZQUFZLElBQUksSUFBSSxHQUFHLEtBQUssQ0FBQztvQkFDL0IsQ0FBQztnQkFDSCxDQUFDO1lBQ0gsQ0FBQztZQUNELG1CQUFLLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDdEIsQ0FBQyxDQUFBO1FBQ0Qsd0JBQXdCO1FBQ3hCLG9CQUFvQixDQUFDLGdCQUFnQixDQUFDLGVBQWUsRUFBRSxjQUFjLEVBQUUsWUFBWSxDQUFDLENBQUM7UUFFckYseUJBQXlCO1FBQ3pCLG9CQUFvQixDQUFDLDJCQUEyQixDQUFDLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDO0lBQ3hFLENBQUM7SUFFRCwwQ0FBYSxHQUFiO1FBQ0UsbUJBQUssQ0FBQyxjQUFjLENBQUMsQ0FBQztJQUN4QixDQUFDO0lBM0pEO1FBQUMsZ0JBQVMsQ0FBQyxlQUFlLENBQUM7OzZEQUFBO0lBQzNCO1FBQUMsZ0JBQVMsQ0FBQyxlQUFlLENBQUM7OzZEQUFBO0lBbkI3QjtRQUFDLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsUUFBUTtZQUNsQixTQUFTLEVBQUUsQ0FBQyw4Q0FBcUIsQ0FBQztZQUNsQyxXQUFXLEVBQUUsd0JBQXdCO1lBQ3JDLFNBQVMsRUFBRSxDQUFDLDhCQUE4QixFQUFFLHVCQUF1QixDQUFDO1lBQ3BFLEtBQUssRUFBRSxDQUFDLHdCQUFlLENBQUM7U0FDekIsQ0FBQzs7MEJBQUE7SUF3S0YseUJBQUM7QUFBRCxDQUFDLEFBdktELElBdUtDO0FBdktZLDBCQUFrQixxQkF1SzlCLENBQUEifQ==