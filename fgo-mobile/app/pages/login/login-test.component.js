"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var page_1 = require("ui/page");
var gestures_1 = require("ui/gestures");
var LoginTestComponent = (function () {
    function LoginTestComponent(_router, page) {
        this._router = _router;
        this.page = page;
        this.state = "main";
        /*
        let lbl = <Label>this.helloMain.nativeElement;
        lbl.on('swipe', zonedCallback((args) => {
          if(args.direction === SwipeDirection.right) {
              console.log('from ' + this.state + ' to secondary');
              this.state = "secondary";
          } else if(args.direction === SwipeDirection.left) {
              console.log('from ' + this.state + ' to main');
              this.state = "main";
          }
        }));*/
    }
    LoginTestComponent.prototype.test = function () {
        var _this = this;
        console.log('' + this.helloMain);
        var lbl = this.helloMain.nativeElement;
        lbl.on(gestures_1.GestureTypes.swipe, zonedCallback(function (args) {
            if (args.direction === gestures_1.SwipeDirection.right) {
                console.log('from ' + _this.state + ' to secondary');
                _this.state = "secondary";
            }
            else if (args.direction === gestures_1.SwipeDirection.left) {
                console.log('from ' + _this.state + ' to main');
                _this.state = "main";
            }
        }));
    };
    __decorate([
        core_1.ViewChild("mainContainer"), 
        __metadata('design:type', core_1.ElementRef)
    ], LoginTestComponent.prototype, "helloMain", void 0);
    LoginTestComponent = __decorate([
        core_1.Component({
            selector: "my-app",
            templateUrl: "pages/login/login-test.html",
        }), 
        __metadata('design:paramtypes', [router_1.Router, page_1.Page])
    ], LoginTestComponent);
    return LoginTestComponent;
}());
exports.LoginTestComponent = LoginTestComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4tdGVzdC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJsb2dpbi10ZXN0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEscUJBQXVELGVBQWUsQ0FBQyxDQUFBO0FBQ3ZFLHVCQUFxQixpQkFBaUIsQ0FBQyxDQUFBO0FBQ3ZDLHFCQUFtQixTQUFTLENBQUMsQ0FBQTtBQUM3Qix5QkFBMkMsYUFBYSxDQUFDLENBQUE7QUFTekQ7SUFJRSw0QkFBb0IsT0FBZSxFQUN6QixJQUFVO1FBREEsWUFBTyxHQUFQLE9BQU8sQ0FBUTtRQUN6QixTQUFJLEdBQUosSUFBSSxDQUFNO1FBSnBCLFVBQUssR0FBRyxNQUFNLENBQUM7UUFNWDs7Ozs7Ozs7OztjQVVNO0lBQ1IsQ0FBQztJQUVELGlDQUFJLEdBQUo7UUFBQSxpQkFZQztRQVhMLE9BQU8sQ0FBQyxHQUFHLENBQUMsRUFBRSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUNqQyxJQUFJLEdBQUcsR0FBVSxJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQztRQUM5QyxHQUFHLENBQUMsRUFBRSxDQUFDLHVCQUFZLENBQUMsS0FBSyxFQUFFLGFBQWEsQ0FBQyxVQUFDLElBQUk7WUFDdEMsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLFNBQVMsS0FBSyx5QkFBYyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7Z0JBQ3pDLE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxHQUFHLEtBQUksQ0FBQyxLQUFLLEdBQUcsZUFBZSxDQUFDLENBQUM7Z0JBQ3BELEtBQUksQ0FBQyxLQUFLLEdBQUcsV0FBVyxDQUFDO1lBQzdCLENBQUM7WUFBQyxJQUFJLENBQUMsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLFNBQVMsS0FBSyx5QkFBYyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7Z0JBQy9DLE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxHQUFHLEtBQUksQ0FBQyxLQUFLLEdBQUcsVUFBVSxDQUFDLENBQUM7Z0JBQy9DLEtBQUksQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDO1lBQ3hCLENBQUM7UUFDSCxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ04sQ0FBQztJQTlCSDtRQUFDLGdCQUFTLENBQUMsZUFBZSxDQUFDOzt5REFBQTtJQU43QjtRQUFDLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsUUFBUTtZQUNsQixXQUFXLEVBQUUsNkJBQTZCO1NBQzNDLENBQUM7OzBCQUFBO0lBa0NGLHlCQUFDO0FBQUQsQ0FBQyxBQWpDRCxJQWlDQztBQWpDWSwwQkFBa0IscUJBaUM5QixDQUFBIn0=