import {Component, ElementRef, OnInit, ViewChild} from "@angular/core";
import {Router} from "@angular/router";
import {Color} from "color";
import {connectionType, getConnectionType} from "connectivity";
import {Animation} from "ui/animation";
import {View} from "ui/core/view";
import {prompt} from "ui/dialogs";
import {Page} from "ui/page";
import {TextField} from "ui/text-field";
import {SignInInfo} from '../../shared/auth/sign-in'
import {RegisterInfo} from '../../shared/auth/register'
import {AuthenticationService} from "../../shared/auth/authentication.service";
import {setHintColor} from "../../utils/hint-util";
import {alert} from "../../utils/dialog-util";
var FacebookLoginHandler = require("nativescript-facebook-login");
import {topmost} from "ui/frame";

import TranslationPipe from '../../shared/pipes/translate.pipe';
import Translation from '../../shared/translation';
import Config from '../../shared/config';

declare var zonedCallback: Function;

@Component({
  selector: "my-app",
  providers: [AuthenticationService],
  templateUrl: "pages/login/login.html",
  styleUrls: ["pages/login/login-common.css", "pages/login/login.css"],
  pipes: [TranslationPipe]
})
export class LoginPageComponent implements OnInit {
  isLoggingIn = true;
  isAuthenticating = false;
  selectedState = 'main';
  signInInfo: SignInInfo;
  registerInfo: RegisterInfo;
  selectedIndex = 0;
  positions: Array<string>;
  translation: Translation;


  @ViewChild("joinContainer") joinContainer: ElementRef;
  @ViewChild("mainContainer") mainContainer: ElementRef;

  constructor(private _router: Router,
    private _authService: AuthenticationService,
    private page: Page,
    private _translation: Translation) {
    this.signInInfo = new SignInInfo("yo@yo.com", "123");
    this.registerInfo = new RegisterInfo('', '', '', '', 0);
    this.translation = _translation;

    // initialize the swipe event - cast swipe to string
    page.on('swipe', zonedCallback((args) => {
      if (args.direction === 1) {
        // right
        // if we swipe right, go back to the main if we're in register/signin pages
        if (this.selectedState == 'register' || this.selectedState == 'signin') {
          this.selectedState = 'main';
        }
      }
    }));

    this.initPositions();
  }

  initPositions() {
    this.positions = [
      this.translation.translate('None'),
      this.translation.translate('Keeper'),
      this.translation.translate('Defender'),
      this.translation.translate('Midfielder'),
      this.translation.translate('Attacker')
    ];
  }

  ngOnInit() {
    this.page.actionBarHidden = true;
  }

  public onchange(selected) {
    console.log("selected index " + selected);
  }

  setLang(lang) {
    //console.log('lang = ' + lang);
    this._translation.setLang(lang);
    // update the values of the positions dropdown
    this.initPositions();
  }

  showContainer(selection) {
    this.selectedState = selection;
  }

  joinNow() {
    //alert('join now');
    this.showContainer('register');
  }

  signIn() {
    this.showContainer('signin');
  }

  onSignUp() {
    console.log('signing up with: ' + JSON.stringify(this.registerInfo));
    this._authService.register(this.registerInfo)
      .then((user) => {
        // navigate
        alert('hello after register');
      }).catch((e) => {
        alert('error occured.' + e);
      });;
  }

  onSignIn() {
    // login here...
    //alert('sign in: ' + JSON.stringify(this.signInInfo));
    this._authService.login(this.signInInfo)
    .then(() => {
      this._router.navigate([""]);
    }).catch((e) => {
      alert('error occured. ' + e);
    });;
  }

  onFacebookLogin() {
    console.log('on facebook login');
    //Here we select the login behaviour

    //Recomended system account with native fallback for iOS
    if (topmost().ios) {
      console.log('is ios');
      FacebookLoginHandler.init(2);
    }
    //Recomended default for android 
    else if (topmost().android) {
      FacebookLoginHandler.init();
    }

    var successCallback = function (result) {
      //Do something with the result, for example get the AccessToken
      var token;
      if (topmost().android) {
        token = result.getAccessToken().getToken();
      }
      else if (topmost().ios) {
        token = result.token.tokenString;
        console.log('full result: ' + JSON.stringify(result));
      }

      console.log('Received token: ' + token);
    }

    var cancelCallback = function () {
      alert("Login was cancelled");
    }

    var failCallback = function (error) {
      var errorMessage = "Error with Facebook";
      //Try to get as much information as possible from error
      if (error) {
        if (topmost().ios) {
          if (error.localizedDescription) {
            errorMessage += ": " + error.localizedDescription;
          }
          else if (error.code) {
            errorMessage += ": Code " + error.code;
          }
          else {
            errorMessage += ": " + error;
          }
        }
        else if (topmost().android) {
          if (error.getErrorMessage) {
            errorMessage += ": " + error.getErrorMessage();
          }
          else if (error.getErrorCode) {
            errorMessage += ": Code " + error.getErrorCode();
          }
          else {
            errorMessage += ": " + error;
          }
        }
      }
      alert(errorMessage);
    }
    //Register our callbacks
    FacebookLoginHandler.registerCallback(successCallback, cancelCallback, failCallback);
    
    //Start the login process
    FacebookLoginHandler.logInWithPublishPermissions(["publish_actions"]);
  }

  onGoogleLogin() {
    alert('google login');
  }
}
