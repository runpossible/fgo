import {Component, ElementRef, OnInit, ViewChild} from "@angular/core";
import {Router} from "@angular/router";
import {Page} from "ui/page";
import {GestureTypes, SwipeDirection} from "ui/gestures";
import {Label} from "ui/label";

declare var zonedCallback: Function;

@Component({
  selector: "my-app",
  templateUrl: "pages/login/login-test.html",
})
export class LoginTestComponent {
  state = "main";
  @ViewChild("mainContainer") helloMain: ElementRef;

  constructor(private _router: Router,
    private page: Page) {
      
      /*
      let lbl = <Label>this.helloMain.nativeElement;
      lbl.on('swipe', zonedCallback((args) => {
        if(args.direction === SwipeDirection.right) {
            console.log('from ' + this.state + ' to secondary');
            this.state = "secondary";
        } else if(args.direction === SwipeDirection.left) {
            console.log('from ' + this.state + ' to main');
            this.state = "main";
        }
      }));*/
    }

    test() {
console.log('' + this.helloMain);
let lbl = <Label>this.helloMain.nativeElement;
lbl.on(GestureTypes.swipe, zonedCallback((args) => {
        if(args.direction === SwipeDirection.right) {
            console.log('from ' + this.state + ' to secondary');
            this.state = "secondary";
        } else if(args.direction === SwipeDirection.left) {
            console.log('from ' + this.state + ' to main');
            this.state = "main";
        }
      }));
    }
}