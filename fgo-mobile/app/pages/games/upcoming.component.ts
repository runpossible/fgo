import {Component, ElementRef, OnInit, ViewChild} from "@angular/core";
import {Router} from "@angular/router";
import {Page} from "ui/page";
import {GestureTypes, SwipeDirection} from "ui/gestures";
import {Label} from "ui/label";
import Config from '../../shared/config';
import UpcomingGame from '../../shared/game/upcomingGame';
import GameInvite from '../../shared/game/game-invite';
import GroupInvite from '../../shared/game/group-invite';

var http = require("http");

declare var zonedCallback: Function;

@Component({
    selector: "my-app",
    templateUrl: "pages/games/upcoming.html",
})
export class UpcomingComponent {
    state = "main";
    upcoming: UpcomingGame;
    gameInvites: Array<GameInvite> = [];
    groupInvites: Array<GroupInvite> = [];

    constructor(private _router: Router,
        private page: Page) {
        this.loadUpcomingGame().then(zonedCallback((upcomingGame) => {
            //console.log('in zoned callback with upcoming game: ' + JSON.stringify(upcomingGame));
            this.upcoming = upcomingGame;
        }));

        console.log('loading the invites...');
        this.loadInvites().then(zonedCallback(() => {
            console.log('loaded the invites...');
        }));
    }

    loadUpcomingGame() {
        return new Promise((resolve, reject) => {
            var url = Config.apiBearerUrl + '/games/upcoming';
            //console.log('url ' + url);
            http.request({
                url: url,
                method: "GET",
                headers: {
                    'Authorization': Config.bearerAuthorization
                }
            }).then(function (response) {
                var content = JSON.parse(response.content);
                //console.log('got next game: ' + response.content);
                return resolve(new UpcomingGame(content.numberOfPlayers, content.locationName, content.date_time));

            }).catch((e) => {
                console.log('an error has occured.');
                reject(e);
            });
        });
    }

    loadInvites() {
        var that = this;
        return new Promise((resolve, reject) => {
            var url = Config.apiBearerUrl + '/invite';
            console.log('url ' + url);
            http.request({
                url: url,
                method: 'GET',
                headers: {
                    'Authorization': Config.bearerAuthorization
                }
            }).then(function (response) {
                var content = JSON.parse(response.content);
                //console.log('got invites: ' + response.content);
                for(let invite of content) {
                    if(invite.game) {
                        that.gameInvites.push(new GameInvite(invite.createdBy.name,
                            invite.createdBy.email, 
                            invite.game._id, 
                            invite.game.name, 
                            invite.game.date_time,
                            invite.game.locationName)); 
                    } else if(invite.group) {
                        that.groupInvites.push(new GroupInvite(invite.createdBy.name,
                            invite.createdBy.email,
                            invite.group._id,
                            invite.group.name))
                    }
                }
                //console.log('game invites = ' + JSON.stringify(that.gameInvites));
                return resolve();
            }).catch((e) => {
                console.log('an error has occured ' + e);
                reject(e);
            })
        });
    }
}