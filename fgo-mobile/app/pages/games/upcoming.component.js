"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var page_1 = require("ui/page");
var config_1 = require('../../shared/config');
var upcomingGame_1 = require('../../shared/game/upcomingGame');
var game_invite_1 = require('../../shared/game/game-invite');
var group_invite_1 = require('../../shared/game/group-invite');
var http = require("http");
var UpcomingComponent = (function () {
    function UpcomingComponent(_router, page) {
        var _this = this;
        this._router = _router;
        this.page = page;
        this.state = "main";
        this.gameInvites = [];
        this.groupInvites = [];
        this.loadUpcomingGame().then(zonedCallback(function (upcomingGame) {
            //console.log('in zoned callback with upcoming game: ' + JSON.stringify(upcomingGame));
            _this.upcoming = upcomingGame;
        }));
        console.log('loading the invites...');
        this.loadInvites().then(zonedCallback(function () {
            console.log('loaded the invites...');
        }));
    }
    UpcomingComponent.prototype.loadUpcomingGame = function () {
        return new Promise(function (resolve, reject) {
            var url = config_1.default.apiBearerUrl + '/games/upcoming';
            //console.log('url ' + url);
            http.request({
                url: url,
                method: "GET",
                headers: {
                    'Authorization': config_1.default.bearerAuthorization
                }
            }).then(function (response) {
                var content = JSON.parse(response.content);
                //console.log('got next game: ' + response.content);
                return resolve(new upcomingGame_1.default(content.numberOfPlayers, content.locationName, content.date_time));
            }).catch(function (e) {
                console.log('an error has occured.');
                reject(e);
            });
        });
    };
    UpcomingComponent.prototype.loadInvites = function () {
        var that = this;
        return new Promise(function (resolve, reject) {
            var url = config_1.default.apiBearerUrl + '/invite';
            console.log('url ' + url);
            http.request({
                url: url,
                method: 'GET',
                headers: {
                    'Authorization': config_1.default.bearerAuthorization
                }
            }).then(function (response) {
                var content = JSON.parse(response.content);
                //console.log('got invites: ' + response.content);
                for (var _i = 0, content_1 = content; _i < content_1.length; _i++) {
                    var invite = content_1[_i];
                    if (invite.game) {
                        that.gameInvites.push(new game_invite_1.default(invite.createdBy.name, invite.createdBy.email, invite.game._id, invite.game.name, invite.game.date_time, invite.game.locationName));
                    }
                    else if (invite.group) {
                        that.groupInvites.push(new group_invite_1.default(invite.createdBy.name, invite.createdBy.email, invite.group._id, invite.group.name));
                    }
                }
                //console.log('game invites = ' + JSON.stringify(that.gameInvites));
                return resolve();
            }).catch(function (e) {
                console.log('an error has occured ' + e);
                reject(e);
            });
        });
    };
    UpcomingComponent = __decorate([
        core_1.Component({
            selector: "my-app",
            templateUrl: "pages/games/upcoming.html",
        }), 
        __metadata('design:paramtypes', [router_1.Router, page_1.Page])
    ], UpcomingComponent);
    return UpcomingComponent;
}());
exports.UpcomingComponent = UpcomingComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXBjb21pbmcuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsidXBjb21pbmcuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBdUQsZUFBZSxDQUFDLENBQUE7QUFDdkUsdUJBQXFCLGlCQUFpQixDQUFDLENBQUE7QUFDdkMscUJBQW1CLFNBQVMsQ0FBQyxDQUFBO0FBRzdCLHVCQUFtQixxQkFBcUIsQ0FBQyxDQUFBO0FBQ3pDLDZCQUF5QixnQ0FBZ0MsQ0FBQyxDQUFBO0FBQzFELDRCQUF1QiwrQkFBK0IsQ0FBQyxDQUFBO0FBQ3ZELDZCQUF3QixnQ0FBZ0MsQ0FBQyxDQUFBO0FBRXpELElBQUksSUFBSSxHQUFHLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQztBQVEzQjtJQU1JLDJCQUFvQixPQUFlLEVBQ3ZCLElBQVU7UUFQMUIsaUJBOEVDO1FBeEV1QixZQUFPLEdBQVAsT0FBTyxDQUFRO1FBQ3ZCLFNBQUksR0FBSixJQUFJLENBQU07UUFOdEIsVUFBSyxHQUFHLE1BQU0sQ0FBQztRQUVmLGdCQUFXLEdBQXNCLEVBQUUsQ0FBQztRQUNwQyxpQkFBWSxHQUF1QixFQUFFLENBQUM7UUFJbEMsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxVQUFDLFlBQVk7WUFDcEQsdUZBQXVGO1lBQ3ZGLEtBQUksQ0FBQyxRQUFRLEdBQUcsWUFBWSxDQUFDO1FBQ2pDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFFSixPQUFPLENBQUMsR0FBRyxDQUFDLHdCQUF3QixDQUFDLENBQUM7UUFDdEMsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUM7WUFDbEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDO1FBQ3pDLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDUixDQUFDO0lBRUQsNENBQWdCLEdBQWhCO1FBQ0ksTUFBTSxDQUFDLElBQUksT0FBTyxDQUFDLFVBQUMsT0FBTyxFQUFFLE1BQU07WUFDL0IsSUFBSSxHQUFHLEdBQUcsZ0JBQU0sQ0FBQyxZQUFZLEdBQUcsaUJBQWlCLENBQUM7WUFDbEQsNEJBQTRCO1lBQzVCLElBQUksQ0FBQyxPQUFPLENBQUM7Z0JBQ1QsR0FBRyxFQUFFLEdBQUc7Z0JBQ1IsTUFBTSxFQUFFLEtBQUs7Z0JBQ2IsT0FBTyxFQUFFO29CQUNMLGVBQWUsRUFBRSxnQkFBTSxDQUFDLG1CQUFtQjtpQkFDOUM7YUFDSixDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsUUFBUTtnQkFDdEIsSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQzNDLG9EQUFvRDtnQkFDcEQsTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLHNCQUFZLENBQUMsT0FBTyxDQUFDLGVBQWUsRUFBRSxPQUFPLENBQUMsWUFBWSxFQUFFLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBRXZHLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFDLENBQUM7Z0JBQ1AsT0FBTyxDQUFDLEdBQUcsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDO2dCQUNyQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDZCxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELHVDQUFXLEdBQVg7UUFDSSxJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7UUFDaEIsTUFBTSxDQUFDLElBQUksT0FBTyxDQUFDLFVBQUMsT0FBTyxFQUFFLE1BQU07WUFDL0IsSUFBSSxHQUFHLEdBQUcsZ0JBQU0sQ0FBQyxZQUFZLEdBQUcsU0FBUyxDQUFDO1lBQzFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxHQUFHLEdBQUcsQ0FBQyxDQUFDO1lBQzFCLElBQUksQ0FBQyxPQUFPLENBQUM7Z0JBQ1QsR0FBRyxFQUFFLEdBQUc7Z0JBQ1IsTUFBTSxFQUFFLEtBQUs7Z0JBQ2IsT0FBTyxFQUFFO29CQUNMLGVBQWUsRUFBRSxnQkFBTSxDQUFDLG1CQUFtQjtpQkFDOUM7YUFDSixDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsUUFBUTtnQkFDdEIsSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQzNDLGtEQUFrRDtnQkFDbEQsR0FBRyxDQUFBLENBQWUsVUFBTyxFQUFQLG1CQUFPLEVBQVAscUJBQU8sRUFBUCxJQUFPLENBQUM7b0JBQXRCLElBQUksTUFBTSxnQkFBQTtvQkFDVixFQUFFLENBQUEsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQzt3QkFDYixJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLHFCQUFVLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxJQUFJLEVBQ3RELE1BQU0sQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUN0QixNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFDZixNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksRUFDaEIsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQ3JCLE1BQU0sQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQztvQkFDbkMsQ0FBQztvQkFBQyxJQUFJLENBQUMsRUFBRSxDQUFBLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7d0JBQ3JCLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLElBQUksc0JBQVcsQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLElBQUksRUFDeEQsTUFBTSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQ3RCLE1BQU0sQ0FBQyxLQUFLLENBQUMsR0FBRyxFQUNoQixNQUFNLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUE7b0JBQzNCLENBQUM7aUJBQ0o7Z0JBQ0Qsb0VBQW9FO2dCQUNwRSxNQUFNLENBQUMsT0FBTyxFQUFFLENBQUM7WUFDckIsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQUMsQ0FBQztnQkFDUCxPQUFPLENBQUMsR0FBRyxDQUFDLHVCQUF1QixHQUFHLENBQUMsQ0FBQyxDQUFDO2dCQUN6QyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDZCxDQUFDLENBQUMsQ0FBQTtRQUNOLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQWpGTDtRQUFDLGdCQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsUUFBUTtZQUNsQixXQUFXLEVBQUUsMkJBQTJCO1NBQzNDLENBQUM7O3lCQUFBO0lBK0VGLHdCQUFDO0FBQUQsQ0FBQyxBQTlFRCxJQThFQztBQTlFWSx5QkFBaUIsb0JBOEU3QixDQUFBIn0=