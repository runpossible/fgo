import * as dialogsModule from "ui/dialogs";

export function alert(message: string) {
  return dialogsModule.alert({
    title: "FGO IO",
    okButtonText: "OK",
    message: message
  });
}
