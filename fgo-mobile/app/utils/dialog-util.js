"use strict";
var dialogsModule = require("ui/dialogs");
function alert(message) {
    return dialogsModule.alert({
        title: "FGO IO",
        okButtonText: "OK",
        message: message
    });
}
exports.alert = alert;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGlhbG9nLXV0aWwuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJkaWFsb2ctdXRpbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsSUFBWSxhQUFhLFdBQU0sWUFBWSxDQUFDLENBQUE7QUFFNUMsZUFBc0IsT0FBZTtJQUNuQyxNQUFNLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQztRQUN6QixLQUFLLEVBQUUsUUFBUTtRQUNmLFlBQVksRUFBRSxJQUFJO1FBQ2xCLE9BQU8sRUFBRSxPQUFPO0tBQ2pCLENBQUMsQ0FBQztBQUNMLENBQUM7QUFOZSxhQUFLLFFBTXBCLENBQUEifQ==