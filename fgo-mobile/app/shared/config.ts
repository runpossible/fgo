import {getString, setString} from "application-settings";
import {connectionType, getConnectionType, startMonitoring} from "connectivity";
import {User} from "./user/user";

// https://www.typescriptlang.org/docs/handbook/classes.html
export default class Config {
  static hostUrl() {
    return 'http://localhost:3000';
  }

  static get apiBearerUrl() {
    return "http://localhost:3000/api/v1/bearer";
  }

  static get bearerAuthorization():string {
    return 'Bearer ' + this.token;
  }

  static apiUrl() {
    return 'http://localhost:3000/api/v1';
  }

  static currentUser() : User {
    var userString = getString("user");
    var userObj = JSON.parse(userString);
    var usr = new User();

    usr.email = userObj.username;
    return usr;
  }

  static set user(user: string) {
    setString("user", user);
  }
  static hasActiveToken() {
    return !!getString("token");
  }
  static set token(token: string) {
    setString("token", token);
  }
  static get token():string {
    return getString("token");
  }

  static invalidateUser() {
    Config.user = "";
    Config.token = "";
  }
}
