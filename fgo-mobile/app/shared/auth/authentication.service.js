"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var config_1 = require("../config");
var http = require("http");
var AuthenticationService = (function () {
    function AuthenticationService() {
    }
    AuthenticationService.prototype.login = function (signInInfo) {
        console.log('start logging in.');
        return new Promise(function (resolve, reject) {
            var loginUrl = config_1.default.apiUrl() + "/auth/login-access-token";
            console.log('logging to : ' + loginUrl);
            http.request({
                url: loginUrl,
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                content: JSON.stringify({
                    username: signInInfo.email,
                    password: signInInfo.password
                })
            }).then(function (response) {
                //console.log('got response ' + response.content);
                //console.log('full response ' + JSON.stringify(response));
                var content = JSON.parse(response.content);
                if (content.success) {
                    // set the user in the Application here.
                    config_1.default.user = JSON.stringify(content.user);
                    config_1.default.token = content.accessToken;
                    console.log('sucessfully logged in.');
                    return resolve(content.user);
                }
                else {
                    console.log('success is false);');
                    return reject("Login failed.");
                }
            }).catch(function (e) {
                console.log('an error has occured.');
                reject(e);
            });
        });
    };
    AuthenticationService.prototype.register = function (registerInfo) {
        return new Promise(function (resolve, reject) {
            http.request({
                url: config_1.default.apiUrl + 'auth/register',
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                content: JSON.stringify({
                    email: registerInfo.email,
                    name: registerInfo.fullMame,
                    password: registerInfo.password,
                    language: 'bg',
                    preferredPosition: registerInfo.preferredPosition
                })
            }).then(function (response) {
                //alert('got response: ' + response);
                if (typeof response === 'object') {
                    console.log('we`ve got response: ' + JSON.stringify(response));
                }
                else {
                    console.log('response string: ' + response);
                }
                var content = JSON.parse(response.content);
                if (content.success) {
                    // set the user in the Application here.
                    config_1.default.user = JSON.stringify(content.user);
                    console.log('current user = ' + config_1.default.currentUser());
                    return resolve(config_1.default.currentUser());
                }
                else {
                    return reject('log-in has failed');
                }
            }).catch(function (e) { return reject(e); });
        });
    };
    AuthenticationService.prototype.handleErrors = function (error) {
        console.log(JSON.stringify(error));
        return Promise.reject(error.message);
    };
    AuthenticationService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], AuthenticationService);
    return AuthenticationService;
}());
exports.AuthenticationService = AuthenticationService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aGVudGljYXRpb24uc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImF1dGhlbnRpY2F0aW9uLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLHFCQUF5QixlQUFlLENBQUMsQ0FBQTtBQUN6Qyx1QkFBbUIsV0FBVyxDQUFDLENBQUE7QUFJL0IsSUFBSSxJQUFJLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0FBRzNCO0lBQUE7SUErRUEsQ0FBQztJQTlFQyxxQ0FBSyxHQUFMLFVBQU0sVUFBc0I7UUFDeEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO1FBQ2pDLE1BQU0sQ0FBQyxJQUFJLE9BQU8sQ0FBQyxVQUFDLE9BQU8sRUFBRSxNQUFNO1lBQ2pDLElBQUksUUFBUSxHQUFHLGdCQUFNLENBQUMsTUFBTSxFQUFFLEdBQUcsMEJBQTBCLENBQUM7WUFDNUQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxlQUFlLEdBQUcsUUFBUSxDQUFDLENBQUM7WUFDeEMsSUFBSSxDQUFDLE9BQU8sQ0FBQztnQkFDVCxHQUFHLEVBQUUsUUFBUTtnQkFDYixNQUFNLEVBQUUsTUFBTTtnQkFDZCxPQUFPLEVBQUU7b0JBQ0wsY0FBYyxFQUFFLGtCQUFrQjtpQkFDckM7Z0JBQ0QsT0FBTyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUM7b0JBQ3BCLFFBQVEsRUFBRSxVQUFVLENBQUMsS0FBSztvQkFDMUIsUUFBUSxFQUFFLFVBQVUsQ0FBQyxRQUFRO2lCQUNoQyxDQUFDO2FBQ0wsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFTLFFBQVE7Z0JBQ3JCLGtEQUFrRDtnQkFDbEQsMkRBQTJEO2dCQUMzRCxJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQztnQkFDM0MsRUFBRSxDQUFBLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7b0JBQ2pCLHdDQUF3QztvQkFDeEMsZ0JBQU0sQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQzNDLGdCQUFNLENBQUMsS0FBSyxHQUFHLE9BQU8sQ0FBQyxXQUFXLENBQUM7b0JBQ25DLE9BQU8sQ0FBQyxHQUFHLENBQUMsd0JBQXdCLENBQUMsQ0FBQztvQkFDdEMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ2pDLENBQUM7Z0JBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ0osT0FBTyxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFBO29CQUNqQyxNQUFNLENBQUMsTUFBTSxDQUFDLGVBQWUsQ0FBQyxDQUFDO2dCQUNuQyxDQUFDO1lBRUwsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQUMsQ0FBQztnQkFDUCxPQUFPLENBQUMsR0FBRyxDQUFDLHVCQUF1QixDQUFDLENBQUM7Z0JBQ3JDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNkLENBQUMsQ0FBQyxDQUFDO1FBQ0wsQ0FBQyxDQUFDLENBQUE7SUFDTixDQUFDO0lBRUQsd0NBQVEsR0FBUixVQUFTLFlBQTBCO1FBQy9CLE1BQU0sQ0FBQyxJQUFJLE9BQU8sQ0FBQyxVQUFDLE9BQU8sRUFBRSxNQUFNO1lBQzdCLElBQUksQ0FBQyxPQUFPLENBQUM7Z0JBQ0wsR0FBRyxFQUFFLGdCQUFNLENBQUMsTUFBTSxHQUFHLGVBQWU7Z0JBQ3BDLE1BQU0sRUFBRSxNQUFNO2dCQUNkLE9BQU8sRUFBRTtvQkFDTCxjQUFjLEVBQUUsa0JBQWtCO2lCQUNyQztnQkFDRCxPQUFPLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQztvQkFDcEIsS0FBSyxFQUFFLFlBQVksQ0FBQyxLQUFLO29CQUN6QixJQUFJLEVBQUUsWUFBWSxDQUFDLFFBQVE7b0JBQzNCLFFBQVEsRUFBRSxZQUFZLENBQUMsUUFBUTtvQkFDL0IsUUFBUSxFQUFFLElBQUk7b0JBQ2QsaUJBQWlCLEVBQUUsWUFBWSxDQUFDLGlCQUFpQjtpQkFDcEQsQ0FBQzthQUNMLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBUyxRQUFRO2dCQUNyQixxQ0FBcUM7Z0JBQ3JDLEVBQUUsQ0FBQSxDQUFDLE9BQU8sUUFBUSxLQUFLLFFBQVEsQ0FBQyxDQUFDLENBQUM7b0JBQzlCLE9BQU8sQ0FBQyxHQUFHLENBQUMsc0JBQXNCLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO2dCQUNuRSxDQUFDO2dCQUFDLElBQUksQ0FBQyxDQUFDO29CQUNKLE9BQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLEdBQUcsUUFBUSxDQUFDLENBQUM7Z0JBQ2hELENBQUM7Z0JBQ0QsSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQzNDLEVBQUUsQ0FBQSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO29CQUNqQix3Q0FBd0M7b0JBQ3hDLGdCQUFNLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUMzQyxPQUFPLENBQUMsR0FBRyxDQUFDLGlCQUFpQixHQUFHLGdCQUFNLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQztvQkFDdEQsTUFBTSxDQUFDLE9BQU8sQ0FBQyxnQkFBTSxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUM7Z0JBQ3pDLENBQUM7Z0JBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ0osTUFBTSxDQUFDLE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO2dCQUN2QyxDQUFDO1lBRUwsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQUMsQ0FBQyxJQUFLLE9BQUEsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFULENBQVMsQ0FBQyxDQUFDO1FBQ3JDLENBQUMsQ0FBQyxDQUFDO0lBRVAsQ0FBQztJQUVELDRDQUFZLEdBQVosVUFBYSxLQUFLO1FBQ2hCLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO1FBQ25DLE1BQU0sQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUN2QyxDQUFDO0lBL0VIO1FBQUMsaUJBQVUsRUFBRTs7NkJBQUE7SUFnRmIsNEJBQUM7QUFBRCxDQUFDLEFBL0VELElBK0VDO0FBL0VZLDZCQUFxQix3QkErRWpDLENBQUEifQ==