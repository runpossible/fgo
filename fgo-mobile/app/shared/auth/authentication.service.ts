import {Injectable} from "@angular/core";
import Config from "../config";
import {SignInInfo} from "./sign-in"
import {RegisterInfo} from "./register"
import {alert} from "../../utils/dialog-util";
var http = require("http");

@Injectable()
export class AuthenticationService {
  login(signInInfo: SignInInfo) { 
      console.log('start logging in.');
      return new Promise((resolve, reject) => {
        var loginUrl = Config.apiUrl() + "/auth/login-access-token";
        console.log('logging to : ' + loginUrl);
        http.request({
            url: loginUrl,
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            content: JSON.stringify({
                username: signInInfo.email,
                password: signInInfo.password
            })
        }).then(function(response) {
            //console.log('got response ' + response.content);
            //console.log('full response ' + JSON.stringify(response));
            var content = JSON.parse(response.content);
            if(content.success) {
                // set the user in the Application here.
                Config.user = JSON.stringify(content.user);
                Config.token = content.accessToken;
                console.log('sucessfully logged in.');
                return resolve(content.user); 
            } else {
                console.log('success is false);')
                return reject("Login failed.");
            }
            
        }).catch((e) => {
            console.log('an error has occured.');
            reject(e);
        });
      })
  }

  register(registerInfo: RegisterInfo) {
      return new Promise((resolve, reject) => {
            http.request({
                    url: Config.apiUrl + 'auth/register',
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    content: JSON.stringify({
                        email: registerInfo.email,
                        name: registerInfo.fullMame,
                        password: registerInfo.password,
                        language: 'bg', // TODO: Fix this
                        preferredPosition: registerInfo.preferredPosition
                    }) 
                }).then(function(response) {
                    //alert('got response: ' + response);
                    if(typeof response === 'object') {
                        console.log('we`ve got response: ' + JSON.stringify(response));
                    } else {
                        console.log('response string: ' + response);
                    }
                    var content = JSON.parse(response.content);
                    if(content.success) {
                        // set the user in the Application here.
                        Config.user = JSON.stringify(content.user);
                        console.log('current user = ' + Config.currentUser());
                        return resolve(Config.currentUser());
                    } else {
                        return reject('log-in has failed');
                    }
                    
                }).catch((e) => reject(e));
      });
      
  }

  handleErrors(error) {
    console.log(JSON.stringify(error));
    return Promise.reject(error.message);
  }
}