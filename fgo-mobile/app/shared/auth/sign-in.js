"use strict";
var SignInInfo = (function () {
    function SignInInfo(email, password) {
        if (email === void 0) { email = ""; }
        if (password === void 0) { password = ""; }
        this.email = email;
        this.password = password;
    }
    return SignInInfo;
}());
exports.SignInInfo = SignInInfo;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2lnbi1pbi5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInNpZ24taW4udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBO0lBQ0Usb0JBQW1CLEtBQWtCLEVBQVMsUUFBcUI7UUFBdkQscUJBQXlCLEdBQXpCLFVBQXlCO1FBQUUsd0JBQTRCLEdBQTVCLGFBQTRCO1FBQWhELFVBQUssR0FBTCxLQUFLLENBQWE7UUFBUyxhQUFRLEdBQVIsUUFBUSxDQUFhO0lBRW5FLENBQUM7SUFDSCxpQkFBQztBQUFELENBQUMsQUFKRCxJQUlDO0FBSlksa0JBQVUsYUFJdEIsQ0FBQSJ9