"use strict";
var RegisterInfo = (function () {
    function RegisterInfo(email, password, confirmPassword, fullMame, preferredPosition) {
        if (email === void 0) { email = ""; }
        if (password === void 0) { password = ""; }
        if (confirmPassword === void 0) { confirmPassword = ""; }
        if (fullMame === void 0) { fullMame = ""; }
        if (preferredPosition === void 0) { preferredPosition = 0; }
        this.email = email;
        this.password = password;
        this.confirmPassword = confirmPassword;
        this.fullMame = fullMame;
        this.preferredPosition = preferredPosition;
    }
    return RegisterInfo;
}());
exports.RegisterInfo = RegisterInfo;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVnaXN0ZXIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJyZWdpc3Rlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUE7SUFDRSxzQkFBbUIsS0FBa0IsRUFDNUIsUUFBcUIsRUFDckIsZUFBNEIsRUFDNUIsUUFBcUIsRUFDckIsaUJBQTZCO1FBSjFCLHFCQUF5QixHQUF6QixVQUF5QjtRQUNuQyx3QkFBNEIsR0FBNUIsYUFBNEI7UUFDNUIsK0JBQW1DLEdBQW5DLG9CQUFtQztRQUNuQyx3QkFBNEIsR0FBNUIsYUFBNEI7UUFDNUIsaUNBQW9DLEdBQXBDLHFCQUFvQztRQUpuQixVQUFLLEdBQUwsS0FBSyxDQUFhO1FBQzVCLGFBQVEsR0FBUixRQUFRLENBQWE7UUFDckIsb0JBQWUsR0FBZixlQUFlLENBQWE7UUFDNUIsYUFBUSxHQUFSLFFBQVEsQ0FBYTtRQUNyQixzQkFBaUIsR0FBakIsaUJBQWlCLENBQVk7SUFFdEMsQ0FBQztJQUNILG1CQUFDO0FBQUQsQ0FBQyxBQVJELElBUUM7QUFSWSxvQkFBWSxlQVF4QixDQUFBIn0=