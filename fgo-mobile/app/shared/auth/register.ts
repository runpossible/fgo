export class RegisterInfo {
  constructor(public email: string = "", 
    public password: string = "",
    public confirmPassword: string = "",
    public fullMame: string = "",
    public preferredPosition: number = 0) {

  }
}