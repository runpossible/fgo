import {Injectable} from '@angular/core';
import Map from './map';

@Injectable()
export default class Translation {
  private bgMap: Map<string>;
  private enMap: Map<string>;
  private lang: string;
  
  constructor() {    
    this.bgMap = new Map<string>();
    this.enMap = new Map<string>();
    
    this.init();

    this.setLang('bg'); // set bg by default;
  }
  
  translate(value: string): string {
    if(this.lang === "bg") {
      return this.bgMap[value] || value;
    }
    return this.enMap[value] || value;
  }

  setLang(lang: string) {
    //console.log('from ' + this.lang + ' to ' + lang);
    this.lang = lang;
    //console.log('lang = ' + this.lang);
  }
  
  init() {
    this.bgMap['JOIN NOW'] = 'РЕГИСТРИРАЙ СЕ';
    this.enMap['JOIN NOW'] = 'JOIN NOW';
    
    this.bgMap['BG'] = 'БГ';
    this.enMap['BG'] = 'BG';
    
    this.bgMap['EN'] = 'EN';
    this.enMap['EN'] = 'EN';
    
    this.bgMap['SIGN IN'] = 'ВЛЕЗ';
    this.enMap['SIGN IN'] = 'SIGN IN';

    this.bgMap['LOGIN'] = 'ВЛЕЗ';
    this.enMap['LOGIN'] = 'LOGIN';

    this.bgMap['OR USE SOCIAL LOGIN'] = 'ИЛИ ВЛЕЗТЕ С';
    this.enMap['OR USE SOCIAL LOGIN'] = 'OR USE SOCIAL LOGIN';

    this.bgMap['Password'] = 'Парола';
    this.bgMap['Confirm Password'] = 'Потвърди паролата';
    this.bgMap['Full Name'] = 'Име';
    this.bgMap['SIGN UP'] = 'Регистрирай се';

    this.bgMap['None'] = 'Няма';
    this.bgMap["Keeper"] = "Вратар";
    this.bgMap["Defender"] = "Защитник";
    this.bgMap["Midfielder"] = "Полузащитник";
    this.bgMap["Attacker"] = "Нападател";
  }
}
