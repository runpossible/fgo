"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var map_1 = require('./map');
var Translation = (function () {
    function Translation() {
        this.bgMap = new map_1.default();
        this.enMap = new map_1.default();
        this.init();
        this.setLang('bg'); // set bg by default;
    }
    Translation.prototype.translate = function (value) {
        if (this.lang === "bg") {
            return this.bgMap[value] || value;
        }
        return this.enMap[value] || value;
    };
    Translation.prototype.setLang = function (lang) {
        //console.log('from ' + this.lang + ' to ' + lang);
        this.lang = lang;
        //console.log('lang = ' + this.lang);
    };
    Translation.prototype.init = function () {
        this.bgMap['JOIN NOW'] = 'РЕГИСТРИРАЙ СЕ';
        this.enMap['JOIN NOW'] = 'JOIN NOW';
        this.bgMap['BG'] = 'БГ';
        this.enMap['BG'] = 'BG';
        this.bgMap['EN'] = 'EN';
        this.enMap['EN'] = 'EN';
        this.bgMap['SIGN IN'] = 'ВЛЕЗ';
        this.enMap['SIGN IN'] = 'SIGN IN';
        this.bgMap['LOGIN'] = 'ВЛЕЗ';
        this.enMap['LOGIN'] = 'LOGIN';
        this.bgMap['OR USE SOCIAL LOGIN'] = 'ИЛИ ВЛЕЗТЕ С';
        this.enMap['OR USE SOCIAL LOGIN'] = 'OR USE SOCIAL LOGIN';
        this.bgMap['Password'] = 'Парола';
        this.bgMap['Confirm Password'] = 'Потвърди паролата';
        this.bgMap['Full Name'] = 'Име';
        this.bgMap['SIGN UP'] = 'Регистрирай се';
        this.bgMap['None'] = 'Няма';
        this.bgMap["Keeper"] = "Вратар";
        this.bgMap["Defender"] = "Защитник";
        this.bgMap["Midfielder"] = "Полузащитник";
        this.bgMap["Attacker"] = "Нападател";
    };
    Translation = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], Translation);
    return Translation;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Translation;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidHJhbnNsYXRpb24uanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ0cmFuc2xhdGlvbi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEscUJBQXlCLGVBQWUsQ0FBQyxDQUFBO0FBQ3pDLG9CQUFnQixPQUFPLENBQUMsQ0FBQTtBQUd4QjtJQUtFO1FBQ0UsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLGFBQUcsRUFBVSxDQUFDO1FBQy9CLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxhQUFHLEVBQVUsQ0FBQztRQUUvQixJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7UUFFWixJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMscUJBQXFCO0lBQzNDLENBQUM7SUFFRCwrQkFBUyxHQUFULFVBQVUsS0FBYTtRQUNyQixFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsSUFBSSxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDdEIsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxDQUFDO1FBQ3BDLENBQUM7UUFDRCxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLENBQUM7SUFDcEMsQ0FBQztJQUVELDZCQUFPLEdBQVAsVUFBUSxJQUFZO1FBQ2xCLG1EQUFtRDtRQUNuRCxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztRQUNqQixxQ0FBcUM7SUFDdkMsQ0FBQztJQUVELDBCQUFJLEdBQUo7UUFDRSxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxHQUFHLGdCQUFnQixDQUFDO1FBQzFDLElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLEdBQUcsVUFBVSxDQUFDO1FBRXBDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDO1FBQ3hCLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDO1FBRXhCLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDO1FBQ3hCLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDO1FBRXhCLElBQUksQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLEdBQUcsTUFBTSxDQUFDO1FBQy9CLElBQUksQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLEdBQUcsU0FBUyxDQUFDO1FBRWxDLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLEdBQUcsTUFBTSxDQUFDO1FBQzdCLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLEdBQUcsT0FBTyxDQUFDO1FBRTlCLElBQUksQ0FBQyxLQUFLLENBQUMscUJBQXFCLENBQUMsR0FBRyxjQUFjLENBQUM7UUFDbkQsSUFBSSxDQUFDLEtBQUssQ0FBQyxxQkFBcUIsQ0FBQyxHQUFHLHFCQUFxQixDQUFDO1FBRTFELElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLEdBQUcsUUFBUSxDQUFDO1FBQ2xDLElBQUksQ0FBQyxLQUFLLENBQUMsa0JBQWtCLENBQUMsR0FBRyxtQkFBbUIsQ0FBQztRQUNyRCxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxHQUFHLEtBQUssQ0FBQztRQUNoQyxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxHQUFHLGdCQUFnQixDQUFDO1FBRXpDLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLEdBQUcsTUFBTSxDQUFDO1FBQzVCLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLEdBQUcsUUFBUSxDQUFDO1FBQ2hDLElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLEdBQUcsVUFBVSxDQUFDO1FBQ3BDLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLEdBQUcsY0FBYyxDQUFDO1FBQzFDLElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLEdBQUcsV0FBVyxDQUFDO0lBQ3ZDLENBQUM7SUF6REg7UUFBQyxpQkFBVSxFQUFFOzttQkFBQTtJQTBEYixrQkFBQztBQUFELENBQUMsQUF6REQsSUF5REM7QUF6REQ7NkJBeURDLENBQUEifQ==