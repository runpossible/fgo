"use strict";
var application_settings_1 = require("application-settings");
var user_1 = require("./user/user");
// https://www.typescriptlang.org/docs/handbook/classes.html
var Config = (function () {
    function Config() {
    }
    Config.hostUrl = function () {
        return 'http://localhost:3000';
    };
    Object.defineProperty(Config, "apiBearerUrl", {
        get: function () {
            return "http://localhost:3000/api/v1/bearer";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Config, "bearerAuthorization", {
        get: function () {
            return 'Bearer ' + this.token;
        },
        enumerable: true,
        configurable: true
    });
    Config.apiUrl = function () {
        return 'http://localhost:3000/api/v1';
    };
    Config.currentUser = function () {
        var userString = application_settings_1.getString("user");
        var userObj = JSON.parse(userString);
        var usr = new user_1.User();
        usr.email = userObj.username;
        return usr;
    };
    Object.defineProperty(Config, "user", {
        set: function (user) {
            application_settings_1.setString("user", user);
        },
        enumerable: true,
        configurable: true
    });
    Config.hasActiveToken = function () {
        return !!application_settings_1.getString("token");
    };
    Object.defineProperty(Config, "token", {
        get: function () {
            return application_settings_1.getString("token");
        },
        set: function (token) {
            application_settings_1.setString("token", token);
        },
        enumerable: true,
        configurable: true
    });
    Config.invalidateUser = function () {
        Config.user = "";
        Config.token = "";
    };
    return Config;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Config;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29uZmlnLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29uZmlnLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxxQ0FBbUMsc0JBQXNCLENBQUMsQ0FBQTtBQUUxRCxxQkFBbUIsYUFBYSxDQUFDLENBQUE7QUFFakMsNERBQTREO0FBQzVEO0lBQUE7SUEyQ0EsQ0FBQztJQTFDUSxjQUFPLEdBQWQ7UUFDRSxNQUFNLENBQUMsdUJBQXVCLENBQUM7SUFDakMsQ0FBQztJQUVELHNCQUFXLHNCQUFZO2FBQXZCO1lBQ0UsTUFBTSxDQUFDLHFDQUFxQyxDQUFDO1FBQy9DLENBQUM7OztPQUFBO0lBRUQsc0JBQVcsNkJBQW1CO2FBQTlCO1lBQ0UsTUFBTSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO1FBQ2hDLENBQUM7OztPQUFBO0lBRU0sYUFBTSxHQUFiO1FBQ0UsTUFBTSxDQUFDLDhCQUE4QixDQUFDO0lBQ3hDLENBQUM7SUFFTSxrQkFBVyxHQUFsQjtRQUNFLElBQUksVUFBVSxHQUFHLGdDQUFTLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDbkMsSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUNyQyxJQUFJLEdBQUcsR0FBRyxJQUFJLFdBQUksRUFBRSxDQUFDO1FBRXJCLEdBQUcsQ0FBQyxLQUFLLEdBQUcsT0FBTyxDQUFDLFFBQVEsQ0FBQztRQUM3QixNQUFNLENBQUMsR0FBRyxDQUFDO0lBQ2IsQ0FBQztJQUVELHNCQUFXLGNBQUk7YUFBZixVQUFnQixJQUFZO1lBQzFCLGdDQUFTLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQzFCLENBQUM7OztPQUFBO0lBQ00scUJBQWMsR0FBckI7UUFDRSxNQUFNLENBQUMsQ0FBQyxDQUFDLGdDQUFTLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDOUIsQ0FBQztJQUNELHNCQUFXLGVBQUs7YUFHaEI7WUFDRSxNQUFNLENBQUMsZ0NBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUM1QixDQUFDO2FBTEQsVUFBaUIsS0FBYTtZQUM1QixnQ0FBUyxDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUMsQ0FBQztRQUM1QixDQUFDOzs7T0FBQTtJQUtNLHFCQUFjLEdBQXJCO1FBQ0UsTUFBTSxDQUFDLElBQUksR0FBRyxFQUFFLENBQUM7UUFDakIsTUFBTSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7SUFDcEIsQ0FBQztJQUNILGFBQUM7QUFBRCxDQUFDLEFBM0NELElBMkNDO0FBM0NEO3dCQTJDQyxDQUFBIn0=