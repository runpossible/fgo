"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var translation_1 = require('../translation');
var TranslatePipe = (function () {
    function TranslatePipe(_translation) {
        this._translation = _translation;
        this.translations = _translation;
    }
    TranslatePipe.prototype.transform = function (value) {
        return this.translations.translate(value);
    };
    TranslatePipe = __decorate([
        core_1.Injectable(),
        core_1.Pipe({
            name: 'translate',
            pure: false
        }), 
        __metadata('design:paramtypes', [translation_1.default])
    ], TranslatePipe);
    return TranslatePipe;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = TranslatePipe;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidHJhbnNsYXRlLnBpcGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ0cmFuc2xhdGUucGlwZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEscUJBQWdELGVBQWUsQ0FBQyxDQUFBO0FBQ2hFLDRCQUF3QixnQkFBZ0IsQ0FBQyxDQUFBO0FBT3pDO0lBR0UsdUJBQW9CLFlBQXlCO1FBQXpCLGlCQUFZLEdBQVosWUFBWSxDQUFhO1FBQ3pDLElBQUksQ0FBQyxZQUFZLEdBQUcsWUFBWSxDQUFDO0lBQ3JDLENBQUM7SUFDRCxpQ0FBUyxHQUFULFVBQVUsS0FBYTtRQUNuQixNQUFNLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDOUMsQ0FBQztJQWJIO1FBQUMsaUJBQVUsRUFBRTtRQUNaLFdBQUksQ0FBQztZQUNGLElBQUksRUFBRSxXQUFXO1lBQ2pCLElBQUksRUFBRSxLQUFLO1NBQ2QsQ0FBQzs7cUJBQUE7SUFVRixvQkFBQztBQUFELENBQUMsQUFURCxJQVNDO0FBVEQ7K0JBU0MsQ0FBQSJ9