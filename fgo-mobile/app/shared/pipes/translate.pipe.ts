import { Pipe, PipeTransform, Injectable } from '@angular/core';
import Translation from '../translation';

@Injectable()
@Pipe({
    name: 'translate',
    pure: false
})
export default class TranslatePipe implements PipeTransform {
  private translations;
  
  constructor(private _translation: Translation) {
      this.translations = _translation;
  }
  transform(value: string): string { 
      return this.translations.translate(value);
  }
}
