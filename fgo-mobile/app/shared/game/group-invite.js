"use strict";
var GroupInvite = (function () {
    function GroupInvite(createdByName, createdByEmail, groupId, name) {
        if (createdByName === void 0) { createdByName = ""; }
        if (createdByEmail === void 0) { createdByEmail = ""; }
        this.createdByName = createdByName;
        this.createdByEmail = createdByEmail;
        this.groupId = groupId;
        this.name = name;
    }
    return GroupInvite;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = GroupInvite;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ3JvdXAtaW52aXRlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiZ3JvdXAtaW52aXRlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQTtJQUNFLHFCQUFtQixhQUEwQixFQUNwQyxjQUEyQixFQUMzQixPQUFlLEVBQ2YsSUFBWTtRQUhULDZCQUFpQyxHQUFqQyxrQkFBaUM7UUFDM0MsOEJBQWtDLEdBQWxDLG1CQUFrQztRQURqQixrQkFBYSxHQUFiLGFBQWEsQ0FBYTtRQUNwQyxtQkFBYyxHQUFkLGNBQWMsQ0FBYTtRQUMzQixZQUFPLEdBQVAsT0FBTyxDQUFRO1FBQ2YsU0FBSSxHQUFKLElBQUksQ0FBUTtJQUVyQixDQUFDO0lBQ0gsa0JBQUM7QUFBRCxDQUFDLEFBUEQsSUFPQztBQVBEOzZCQU9DLENBQUEifQ==