export default class GameInvite {
  constructor(public createdByName: string = "", 
    public createdByEmail: string = "", 
    public gameId: string,
    public gameName: string,
    public dateTime: string,
    public locationName: string) {
        
  }
}