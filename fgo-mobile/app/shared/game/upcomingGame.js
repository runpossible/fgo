"use strict";
var UpcomingGame = (function () {
    function UpcomingGame(numberOfPlayers, locationName, dateTime) {
        if (numberOfPlayers === void 0) { numberOfPlayers = 12; }
        this.numberOfPlayers = numberOfPlayers;
        this.locationName = locationName;
        this.dateTime = dateTime;
    }
    return UpcomingGame;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = UpcomingGame;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXBjb21pbmdHYW1lLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsidXBjb21pbmdHYW1lLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQTtJQUNFLHNCQUFtQixlQUE0QixFQUFTLFlBQW9CLEVBQVMsUUFBZ0I7UUFBekYsK0JBQW1DLEdBQW5DLG9CQUFtQztRQUE1QixvQkFBZSxHQUFmLGVBQWUsQ0FBYTtRQUFTLGlCQUFZLEdBQVosWUFBWSxDQUFRO1FBQVMsYUFBUSxHQUFSLFFBQVEsQ0FBUTtJQUVyRyxDQUFDO0lBQ0gsbUJBQUM7QUFBRCxDQUFDLEFBSkQsSUFJQztBQUpEOzhCQUlDLENBQUEifQ==