"use strict";
var GameInvite = (function () {
    function GameInvite(createdByName, createdByEmail, gameId, gameName, dateTime, locationName) {
        if (createdByName === void 0) { createdByName = ""; }
        if (createdByEmail === void 0) { createdByEmail = ""; }
        this.createdByName = createdByName;
        this.createdByEmail = createdByEmail;
        this.gameId = gameId;
        this.gameName = gameName;
        this.dateTime = dateTime;
        this.locationName = locationName;
    }
    return GameInvite;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = GameInvite;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ2FtZS1pbnZpdGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJnYW1lLWludml0ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUE7SUFDRSxvQkFBbUIsYUFBMEIsRUFDcEMsY0FBMkIsRUFDM0IsTUFBYyxFQUNkLFFBQWdCLEVBQ2hCLFFBQWdCLEVBQ2hCLFlBQW9CO1FBTGpCLDZCQUFpQyxHQUFqQyxrQkFBaUM7UUFDM0MsOEJBQWtDLEdBQWxDLG1CQUFrQztRQURqQixrQkFBYSxHQUFiLGFBQWEsQ0FBYTtRQUNwQyxtQkFBYyxHQUFkLGNBQWMsQ0FBYTtRQUMzQixXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQ2QsYUFBUSxHQUFSLFFBQVEsQ0FBUTtRQUNoQixhQUFRLEdBQVIsUUFBUSxDQUFRO1FBQ2hCLGlCQUFZLEdBQVosWUFBWSxDQUFRO0lBRTdCLENBQUM7SUFDSCxpQkFBQztBQUFELENBQUMsQUFURCxJQVNDO0FBVEQ7NEJBU0MsQ0FBQSJ9