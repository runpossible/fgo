export default class GroupInvite {
  constructor(public createdByName: string = "", 
    public createdByEmail: string = "", 
    public groupId: string,
    public name: string) {
        
  }
}