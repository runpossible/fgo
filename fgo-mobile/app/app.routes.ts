import {RouterConfig} from "@angular/router";
import {nsProvideRouter} from "nativescript-angular/router"
import {LoginPageComponent} from "./pages/login/login.component";
import {LoginTestComponent} from "./pages/login/login-test.component";
import {UpcomingComponent} from "./pages/games/upcoming.component";
import {AuthGuard} from "./auth.guard";

export const routes: RouterConfig = [
  { path: "", component: UpcomingComponent, canActivate: [AuthGuard] },
  { path: "login", component: LoginPageComponent }
];

export const APP_ROUTER_PROVIDERS = [
  nsProvideRouter(routes, { enableTracing: false }),
  AuthGuard
];
