# Android

For facebook auth integration you have to execute the following steps:

1. Generate a certificate locally on your machine:

> keytool -exportcert -alias androiddebugkey -keystore ~/.android/debug.keystore | openssl sha1 -binary | openssl base64

2. Add the Hash of the certificate in the Facebook Developer page in Key Hashes:

https://developers.facebook.com/apps/877406112341422/settings/

3. Test it...


# iOS

Everything's good in the hood. Except, the browser is not closed automatically.