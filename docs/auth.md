# Authentication

We use Passport.JS to authenticate the users.

# Remember me

The Express.js session is stored in Mongo DB, Passport uses the session internally to store the user info. Therefore, after you enter the website you have a "connect.sid" named cookie for the domain, which corresponds to the express session. After you login, the Passport library utilizes the same session id and stores the user details in the session. The session (and therefore the authentication after login) is set to expire for 1 month. We use mongo db, so that we have a single place where the information is stored and in case we use many node.js processes they can read the session information from the same place.

# User document

The record of the user looks like this:

    {
        "_id": "55b360063aa1e5ec14b1cfa9",
        "email": "cypressx@gmail.com",
        "facebook": {
            "token": "CAAMirlwciF8BABGpL5aUUcaYMKRlvH2aXzrAcek6IEa34nrgWg3RZCHYp3vD99YUPTAcZCmImRSJQJxXRUTIrcv4yS4iCXkbutPz6DJapBlgZAECH28HB7o0pgRlBOzGTuXnV2V7ZB7n86Vje7pRoXZAX0N48GmYYhgQJ5phyZAT9rSJxqUgZCgLxIdd8j4FQlIUns0VeMoilMtKcp4qQdH",
            "name": "Yosif Yosifov",
            "id": "10153071715953207"
        },
        "google": {
            "id": "107419975721218325660",
            "token": "ya29.uwHFfrB8Zlve7GEBZq9cOfuzHHGS1Lkdm_KvbvjxC6F41Mi1KjGcsxCACOnElX-pcH8B",
            "name": "Yosif Yosifov"
        }
    }

When a user logs in with a different provider, by the main email we match the user and add the details for the provider. We support local authentication, by storing a salt and hashed password in the local: {} part of the

# Strategy

The passport relies on different strategies. We've configured several for authentication:

## Local 

POST to /api/v1/login with body and content-type: application/json

    {
        username: "email@email.com",
        password: "pass"
    }

## Facebook

Everything is configured as in this article: https://scotch.io/tutorials/easy-node-authentication-facebook

## Google

Configured as in this article: https://scotch.io/tutorials/easy-node-authentication-google

## RunKeeper

If we need to, we can use the following https://github.com/jaredhanson/passport-runkeeper strategy for authenticating with runkeeper accounts