$( document ).ready(function() {

    $(".btn-invite-player").click(function(){
        if($(this).parent().children(".player-invite").attr("val") == 0){
            $(this).parent().children(".player-invite").attr("val","1");
            $(this).addClass("checked");
        }else{
            $(this).parent().children(".player-invite").attr("val","0");
            $(this).removeClass("checked");
        }
    });

});

function userLogin(_type){
    //console.log(_type);
    if(_type == 0){
        $("#user-login").addClass("dNone");
        $("body").css("overflow-y","auto");
    }else{
        $("#user-login").removeClass("dNone");
        $("body").css("overflow-y","hidden");
    }

    $(".form-cont").addClass("dNone");

    if(_type == 1){
        $("#signup").removeClass("dNone");
    }

    if(_type == 2){
        $("#login").removeClass("dNone");
    }
    if(_type == 3){
        $("#forgot-password").removeClass("dNone");
    }

    if(_type == 4){
        $("#contacts").removeClass("dNone");
    }

    if(_type == 5){
        $("#terms").removeClass("dNone");
    }
}

function showNav(){
    $("#main-menu").addClass("menu-show");
    $("#close-nav-btn").addClass("close-show");
    $("body").css("overflow-y","hidden");
}

function closeNav(){
    $(".menu-show").removeClass("menu-show");
    $("#notifications-list").removeClass("notification-show");
    $("#close-nav-btn").removeClass("close-show");
    $("body").css("overflow-y","auto");
}

function showNotifications(){
    if($("#notifications-list").hasClass("notification-show")){
        $("#notifications-list").removeClass("notification-show");
        $("#close-nav-btn").removeClass("close-show");
    }else{
        $("#notifications-list").addClass("notification-show");
        $("#close-nav-btn").addClass("close-show");
        $("body").css("overflow-y","hidden");
    }
}

function showSettings(){
    if($("#profile-settings").hasClass("settings-show")){
        $("#profile-settings").removeClass("settings-show");
    }else{
        $("#profile-settings").addClass("settings-show");
    }
}