var express = require('express'),
	app = express(),
	utilities = require('./server/Utilities'),
    fs = require('fs'),
    https = require('https');

var env = global.getEnvironment();

// load configuration
var config = require('./server/config/config')[env];
// set the Config in the global scope.
global.config = config;

//console.log(config.rootPath);

// connect to database
require('./server/db/mongoose')(config, function done(err, db) {
	// set DB in the global
	global.db = db;

	//console.log('done: ' + err + ' and ' + db);
	if(err) {
		console.log('Failed to connect to DB. Error: ' + err);
		return;
	}

    // bootstrap express application
	require('./server/config/express')(app, config, db);

	// add authentication layer
	require('./server/config/passport')(db, config);

    if(config.sslEnabled && config.env == 'development')
    {
        var options = {
            key: fs.readFileSync('./ssl/server.key'),
            cert: fs.readFileSync('./ssl/server.crt'),
            requestCert: false,
            rejectUnauthorized: false
        };
        
        // start server
        https.createServer(options, app).listen(config.port, config.ip_address, function (err) {
            if (err) {
                console.log('Node server failed to start! Error = ' + err);
                return;
            }

            console.log("Node server running at https://localhost:" + config.port + ' Environment: ' + process.env.NODE_ENV);
        });
    } else {
        app.listen(config.port, config.ip_address, function (err) {
            if (err) {
                console.log('Node server failed to start! Error = ' + err);
                return;
            }

            console.log("Node server running at http://localhost:" + config.port + ' Environment: ' + process.env.NODE_ENV);
        });
    }
});
