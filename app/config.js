'use strict';

define([], function () {
    var isMobile = false;
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        isMobile = true;
    }
    return {
		// nothing here, yet.
        baseApiUrl: '/api/v1/',
        defaultAvatarUrl: '/assets/img/profile-default.png',
        defaultLanguage: 'bg',
        isMobile: isMobile
	};
});

if (!String.prototype.format) {
    String.prototype.format = function() {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function(match, number) {
            return typeof args[number] != 'undefined'
                ? args[number]
                : match
                ;
        });
    };
}
