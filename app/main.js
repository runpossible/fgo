require.config({
    //urlArgs: "bust=" + (new Date()).getTime(),
    waitSeconds: 100,
    preserveLicenseComments: false,
    paths: {
        'angular': 'bower_components/angular/angular',
        'ngAnimate':'bower_components/angular-animate/angular-animate',
        'ngCookies': 'bower_components/angular-cookies/angular-cookies',
        'jquery' : 'bower_components/jquery/dist/jquery',
        'jquery-placeholder' : 'bower_components/jquery-placeholder/jquery.placeholder',
        'jquery.smartbanner' : 'js/dependencies/jquery-smartbanner/jquery.smartbanner',
        'bootstrap' :  'bower_components/bootstrap/dist/js/bootstrap',
        'ui.router': 'bower_components/angular-ui-router/release/angular-ui-router',
        'ui.bootstrap': 'bower_components/angular-bootstrap/ui-bootstrap',
        'ui.bootstrap.tpls': 'bower_components/angular-bootstrap/ui-bootstrap-tpls',
        'ngSanitize': 'bower_components/angular-sanitize/angular-sanitize',
        'ui.select': 'bower_components/ui-select/dist/select',
        'metismenu': 'bower_components/metisMenu/dist/metisMenu',
        'sb-admin-2': 'bower_components/startbootstrap-sb-admin-2/dist/js/sb-admin-2',
        'pascalprecht.translate': 'js/dependencies/angular-translate',
        'pascalprecht.translate.static.files': 'bower_components/angular-translate-loader-static-files/angular-translate-loader-static-files',
        'pascalprecht.translate.storage.cookie': 'bower_components/angular-translate-storage-cookie/angular-translate-storage-cookie',
        'pascalprecht.translate.storage.local': 'bower_components/angular-translate-storage-local/angular-translate-storage-local',
        'lodash': 'bower_components/lodash/lodash',
        'ngTable': 'bower_components/ng-table/dist/ng-table',
        'angularUtils.directives.dirPagination': 'bower_components/angularUtils-pagination/dirPagination',
        //'facebook-api': '//connect.facebook.net/en_US/sdk', - using our copy as there are some problems on firefox not loading FB sdk
        'facebook-api': 'js/dependencies/connect.facebook.net.sdk',
        'angular.file.upload': 'bower_components/angular-file-upload/dist/angular-file-upload',
        'ngStorage': 'bower_components/ngstorage/ngStorage',
        'toaster': 'bower_components/AngularJS-Toaster/toaster',
        'ngAria': 'bower_components/angular-aria/angular-aria.min',
        'ngMaterial': 'bower_components/angular-material/angular-material',
        'ngFileUpload': 'bower_components/ng-file-upload/ng-file-upload',
        'ngFileUploadShim': 'bower_components/ng-file-upload/ng-file-upload-shim',
        'ngImgCrop': 'bower_components/ng-img-crop/compile/unminified/ng-img-crop',
        'jstz': 'js/external/jstz'

        // Angular Google Maps
        //'lodash': 'bower_components/lodash/lodash',
        //'nemLogging': 'bower_components/angular-simple-logger/dist/angular-simple-logger',
        //'uiGmapgoogle-maps': 'bower_components/angular-google-maps/dist/angular-google-maps'
    },
    shim: {
        'angular': {
            exports: 'angular'
        },
        'ngAnimate': {
            deps: ['angular']
        },
        'ngTable': {
            deps: ['angular']
        },
        'ngCookies': {
            deps: ['angular']
        },
        'bootstrap': {
            deps: ['jquery']
        },
        'jquery-placeholder':{
            deps: ['jquery']
        },
        'jquery.smartbanner':{
            deps: ['jquery']
        },
        'metismenu': {
            deps: ['jquery', 'bootstrap']
        },
        'ui.router': {
            deps: ['angular']
        },
        'ui.bootstrap': {
            deps: ['angular', 'bootstrap']
        },
        'ui.bootstrap.tpls': {
            deps: ['ui.bootstrap']
        },
        'ui.select': {
            deps: ['angular']
        },
        'sb-admin-2': {
            deps: ['metismenu']
        },
        'ngSanitize':{
            deps: ['angular']
        },
        'angularUtils.directives.dirPagination':{
            deps: ['angular']
        },
        'pascalprecht.translate.static.files': {
            deps: ['angular', 'pascalprecht.translate'],
            exports: 'pascalprecht.translate'
        },
        'pascalprecht.translate.storage.cookie': {
            deps: ['angular', 'ngCookies', 'pascalprecht.translate'],
            exports: 'pascalprecht.translate'
        },
        'pascalprecht.translate.storage.local': {
            deps: ['angular', 'pascalprecht.translate.storage.cookie'],
            exports: 'pascalprecht.translate'
        },
        'pascalprecht.translate': {
            deps: ['angular'],
            exports: 'pascalprecht.translate'
        },
        'angular.file.upload': {
            deps: ['angular']
        },
        'ngStorage': {
            deps: ['angular']
        },
        'toaster': {
            deps: ['angular']
        },
        'ngAria': {
            deps: ['angular']
        },
        'ngMaterial': {
            deps: ['angular', 'ngAria']
        },
        'ngFileUploadShim': {
            deps: ['angular']
        },
        'ngFileUpload': {
            deps: ['angular', 'ngFileUploadShim']
        },
        'ngImgCrop': {
            deps: ['angular']
        }
        // Angular Google Maps
        //'nemLogging': ['angular'],
        //'uiGmapgoogle-maps': ['angular', 'nemLogging', 'lodash'],
    }
});

//http://code.angularjs.org/1.2.1/docs/guide/bootstrap#overview_deferred-bootstrap
window.name = "NG_DEFER_BOOTSTRAP!";

require([
    'angular',
    'app'
], function(angular, app) {
    angular.element().ready(function() {
        angular.resumeBootstrap([app.name]);
    });
});