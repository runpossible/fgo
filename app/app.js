define([
	'angular',
    'require',
    'pascalprecht.translate',
    'pascalprecht.translate.static.files',
    'pascalprecht.translate.storage.cookie',
    'pascalprecht.translate.storage.local',
    'jquery-placeholder',
    'jquery.smartbanner',
    'ngCookies',
    'ui.router',
    'ui.bootstrap',
    'ui.bootstrap.tpls',
    'ngSanitize',
    'ui.select',
    'ngAnimate',
    'ngTable',
    'angularUtils.directives.dirPagination',
    'facebook-api',
    'angular.file.upload',
    'ngStorage',
    'toaster',
    'ngAria',
    'ngMaterial',
    'ngFileUpload',
    'ngImgCrop',
    'jstz',

    // Angular Google Maps
    //'uiGmapgoogle-maps',

	'./js/home/home',
    './js/games/games',
    './js/authentication/authentication',
    './js/dashboard/dashboard',
    './js/profile/profile',
    './js/friends/friends',
    './js/groups/groups',
    './js/locations/locations',
    './js/info/info',
    './js/admin/admin',
    './config'
], function(angular, require) {
    var runApp = angular.module('runApp', [
        'pascalprecht.translate',
        'ngCookies',
        'ui.router',
        'ui.bootstrap',
        'ui.bootstrap.tpls',
        'ngSanitize',
        'ui.select',
        'ngAnimate',
        'ngTable',
        'angularUtils.directives.dirPagination',
        'angularFileUpload',
        'ngStorage',
        'toaster',
        'ngAria',
        'ngMaterial',
        'ngFileUpload',
        'ngImgCrop',

        // Angular Google Maps
        //'uiGmapgoogle-maps',

        'home',
        'games',
        'authentication',
        'dashboard',
        'profile',
        'friends',
        'groups',
		'locations',
        'info',
        'admin'
    ]);

    var appConfig = require('./config');

    runApp.constant('appConfig', appConfig);

    runApp.config(['$translateProvider', '$locationProvider', function ($translateProvider, $locationProvider) {
        $translateProvider.useStaticFilesLoader({
            prefix: '/languages/',
            suffix: '.json?t=' + window.requestSuffix
        });

        $translateProvider.useSanitizeValueStrategy(null);
        $translateProvider.useStorage('translateStorage');

        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });

        // Angular Google Maps
        //GoogleMapApiProviders.configure({
        //   china: true
        //});
    }]);

    // don't calculate this every time
    var jstz = require('jstz');
    var timezone = jstz.determine();
    var timezoneName = timezone.name();

    var updateUserTimezone = function(appConfig, $http, $window) {
        // user is not logged in!
        if(!$window.currentUser) {
            return;
        }

        if($window.currentUser.timezone !== timezoneName) {
            var timezoneUrl = appConfig.baseApiUrl + 'profile/timezone';
            // don't do anything if it fails
            $http.put(timezoneUrl, { timezone: timezoneName });
        }
    };


    runApp.run(['$rootScope', '$window', '$state', 'UIService', '$translate', '$http', 'appConfig',
        function($rootScope, $window, $state, UIService, $translate, $http, appConfig) {
            $rootScope.$on('$stateChangeStart',
                function(event, toState, toParams, fromState, fromParams){
                    //if(window.sslEnabled) {
                    //    var targetUrl = $state.href(toState, toParams, {absolute: true});
                    //    if ((toState.data || {}).requireSSL && targetUrl.indexOf('http:') > -1) {
                    //        window.location.href = targetUrl.replace('http:', 'https:');
                    //    }
                    //    if (!((toState.data || {}).requireSSL) && targetUrl.indexOf('https:') > -1) {
                    //        window.location.href = targetUrl.replace('https:', 'http:');
                    //    }
                    //}

                    toState.templateUrl += '?t=' + window.requestSuffix;
                    
                    var requireLogin = toState.data.requireLogin;

                    if(requireLogin && !$window.currentUser) {
                        // we're not logged in => go to login page.
                        event.preventDefault(); // we need this row, otherwise stage.go is not working

                        var redirectUri = window.location.pathname + window.location.search;
                        //var notify = (redirectUri == '/');

                        window.location = $state.href('landingPage', (redirectUri !== '' && redirectUri !== '/') ? { redirect_uri: redirectUri, login: true} : {}, {absolute: true});
                    }
                    else {
                        UIService.checkScreenSize(toState.url);

                        // check if we need to update the user's timezone
                        updateUserTimezone(appConfig, $http, $window);
                        //UIService.removeFacebookAppendedHash();
                    }

                    // ensure we're logged in.
                    //console.log('from: ' + JSON.stringify(fromState) + ' to: ' + JSON.stringify(toState));
                });

            $rootScope.previousState;
            $rootScope.currentState;
            $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
                $rootScope.$broadcast('onStateChanged', toState);

                $rootScope.previousState = fromState.name;
                $rootScope.currentState = toState.name;

                UIService.checkBodyClass(toState);

                // we need this as when we are logging in with a local user we are not calling a page reload
                // so $translate.use will not trigger the get method from the translateProvider
                // and we need to call it
                if($window.currentUser && $window.currentUser.language
                    && $window.currentUser.language != $translate.use()) {
                    $translate.use($window.currentUser.language);
                }

                setTimeout(function(){
                    $('input, textarea').placeholder({ customClass: 'my-placeholder' });
                    UIService.numericTextboxFix();
                }, 300);
            });

    }]);

    return runApp;
});
