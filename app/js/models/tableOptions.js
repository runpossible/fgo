define(function () {
    'use strict';


    return [function () {
        // grid options -> page number, page size, total games
        function TableOptions(goFn) {
            var page = 1;
            var pageSz = 10;
            var total = 0;

            this.getPage = function () {
                return page;
            };

            this.setPage = function (val) {
                page = val;
            };

            this.getTotal = function () {
                return total;
            };

            this.setTotal = function (val) {
                total = val;
            };

            this.getPageSize = function () {
                return pageSz;
            };

            this.go = function (page) {
                return goFn(page, pageSz);
            };

            this.hasPrev = function () {
                return page > 1;
            };
        }

        return TableOptions;
    }];
});