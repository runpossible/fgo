define(['lodash'], function(_) {
    'use strict';

    return ['$http', 'appConfig', 'notificationManager', '$filter', '$q', '$window',
        function($http, appConfig, notificationManager, $filter, $q, $window) {
        return {
            restrict: 'E',
            scope: {
                refresh: '@',
                showHeader: '@',
                inviteFriendAction: '&',
                checkInvited: '&'
            },
            templateUrl: '/js/friends/invite-current-friends.html?t=' + window.requestSuffix,
            link: function($scope, element, attrs) {
                var friendsUrl = appConfig.baseApiUrl + 'profile/friends';
                $scope.isCurrentFriendsLoading = true;

                $scope.currentUserFriendsCount = $window.currentUser.friendsCount;

                $scope.pageSize = 12;

                $scope.search = null;
                $scope.playersLimit = $scope.pageSize;

                var getFriends = function getFriends() {
                    var deferred = $q.defer();
                    if($scope.search){
                        $http.post(appConfig.baseApiUrl + 'user/friends/search', {
                                search: $scope.search
                            })
                            .then(function (response) {
                                deferred.resolve(response.data);
                            }, function () {
                                notificationManager.error('GENERAL ERROR');
                                deferred.reject();
                            });
                    } else {
                        $http.get(friendsUrl)
                            .then(function (response) {
                                deferred.resolve(response.data);
                            }, function () {
                                notificationManager.error('GENERAL ERROR');
                                deferred.reject();
                            });
                    }
                    return deferred.promise;
                };

                $scope.pageSize = $scope.googleFriendsMode ? 24 : 12;
                $scope.inviteFriend = function(player){
                    if(!player.checked) {
                        $scope.inviteFriendAction()(player, function(err, player){
                            if(!err){
                                player.checked = true;
                            }
                        });
                    }
                };

                var checkInvited = $scope.checkInvited();

                $scope.load = function() {
                    $scope.isCurrentFriendsLoading = true;
                    $scope.friends = [];
                    getFriends().then(function (friends) {
                        $scope.friends = friends;
                        if(checkInvited){
                            _.forEach($scope.friends, function(friend){
                                var role = checkInvited(friend);
                                friend.alreadyPart = (role == 1);
                                friend.alreadyInvited = (role == 2);
                                friend.checked = friend.alreadyPart || friend.alreadyInvited;
                            });
                        }
                        $scope.isCurrentFriendsLoading = false;
                    }, function err() {
                        return notificationManager.error('GENERAL ERROR');
                    });
                };

                $scope.searchFriends = function(){
                    if(($scope.search || '').trim().length > 2){
                        $scope.load();
                    }
                };

                $scope.load();

                $scope.removeFriend = function(friend){
                    if(confirm($filter('translate')('ARE YOU SURE TO DELETE FRIEND'))){
                        $http.delete(friendsUrl + '/' + friend._id)
                            .then(function(response) {
                                var friendIndex = $scope.friends.indexOf(friend);
                                if (friendIndex > -1) {
                                    $scope.friends.splice(friendIndex, 1);
                                    $window.currentUser.friendsCount = $window.currentUser.friendsCount - 1;
                                }
                            }, function() {
                                notificationManager.error('GENERAL ERROR');
                            });
                        return true;
                    } else {
                        return false;
                    }
                };

                $scope.$watch("refresh",function() {
                    $scope.load();
                });

                $scope.$watch("showHeader",function(oldValue, newValue) {
                    $scope.showHeader = newValue;
                });

                $scope.searchKeyPress = function (e) {
                    if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
                        $scope.searchFriends();
                        return false;
                    } else {
                        return true;
                    }
                };
            }
        };
    }];
});