define(['lodash'], function (_) {
    'use strict';

    return ['$scope', '$stateParams', 'appConfig', '$state', '$http', '$q', 'notificationManager', '$window', function($scope, $stateParams, appConfig, $state, $http, $q, notificationManager, $window) {
        var searchUsersUrl = appConfig.baseApiUrl + 'search-users',
            friendsUrl = appConfig.baseApiUrl + 'profile/friends',
            facebookFriendsUrl = appConfig.baseApiUrl + 'user/friends/facebook',
            googleFriendsUrl = appConfig.baseApiUrl + 'user/friends/google';

        $scope.currentUser = $window.currentUser;
        $scope.results = {};
        $scope.refreshFriends = new Date().getMilliseconds();
        var reissueTokenUrl = "/api/v1/auth/google?redirect_uri=" + $state.href('base.friends.add', {searchGoogle: true}) + '#google-friends';

        var getFriends = function getGame() {
            var deferred = $q.defer();
            $http.get(friendsUrl)
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function() {
                    notificationManager.error('GENERAL ERROR');
                    deferred.reject();
                });
            return deferred.promise;
        };

        var processSearchResults = function(promises, provider){
            $q.all(promises).then(function(results) {
                var searchResults = results[0].data,
                    currentFriends = results[1];


                var mapped = _.map(searchResults, function(resultUser) {
                    var res = _.clone(resultUser);
                    res.alreadyFriend = _.some(currentFriends, { _id: resultUser._id });

                    return res;
                });

                // filter current client from the received results
                mapped = _.filter(mapped, function(user){
                    return user._id != window.currentUser._id;
                });

                $scope.results[provider] = mapped;

                $scope.isSearchLoading = false;
            }, function err() {
                if(provider == 'google' && !$stateParams.searchGoogle){
                    //try to issue a new token and then come back and list results
                    window.location.href = reissueTokenUrl;
                }
                else {
                    $scope.isSearchLoading = false;
                    return notificationManager.error('GENERAL ERROR');
                }
            });
        };

        $scope.searchEMail = function searchByEMail(isValid) {
            if(isValid) {
                $scope.isSearchLoading = true;

                $scope.data = $scope.data || {};
                var email = $scope.data.email;

                var queryString = 'email=' + email;

                var url = searchUsersUrl + '?' + queryString;
                var promises = [$http.get(url), getFriends()];
                processSearchResults(promises, 'local-email');
            }
        };

        $scope.searchName = function searchByName(isValid) {
            if(isValid) {
                $scope.isSearchLoading = true;

                $scope.data = $scope.data || {};
                var name = $scope.data.name;

                var queryString = 'name=' + name;

                var url = searchUsersUrl + '?' + queryString;
                var promises = [$http.get(url), getFriends()];
                processSearchResults(promises, 'local-name');
            }
        };

        $scope.searchFBFriends = function() {
            $scope.isSearchLoading = true;

            $scope.data = $scope.data || {};

            var promises = [ $http.get(facebookFriendsUrl), getFriends()];
            processSearchResults(promises, 'facebook');
        };

        $scope.searchGoogleFriends = function() {
            $scope.isSearchLoading = true;

            $scope.data = $scope.data || {};

            var promises = [ $http.get(googleFriendsUrl), getFriends()];
            processSearchResults(promises, 'google');
        };

        $scope.addFriend = function addFriend(user) {
            $http.post(friendsUrl, {
                users: [user._id]
            }).then(function () {
                user.alreadyFriend = true; // is a friend now
                return notificationManager.info('FRIEND ADDED');
            }, function err() {
                return notificationManager.error('GENERAL ERROR');
            });
        };

        $scope.removeFriend = function removeFriend(user) {
            $http.delete(friendsUrl + "/" + user._id).then(function () {
                user.alreadyFriend = false; // is not a friend anymore
                return notificationManager.warning('FRIEND REMOVED');
            }, function err() {
                return notificationManager.error('GENERAL ERROR');
            });
        };

        $scope.reloadFriends = function() {
            $scope.refreshFriends = new Date().getMilliseconds();
        };

        if($stateParams.searchGoogle){
            window.location.hash = '#google-friends';
            var googleTab = $('.nav-tabs a[href=#google-friends]');
            if(googleTab.length){
                googleTab.tab('show');
            }
            $scope.searchGoogleFriends();
        }

        var url = document.location.toString();
        if (url.match('#')) {
            var navigationTab = $('.nav-tabs a[href=#'+url.split('#')[1]+']');
            if(navigationTab.length) {
                navigationTab.tab('show');
            }

            var hash = url.split('#')[1];
            if(hash == 'facebook-friends'){
                $scope.searchFBFriends();
            }else if(hash == 'google-friends'){
                $scope.searchGoogleFriends();
            }
        }
    }];
});
