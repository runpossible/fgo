define(['lodash'], function (_) {
    'use strict';

    return ['$scope', '$stateParams', 'appConfig', '$state', '$http', '$q', 'notificationManager', '$window', '$filter', 'facebookService',
        function($scope, $stateParams, appConfig, $state, $http, $q, notificationManager, $window, $filter, facebookService) {
        var friendsUrl = appConfig.baseApiUrl + 'profile/friends',
            facebookFriendsUrl = appConfig.baseApiUrl + 'user/friends/facebook/all',
            googleFriendsUrl = appConfig.baseApiUrl + 'user/friends/google/all';

        $scope.isMobile = appConfig.isMobile;
        $scope.currentUser = $window.currentUser;
        $scope.results = {};
        $scope.listMode = 'none';
        $scope.selectedFriends = {};
        var reissueFBTokenUrl = "/api/v1/auth/facebook?redirect_uri=" + $state.href('base.friends.invite', {searchFacebook: true});
        var reissueGoogleTokenUrl = "/api/v1/auth/google?redirect_uri=" + $state.href('base.friends.invite', {searchGoogle: true}) + '#google-friends';

        var getFriends = function getGame() {
            var deferred = $q.defer();
            $http.get(friendsUrl)
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function() {
                    notificationManager.error('GENERAL ERROR');
                    deferred.reject();
                });
            return deferred.promise;
        };

        var processSearchResults = function(promises, provider){
            $q.all(promises).then(function(results) {
                var searchResults = results[0].data,
                    currentFriends = results[1];


                var mapped = _.map(searchResults, function(resultUser) {
                    var res = _.clone(resultUser);
                    res.alreadyFriend = _.some(currentFriends, { _id: resultUser._id });

                    return res;
                });

                // filter current client from the received results
                mapped = _.filter(mapped, function(user){
                    return user._id != window.currentUser._id;
                });

                $scope.results = mapped;
                $scope.inviteText = $filter('translate')('INVITE POST DEFAULT MESSAGE');
                $scope.listMode = provider;

                $scope.isSearchLoading = false;
            }, function err() {
                if(provider == 'google' && !$stateParams.searchGoogle){
                    //try to issue a new token and then come back and list results
                    window.location.href = reissueGoogleTokenUrl;
                } else if(provider == 'facebook' && !$stateParams.searchFacebook){
                    //try to issue a new token and then come back and list results
                    window.location.href = reissueFBTokenUrl;
                }
                else {
                    $scope.isSearchLoading = false;
                    return notificationManager.error('GENERAL ERROR');
                }
            });
        };

        $scope.hasSelected = function () {
            return _.some($scope.results, { selected: true});
        };

        $scope.selectionChanged = function(){
            if($scope.listMode == 'google'){
                var selectedUsers = _.filter($scope.results, { selected: true});
                var userIds = _.map(selectedUsers, function(selectedUser){
                    return selectedUser.googleId;
                });

                var recipients = userIds.join();

                var origin =
                    window.location.protocol + '://' + window.location.host;

                if(window.location.hostname == 'localhost'){
                    origin = "https://www.fgo.io";
                }

                var options = {
                    origin: origin,
                    contenturl: origin,
                    clientid: window.googleClientId,
                    cookiepolicy: 'single_host_origin',
                    prefilltext: $scope.inviteText,
                    calltoactionlabel: 'REGISTER',
                    calltoactionurl: origin + '/login',
                    recipients: recipients
                };
                gapi.interactivepost.render('sharePost', options);
            }
        };

        $scope.inviteFacebookFriends = function(){
            var confirmationMessage = $filter('translate')('CONFIRM POST TO FACEBOOK');
            if(confirm(confirmationMessage)) {
                var selectedUsers = _.filter($scope.results, {selected: true});
                var tags = _.map(selectedUsers, function (selectedUser) {
                    return selectedUser.facebookId;
                });
                $http.post(appConfig.baseApiUrl + 'user/friends/facebook/post', {
                    message: $scope.inviteText,
                    tags: tags
                }).then(function () {
                    return notificationManager.info('FACEBOOK POST ADDED SUCCESSFULLY');
                }, function err() {
                    return notificationManager.error('GENERAL ERROR');
                });
            }
        };

        $scope.sendFacebookMessage = function(){
            facebookService.send(document.location.hostname == "localhost" ? "http://www.fgo.io" : document.location.origin);
        };

        $scope.searchFBFriends = function() {
            $scope.isSearchLoading = true;

            $scope.data = $scope.data || {};

            var promises = [ $http.get(facebookFriendsUrl), getFriends()];
            processSearchResults(promises, 'facebook');
        };

        $scope.searchGoogleFriends = function() {
            $scope.isSearchLoading = true;

            $scope.data = $scope.data || {};

            var promises = [ $http.get(googleFriendsUrl), getFriends()];
            processSearchResults(promises, 'google');
        };

        $scope.addFriend = function addFriend(user) {
            $http.post(friendsUrl, {
                users: [user._id]
            }).then(function () {
                user.alreadyFriend = true; // is a friend now
                $window.currentUser.friendsCount = $window.currentUser.friendsCount + 1;
                return notificationManager.info('FRIEND ADDED');
            }, function err() {
                return notificationManager.error('GENERAL ERROR');
            });
        };

        $scope.removeFriend = function removeFriend(user) {
            $http.delete(friendsUrl + "/" + user._id).then(function () {
                user.alreadyFriend = false; // is not a friend anymore
                $window.currentUser.friendsCount = $window.currentUser.friendsCount - 1;
                return notificationManager.warning('FRIEND REMOVED');
            }, function err() {
                return notificationManager.error('GENERAL ERROR');
            });
        };

        if($stateParams.searchGoogle){
            $scope.searchGoogleFriends();
        } else if($stateParams.searchFacebook){
            $scope.searchGoogleFriends();
        }
    }];
});
