define(['lodash'], function(_) {
    'use strict';

    return ['appConfig', 'notificationManager', '$filter', function(appConfig, notificationManager, $filter) {
        return {
            restrict: 'E',
            scope: {
                friendsList: '=',
                addFriendAction: '&',
                removeFriendAction: '&',
                selectionChanged: '&',
                search: '=',
                searchType: '='
            },
            templateUrl: '/js/friends/friend-search-result.html?t=' + window.requestSuffix,
            link: function($scope, element, attrs) {
                $scope.pageSize = 12;
                $scope.playersLimit = $scope.pageSize;

                $scope.googleFriendsMode = attrs && attrs.googleFriendsMode;
                $scope.pageSize = $scope.googleFriendsMode ? 12 : 12;

                $scope.addFriend = $scope.addFriendAction();
                $scope.removeFriend = $scope.removeFriendAction();

                var selectionChangedMethod = $scope.selectionChanged();

                $scope.changeSelected = function(user){
                    user.selected = !user.selected;
                    if(selectionChangedMethod){
                        selectionChangedMethod();
                    }
                };

                Object.defineProperty($scope, 'friendsList', {
                    get: function () {
                        return $scope.results;
                    },
                    set: function(value){
                        $scope.results = value;
                    }
                });

                Object.defineProperty($scope, 'selectedFriends', {
                    get: function () {
                        return _.filter($scope.results, function(f){ return f.selected;});
                    }
                });

                $scope.select = function(friend){
                    friend.selected = !friend.selected;
                };

                $scope.inviteFacebookFriends = function(){
                    var confirmationMessage = $filter('translate')('CONFIRM POST TO FACEBOOK');
                    if(confirm(confirmationMessage)) {
                        var tags = _.map($scope.selectedFriends, function (selectedFriend) {
                            return selectedFriend.facebookId;
                        });
                        $http.post(appConfig.baseApiUrl + 'user/friends/facebook/post', {
                            message: $scope.inviteText,
                            tags: tags
                        }).then(function () {
                            return notificationManager.info('FACEBOOK POST ADDED SUCCESSFULLY');
                        }, function err() {
                            return notificationManager.error('GENERAL ERROR');
                        });
                    }
                };
            }
        };
    }];
});