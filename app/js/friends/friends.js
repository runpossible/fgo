define(function(require) {
    var angular = require('angular');

    var friends = angular.module('friends', ['ui.router']);
    var friendsAddCtrl = require('/js/friends/friends-add.js');
    var friendsInviteCtrl = require('/js/friends/friends-invite.js');
    var friendsListCtrl = require('/js/friends/friends-list.js');

    friends.controller('friendsAddCtrl', friendsAddCtrl);
    friends.controller('friendsInviteCtrl', friendsInviteCtrl);
    friends.controller('friendsListCtrl', friendsListCtrl);

    friends.config(['$stateProvider', function($stateProvider) {
        $stateProvider.state('base.friends', {
            url: 'friends',
            abstract: true,
            templateUrl: '/js/home/empty.html'
        });

        $stateProvider.state('base.friends.add', {
            url: '/friends?:searchGoogle',
            templateUrl: '/js/friends/friends-add.html',
            controller: 'friendsAddCtrl',
            data:{
                pageTitle: 'ADD FRIENDS'
            }
        });

        $stateProvider.state('base.friends.invite', {
            url: '/friends-invite?:searchGoogle&:searchFacebook',
            templateUrl: '/js/friends/friends-invite.html',
            controller: 'friendsInviteCtrl',
            data:{
                pageTitle: 'INVITE YOUR FRIENDS'
            }
        });

        $stateProvider.state('base.friends.list', {
            url: '/friends-list',
            templateUrl: '/js/friends/friends-list.html',
            controller: 'friendsListCtrl',
            data:{
                pageTitle: 'VIEW FRIENDS'
            }
        });
    }]);

    return friends;
});
