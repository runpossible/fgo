define([], function () {
    'use strict';

    return ['$scope', '$window', function($scope, $window) {
        $scope.currentUser = $window.currentUser;
        $scope.refreshFriends = new Date().getMilliseconds();

        $scope.reloadFriends = function() {
            $scope.refreshFriends = new Date().getMilliseconds();
        };
    }];
});
