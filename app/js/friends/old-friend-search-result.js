define([], function() {
    'use strict';

    return [function() {
        return {
            restrict: 'E',
            scope: {
                friendsList: '=',
                addFriendAction: '&',
                removeFriendAction: '&',
                selectionChanged: '&'
            },
            templateUrl: '/js/friends/friend-search-result.html?t=' + window.requestSuffix,
            link: function($scope, element, attrs) {
                $scope.googleFriendsMode = attrs && attrs.googleFriendsMode;
                $scope.pageSize = $scope.googleFriendsMode ? 24 : 12;

                $scope.addFriend = $scope.addFriendAction();
                $scope.removeFriend = $scope.removeFriendAction();

                var selectionChangedMethod = $scope.selectionChanged();

                $scope.changeSelected = function(user){
                    user.selected = !user.selected
                    if(selectionChangedMethod){
                        selectionChangedMethod();
                    }
                };

                Object.defineProperty($scope, 'friendsList', {
                    get: function () {
                        return $scope.results;
                    },
                    set: function(value){
                        $scope.results = value;
                    }
                });
            }
        };
    }];
});