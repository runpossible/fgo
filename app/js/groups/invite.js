define(['lodash'], function (_) {
    'use strict';

    return ['$scope', '$stateParams', 'appConfig', '$state', '$http', '$q', 'notificationManager', '$window',
        function($scope, $stateParams, appConfig, $state, $http, $q, notificationManager, $window) {
        var groupId = $stateParams.groupId,
            groupUrl = appConfig.baseApiUrl + 'group',// TODO: add these to some constants.
            inviteUrl = appConfig.baseApiUrl + 'invite',
            inviteGroupUrl = inviteUrl + '/' + groupId,
            searchUsersUrl = appConfig.baseApiUrl + 'search-users',
            facebookFriendsUrl = appConfig.baseApiUrl + 'user/friends/facebook',
            googleFriendsUrl = appConfig.baseApiUrl + 'user/friends/google';

        $scope.currentUser = $window.currentUser;
        //$scope.redirect_uri = "?redirect_uri=" + $state.href('base.invite.group', {groupId: groupId, searchGoogle: true});
            var reissueTokenUrl = "/api/v1/auth/google?redirect_uri=" + $state.href('base.invite.group', {groupId: groupId, searchGoogle: true});

        var getGroup = function getGroup() {
            var deferred = $q.defer();
            $http.get(groupUrl + '/' + groupId)
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function() {
                    notificationManager.error('GENERAL ERROR');
                    deferred.reject();
                });
            return deferred.promise;
        };

        var getActiveGroupInvites = function() {
            var deferred = $q.defer();

            $http.get(inviteGroupUrl)
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function() {
                    notificationManager.error('GENERAL ERROR');
                    deferred.reject();
                });

            return deferred.promise;
        };

        $scope.isMainLoading = true;
        getGroup().then(function(group) {
            groupId = group._id;
            inviteGroupUrl = inviteUrl + '/' + groupId;
            $scope.group = group;
            $scope.users = (group.admin_users || []).concat(group.users || []);

            $scope.isMainLoading = false;

            if($stateParams.searchGoogle){
                $scope.searchGoogleFriends();
            }
        });

        var processSearchResults = function(promises, provider){
            $q.all(promises).then(function(results) {
                var searchResults = results[0].data,
                    groupInvites = results[1];

                var mapped = _.map(searchResults, function(resultUser) {
                    var res = _.clone(resultUser);
                    res.alreadyInGroup = _.some($scope.users, { _id: resultUser._id });
                    res.alreadyInvited = _.some(groupInvites, function(inv) {
                        return inv.player._id === resultUser._id;
                    });

                    return res;
                });

                // filter current client from the received results
                mapped = _.filter(mapped, function(user){
                    return user._id != window.currentUser._id;
                });

                $scope.results = mapped;

                $scope.hasResults = true;

                $scope.isSearchLoading = false;
            }, function err() {
                if(provider == 'google' && !$stateParams.searchGoogle){
                    //try to issue a new token and then come back and list results
                    window.location.href = reissueTokenUrl;
                }
                else {
                    $scope.isSearchLoading = false;
                    return notificationManager.error('GENERAL ERROR');
                }
            });
        };

        $scope.search = function search() {
            $scope.isSearchLoading = true;

            $scope.data = $scope.data || {};
            var email = $scope.data.email,
                name = $scope.data.name;

            if(!email && !name) {
                // cannot search without any of the fields
                return notificationManager.error('ENTER MAIL OR NAME');
            }

            var queryString = '';
            if(email) queryString += 'email=' + email;
            if(name) {
                if(email) queryString += ';';
                queryString += 'name=' + name;
            }

            var url = searchUsersUrl + '?' + queryString;
            var promises = [ $http.get(url), getActiveGroupInvites() ];
            processSearchResults(promises, 'local');
        };

        $scope.searchFBFriends = function() {
            $scope.isSearchLoading = true;

            $scope.data = $scope.data || {};

            var promises = [ $http.get(facebookFriendsUrl), getActiveGroupInvites()];
            processSearchResults(promises, 'facebook');
        };

        $scope.searchGoogleFriends = function() {
            $scope.isSearchLoading = true;

            $scope.data = $scope.data || {};

            var promises = [ $http.get(googleFriendsUrl), getActiveGroupInvites()];
            processSearchResults(promises, 'google');
        };

        $scope.invite = function invite(user) {
            $http.post(inviteUrl, {
                playerId: user._id,
                groupId: groupId
            }).then(function () {
                user.alreadyInvited = true; // it's invited now
            }, function err() {
                return notificationManager.error('GENERAL ERROR');
            });
        };
    }];
});
