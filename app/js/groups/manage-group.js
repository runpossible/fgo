define(['lodash'], function (_) {
    'use strict';

    return ['$scope', '$stateParams', 'appConfig', '$state', '$http', '$q', '$translate', 'notificationManager', 'facebookService', 'imageService',
        function ($scope, $stateParams, appConfig, $state, $http, $q, $translate, notificationManager, facebookService, imageService) {
            var groupId = $stateParams.groupId,
                groupUrl = appConfig.baseApiUrl + 'group';

            $scope.isMobile = appConfig.isMobile;
            // show some loader
            $scope.isLoading = true;

            var initialize = function initialize() {
                var deferred = $q.defer();
                $http.get(groupUrl + '/' + groupId)
                    .then(function (response) {
                        deferred.resolve(response.data);
                    }, deferred.reject);
                return deferred.promise;
            };

            var intializeGroupDetails = function (group) {
                groupId = group._id;
                group.imageUrl = imageService.getGroupImageUrl(group);

                $scope.group = group;
                $scope.userIsAdmin = group.createdBy == window.currentUser._id || _.some(group.admin_users || [], {_id: window.currentUser._id});
                $scope.userIsOwner = group.createdBy == window.currentUser._id;
                $scope.currentUserId = window.currentUser._id;
                var adminUsers = group.admin_users || [];
                var regularUsers = group.users || [];
                for (var i = 0; i < adminUsers.length; i++) {
                    adminUsers[i].isAdminUser = true;
                }

                $scope.users = adminUsers.concat(regularUsers);

                if (!group.invitation_code) {
                    $scope.generateActivationCode();
                }

                $scope.isLoading = false;
            };
            var initializeError = function (response) {

                if (response.status == 401) {
                    notificationManager.error('ACCESS DENIED');
                } else {
                    notificationManager.error('GENERAL ERROR');
                }
                $state.go('base.dashboard', {});
            };

            initialize().then(intializeGroupDetails, initializeError);

            $scope.groupInviteUrl = function (fullUrl) {
                if ($scope.group && $scope.group.invitation_code) {
                    if ($scope.group.invitation_shorturl && !fullUrl) {
                        return $scope.group.invitation_shorturl
                    }
                    else {
                        return $state.href('base.invitation', {
                            groupId: groupId,
                            activationCode: $scope.group.invitation_code
                        }, {absolute: true})
                    }
                }
                else {
                    return null;
                }
            };

            $scope.inviteThroughFacebook = function () {
                var url = $scope.groupInviteUrl(true);
                facebookService.send(url);
            };

            $scope.removeUser = function (user) {
                $translate('CONFIRM DELETE PLAYER FROM GROUP').then(function (confirmationMessage) {
                    var confirmed = confirm(confirmationMessage);
                    if (confirmed) {
                        $http.delete(groupUrl + '/' + groupId + '/members/' + user._id)
                            .then(function () {
                                _.remove($scope.users, function (u) {
                                    return u._id === user._id;
                                });
                            }, function err() {
                                return notificationManager.error('GENERAL ERROR');
                            });
                    }
                })
            };

            $scope.generateActivationCode = function () {
                $http.post(groupUrl + '/' + groupId + '/invitation_code')
                    .then(function (response) {
                        $scope.group.invitation_code = response.data.invitation_code;
                        $scope.group.invitation_shorturl = response.data.invitation_shorturl;
                    }, function err() {
                        return notificationManager.error('GENERAL ERROR');
                    });
            };

            $scope.removeActivationCode = function () {
                $translate('CONFIRM DELETE GROUP INVITATION CODE').then(function (confirmationMessage) {
                    if (confirm(confirmationMessage)) {
                        $http.delete(groupUrl + '/' + groupId + '/invitation_code')
                            .then(function (response) {
                                if ($scope.group.invitation_code) {
                                    delete $scope.group['invitation_code'];
                                }
                                if ($scope.group.invitation_shorturl) {
                                    delete $scope.group['invitation_shorturl'];
                                }
                            }, function err() {
                                return notificationManager.error('GENERAL ERROR');
                            });
                    }
                });
            };
            $scope.deleteGroup = function () {
                $translate('CONFIRM DELETE GROUP').then(function (confirmationMessage) {
                    if (confirm(confirmationMessage)) {
                        $http.delete(groupUrl + '/' + groupId)
                            .then(function () {
                                notificationManager.info('GROUP DELETED');
                                $state.go("base.mygroups");
                            }, function err() {
                                return notificationManager.error('GENERAL ERROR');
                            });
                    }
                });
            };

            $scope.join = function () {
                $http.put(groupUrl + '/' + groupId + '/join')
                    .then(function () {
                        notificationManager.info("SUCCESSFULLY JOINED THE GROUP");

                        // re-initialize
                        initialize().then(intializeGroupDetails, initializeError);
                    }, function err() {
                        return notificationManager.error('GENERAL ERROR');
                    });
            };

            $scope.leave = function () {
                $http.delete(groupUrl + '/' + groupId + '/join')
                    .then(function () {
                        notificationManager.info("SUCCESSFULLY LEFT THE GROUP");

                        // re-initialize
                        initialize().then(intializeGroupDetails, initializeError);
                    }, function err() {
                        return notificationManager.error('GENERAL ERROR');
                    });
            };

            $scope.userIsSigned = function () {
                if (!$scope.group) {
                    return false;
                }

                var currentUserId = window.currentUser._id;
                var isSigned = _.some($scope.users, function (u) {
                    return u._id === currentUserId;
                });
                return isSigned;
            };
        }];
});
