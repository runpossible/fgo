define(['jquery'], function ($) {
    'use strict';

    return ['$scope', '$http', 'appConfig', '$state', '$translate', 'notificationManager', '$filter',
        function($scope, $http, appConfig, $state, $translate, notificationManager, $filter) {
        $scope.isLoading = false;
        $scope.createGroup = function(isValid) {
            if(isValid) {
                var fileSelector = $('#file-selector-id');
                if(fileSelector && fileSelector[0] && fileSelector[0].files && fileSelector[0].files[0]) {
                    var szInMb = fileSelector[0].files[0].size / (1024.0 * 1024.0);
                    if(szInMb >= 5) {
                        return notificationManager.error('FILE SIZE IS BIGGER THAN 5MB');
                    }
                }

                var url = appConfig.baseApiUrl + 'group';
                $http.post(url, {
                    name: $scope.group.name,
                    description: $scope.group.description
                }).then(function ok(response) {
                    var createdGroup = response.data;
                    var imgUrl = appConfig.baseApiUrl + 'group/' + response.data._id + '/image';
                    $scope.uploadFile(imgUrl);

                    return $state.go('base.managegroup.group', { groupId: createdGroup.urlName || createdGroup._id });
                }, function error(errorResponse) {
                    if(errorResponse.data && errorResponse.data.errorMessage && errorResponse.data.errorMessage.errmsg){
                        if(errorResponse.data.errorMessage.errmsg.indexOf('duplicate key')){
                            return notificationManager.error('GROUP ALREADY EXISTS');
                        }
                    }

                    return notificationManager.error('GROUP SAVE FAILED');
                });
            }
        };

        $scope.fileSelectorHolderText = $filter('translate')('GROUP IMAGE');
    }];
});
