define(function(require) {
    var angular = require('angular');

    var groups = angular.module('groups', ['ui.router']);
    var newGroupCtrl = require('/js/groups/new-group.js');
    var manageGroupCtrl = require('/js/groups/manage-group.js');
    var myGroupsCtrl = require('/js/groups/my-groups.js');
    //var inviteCtrl = require('/js/groups/invite.js');
    var inviteFriendsCtrl = require('/js/groups/invite-friends.js');

    groups.controller('newGroupCtrl', newGroupCtrl);
    groups.controller('manageGroupCtrl', manageGroupCtrl);
    groups.controller('myGroupsCtrl', myGroupsCtrl);
    //groups.controller('inviteCtrl', inviteCtrl);
    groups.controller('inviteFriendsCtrl', inviteFriendsCtrl);

    groups.config(['$stateProvider', function($stateProvider) {
        $stateProvider.state('base.newgroup', {
            url: 'new-group',
            templateUrl: '/js/groups/new-group.html',
            controller: 'newGroupCtrl',
            data:{
                pageTitle: 'NEW GROUP'
            }
        });

        $stateProvider.state('base.managegroup', {
            url: 'manage-group',
            abstract: true,
            templateUrl: '/js/home/empty.html'
        });

        $stateProvider.state('base.managegroup.group', {
            url: '/:groupId',
            templateUrl: '/js/groups/manage-group.html',
            controller: 'manageGroupCtrl',
            data:{
                pageTitle: 'GROUP'
            }
        });

        $stateProvider.state('base.mygroups', {
            url: 'my-groups',
            templateUrl: '/js/groups/my-groups.html',
            controller: 'myGroupsCtrl',
            data:{
                pageTitle: 'MY GROUPS'
            }
        });

        $stateProvider.state('base.invite', {
            url: 'invite',
            abstract: true,
            templateUrl: '/js/home/empty.html'
        });

        // $stateProvider.state('base.invite.group', {
        //     url: '/:groupId?:searchGoogle',
        //     templateUrl: '/js/groups/invite.html',
        //     controller: 'inviteCtrl'
        // });
        
        $stateProvider.state('base.invite.group', {
            url: '/:groupId',
            templateUrl: '/js/groups/invite-friends.html',
            controller: 'inviteFriendsCtrl',
            data:{
                pageTitle: 'INVITE YOUR FRIENDS'
            }
        });
    }]);

    return groups;
});
