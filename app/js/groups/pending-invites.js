define(['lodash'], function (_) {
    'use strict';

    return ['$parse', '$translate', '$http', 'appConfig', 'notificationManager', function ($parse, $translate, $http, appConfig, notificationManager) {
        return {
            restrict: 'E',
            scope: {}, // isolate scope.
            templateUrl: '/js/groups/pending-invites.html?t=' + window.requestSuffix,
            link: function ($scope, element, attrs) {
                //group/:groupId/invite/:inviteId/:status
                var invitesUrl = appConfig.baseApiUrl + 'invite';

                $scope.isLoading = true;
                var loadInvites = function () {
                    $http.get(invitesUrl).then(function (response) {
                        $scope.invites = response.data || [];

                        $scope.groupInvites = _.filter($scope.invites, function (inv) {
                            return inv.group !== undefined;
                        });

                        $scope.gameInvites = _.filter($scope.invites, function (inv) {
                            return inv.game !== undefined;
                        });

                        $scope.hasGroupInvites = $scope.groupInvites.length > 0;
                        $scope.hasGameInvites = $scope.gameInvites.length > 0;

                        $scope.isLoading = false;
                    }, function err() {
                        return notificationManager.error('GENERAL ERROR');
                    });
                };

                // initial invite loading
                loadInvites();

                $scope.inviteAction = function (invite, status) {
                    $scope.isLoading = true;

                    var url = '';
                    if(invite.group) {
                        url = appConfig.baseApiUrl + 'group/' + invite.group._id
                            + '/invite/' + invite._id;
                    } else {
                        url = appConfig.baseApiUrl + 'game/' + invite.game._id
                            + '/invite/' + invite._id;
                    }

                    $http.put(url, {
                        status: status
                    }).then(function () {
                        // no translation here yet.
                        if (status == 'Accepted') {
                            $translate('SUCCESSFULLY JOINED THE ' + (invite.group ? 'GROUP' : 'GAME')).then(function (translatedMessage) {
                                notificationManager.success(translatedMessage, false);
                            });
                        }
                        else {
                            $translate('REJECTED JOINING TO ' + (invite.group ? 'GROUP' : 'GAME')).then(function (translatedMessage) {
                                notificationManager.info(translatedMessage, false);
                            });
                        }
                        loadInvites(); // refresh the invites
                    }, function err() {
                        notificationManager.error('GENERAL ERROR');

                        // should anyway load the invites again.
                        loadInvites();
                    });
                };
            }
        };
    }];
});