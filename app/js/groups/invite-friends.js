define(['lodash'], function (_) {
    'use strict';

    return ['$scope', '$stateParams', 'appConfig', '$state', '$http', '$q', 'notificationManager', '$window', function($scope, $stateParams, appConfig, $state, $http, $q, notificationManager, $window) {
        var groupId = $stateParams.groupId,
            groupUrl = appConfig.baseApiUrl + 'group',// TODO: add these to some constants.
            inviteUrl = appConfig.baseApiUrl + 'invite',
            inviteGroupUrl = inviteUrl + '/' + groupId;

        $scope.currentUser = $window.currentUser;
        $scope.gameInvites = {};

        var getGame = function getGame(callback) {
            $http.get(groupUrl + '/' + groupId)
                .then(function(response) {
                    groupId = response.data._id;
                    inviteGroupUrl = inviteUrl + '/' + groupId;
                    $scope.group = response.data;
                    $scope.users = ($scope.group.admin_users || []).concat($scope.group.users || []);
                    
                    return callback(null, response.data);
                }, function() {
                    return callback('GENERAL ERROR');
                });
        };

        var getActiveGroupInvites = function(callback) {
            $http.get(inviteGroupUrl)
                .then(function(response) {
                    $scope.gameInvites = response.data || [];
                    return callback(null, response.data);
                }, function() {
                    return callback('GENERAL ERROR');
                });
        };

        $scope.isMainLoading = true;

        var processData = function(){
            getGame(function(err, game){
                if(err){
                    $scope.isMainLoading = false;
                    return notificationManager.error(err);
                }
                else{
                    getActiveGroupInvites(function(err){
                        if(err){
                            $scope.isMainLoading = false;
                            return notificationManager.error(err);
                        }
                        else{
                            $scope.refreshFriends = new Date().getMilliseconds();
                            $scope.isMainLoading = false;
                        }
                    });
                }
            })
        };

        processData();

        $scope.invite = function invite(user, done) {
            $http.post(inviteUrl, {
                playerId: user._id,
                groupId: groupId
            }).then(function () {
                user.alreadyInvited = true; // it's invited now
                $scope.gameInvites.push(user);
                done(null, user);
            }, function err() {
                done('GENERAL ERROR');
                return notificationManager.error('GENERAL ERROR');
            });
        };

        $scope.checkInvited = function checkInvited(user) {
            if(_.some($scope.users, { _id: user._id })){
                return 1;
            } else if(_.some($scope.gameInvites, function(inv) {
                return inv.player._id === user._id;
            })){
                return 2;
            }
            return 0;
        };
    }];
});
