define(['lodash'], function (_) {
    'use strict';

    return ['$scope', '$stateParams', 'appConfig', '$state', '$http', '$q', 'notificationManager', '$window', 'imageService',
        function ($scope, $stateParams, appConfig, $state, $http, $q, notificationManager, $window, imageService) {
        var groupUrl = appConfig.baseApiUrl + 'group';

        // group status functions
        var groupStatus = function(userId, group) {
            return {
                isAdmin: _.some(group.admin_users, {_id: userId}),
                isOwner: group.createdBy && (group.createdBy._id === userId)
            };
        };

        $scope.isLoading = true;
        var initialize = function initialize() {
            var deferred = $q.defer();
            $http.get(groupUrl)
                .then(function (response) {
                    deferred.resolve(response.data);
                }, function() {
                    deferred.reject();
                });
            return deferred.promise;
        };

        initialize().then(function (groups) {
            groups = _.sortBy(groups, 'name');

            $scope.groups = groups;

            _.each(groups, function(g) {
                var gs = groupStatus($window.currentUser._id, g);

                g.imageUrl = imageService.getGroupImageUrl(g);

                _.extend(g, gs); // extend with statuses.
            });

            $scope.isLoading = false;
        }, function err() {
            return notificationManager.error('GENERAL ERROR');
        });
    }];
});
