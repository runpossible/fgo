define([], function() {
    'use strict';

    return ['$window', 'menuService', '$state', function($window, menuService, $state) {
        return {
            restrict: 'E',
            templateUrl: '/js/common/sub-nav.html?t=' + window.requestSuffix,
            link: function($scope) {
                $scope.mainItemNames = menuService.mainItemNames;

                var selectFromState = function(stateName) {
                    // force angular to rebind
                    $scope.$evalAsync(function() {
                        $scope.mainSelectedItem = menuService.getMainSelectedItem(stateName);
                        $scope.subSelectedItem = stateName;
                    });
                };

                $scope.$on('onStateChanged', function(evt, toState) {
                    return selectFromState(toState.name);
                });

                var window = angular.element($window);
                window.ready(function() {
                    selectFromState($state.current.name);
                });
            }
        };
    }];
});