define(['jquery', 'metismenu'], function($) {
    'use strict';

    return ['appConfig', 'inviteService', '$filter', '$rootScope', '$window', function(appConfig, inviteService, $filter, $rootScope, $window) {

        return {
            restrict: 'E',
            templateUrl: '/js/common/player-item.html?t=' + window.requestSuffix,
            scope: {
                player: '=',
                view: '=',
                game: '=',
                showRemovePlayer: '=',
                players: '='
            },
            link: function($scope) {
                if($scope.player && !$scope.player.age && $scope.player.birthDay){
                    $scope.player.age = new Date(new Date - new Date($scope.player.birthDay)).getFullYear()-1970;
                }

                $scope.removePlayerFromGame = function($event) {
                    // stop the event propagation and navigation to the player's profile
                    $event.preventDefault();
                    $event.stopPropagation();

                    if($window.confirm($filter('translate')('ARE YOU SURE YOU WANT TO SKIP THE GAME'))) {
                        // reject the invite
                        inviteService.removeFromGame($scope.player._id, $scope.game._id, function done() {
                            // todo: add notification service here.
                            _.remove($scope.players, function(p) {
                                return p._id === $scope.player._id;
                            });

                            $rootScope.$emit('invitationsChange'); // changed
                        });
                    }
                };
            }
        };
    }];
});