define([], function() {
    'use strict';

    return function() {
        return {
            restrict: 'E',
            templateUrl: '/js/common/loader.html?t=' + window.requestSuffix,
            link: function() {
            }
        };
    }
});