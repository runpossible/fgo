define(['lodash'], function (_) {
    'use strict';

    return function () {
        return {
            restrict: 'E',
            templateUrl: '/js/common/tablePager.html?t=' + window.requestSuffix,
            replace: true,
            scope: {
                options: '='
            },
            link: function ($scope) {
                $scope.prev = $scope.options.prev;
                $scope.hasPrev = $scope.options.hasPrev;
                $scope.hasNext = $scope.options.hasNext;
                var pageSz = $scope.options.getPageSize();

                $scope.navigate = function (pageNumber) {
                    $scope.currentPage = pageNumber;
                    $scope.options.go(pageNumber);
                };

                var updatePager = function () {
                    var total = $scope.options.getTotal();
                    var cPage = $scope.options.getPage();

                    if (_.some($scope.pageNumbers, cPage)) {
                        return; // it's ok
                    } else {
                        // begin new ...
                        var count = 1;
                        $scope.pageNumbers = [];
                        $scope.pageNumbers.push(cPage);
                        while (cPage * pageSz < total) {
                            $scope.pageNumbers.push(cPage + 1);
                            cPage++;
                            count++;

                            //if (count == 5) break;
                        }

                        /*
                        if (count === 5 && cPage * pageSz < total) {
                            $scope.hasNext = true;
                        } else {
                            $scope.hasNext = false;
                        }*/
                    }
                    //console.log('current page = ' + $scope.options.getPage());
                    $scope.currentPage = $scope.options.getPage();
                };

                //$scope.$watch('options.page', updatePager);
                $scope.$watch('{ total: options.getTotal() }', updatePager);

                $scope.next = function(){
                    if($scope.currentPage < $scope.pageNumbers.length) {
                        $scope.navigate($scope.currentPage + 1);
                    }
                };

                $scope.prev = function(){
                    if($scope.currentPage > 1) {
                        $scope.navigate($scope.currentPage - 1);
                    }
                };

                updatePager();
            }
        };
    }
});