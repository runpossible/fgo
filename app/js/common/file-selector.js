define(['jquery'], function ($) {
    'use strict';

    return ['FileUploader', function (FileUploader) {
        return {
            restrict: 'E',
            templateUrl: '/js/common/file-selector.html?t=' + window.requestSuffix,
            replace: true,
            compile: function compile(tElement, tAttributes, transcludeFn) {
                // Compile code goes here.
                return {
                    pre: function preLink($scope, element, attributes, controller, transcludeFn) {
                        // Pre-link code goes here
                        //console.log('pre');
                        var uploader = new FileUploader();
                        uploader.filters.push({
                            name: 'imageFilter',
                            fn: function (item /*{File|FileLikeObject}*/, options) {
                                var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                                return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
                            }
                        });

                        //https://github.com/nervgh/angular-file-upload/blob/master/examples/image-preview/controllers.js
                        uploader.onWhenAddingFileFailed = function (item /*{File|FileLikeObject}*/, filter, options) {
                            //console.info('onWhenAddingFileFailed', item, filter, options);
                        };

                        // file upload
                        uploader.onAfterAddingFile = function (fileItem) {
                            $scope.selectedPicture = fileItem.file.name;
                        };

                        $scope.uploadFile = function uploadFile(url) {
                            if($scope.uploader.queue.length) {
                                $scope.uploader.queue[0].url = url;
                                $scope.uploader.uploadAll();
                            }
                        };

                        $scope.uploader = uploader;

                        $scope.uploadBtnClicked = function() {
                            $('#file-selector-id').click();
                        };
                    }
                };
            }
        };
    }];
});