define([], function() {
    'use strict';

    return ['$rootScope', '$timeout', '$filter', function($rootScope, $timeout, $filter) {
        return {
            link: function(scope, element) {

                var listener = function(event, toState) {

                    var pageTitle = "FGO TITLE";
                    var custom = false;

                    if (toState.data && toState.data.pageTitle) {
                        pageTitle = toState.data.pageTitle;
                        custom = true;
                    }

                    var translated = (custom ? "FGO: " : '') + $filter('translate')(pageTitle);

                    element.text(translated);

                    $timeout(function() {
                        translated = (custom ? "FGO: " : '') + $filter('translate')(pageTitle);
                        if(element.text() != translated){
                            element.text(translated);
                        }
                    }, 500, false);
                };

                $rootScope.$on('$stateChangeSuccess', listener);
            }
        };
    }];
});