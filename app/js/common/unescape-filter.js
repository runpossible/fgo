define([], function() {
    return function () {
        return function (val) {
            if (!val || typeof val !== 'string') {
                return val;
            }

            return val.replace(/&quot;/g, '"')
                .replace(/&amp;/g, '&')
                .replace(/&#39;/g, '\'')
                .replace(/&#39;/g, '\'')
                .replace(/&lt;/g, '<')
                .replace(/&gt;/g, '>');
        }
    }
});