define(['jquery', 'metismenu'], function($) {
    'use strict';

    return ['appConfig', 'inviteService', '$filter', '$rootScope', '$window', '$http', 'notificationManager', function(appConfig, inviteService, $filter, $rootScope, $window, $http, notificationManager) {

        return {
            restrict: 'E',
            templateUrl: '/js/common/guest-item.html?t=' + window.requestSuffix,
            scope: {
                guest: '=',
                guests: '=',
                game: '='
            },
            link: function($scope) {
                $scope.removeGuestFromGame = function($event) {
                    // stop the event propagation and navigation to the player's profile
                    $event.preventDefault();
                    $event.stopPropagation();

                    if($window.confirm($filter('translate')('ARE YOU SURE YOU WANT TO REMOVE THE GUEST FROM THE GAME'))) {
                        // remove the guest
                        var url = appConfig.baseApiUrl + 'game/' + $scope.game._id + '/guest/' + $scope.guest._id;
                        $http.delete(url).then(function() {
                            _.remove($scope.guests, function(g) {
                                return g._id === $scope.guest._id;
                            });

                            return notificationManager.info('SUCCESSFULLY REMOVED GUEST FROM THE GAME');
                        }, function err() {
                            return notificationManager.error('GENERAL ERROR');
                        });
                    }
                };
            }
        };
    }];
});