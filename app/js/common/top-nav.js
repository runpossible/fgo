define([], function() {
    'use strict';

    return ['$window', '$translate', '$http', '$state', 'appConfig', 'menuService', function($window, $translate, $http, $state, appConfig, menuService) {
        return {
            restrict: 'E',
            templateUrl: '/js/common/top-nav.html?t=' + window.requestSuffix,
            link: function($scope) {
                // navigation
                var selectFromState = function(stateName) {
                    // force angular to rebind
                    $scope.$evalAsync(function() {
                        $scope.selectedItem = menuService.getMainSelectedItem(stateName);
                    });
                };

                $scope.showAdministration = $window.currentUser.master;

                $scope.itemNames = menuService.mainItemNames;

                $scope.$on('onStateChanged', function(evt, toState) {
                    return selectFromState(toState.name);
                });

                var window = angular.element($window);
                window.ready(function() {
                    $('a', '#main-menu').click(closeNav);
                    return selectFromState($state.current.name);
                });

                Object.defineProperty($scope, 'currentLanguage', {
                    get: function() {
                        var currentLanguage = $translate.use();
                        if(currentLanguage){
                            currentLanguage = currentLanguage.toLowerCase();
                        }
                        return currentLanguage;
                    }
                });

                $scope.changeLanguage = function (language) {
                    $translate.use(language);
                };

                $scope.currentUser = $window.currentUser;

                $scope.logoutFromTop = function() {
                    // logout user
                    $http.post(appConfig.baseApiUrl + 'auth/logout', {})
                        .then(function() {
                            // clear out the current user.
                            $window.currentUser = undefined;

                            // and navigate to the login page.
                            $window.location.href = "/welcome";
                        });
                };
            }
        };
    }];
});