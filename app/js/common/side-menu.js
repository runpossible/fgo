define(['jquery', 'metismenu'], function($) {
    'use strict';

    return ['$window', 'UIService', '$http', 'appConfig', '$state', function($window, UIService, $http, appConfig, $state) {

        return {
            restrict: 'E',
            templateUrl: '/js/common/side-menu.html?t=' + window.requestSuffix,
            link: function($scope) {
                //Loads the correct sidebar on window load,
                //collapses the sidebar on window resize.
                // Sets the min-height of #page-wrapper to window size
                var window = angular.element($window);

                window.ready(function () {
                    $('#side-menu').metisMenu();
                    UIService.checkScreenSize($window.location.pathname);
                });

                window.bind("resize", function(){
                    UIService.checkScreenSize($window.location.pathname);
                });

                $scope.logout = function() {
                    // logout user
                    $http.post(appConfig.baseApiUrl + 'auth/logout', {})
                        .then(function() {
                            // clear out the current user.
                            $window.currentUser = undefined;

                            // and navigate to the login page.
                            window.location.href = "/welcome";
                        });
                };

                $scope.isFeatureEnabled = function() {
                    return true;
                    //return $window.location.hostname === 'localhost';
                };
            }
        };
    }];
});