define(['jquery', 'metismenu'], function($) {
    'use strict';

    return ['appConfig', function(appConfig) {

        return {
            restrict: 'E',
            templateUrl: '/js/common/friend-player-item.html?t=' + window.requestSuffix,
            scope: {
                player: '=?',
                removeMode: '=?',
                clickAction: '&clickAction',
                clickName: '=?',
                clicked: '=?'
            },
            link: function($scope) {
                if($scope.view){
                    $scope.view = 'rating';
                }

                if($scope.player && !$scope.player.age && $scope.player.birthDay){
                    $scope.player.age = new Date(new Date - new Date($scope.player.birthDay)).getFullYear()-1970;
                }

                $scope.iconClick = $scope.clickAction();
                $scope.click = function(player){
                    if($scope.iconClick(player)){
                        $scope.clicked = true;
                    }
                }
            }
        };
    }];
});