define(['jquery', 'metismenu'], function($) {
    'use strict';

    return ['appConfig', function(appConfig) {

        return {
            restrict: 'E',
            templateUrl: '/js/common/validation-summary.html?t=' + window.requestSuffix,
            scope: {
                formToValidate: '='
            },
            link: function($scope) {
            }
        };
    }];
});