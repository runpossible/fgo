define(['lodash'], function(_) {
    'use strict';

    return ['$http', 'appConfig', '$translate', function($http, appConfig, $translate) {
        return {
            restrict: 'E',
            templateUrl: '/js/common/location-selector.html?t=' + window.requestSuffix,
            replace: true,
            scope: {
                selectedLocationId: '=',
                selectedLocation: '='
            },
            link: function($scope) {
                $scope.location = {};

                // TODO: Location service
                $http.get(appConfig.baseApiUrl + 'location').then(
                    function(result) {
                        /* works for favourite/location
                        var locations = _.map(result.data, function(item) {
                            return {
                                id: item.location._id,
                                name: item.location.name,
                                price: item.location.price
                            }
                        });*/
                        var locations = _.map(result.data, function(item) {
                            return {
                                id: item._id,
                                name: item.name,
                                price: item.price
                            }
                        });;

                        $translate('LOCATION').then(function(locationText) {
                            locations.unshift({
                                name: locationText,
                                id: 'empty',
                                price: ''
                            });

                            $scope.locations = locations;

                            // if we don't have anything selected, set the first location by default.
                            if(!$scope.selectedLocationId && locations.length) {
                                $scope.selectedLocationId = locations[0].id;
                                $scope.selectedLocation = locations[0];
                            } else if ($scope.selectedLocationId && locations.length) {
                                $scope.selectedLocation = _.find(locations, function(l){ return l.id == $scope.selectedLocationId});
                            }
                        });
                    }, function(err) {
                        // todo; error handling, global.
                    });

                $scope.$watch('selectedLocationId', function(id) {
                    $scope.selectedLocation = _.find($scope.locations, { 'id': id });
                    //console.log($scope.selectedLocation);
                });
            }
        };
    }]
});