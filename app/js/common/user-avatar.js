define([], function() {
    'use strict';

    // we have small = 50px, normal = 100px and large = 200px
    // add userid or email if you want to get the avatar of another user
    return ['$parse', '$translate', '$http', 'appConfig',
        function($parse, $translate, $http, appConfig) {
        return {
            restrict: 'E',
            scope: {
                user: '='
            },
            templateUrl: '/js/common/user-avatar.html?t=' + window.requestSuffix,
            link: function($scope, element, attrs) {
                if(attrs.disabled){
                    return;
                }

                $scope.height = attrs.height;
                $scope.width = attrs.width;

                if (attrs.style){
                    element.attr('style', attrs.style);
                }

                //if($scope.user) {
                //    $scope.avatarUrl = $scope.user.avatarUrl || appConfig.defaultAvatarUrl;
                //}

                $scope.$watch('user', function() {
                    if($scope.user) {
                        $scope.avatarUrl = $scope.user.avatarUrl || appConfig.defaultAvatarUrl;
                    } else {
                        $scope.avatarUrl = appConfig.defaultAvatarUrl;
                    }
                });
            }
        };
    }];
});