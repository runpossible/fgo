define([], function() {
    'use strict';

    // we have small = 50px, normal = 100px and large = 200px
    // add userid or email if you want to get the avatar of another user
    return ['$stateParams', 'appConfig', '$state', '$http', '$translate', '$interval', '$q', 'notificationManager', 'timeService',
        function($stateParams, appConfig, $state, $http, $translate, $interval, $q, notificationManager, timeService) {
        return {
            restrict: 'E',
            templateUrl: '/js/common/comments.html?t=' + window.requestSuffix,
            replace: true,
            link: function($scope, element, attrs) {
                var entityId = $stateParams.gameId || $stateParams.groupId;
                var commentsUrl = appConfig.baseApiUrl + 'comments/'
                        + ($stateParams.gameId ? 'game' : 'group');

                var loadComments = function loadComments(){
                    var deferred = $q.defer();
                    $http.get(commentsUrl + '/' + entityId)
                        .then(function(response) {
                            deferred.resolve(response.data);
                        }, function() {
                            deferred.reject();
                        });
                    return deferred.promise;
                };

                $scope.refreshComments = function(){
                    loadComments().then(function(comments) {
                        $scope.comments = comments;
                    })
                };

                $scope.submitComment = function(){
                    if( $scope.newComment &&  $scope.newComment.trim() != '') {
                        var body = {
                            text: $scope.newComment
                        };
                        $http.post(commentsUrl + '/' + entityId, body)
                            .then(function (response) {
                                response.data.createdBy = window.currentUser;
                                response.data.game = $scope.game;
                                $scope.comments.push(response.data);
                                $scope.newComment = '';
                            }, function error() {
                                return notificationManager.error('GENERAL ERROR');
                            });
                    }
                };

                $scope.timeSince = function(date_string){
                    return timeService.timeSince(date_string);
                };

                $scope.refreshComments();
            }
        };
    }];
});