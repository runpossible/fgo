define([], function() {
    'use strict';

    // we have small = 50px, normal = 100px and large = 200px
    // add userid or email if you want to get the avatar of another user
    return ['$parse', '$translate', '$http', 'appConfig', '$window', 'avatarService',
        function($parse, $translate, $http, appConfig, $window, avatarService) {
        return {
            restrict: 'E',
            scope: {
                userid: '=',
                email: '='
            },
            templateUrl: '/js/common/avatar.html?t=' + window.requestSuffix,
            link: function($scope, element, attrs) {
                if(attrs.disabled){
                    return;
                }
                var preferredAvatar = attrs.preferred;
                var type = attrs.type;
                if (!type) {
                    type = 'small';
                }
                $scope.height = attrs.height;
                $scope.width = attrs.width;

                if (attrs.style){
                    element.attr('style', attrs.style);
                }

                var getAvatarUrl = function (type, userId, email, preferredAvatar) {
                    var avatarUrl = avatarService.getAvatarUrl(type, userId, email, preferredAvatar);
                    if(avatarUrl){
                        $scope.avatarUrl = avatarUrl;
                        return
                    }

                    if((userId == $window.currentUser._id) && $window.currentUser.avatarUrl
                        && (!preferredAvatar || preferredAvatar == $window.currentUser.preferredAvatar) && type == 'normal'){
                        $scope.avatarUrl = $window.currentUser.avatarUrl;
                    } else {

                        var url = appConfig.baseApiUrl + 'avatar/' + (userId || email) + '/' + type;
                        if(preferredAvatar)
                        {
                            url = url + "?preferredAvatar=" + preferredAvatar;
                        }

                        $http.get(url).then(
                            function (result) {
                                avatarService.setAvatarUrl(type, userId, email, preferredAvatar, result.data);
                                $scope.avatarUrl = result.data;
                            }, function (err) {
                                // todo; error handling, global.
                            });
                    }
                };

                if($scope.userid){
                    getAvatarUrl(type, $scope.userid, $scope.email, preferredAvatar, $scope.avatarUrl);
                }

                $scope.$watch('userid', function(id) {
                    if($scope.userid) {
                        getAvatarUrl(type, $scope.userid, $scope.email, preferredAvatar, $scope.avatarUrl);
                    }
                });
            }
        };
    }];
});