define(['jquery'], function ($) {
    'use strict';

    return ['$parse', '$translate', '$http', 'countryService', 'notificationManager', function ($parse, $translate, $http, countryService, notificationManager) {
        return {
            restrict: 'E',
            templateUrl: '/js/common/country-select.html?t=' + window.requestSuffix,
            replace: true,
            scope: {
                selectedCountryId: '='
            },
            link: function ($scope) {

                countryService.getCountries(function (err, countries) {
                    if (err) {
                        return notificationManager.error('GENERAL ERROR');
                    } else {
                        $scope.countries = countries;
                    }
                });

                //var lang = $translate.use();

                //$http.get('/languages/countries.json').then(
                //  function(result) {
                //    var countries = [];
                //    var data = result.data;
                //    for(var i=0;i<data.length;i++) {
                //      countries.push({
                //        id: data[i].id,
                //        name: data[i][lang],
                //        order: data[i].order
                //      });
                //    }
                //
                //    $scope.countries = countries;
                //    //if(countries.length > 0){
                //    //  $scope.selectedCountryId = countries[0].id;
                //    //}
                //  }, function(err) {
                //    // todo; error handling, global.
                //  });
            }
        };
    }]
});