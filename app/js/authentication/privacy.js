define([], function () {
    'use strict';

    return ['$scope', '$translate',
        function($scope, $translate) {
            Object.defineProperty($scope, 'currentLanguage', {
                get: function() {
                    var currentLanguage = $translate.use();
                    if(currentLanguage){
                        currentLanguage = currentLanguage.toLowerCase();
                    }
                    return currentLanguage;
                }
            });
        }];
});