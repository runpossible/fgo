define([], function () {
    'use strict';

    return ['$scope', '$stateParams', '$state', '$http', 'appConfig',
        function($scope, $stateParams, $state, $http, appConfig) {
            var userUrl = appConfig.baseApiUrl + 'user';
            $scope.isLoading = true;

            $http.post(userUrl + '/activate',{
                code: $stateParams.code
            }).then(function ok(response) {
                $scope.successfulActivation = true;
                $scope.isLoading = false;
            }, function error(response) {
                if(response.status == 404){
                    $scope.activationError = 'WRONG ACTIVATION CODE';
                } else {
                    $scope.activationError = 'GENERAL ERROR';
                }
                $scope.isLoading = false;
            });
    }];
});
