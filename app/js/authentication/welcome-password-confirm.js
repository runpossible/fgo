define(['jquery'], function($) {
    'use strict';

    // we have small = 50px, normal = 100px and large = 200px
    // add userid or email if you want to get the avatar of another user
    return ['$parse', '$translate', '$http', 'appConfig', function($parse, $translate, $http, appConfig) {
        return {
            restrict: 'E',
            require: ['^form', 'ngModel'],
            templateUrl: '/js/authentication/welcome-password-confirm.html?t=' + window.requestSuffix,
            link: function($scope, element, attrs, ctrls) {
                $scope.$watch(function() { return element.attr('allowempty'); }, function(value){
                    $scope.fieldsRequired = !(value === 'true');
                });

                $scope.passwordlabel = attrs.passwordlabel;

                $scope.form = ctrls[0];
                var ngModel = ctrls[1];

                var setFormValidation = function () {
                    if($scope.form.confirm_password) {
                        $scope.form.confirm_password.$invalid = (!$scope.fieldsRequired && $scope.password == '') || ($scope.password != $scope.password_verify);
                    }
                    ngModel.$setValidity('password_confirmed', (!$scope.fieldsRequired && $scope.password == '') || $scope.password == $scope.password_verify);
                };

                $scope.$watch('password', function () {
                    ngModel.$setViewValue($scope.password);
                    setFormValidation();
                });

                $scope.$watch('password_verify', function () {
                    setFormValidation();
                });
            }
        };
    }];
});