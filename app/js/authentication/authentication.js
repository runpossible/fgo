define(function(require) {
	var angular = require('angular');

    var authentication = angular.module('authentication', ['ui.router']);
    var loginCtrl = require('/js/authentication/login.js');
    var landingPageCtrl = require('/js/authentication/landing-page.js');
    var loginDir = require('/js/authentication/login-control.js');
    var welcomeLoginDir = require('/js/authentication/welcome-login-control.js');
    var passwordConfirmDir = require('/js/authentication/password-confirm.js');
    var welcomePasswordConfirmDir = require('/js/authentication/welcome-password-confirm.js');
    var welcomePasswordResetDir = require('/js/authentication/welcome-password-reset.js');
    var welcomeSetNewPasswordDir = require('/js/authentication/welcome-set-new-password.js');
    var welcomeActivateUserDir = require('/js/authentication/welcome-activate-user.js');
    var contactUsDir = require('/js/authentication/contact-us.js');
    var termsDir = require('/js/authentication/terms-and-conditions.js');
    var privacyCtrl = require('/js/authentication/privacy.js');
    var passwordResetCtrl = require('/js/authentication/password-reset.js');
    var activateUserCtrl = require('/js/authentication/activate-user.js');

    authentication.controller('LoginCtrl', loginCtrl);
    authentication.controller('LandingPageCtrl', landingPageCtrl);
    authentication.controller('PasswordResetCtrl', passwordResetCtrl);
    authentication.controller('ActivateUserCtrl', activateUserCtrl);
    authentication.controller('PrivacyCtrl', privacyCtrl);
    authentication.directive('login', loginDir);
    authentication.directive('welcomeLogin', welcomeLoginDir);
    authentication.directive('passwordConfirm', passwordConfirmDir);
    authentication.directive('welcomePasswordConfirm', welcomePasswordConfirmDir);
    authentication.directive('welcomePasswordReset', welcomePasswordResetDir);
    authentication.directive('welcomeSetNewPassword', welcomeSetNewPasswordDir);
    authentication.directive('welcomeActivateUser', welcomeActivateUserDir);
    authentication.directive('contactUs', contactUsDir);
    authentication.directive('termsAndConditions', termsDir);

	authentication.config(['$stateProvider', function($stateProvider) {
        $stateProvider.state('login', {
            url: '/login?:redirect_uri',
            data: {
                requireLogin: false,
                pageTitle: 'Login'
            },
            templateUrl: '/js/authentication/login.html',
            controller: 'LoginCtrl'
        });

        $stateProvider.state('landingPage', {
            url: '/welcome?:secret&:code&:login&:contact&:redirect_uri&:terms&:missingEmail',
            data: {
                requireLogin: false,
                requireSSL: true
            },
            templateUrl: '/js/authentication/landing-page.html',
            controller: 'LandingPageCtrl'
        });

        $stateProvider.state('privacy', {
            url: '/privacy.html',
            data: {
                requireLogin: false,
                requireSSL: true
            },
            templateUrl: '/js/authentication/privacy.html',
            controller: 'PrivacyCtrl'
        });
            
        $stateProvider.state('passwordReset', {
            url: '/password-reset?:secret',
            data: {
                requireLogin: false,
                pageTitle: 'Password Reset'
            },
            templateUrl: '/js/authentication/password-reset.html',
            controller: 'PasswordResetCtrl'
        });

        $stateProvider.state('activateUser', {
            url: '/activate?:code',
            data: {
                requireLogin: false,
                pageTitle: 'User activation'
            },
            templateUrl: '/js/authentication/activate-user.html',
            controller: 'ActivateUserCtrl'
        });
    }]);

    return authentication;
});
