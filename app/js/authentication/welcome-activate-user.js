define(['jquery'], function($) {
    'use strict';

    return ['appConfig', '$http', '$state', '$translate', '$window', '$filter', '$stateParams',
        function(appConfig, $http, $state, $translate, $window, $filter, $stateParams) {
        return {
            restrict: 'E',
            templateUrl: '/js/authentication/welcome-activate-user.html?t=' + window.requestSuffix,
            link: function($scope) {
                if($stateParams.code){
                    var userUrl = appConfig.baseApiUrl + 'user';
                    
                    $scope.activatingUser = true;
                    $scope.resetActivateUser = function(){
                        $scope.activatingUserForm.$setPristine();
                        $scope.activatingUserForm.$setUntouched();

                        $scope.activatingUser = true;
                        $scope.activationError = '';
                    };
                    
                    $scope.userLogin(5);

                    $http.post(userUrl + '/activate',{
                        code: $stateParams.code
                    }).then(function ok(response) {
                        $scope.successfulActivation = true;
                        $scope.activatingUser = false;
                    }, function error(response) {
                        if(response.status == 404){
                            $scope.activationError = 'WRONG ACTIVATION CODE';
                        } else {
                            $scope.activationError = 'GENERAL ERROR';
                        }
                        $scope.activatingUser = false;
                    });
                }
            }
        };
    }];
});