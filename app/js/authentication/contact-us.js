define(['jquery'], function($) {
    'use strict';

    return ['appConfig', '$http', '$state', '$translate', '$window', '$filter', '$stateParams',
        function(appConfig, $http, $state, $translate, $window, $filter, $stateParams) {
            return {
                restrict: 'E',
                templateUrl: '/js/authentication/contact-us.html?t=' + window.requestSuffix,
                link: function($scope) {
                    $scope.isLoading = false;

                    $scope.resetContactUs = function(){
                        $scope.name = '';
                        $scope.email = '';
                        $scope.message = '';
                        $scope.contactForm.$setPristine();
                        $scope.contactForm.$setUntouched();

                        $scope.messageSent = false;
                    };

                    var contactUsUrl = appConfig.baseApiUrl + 'user/contact';
                    $scope.messageSent = false;
                    $scope.send = function(isValid) {
                        $scope.isLoading = true;
                        if(isValid) {
                            $http.post(contactUsUrl, {
                                name: $scope.name,
                                email: $scope.email,
                                message: $scope.message
                            }).then(function ok(response){
                                $scope.messageSent = true;
                                //$scope.passwordChanged = true;
                                $scope.isLoading = false;
                            }, function error() {
                                //$scope.passwordChangeError = 'INVALID REQUEST SECRET';
                                $scope.isLoading = false;
                            });
                        }
                    };
                }
            };
        }];
});