define(['jquery'], function($) {
    'use strict';

    return ['appConfig', '$http', '$state', '$translate', '$window', '$filter', function(appConfig, $http, $state, $translate, $window, $filter) {
        return {
            restrict: 'E',
            templateUrl: '/js/authentication/welcome-password-reset.html?t=' + window.requestSuffix,
            link: function($scope) {
                var userUrl = appConfig.baseApiUrl + 'user';

                $scope.resetForgotPassword = function(){
                    $scope.email = '';
                    $scope.enterEmailForm.$setPristine();
                    $scope.enterEmailForm.$setUntouched();

                    $scope.resetPasswordError = null;
                    $scope.passwordSendingLoading = false;
                };

                $scope.resetPassword = function(isValid) {
                    $scope.resetPasswordError = null;
                    $scope.passwordSendingLoading = true;
                    if(isValid) {
                        $http.post(userUrl + '/reset-password',{
                            email: $scope.email
                        }).then(function ok(response) {
                            $scope.passwordChangeRequested = true;
                            $scope.passwordSendingLoading = false;
                        }, function error() {
                            $scope.resetPasswordError = 'GENERAL ERROR';
                            $scope.passwordSendingLoading = false;
                        });
                    }
                };
            }
        };
    }];
});