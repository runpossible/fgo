define(['jquery'], function($) {
    'use strict';

    return ['appConfig', '$http', '$state', '$translate', '$window', '$filter', '$stateParams',
        function(appConfig, $http, $state, $translate, $window, $filter, $stateParams) {
        return {
            restrict: 'E',
            templateUrl: '/js/authentication/welcome-login-control.html?t=' + window.requestSuffix,
            link: function($scope) {
                if($state.params.redirect_uri){
                    $scope.redirect_uri = "?redirect_uri=" + $state.params.redirect_uri;
                }

                var resetForms = function() {
                    $scope.resetLogin();
                    $scope.resetSignUp();
                };

                $scope.resetLogin = function(){
                    $scope.loginForm.$setPristine();
                    $scope.loginForm.$setUntouched();
                    $scope.data = angular.copy({});
                    $scope.loginError = undefined;
                };

                $scope.resetSignUp = function(){
                    $scope.data = angular.copy({
                        password: '',
                        preferredPosition: ''
                    });
                    $scope.regForm.$setPristine();
                    $scope.regForm.$setUntouched();
                    $scope.registering = false;
                    $scope.regError = undefined;
                };

                if($stateParams.login){
                    if($stateParams.missingEmail){
                        $scope.userLogin(8);
                    } else {
                        $scope.userLogin(2);
                    }
                }
                if($stateParams.contact){
                    $scope.userLogin(6);
                }
                if($stateParams.terms){
                    $scope.userLogin(7);
                }

                $scope.login = function(isValid) {
                    $scope.profileCreated = false;
                    if (isValid) {
                        $scope.loggingIn = true;
                        // can proceed with login
                        var url = appConfig.baseApiUrl + 'auth/login';
                        var onFailedLogin = function(message) {
                            $scope.loginError = $filter('translate')(message || 'WRONG CREDENTIALS');
                            $scope.loggingIn = false;
                        };
                        $http.post(url, {
                            username: $scope.data.email,
                            password: $scope.data.password,
                            rememberMe: $scope.data.rememberMe
                        }).then(function(response) {
                            if(response.data && response.data.success) {
                                // set the current user in the $window
                                $window.currentUser = response.data.user;

                                // redirect
                                if($state.params.redirect_uri){
                                    $window.location = decodeURIComponent($state.params.redirect_uri);
                                }
                                else {
                                    return $state.go('base.dashboard', {});
                                }
                            } else {
                                return onFailedLogin(response.data.message);
                            }
                        }, function() {
                            return onFailedLogin();
                        });
                    }
                };

                $scope.register = function(isValid) {
                    //alert($scope.data.selectedCountryId);
                    if(isValid && $scope.data.preferredPosition) {
                        $scope.registering = true;
                        var url = appConfig.baseApiUrl + 'auth/register';

                        var onFailedRegister = function(errMsg) {
                            errMsg = errMsg || 'REGISTRATION FAILED';
                            $translate(errMsg).then(function (translated) {
                                $scope.regError = translated;
                            });
                            $scope.registering = false;
                        };

                        $http.post(url, {
                            email: $scope.data.email,
                            password: $scope.data.password,
                            name: $scope.data.name,
                            language: $translate.use(),
                            preferredPosition: $scope.data.preferredPosition,
                            countryId: $scope.data.selectedCountryId
                        }).then(function(response) {
                            if(response.status !== 201) {
                                // TODO: check for existing user by e-mail.
                                return onFailedRegister(response.data.errorMessage);
                            }
                            if(response.data.user){
                                $scope.login(true);
                            } else {
                                $scope.profileCreated = true;
                                $scope.registering = false;
                                resetForms();
                                $scope.userLogin(2);
                            }
                            //return $state.go('base.dashboard'); // go hard, or go home.
                        }, function(response) {
                            $scope.isLoginLoading = false;
                            var errMsg = (response && response.data) ? response.data.errorMessage : undefined;
                            return onFailedRegister(errMsg);
                        });
                    }
                }
            }
        };
    }];
});

//https://scotch.io/tutorials/angularjs-form-validation