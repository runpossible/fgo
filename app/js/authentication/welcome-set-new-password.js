define(['jquery'], function($) {
    'use strict';

    return ['appConfig', '$http', '$state', '$translate', '$window', '$filter', '$stateParams',
        function(appConfig, $http, $state, $translate, $window, $filter, $stateParams) {
        return {
            restrict: 'E',
            templateUrl: '/js/authentication/welcome-set-new-password.html?t=' + window.requestSuffix,
            link: function($scope) {
                var userUrl = appConfig.baseApiUrl + 'user';

                $scope.resetPasswordSet = function(){
                    $scope.password = '';
                    $scope.setPasswordForm.$setPristine();
                    $scope.setPasswordForm.$setUntouched();

                    $scope.changingPassword = false;
                    $scope.passwordChangeError = null;
                };

                if($stateParams.secret){
                    $scope.userLogin(4);
                }

                $scope.changePassword = function(isValid) {
                    $scope.changingPassword = true;
                    $scope.passwordChangeError = null;
                    if(isValid) {
                        $scope.isLoading = true;
                        $http.post(userUrl + '/password',{
                            secret: $stateParams.secret,
                            password: $scope.password
                        }).then(function ok(response){
                            $scope.passwordChanged = true;
                            $scope.changingPassword = false;
                        }, function error() {
                            $scope.passwordChangeError = 'INVALID REQUEST SECRET';
                            $scope.changingPassword = false;
                        });
                    }
                };
            }
        };
    }];
});