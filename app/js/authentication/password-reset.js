define([], function () {
    'use strict';

    return ['$scope', '$stateParams', '$state', '$http', 'appConfig',
        function($scope, $stateParams, $state, $http, appConfig) {
            var userUrl = appConfig.baseApiUrl + 'user';

            $scope.secret = $stateParams.secret;

            $scope.resetPassword = function(isValid) {
                $scope.resetPasswordError = null;
                if(isValid) {
                    $scope.isLoading = true;
                    $http.post(userUrl + '/reset-password',{
                        email: $scope.email
                    }).then(function ok(response) {
                        $scope.passwordChangeRequested = true;
                        $scope.isLoading = false;
                    }, function error() {
                        $scope.resetPasswordError = 'GENERAL ERROR';
                        $scope.isLoading = false;
                    });
                }
            };

            $scope.changePassword = function(isValid) {
                $scope.passwordChangeError = null;
                if(isValid) {
                    $scope.isLoading = true;
                    $http.post(userUrl + '/password',{
                        secret: $scope.secret,
                        password: $scope.password
                    }).then(function ok(response){
                        $scope.passwordChanged = true;
                        $scope.isLoading = false;
                    }, function error() {
                        $scope.passwordChangeError = 'INVALID REQUEST SECRET';
                        $scope.isLoading = false;
                    });
                }
            };
    }];
});
