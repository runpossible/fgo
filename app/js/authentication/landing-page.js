define([], function () {
    'use strict';

    return ['$scope', '$translate', 'appConfig', '$localStorage', function($scope, $translate, appConfig, $localStorage) {
        var storage = $localStorage.$default({});
        $('body').addClass('landing-page');

        $scope.userLogin = function(_type){
            if(_type != 2){
                $scope.profileCreated = false;
            }

            if(_type == 0){
                $("#user-login").addClass("dNone");
                $("body").css("overflow-y","auto");
            }else{
                $("#user-login").removeClass("dNone");
                $("body").css("overflow-y","hidden");
            }

            $(".form-cont").addClass("dNone");

            if(_type == 1){
                $("#signup").removeClass("dNone");
                $scope.resetSignUp();
            }

            if(_type == 2){
                $("#login").removeClass("dNone");
                $scope.resetLogin();
            }
            if(_type == 3){
                $("#forgot-password").removeClass("dNone");
                $scope.resetForgotPassword();
            }

            if(_type == 4){
                $("#password-set").removeClass("dNone");
                $scope.resetPasswordSet();
            }
            if(_type == 5){
                $("#activate-user").removeClass("dNone");
                $scope.resetActivateUser();
            }
            if(_type == 6){
                $("#contacts").removeClass("dNone");
                $scope.resetContactUs();
            }
            if(_type == 7) {
                $('#terms-and-conditions').removeClass('dNone');
            }
            if(_type == 8) {
                $("#login").removeClass("dNone");
                $scope.loginError = "MISSING EMAIL";
            }

            if(_type != 0 && _type != 6 && _type != 7) {
                $('html').animate({scrollTop: 0}, 'slow');//IE, FF
                $('body').animate({scrollTop: 0}, 'slow');//chrome, don't know if Safari works
                $('.popupPeriod').fadeIn(1000, function () {
                    setTimeout(function () {
                        $('.popupPeriod').fadeOut(2000);
                    }, 3000);
                });
            }
        };

        $scope.changeLanguage = function (language) {
            $translate.use(language);
        };

        $scope.openInAppStore = function (language) {
            window.location.href = "https://itunes.apple.com/us/app/fgo/id1209556306?ls=1&mt=8";
        };

        $scope.openInGooglePlay = function (language) {
            window.location.href = "https://play.google.com/store/apps/details?id=org.io.fgo";
        };

        // set BG by default.
        $translate.use(storage.language || appConfig.defaultLanguage);

        //Object.defineProperty($scope, 'currentLanguage', {
        //    get: function() {
        //       var currentLanguage = $translate.use();
        //        if(currentLanguage){
        //            currentLanguage = currentLanguage.toLowerCase();
        //        }
        //        return currentLanguage;
        //    }
        //});
    }];
});
