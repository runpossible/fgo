define(['jquery'], function($) {
    'use strict';

    return ['$translate',
        function($translate) {
            return {
                restrict: 'E',
                templateUrl: '/js/authentication/terms-and-conditions.html?t=' + window.requestSuffix,
                link: function ($scope) {
                    Object.defineProperty($scope, 'currentLanguage', {
                        get: function() {
                            var currentLanguage = $translate.use();
                            if(currentLanguage){
                                currentLanguage = currentLanguage.toLowerCase();
                            }
                            return currentLanguage;
                        }
                    });
                }
            }
        }];
});