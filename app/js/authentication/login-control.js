define(['jquery'], function($) {
    'use strict';

    return ['appConfig', '$http', '$state', '$translate', '$window', '$filter', function(appConfig, $http, $state, $translate, $window, $filter) {
        return {
            restrict: 'E',
            templateUrl: '/js/authentication/login-control.html?t=' + window.requestSuffix,
            link: function($scope) {
                if($state.params.redirect_uri){
                    $scope.redirect_uri = "?redirect_uri=" + $state.params.redirect_uri;
                }

                var $signUpForm = $('#sign-up-form'),
                    $signInForm = $('#sign-in-form');

                var resetForm = function(form) {
                    if (form) {
                        form.$setPristine();
                        form.$setUntouched();
                    }

                    $scope.data = angular.copy({
                        selectedCountryId: 1,
                        preferredPosition: '0'
                    }); // clean-up

                    $scope.loginError = undefined;
                    $scope.regError = undefined;
                };

                $scope.showSignUp = function() {
                    $scope.profileCreated = false;
                    $signUpForm.show();
                    $signInForm.hide();

                    resetForm($scope.regForm);
                };

                $scope.showSignIn = function() {
                    $signUpForm.hide();
                    $signInForm.show();

                    resetForm($scope.loginForm);
                };

                $scope.login = function(isValid) {
                    $scope.profileCreated = false;
                    if (isValid) {
                        // can proceed with login
                        var url = appConfig.baseApiUrl + 'auth/login';
                        var onFailedLogin = function(message) {
                            $scope.loginError = $filter('translate')(message || 'WRONG CREDENTIALS');
                        };
                        $http.post(url, {
                            username: $scope.data.email,
                            password: $scope.data.password,
                            rememberMe: $scope.data.rememberMe
                        }).then(function(response) {
                            if(response.data && response.data.success) {
                                // set the current user in the $window
                                $window.currentUser = response.data.user;

                                // redirect
                                if($state.params.redirect_uri){
                                    $window.location = decodeURIComponent($state.params.redirect_uri);
                                }
                                else {
                                    return $state.go('base.dashboard', {});
                                }
                            }
                            return onFailedLogin(response.data.message);
                        }, function() {
                            return onFailedLogin();
                        });
                    }
                };

                $scope.register = function(isValid) {
                    //alert($scope.data.selectedCountryId);
                    if(isValid) {
                        var url = appConfig.baseApiUrl + 'auth/register';

                        var onFailedRegister = function(errMsg) {
                            errMsg = errMsg || 'REGISTRATION FAILED';
                            $translate(errMsg).then(function (translated) {
                                $scope.regError = translated;
                            });
                        };

                        $http.post(url, {
                            email: $scope.data.email,
                            password: $scope.data.password,
                            name: $scope.data.name,
                            language: $translate.use(),
                            preferredPosition: $scope.data.preferredPosition,
                            countryId: $scope.data.selectedCountryId
                        }).then(function(response) {
                            if(response.status !== 201) {
                                // TODO: check for existing user by e-mail.
                                return onFailedRegister(response.data.errorMessage);
                            }
                            if(response.data.user){
                                $scope.login(true);
                            } else {
                                $scope.profileCreated = true;
                                $scope.showSignIn();
                            }
                            //return $state.go('base.dashboard'); // go hard, or go home.
                        }, function(response) {
                            var errMsg = (response && response.data) ? response.data.errorMessage : undefined;
                            return onFailedRegister(errMsg);
                        });
                    }
                }
            }
        };
    }];
});

//https://scotch.io/tutorials/angularjs-form-validation