define(function(require) {
	var angular = require('angular');

    var angularSimpleLogger = require('/js/external/angular-simple-logger.js');
    var angularGoogleMaps = require('/js/external/angular-google-maps.js');

	var home = angular.module('home', ['ui.router', 'ngAnimate', 'uiGmapgoogle-maps']);
    var identity = require('/js/authentication/identity.js');
    var sideMenuDir = require('/js/common/side-menu.js');
    var topNavDir = require('/js/common/top-nav.js');
    var subNavDir = require('/js/common/sub-nav.js');
    var countrySelectDir = require('/js/common/country-select.js');
    var locationSelectDir = require('/js/common/location-selector.js');
    var avatarDir = require('/js/common/avatar.js');
    var userAvatarDir = require('/js/common/user-avatar.js');
    var loaderDir = require('/js/common/loader.js');
    var tablePagerDir = require('/js/common/tablePager.js');
    var uiservice = require('/js/services/uiservice.js');
    var notificationManager = require('/js/services/notificationManager.js');
    var pendingInvitesDir = require('/js/groups/pending-invites.js');
    var gameService = require('/js/services/gameService.js');
    var commentsDir = require('/js/common/comments.js');
    var timeService = require('/js/services/timeService.js');
    var inviteService = require('/js/services/inviteService.js');
    var friendSearchResultDir = require('/js/friends/friend-search-result.js');
    var currentFriendsDir = require('/js/friends/current-friends.js');
    var inviteCurrentFriendsDir = require('/js/friends/invite-current-friends.js');
	var locationService = require('/js/services/locationService.js');
    var avatarService = require('/js/services/avatarService.js');
    var facebookService = require('/js/services/facebookService.js');
    var utilityService = require('/js/services/utilityService.js');
    var menuService = require('/js/services/menuService.js');
    var countryService = require('/js/services/countryService.js');
    var tableOptions = require('/js/models/tableOptions.js');
    var playersCountDir = require('/js/games/directives/players-count.js');
    var locationImageDir = require('/js/locations/directives/location-image.js');
    var translateStorage = require('/js/services/translateStorage.js');
    var updateTitleDir = require('/js/common/update-title.js');
    var playerItemDir = require('/js/common/player-item.js');
    var friendPlayerItemDir = require('/js/common/friend-player-item.js');
    var fileSelectorDir = require('/js/common/file-selector.js');
    var validationSummaryDir = require('/js/common/validation-summary.js');
    var imageService = require('/js/services/imageService.js');
    var friendService = require('/js/services/friendService.js');
    var guestItemDir = require('/js/common/guest-item.js');
    var focusMeDir = require('/js/common/focus-me.js');

    var unescapeFilter = require('/js/common/unescape-filter.js');

    home.factory('Identity', identity);
    home.factory('UIService', uiservice);
    home.factory('notificationManager', notificationManager);
    home.factory('gameService', gameService);
    home.factory('timeService', timeService);
    home.factory('inviteService', inviteService);
    home.factory('locationService', locationService);
    home.factory('avatarService', avatarService);
    home.factory('facebookService', facebookService);
    home.factory('utilityService', utilityService);
    home.factory('menuService', menuService);
    home.factory('translateStorage', translateStorage);
    home.factory('countryService', countryService);
    home.factory('imageService', imageService);
    home.factory('friendService', friendService);

    home.factory('TableOptions', tableOptions);

    home.directive('sideMenu', sideMenuDir);
    home.directive('topNav', topNavDir);
    home.directive('subNav', subNavDir);
    home.directive('countrySelector', countrySelectDir);
    home.directive('locationSelector', locationSelectDir);
    home.directive('avatar', avatarDir);
    home.directive('userAvatar', userAvatarDir);
    home.directive('loader', loaderDir);
    home.directive('pendingInvites', pendingInvitesDir);
    home.directive('comments', commentsDir);
    home.directive('friendSearchResult', friendSearchResultDir);
    home.directive('currentFriends', currentFriendsDir);
    home.directive('inviteCurrentFriends', inviteCurrentFriendsDir);
    home.directive('tablePager', tablePagerDir);
    home.directive('playersCount', playersCountDir);
    home.directive('locationImage', locationImageDir);
    home.directive('updateTitle', updateTitleDir);
    home.directive('playerItem', playerItemDir);
    home.directive('friendPlayerItem', friendPlayerItemDir);
    home.directive('fileSelector', fileSelectorDir);
    home.directive('validationSummary', validationSummaryDir);
    home.directive('guestItem', guestItemDir);
    home.directive('focusMe', focusMeDir);

    home.filter('unescape', unescapeFilter);

	home.config(['$stateProvider', '$urlRouterProvider', 'uiGmapGoogleMapApiProvider', function($stateProvider, $urlRouterProvider, uiGmapGoogleMapApiProvider) {
        $urlRouterProvider.otherwise('/');

        // adding smartbanner https://github.com/jasny/jquery.smartbanner
        $.smartbanner();

        // This is the base template, it cannot be viewed - it's abstract.
        $stateProvider.state('base', {
                url: '/',
                data: {
                    requireLogin: true // this property will apply to all children of 'base'
                },
                abstract: true,
                templateUrl: '/js/home/home.html?t=' + window.requestSuffix
            });

        uiGmapGoogleMapApiProvider.configure({
            key: 'AIzaSyA93qnYk4MYsgGTuQfo0xhdvMNl8hbQ-M8',
            v: '3.20', //defaults to latest 3.X anyhow
            libraries: 'weather,geometry,visualization,places'
        });
    }]);

    return home;
});
