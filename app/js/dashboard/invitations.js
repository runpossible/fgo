define([], function () {
    'use strict';

    return ['$state', '$stateParams', 'inviteService', function($state, $stateParams, inviteService) {

        if ($stateParams.groupId) {
            inviteService.inviteInGroup($stateParams);
            $state.go('base.dashboard');
        }
        if($stateParams.gameId){
            inviteService.inviteInGame($stateParams);
            $state.go('base.dashboard');
        }
    }];
});
