define([], function () {
    'use strict';

    return ['$scope', '$window', 'gameService', function($scope, $window, gameService) {
        $scope.currentUser = $window.currentUser;

        gameService.getUpcomingGame().then(function(g) {
            if(g.date_time) {
                $scope.upcomingGame = g;
            }
        });
    }];
});
