define(function(require) {
    var angular = require('angular');

    var dashboard = angular.module('dashboard', ['ui.router']);
    var overviewCtrl = require('/js/dashboard/overview.js');
    var invitationsCtrl = require('/js/dashboard/invitations.js');

    dashboard.controller('OverviewCtrl', overviewCtrl);
    dashboard.controller('invitationsCtrl', invitationsCtrl);

    dashboard.config(['$stateProvider', function($stateProvider) {
        $stateProvider.state('base.dashboard', {
            url: '',
            templateUrl: '/js/dashboard/overview.html',
            controller: 'OverviewCtrl',
            data:{
                pageTitle: 'DASHBOARD'
            }
        });

        $stateProvider.state('base.invitation', {
            url: 'invitations?groupId&gameId&activationCode',
            templateUrl: '/js/dashboard/invitations.html',
            controller: 'invitationsCtrl',
            data:{
                pageTitle: 'INVITATIONS'
            }
        });
    }]);

    return dashboard;
});
