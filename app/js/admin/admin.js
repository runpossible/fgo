define(function(require) {
    var angular = require('angular');

    var admin = angular.module('admin', ['ui.router']);
    var sendMailCtrl = require('/js/admin/send-mail.js');
    var adminRootCtrl = require('/js/admin/admin-root.js');

    admin.controller('sendMailCtrl', sendMailCtrl);
    admin.controller('adminRootCtrl', adminRootCtrl);

    admin.config(['$stateProvider', function($stateProvider) {
        $stateProvider.state('base.admin', {
            url: 'admin',
            abstract: true,
            templateUrl: '/js/home/empty.html',
            controller: 'adminRootCtrl'
        });

        $stateProvider.state('base.admin.sendMail', {
            url: '/send-mail',
            templateUrl: '/js/admin/send-mail.html',
            controller: 'sendMailCtrl',
            data:{
                pageTitle: 'SEND MAIL'
            }
        });
    }]);

    return admin;
});
