define([], function () {
    'use strict';

    return ['$state', function($state) {
        if(!window.currentUser.master){
            $state.go('base.dashboard');
        }
    }];
});
