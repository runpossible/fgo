define([], function () {
    'use strict';

    return ['$scope', 'appConfig', '$http', 'notificationManager', function($scope, appConfig, $http, notificationManager) {
        $scope.data = {
            oneByOne : true
        };

        var sendMailUrl = appConfig.baseApiUrl + 'admin/send-mail';
        $scope.messageSent = false;

        $scope.send = function(isValid) {
            $scope.isLoading = true;
            if(isValid) {
                $http.post(sendMailUrl, $scope.data).then(function ok(response){
                    notificationManager.success("MESSAGE SENT");
                    $scope.isLoading = false;
                }, function error(response) {
                    notificationManager.error(response.data.errorMessage || response.data);
                    $scope.isLoading = false;
                });
            }
        };
    }];
});
