define(function () {
    'use strict';


    return ['$http', '$q', 'appConfig', '$window', function ($http, $q, appConfig, $window) {
        var locationToMarker = function (loc) {
            return {
                id: loc._id,
                coords: {
                    longitude: loc.loc[0],
                    latitude: loc.loc[1]
                },
                formattedAddress: loc.formattedAddress,
                city: loc.city,
                name: loc.name,
                options: {
                    draggable: false,
                    labelContent: loc.name,
                    labelClass: 'marker-labels'
                }
            };
        };

        var getUserLocation = function getUserLocation(done) {
            if(navigator.geolocation) {
                return navigator.geolocation.getCurrentPosition(function(pos) {
                    return done(pos.coords.latitude, pos.coords.longitude);
                }, function err() {
                    console.log("couldn't find user location");
                    return done(45, -73);
                });
            }

            // if no geolocation is supported, return a random location
            return done(45, -73);
        };


        return {
            locationToMarker: locationToMarker,
            getUserLocation: getUserLocation
        }
    }];
});