define(function () {
    'use strict';


    return ['appConfig', '$state', '$http', 'notificationManager', '$q', function (appConfig, $state, $http, notificationManager, $q) {
        var friendsUrl = appConfig.baseApiUrl + 'profile/friends';

        var getFriends = function getFriends() {
            var deferred = $q.defer();
            $http.get(friendsUrl)
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function() {
                    notificationManager.error('GENERAL ERROR');
                    deferred.reject();
                });
            return deferred.promise;
        };

        return {
            getFriends: getFriends
        }
    }];
});