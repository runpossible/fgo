define(function () {
    'use strict';

    return [function () {
        var facebookInitialized = false;

        var facebookUI = function(facebookUIBody){
            if(!facebookInitialized) {
                FB.init({
                    appId: window.facebookAppId,
                    status: true,
                    cookie: true,
                    xfbml: true,
                    version: 'v2.4'
                });
                facebookInitialized = true;
            }

            if(!facebookUIBody.app_id){
                facebookUIBody.app_id = window.facebookAppId;
            }

            FB.ui(facebookUIBody);
        };

        var facebookSend = function(link){
            facebookUI({
                method: 'send',
                link: link
            })
        };

        return {
            ui: facebookUI,
            send: facebookSend
        }
    }];
});