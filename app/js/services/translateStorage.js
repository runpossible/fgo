define(function () {
    'use strict';

    return ['$http', '$q', 'appConfig', '$window', '$localStorage', function ($http, $q, appConfig, $window, $localStorage) {
        var storage = $localStorage.$default({});
        return {
            put: function (name, value) {
                // set the language to the user profile if a user is logged in
                if($window.currentUser && $window.currentUser.language != value) {
                    $http.put(appConfig.baseApiUrl + 'profile/language', {language: value}).then(function () {
                        $window.currentUser.language = value;
                    });
                }
                storage.language = value;
            },
            get: function (name) {
                // if the user has language set in his profile return it
                if($window.currentUser && $window.currentUser.language) {
                    return $window.currentUser.language;
                } else if(storage.language) {
                    // if the user has selected a language before logged in and the user does not have set language
                    // set user language
                    if ($window.currentUser) {
                        $window.currentUser.language = storage.language;
                        $http.put(appConfig.baseApiUrl + 'profile/language', {language: storage.language});
                    }
                    return storage.language;
                } else {
                    // if the client does not have access to https://freegeoip.net do not make another call
                    if(storage.noAccessToIPInfo){
                        return storage.language || appConfig.defaultLanguage;
                    } else {
                        var deferred = $q.defer();
                        // $http.jsonp('https://freegeoip.net/json/?callback=JSON_CALLBACK')
                        // currently using local maxmind database
                        $http.get(appConfig.baseApiUrl + 'current-country')
                            .then(
                            function (response) {
                                if (response.data && response.data.country_code) {
                                    storage.language = response.data.country_code.toLowerCase();
                                    if(storage.language != 'bg'){
                                        storage.language = 'en';
                                    }
                                } else {
                                    storage.language = appConfig.defaultLanguage;
                                }
                                return deferred.resolve(storage.language);
                            }, function (err) {
                                storage.noAccessToIPInfo = true;
                                storage.language = appConfig.defaultLanguage;
                                return deferred.resolve(storage.language);
                            });
                        return deferred.promise;
                    }
                }
            }
        };
    }];
});