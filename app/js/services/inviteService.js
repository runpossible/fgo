define(function () {
    'use strict';


    return ['appConfig', '$state', '$http', 'notificationManager', '$q', '$translate', '$rootScope', function (appConfig, $state, $http, notificationManager, $q, $translate, $rootScope) {
        var inviteUrl = appConfig.baseApiUrl + 'invite',
            gameUrl = appConfig.baseApiUrl + 'game';

        var inviteInGroup = function (stateParams) {
            var groupId = stateParams.groupId;
            var activationCode = stateParams.activationCode;
            if (!groupId || !activationCode) {
                return;
            }
            var groupInviteUrl = inviteUrl + '/' + groupId + '/' + activationCode;
            $http.post(groupInviteUrl).then(function (response) {
                if (response.data && response.data.error) {
                    if (response.data.error == 'USER ALREADY PART OF THE GROUP') {
                        notificationManager.info(response.data.error);
                    } else {
                        notificationManager.error(response.data.error);
                    }
                }
            }, function err(response) {
                if (response.data && response.data.error) {
                    if (response.data.error == 'USER ALREADY PART OF THE GROUP') {
                        notificationManager.info(response.data.error);
                    } else if (response.data.error == 'INVALID INVITATION CODE') {
                        notificationManager.error('INVALID INVITATION CODE');
                    } else {
                        notificationManager.error(response.data.error);
                    }
                }
                else {
                    notificationManager.error('ACCEPT GROUP INVITATION PROBLEM');
                }
            });
        };

        var inviteInGame = function (stateParams) {
            var gameId = stateParams.gameId;
            var activationCode = stateParams.activationCode;
            if (!gameId || !activationCode) {
                return;
            }
            var groupInviteUrl = inviteUrl + '/game/' + gameId + '/' + activationCode;
            $http.post(groupInviteUrl).then(function (response) {
                if (response.data && response.data.error) {
                    if (response.data.error == 'USER ALREADY PART OF THE GAME') {
                        notificationManager.info(response.data.error);
                    } else {
                        notificationManager.error(response.data.error);
                    }
                }
            }, function err(response) {
                if (response.data && response.data.error) {
                    if (response.data.error == 'USER ALREADY PART OF THE GAME') {
                        notificationManager.info(response.data.error);
                    } else if (response.data.error == 'INVALID INVITATION CODE') {
                        notificationManager.error('INVALID INVITATION CODE');
                    } else {
                        notificationManager.error(response.data.error);
                    }
                }
                else {
                    notificationManager.error('ACCEPT GAME INVITATION PROBLEM');
                }
            });
        };

        var removeFromGame = function (playerId, gameId, done) {
            var removePlayerUrl = gameUrl + '/' + gameId + '/players/' + playerId;

            $http.delete(removePlayerUrl).then(function (response) {
                var hasError = response.data && response.data.error;
                return done(hasError);
            }, function err(response) {
                var error = response.data && response.data.error ? response.data.error :
                    'An error occurred during removing player';
                return done(error);
            });
        };

        var getInvitedForGame = function (gameId) {
            var deferred = $q.defer();

            var url = inviteUrl + '/' + gameId;
            $http.get(url).then(function (response) {
                deferred.resolve(response.data);
            }, function err(response) {
                var error = response.data && response.data.error ? response.data.error :
                    'An error occured reading the invited list';
                deferred.reject(error);
            });

            return deferred.promise;
        };

        var inviteAction = function (gameId, inviteId, status) {
            var deferred = $q.defer();

            var url = appConfig.baseApiUrl + 'game/' + gameId + '/invite/' + inviteId;

            $http.put(url, {
                status: status
            }).then(function () {
                // no translation here yet.
                if (status == 'Accepted') {
                    $translate('SUCCESSFULLY JOINED THE GAME').then(function (translatedMessage) {
                        notificationManager.success(translatedMessage, false);
                    });
                }
                else {
                    $translate('REJECTED JOINING TO GAME').then(function (translatedMessage) {
                        notificationManager.info(translatedMessage, false);
                    });
                }

                deferred.resolve();
            }, function err() {
                notificationManager.error('GENERAL ERROR');

                deferred.reject();
            });

            return deferred.promise;
        };

        return {
            inviteInGroup: inviteInGroup,
            inviteInGame: inviteInGame,
            removeFromGame: removeFromGame,
            getInvitedForGame: getInvitedForGame,
            inviteAction: inviteAction
        }
    }];
});