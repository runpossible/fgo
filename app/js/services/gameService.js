define(function () {
    'use strict';


    return ['$http', '$q', 'appConfig', '$window', function ($http, $q, appConfig, $window) {
        var _getUpcomingGame = function () {
            var gameUrl = appConfig.baseApiUrl + 'game/upcomming';

            var deferred = $q.defer();
            $http.get(gameUrl)
                .then(function (response) {
                    return deferred.resolve(response.data);
                }, function () {
                    deferred.reject();
                });
            return deferred.promise;
        };

        var _stat = function(game) {
            if(!game) return {
                booked: 0,
                freeSlots: 0
            };

            if(game.guests && typeof game.guests === 'string') {
                game.guests = JSON.parse(game.guests);
            }
            var booked = game.players.length + game.guests.length;
            var freeSlots = game.numberOfPlayers - game.players.length - game.guests.length;
            return {
                booked: booked,
                freeSlots: freeSlots
            };
        };

        return {
            getUpcomingGame: _getUpcomingGame,
            stat: _stat
        }
    }];
});