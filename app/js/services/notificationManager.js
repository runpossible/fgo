define(function() {
    'use strict';


    return ['toaster', '$translate', '$timeout', '$filter', '$http', 'appConfig', function(toaster, $translate, $timeout, $filter, $http, appConfig) {

        var showNotification = function(type, message, shouldTranslate) {
            shouldTranslate = shouldTranslate !== false; // if user pass false as should translate...
            if(shouldTranslate) {
                var translatedMessage = $filter('translate')(message);
                $timeout(function() {
                    toaster.pop(type, null, translatedMessage);
                });
            } else {
                $timeout(function() {
                    toaster.pop(type, null, message);
                });
            }
        };

        var addOfflineNotification = function(type, game, group){
            if(game){
                group = game.group;
            }

            var notification = {};
            notification.groupId = group ? group._id : undefined;
            notification.gameId = game ? game._id: undefined;
            notification.type = type;

            var postNotificationUrl = appConfig.baseApiUrl + 'notifications';
            $http.post(postNotificationUrl, notification).then(function (response) {
                console.log('Offline notification added');
            }, function err(response) {
                console.log((response.data && response.data.error) ? response.data.error : response);
            });
        };

        return {
            error: angular.bind(null, showNotification, 'error'),
            success: angular.bind(null, showNotification, 'success'),
            info: angular.bind(null, showNotification, 'info'),
            warning: angular.bind(null, showNotification, 'warning'),
            addOfflineNotification: addOfflineNotification
        }
    }];
});