define(function () {
    'use strict';


    return ['appConfig', function (appConfig) {
        return {
            getLocationImageUrl: function(location) {
                if(location && location.primaryImage) {
                    return appConfig.baseApiUrl + 'location/' + location._id + '/image/' + location.primaryImage + '/image.png';
                }

                return location.imageUrl = '/assets/img/location-default.jpg';
            },

            getGroupImageUrl: function(group) {
                if(group && group.groupImage) {
                    return appConfig.baseApiUrl + 'group/' + group._id + '/image/' + group.groupImage + '/image.png';
                }

                return group.imageUrl = '/assets/img/group-default.png';
            }
        }
    }];
});