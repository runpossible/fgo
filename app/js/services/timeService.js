define(function () {
    'use strict';


    return ['$filter', function ($filter) {
        var labels = {};

        var getLabels = function(){
            labels.timeAgoFormat = $filter('translate')("TIME AGO FORMAT");
            labels.minuteLabel = $filter('translate')("MINUTE");
            labels.minutesLabel = $filter('translate')("MINUTES");
            labels.hourLabel = $filter('translate')("HOUR");
            labels.hoursLabel = $filter('translate')("HOURS");
            labels.secondLabel = $filter('translate')("SECOND");
            labels.secondsLabel = $filter('translate')("SECONDS");
            labels.yearLabel = $filter('translate')("YEAR");
            labels.yearsLabel = $filter('translate')("YEARS");
            labels.monthLabel = $filter('translate')("MONTH");
            labels.monthsLabel = $filter('translate')("MONTHS");
            labels.dayLabel = $filter('translate')("DAY");
            labels.daysLabel = $filter('translate')("DAYS");
            labels.justNowLabel = $filter('translate')("JUST NOW");
        };

        var calulateTimeSince = function calulateTimeSince(date_string) {
            var date = new Date(date_string);

            var seconds = Math.floor((new Date() - date) / 1000);

            var interval = Math.floor(seconds / 31536000);

            if (interval > 1) {
                return interval + " " + labels.yearsLabel;
            }
            interval = Math.floor(seconds / 2592000);
            if (interval > 1) {
                return interval + " " + labels.monthsLabel;
            }
            interval = Math.floor(seconds / 86400);
            if (interval > 1) {
                return interval + " " + labels.daysLabel;
            }
            interval = Math.floor(seconds / 3600);
            if (interval > 1) {
                return interval + " " + labels.hoursLabel;
            }
            interval = Math.floor(seconds / 60);
            if (interval > 1) {
                return interval + " " + labels.minutesLabel;
            }
            if(seconds < 5)
            {
                return labels.justNowLabel;
            }
            return Math.floor(seconds) + " " + labels.secondsLabel;

        };

        var startInLabel = function(date_string){
            var dateDiff = (new Date(date_string)).getTime() - (new Date()).getTime();

            var days = Math.floor(dateDiff / (1000*60*60*24));
            var hours = Math.floor((dateDiff -  days * (1000*60*60*24)) / (1000*60*60));
            var minutes = Math.floor((dateDiff - days * (1000*60*60*24) - hours * (1000*60*60)) / (1000*60));

            var daysLabel = $filter('translate')(days == 1 ? "DAY" : "DAYS");
            var hoursLabel = $filter('translate')(hours == 1 ? "HOUR" : "HOURS");
            var minutesLabel = $filter('translate')(minutes == 1 ? "MINUTE" : "MINUTES");
            var dueInLabel = $filter('translate')("DUE IN");
            var andLabel =  $filter('translate')("AND");

            var fullDaysLabel = days == 0 ? "" : (days + " " + daysLabel + ",");

            return "{0} {1} {2} {3} {4} {5} {6}".format(dueInLabel,
                fullDaysLabel, hours, hoursLabel, andLabel, minutes, minutesLabel);
        };

        var timeSince = function timeSince(date_string) {
            getLabels();
            var calculatedTimeSince = calulateTimeSince(date_string);
            if(calculatedTimeSince == labels.justNowLabel){
                return calculatedTimeSince;
            }
            else {
                return labels.timeAgoFormat.format(calculatedTimeSince);
            }
        };

        return {
            timeSince: timeSince,
            startInLabel: startInLabel
        }
    }];
});