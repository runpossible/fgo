define(function () {
    'use strict';
    return ['$http', '$q', 'appConfig', '$window', function ($http, $q, appConfig, $window) {

        var mainItemNames = {
            dashboard: 'dashboard',
            games: 'games',
            friends: 'friends',
            groups: 'groups',
            locations: 'locations',
            about: 'about',
            administration: 'administration'
        };

        var _getMainSelectedWithIndexOf = function(value) {
            var name = value;
            if(~name.indexOf('game')) {
                return mainItemNames.games;
            }

            if(~name.indexOf('group')) {
                return mainItemNames.groups;
            }

            if(~name.indexOf('location')) {
                return mainItemNames.locations;
            }

            if(~name.indexOf('friends')) {
                return mainItemNames.friends;
            }

            if(~name.indexOf('admin')) {
                return mainItemNames.administration;
            }

            return mainItemNames.dashboard;
        };

        var getMainSelectedItem = function(stateName) {
            return _getMainSelectedWithIndexOf(stateName);
        };

        return {
            getMainSelectedItem: getMainSelectedItem,
            mainItemNames: mainItemNames
        }
    }];
});