define(function () {
    'use strict';

    return ['$http', '$q', 'appConfig', '$window', function ($http, $q, appConfig, $window) {
        var cachedUrls = {};

        var getAvatarUrl = function (type, userId, email, preferredAvatar) {
            var key = "{0}_{1}_{2}".format(type, userId || email, preferredAvatar);

            return cachedUrls[key];
        };

        var setAvatarUrl = function (type, userId, email, preferredAvatar, url) {
            var key = "{0}_{1}_{2}".format(type, userId || email, preferredAvatar);

            cachedUrls[key] = url;
        };

        return {
            getAvatarUrl: getAvatarUrl,
            setAvatarUrl: setAvatarUrl
        }
    }];
});