define(['lodash'], function (_) {
    'use strict';

    return ['$http', '$q', 'appConfig', '$window', '$translate', function ($http, $q, appConfig, $window, $translate) {
        var countries = [];

        var loadCountries = function(done){
            $http.get('/languages/countries.json?t=' + window.requestSuffix).then(
                function(result) {
                    countries = result.data;
                    done();
                }, function(err) {
                    done(err);
                });
        };

        var getCountries = function (done) {
            var getCountries = function(){
                var lang = $translate.use();
                var result = [];

                for(var i=0;i< countries.length;i++) {
                    result.push({
                        id: countries[i].id,
                        name: countries[i][lang],
                        order: countries[i].order
                    });
                }
                done(null, result);
            };

            if(countries.length == 0){
                loadCountries(function(err){
                    if(!err){
                        getCountries(done);
                    } else {
                        done(err);
                    }
                })
            } else {
                getCountries(done);
            }
        };

        var getCountryName = function (countryId, done) {
            var getName = function(){
                var lang = $translate.use();

                var country = _.find(countries, function(c){ return c.id == countryId;});
                done(null, country[lang]);
            };

            if(countries.length == 0){
                loadCountries(function(err){
                    if(!err){
                        getName(countryId, done);
                    } else {
                        done(err);
                    }
                })
            } else {
                getName(countryId, done);
            }
        };

        return {
            getCountries: getCountries,
            getCountryName: getCountryName
        }
    }];
});