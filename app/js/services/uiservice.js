define(function(require) {
    'use strict';

    return ['$window', function($window) {
        var checkScreenSize = function(targetLocation){
            var topOffset = 50;
            var width = ($window.innerWidth > 0) ? $window.innerWidth : screen.width;
            if (width < 768) {
                $('div.navbar-collapse').removeClass('in');
                $('div.navbar-collapse').addClass('collapse');
                topOffset = 100; // 2-row-menu
            } else {
                $('div.navbar-collapse').removeClass('collapse');
            }

            var height = (($window.innerHeight > 0) ? $window.innerHeight : screen.height) - 1;
            height = height - topOffset;
            if (height < 1) height = 1;
            if (height > topOffset) {
                $("#page-wrapper").css("min-height", (height) + "px");
            }

            // clear out the old active classes
            var $sideMenuElements = $('#side-menu a');
            var $sideMenuListElements = $('#side-menu li');
            $sideMenuElements.removeClass('active');
            $sideMenuListElements.removeClass('active');

            // get the current Menu Item, by the hash of the URL
            var element = $sideMenuElements.filter(function() {
                var linkHash = $(this).data('hash');
                return linkHash === targetLocation;
            }).addClass('active').parent().parent().addClass('in').parent();
            if (element.is('li')) {
                element.addClass('active');
            }
        };

        var removeFacebookAppendedHash = function(){
            //https://github.com/jaredhanson/passport-facebook/issues/12
            if ((!window.location.hash || window.location.hash !== '#_=_') && (window.location.href.substr(-1) !== "#"))
                return;
            if (window.history && window.history.replaceState)
                return window.history.replaceState("", document.title, window.location.pathname);
            // Prevent scrolling by storing the page's current scroll offset
            var scroll = {
                top: document.body.scrollTop,
                left: document.body.scrollLeft
            };
            window.location.hash = "";
            // Restore the scroll offset, should be flicker free
            document.body.scrollTop = scroll.top;
            document.body.scrollLeft = scroll.left;
        };

        var numericTextboxFix = function(){
            $(function() {
                $('.numeric-input').keypress(function(evt) {
                    var charCode = (evt.which) ? evt.which : event.keyCode;
                    if (charCode > 31 && (charCode < 48 || charCode > 57)){
                        return false;
                    } else {
                        return true;
                    }
                });
            });
        };

        var checkBodyClass = function(toState){
            var landingPageState = function(url){
                return (url == '/' && !$window.currentUser) || (url.indexOf('/welcome') == 0);
            };

            var toLandingPage = landingPageState(toState.url);

            if(toLandingPage && !$('body').hasClass('landing-page')){
                $('body').addClass('landing-page');
            } else if(!toLandingPage && $('body').hasClass('landing-page')){
                $('body').removeClass('landing-page');
            }
        }

        return {
            checkScreenSize: checkScreenSize,
            removeFacebookAppendedHash: removeFacebookAppendedHash,
            numericTextboxFix: numericTextboxFix,
            checkBodyClass: checkBodyClass
        }
    }];

});