define(function (require) {
    var angular = require('angular');

    var info = angular.module('info', ['ui.router']);
    var whoAreWeCtrl = require('/js/info/who-are-we.js');

    info.controller('whoAreWeCtrl', whoAreWeCtrl);

    info.config(['$stateProvider', function ($stateProvider) {
        $stateProvider.state('base.who', {
            url: 'who-are-we',
            templateUrl: '/js/info/who-are-we.html',
            controller: 'whoAreWeCtrl',
            data:{
                pageTitle: 'WHO ARE WE'
            }
        });
    }]);

    return info;
});
