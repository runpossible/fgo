define(function(require) {
	var angular = require('angular');
	
    var games = angular.module('games', ['ui.router']);
    var newGameCtrl = require('/js/games/new-game.js');
    var gameDetailsCtrl = require('/js/games/game-details.js');
    var gameStatisticsCtrl = require('/js/games/game-statistics.js');
    var gameHistoryCtrl = require('/js/games/game-history.js');
    var editGameCtrl = require('/js/games/edit-game.js');
    //var InviteGameCtrl = require('/js/games/invite-game.js');
    var InviteFriendsCtrl = require('/js/games/invite-friends.js');
    var UpcomingGamesCtrl = require('/js/games/upcoming-games.js');
    var findGamesCtrl = require('/js/games/find-games.js');
    var invitedList = require('/js/games/directives/invited-list.js');
    
    games.controller('NewGameCtrl', newGameCtrl);
    games.controller('GameDetailsCtrl', gameDetailsCtrl);
    games.controller('GameStatisticsCtrl', gameStatisticsCtrl);
    games.controller('GameHistoryCtrl', gameHistoryCtrl);
    games.controller('EditGameCtrl', editGameCtrl);
    //games.controller('InviteGameCtrl', InviteGameCtrl);
    games.controller('InviteFriendsCtrl', InviteFriendsCtrl);
    games.controller('UpcomingGamesCtrl', UpcomingGamesCtrl);
    games.controller('findGamesCtrl', findGamesCtrl);
    games.directive('invitedList', invitedList);

	games.config(['$stateProvider', function($stateProvider) {
        $stateProvider.state('base.newgame', {
            url: 'new-game',
            templateUrl: '/js/games/new-game.html',
            controller: 'NewGameCtrl',
            data:{
                pageTitle: 'NEW GAME'
            }
        });

        $stateProvider.state('base.gamedetails', {
            url: 'game-details',
            abstract: true,
            templateUrl: '/js/home/empty.html'
        });

        $stateProvider.state('base.gamedetails.game', {
            url: '/:gameId?action&inviteId',
            templateUrl: '/js/games/game-details.html',
            controller: 'GameDetailsCtrl',
            data:{
                pageTitle: 'GAME DETAILS'
            }
        });

        $stateProvider.state('base.gamestatistics', {
            url: 'game-statistics',
            abstract: true,
            templateUrl: '/js/home/empty.html'
        });

        $stateProvider.state('base.gamestatistics.game', {
            url: '/:gameId',
            templateUrl: '/js/games/game-statistics.html',
            controller: 'GameStatisticsCtrl',
            data:{
                pageTitle: 'GAME STATISTICS'
            }
        });

        $stateProvider.state('base.editgame', {
            url: 'edit-game',
            abstract: true,
            templateUrl: '/js/home/empty.html'
        });

        $stateProvider.state('base.editgame.game', {
            url: '/:gameId',
            templateUrl: '/js/games/edit-game.html',
            controller: 'EditGameCtrl',
            data:{
                pageTitle: 'EDIT GAME'
            }
        });

        $stateProvider.state('base.gamehistory', {
            url: 'game-history',
            templateUrl: '/js/games/game-history.html',
            controller: 'GameHistoryCtrl',
            data:{
                pageTitle: 'HISTORY'
            }
        });

        $stateProvider.state('base.upcominggames', {
            url: 'upcoming-games',
            templateUrl: '/js/games/upcoming-games.html',
            controller: 'UpcomingGamesCtrl',
            data:{
                pageTitle: 'UPCOMING GAMES'
            }
        });

        $stateProvider.state('base.findgames', {
            url: 'find-games',
            templateUrl: '/js/games/find-games.html',
            controller: 'findGamesCtrl',
            data:{
                pageTitle: 'FIND GAMES'
            }
        });

        $stateProvider.state('base.gameinvite', {
            url: 'invite-game',
            abstract: true,
            templateUrl: '/js/home/empty.html'
        });

        //$stateProvider.state('base.gameinvite.game', {
        //    url: '/:gameId?:searchGoogle',
        //    templateUrl: '/js/games/invite-game.html',
        //    controller: 'InviteGameCtrl'
        //});

        $stateProvider.state('base.gameinvite.game', {
            url: '/:gameId',
            templateUrl: '/js/games/invite-friends.html',
            controller: 'InviteFriendsCtrl',
            data:{
                pageTitle: 'INVITE YOUR FRIENDS'
            }
        });
    }]);

    return games;
});
