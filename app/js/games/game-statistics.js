define(['lodash'], function (_) {
    'use strict';

    return ['$scope', '$stateParams', 'appConfig', '$state', '$http', '$translate', '$interval', '$q',
        'notificationManager', 'timeService', '$window', '$filter', 'facebookService',
        function($scope, $stateParams, appConfig, $state, $http, $translate,
                 $interval, $q, notificationManager, timeSevice, $window, $filter, facebookService) {
            var gameId = $stateParams.gameId,
                gameUrl = appConfig.baseApiUrl + 'game',
                currentUserInitialGoals = $window.currentUser.goals,
                thisGameInitialUserGoals = 0;

            var updatingRating = false;
            var updatingGoals = false;

            $scope.isMobile = appConfig.isMobile;

            if(!gameId){
                return $state.go('base.newgame');
            }

            $scope.isLoading = true;

            var calulateStartsIn = function(){
                $scope.startsInLabel = timeSevice.startInLabel($scope.game.date_time);
                    };

                // show some loader
                var setLoaderVisbility = function(isVisible) { };
                setLoaderVisbility(true);

                var initializeGame = function initializeGame() {
                    var deferred = $q.defer();
                    $http.get(gameUrl + '/' + gameId)
                        .then(function(response) {
                            deferred.resolve(response.data);
                        }, function() {
                            deferred.reject();
                        });
                    return deferred.promise;
                };

                var initializeGameStats = function initializeGameStats() {
                    var deferred = $q.defer();
                    $http.get(gameUrl + '/' + gameId + '/player_stats')
                        .then(function(response) {
                        deferred.resolve(response.data);
                    }, function() {
                        deferred.reject();
                    });
                return deferred.promise;
            };

            var initialize = function () {
                var promises = [ initializeGame(), initializeGameStats() ];
                $q.all(promises).then(function(results) {
                    var game = results[0];
                    var gameStats = results[1];

                    gameId = game._id;
                    $scope.game = game;
                    $scope.currentUserId = $window.currentUser._id;
                    $scope.isParticipant = _.any(game.players, function(player){
                        return player._id == $scope.currentUserId;
                    });
                    $scope.userIsAdmin = (game.createdBy && game.createdBy._id == $scope.currentUserId) ||
                        (game.group && game.group.admin_users && game.group.admin_users.indexOf($scope.currentUserId) > -1);
                    $scope.gamePassed = new Date() > new Date($scope.game.date_time);
                    var players = game.players || [];

                    var goalFacts = _.filter(gameStats.gameFacts, function(gameFact){
                        return gameFact.type == 'GoalScored';});

                    for (var index in players) {
                        loadPlayerRating(gameStats.playerRatings, players[index]);
                        loadAverageRating(gameStats.averageRatings, players[index]);
                        loadPlayerGoals(goalFacts, players[index]);
                        if(players[index]._id == $scope.currentUserId) {
                            thisGameInitialUserGoals = players[index].goals || 0;
                        }
                    }

                    $scope.players = players;
                    setLoaderVisbility(false);

                    calulateStartsIn();
                    $interval(calulateStartsIn, 30000);

                    $scope.isLoading = false;
                }, function err() {
                    return notificationManager.error('GENERAL ERROR');
                });
            };

            $scope.gameInviteUrl = function(fullUrl){
                if($scope.game && $scope.game.invitation_code) {
                    if($scope.game.invitation_shorturl && !fullUrl){
                        return $scope.game.invitation_shorturl
                    }
                    else{
                        return $state.href('base.invitation', {gameId: gameId, activationCode: $scope.game.invitation_code}, {absolute: true})
                    }
                }
                else{
                    return null;
                }
            };

            $scope.inviteThroughFacebook = function(){
                var url = $scope.gameInviteUrl(true);
                facebookService.send(url);
            };

            var loadPlayerRating = function(playerRatings, player) {
                var existingRating = _.findLast(playerRatings, function (rating) {
                   return rating.player == player._id;
                });
                if(existingRating){
                    player.rating = existingRating.rating;
                    player.initialRating = existingRating.rating;
                }
            };

            var loadAverageRating = function(averageRatings, player) {
                var existingRating = _.findLast(averageRatings, function (averageRating) {
                    return averageRating.player == player._id;
                });
                if(existingRating){
                    player.averageRating = existingRating.rating;
                } else {
                    player.averageRating = $filter('translate')("RATING NOT AVAILABLE");
                }
            };

            var loadPlayerGoals = function(goalFacts, player){
                var existingGoalFact = _.findLast(goalFacts, function (goalFact) {
                    return goalFact.player == player._id;
                });
                if(existingGoalFact){
                    player.goals = existingGoalFact.value;
                }else{
                    //player.goals = 0;
                }
            };

            initialize();

            $scope.deletePlayer = function (player){
                $translate('CONFIRM DELETE PLAYER FROM GAME').then(function(confirmationMessage){
                    if(confirm(confirmationMessage)) {
                        $http.delete(gameUrl + '/' + gameId + '/players/' + player._id).then(function ok(response) {
                            var playerIndex = $scope.game.players.indexOf(player);
                            if (playerIndex > -1) {
                                $scope.game.players.splice(playerIndex, 1);
                            }
                        }, function error() {
                            return notificationManager.error('GENERAL ERROR');
                        });
                    }
                });
            };

            $scope.updatePlayerRating = function (player){
                if ($.isNumeric(player.rating)) {
                    if(!updatingRating){
                        updatingRating = true;
                        setTimeout(function(){
                            $http.post(gameUrl + '/' + gameId + '/player_stats/rating', {
                                playerId: player._id,
                                rating: player.rating
                            }).then(function ok(result) {
                                player.rating = result.data.rating || player.rating;
                                updatingRating = false;
                            }, function error() {
                                updatingRating = false;
                                return notificationManager.error('GENERAL ERROR');
                            });
                        }, 1500);
                    }
                }
            };

            $scope.updateGoals = function (player) {
                if ($.isNumeric(player.goals)) {
                    if(!updatingGoals){
                        updatingGoals = true;
                        setTimeout(function(){
                            $http.post(gameUrl + '/' + gameId + '/player_stats/goals', {
                                playerId: player._id,
                                value: player.goals
                            }).then(function ok(result) {
                                player.goals = result.data.value || player.goals;
                                if (player._id == $scope.currentUserId) {
                                    $window.currentUser.goals =
                                        currentUserInitialGoals + player.goals - thisGameInitialUserGoals;
                                }
                                updatingGoals = false;
                            }, function error(result) {
                                updatingGoals = false;
                                return notificationManager.error('GENERAL ERROR');
                            });
                        }, 1500);
                    }
                }
            };

            $scope.updateGameStatistics = function(){
                $http.post(gameUrl + '/' + gameId + '/statistics', $scope.game).then(function ok(result) {
                    _.forEach($scope.game.players, function(player){
                        player.initialRating = player.rating;
                        if(player._id == $scope.currentUserId){
                            $window.currentUser.goals = currentUserInitialGoals + player.goals - thisGameInitialUserGoals;
                        }
                    });
                    return notificationManager.success('GAME STATISTICS SAVED');
                }, function error(result) {
                    return notificationManager.error('GENERAL ERROR');
                });
            };

            $scope.addPlayer = function (){
                $http.post(gameUrl + '/' + gameId + '/players/' + window.currentUser._id).then(function ok() {
                    $scope.game.players.push(window.currentUser);
                }, function error() {
                    return notificationManager.error('GENERAL ERROR');
                });
            };

            $scope.userAlreadySigned = function(){
                if($scope.game && $scope.game.players) {
                    for (var index in $scope.game.players) {
                        if ($scope.game.players[index]._id == $scope.currentUserId) {
                            return true;
                        }
                    }
                }
                return false;
            };

            $scope.deleteGame = function() {
                $translate('CONFIRM DELETE GAME').then(function(confirmationMessage) {
                    if(confirm(confirmationMessage)) {
                        $http.delete(gameUrl + '/' + gameId)
                            .then(function (response) {
                                notificationManager.info('GAME DELETED');
                                $state.go("base.dashboard");
                            }, function err() {
                                return notificationManager.error('GENERAL ERROR');
                            });
                    }
                });
            };

            $scope.generateActivationCode = function() {
                $http.post(gameUrl + '/' + gameId + '/invitation_code')
                    .then(function(response) {
                        $scope.game.invitation_code = response.data.invitation_code;
                        $scope.game.invitation_shorturl = response.data.invitation_shorturl;
                    }, function err() {
                        return notificationManager.error('GENERAL ERROR');
                    });
            };

            $scope.removeActivationCode = function() {
                $translate('CONFIRM DELETE GAME INVITATION CODE').then(function(confirmationMessage) {
                    if(confirm(confirmationMessage)) {
                        $http.delete(gameUrl + '/' + gameId + '/invitation_code')
                            .then(function (response) {
                                if ($scope.game.invitation_code) {
                                    delete $scope.game['invitation_code'];
                                }
                                if ($scope.game.invitation_shorturl) {
                                    delete $scope.game['invitation_shorturl'];
                                }
                            }, function err() {
                                return notificationManager.error('GENERAL ERROR');
                            });
                    }
                });
            };
    }];
});
