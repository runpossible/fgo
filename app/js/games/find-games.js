define([], function () {
    'use strict';

    return ['$scope', '$stateParams', 'appConfig', '$state', '$http', '$q', '$window', 'notificationManager', function ($scope, $stateParams, appConfig, $state, $http, $q, $window, notificationManager) {
        var gameUrl = appConfig.baseApiUrl + 'public/upcoming_games';
        $scope.isLoading = true;

        var initialize = function initialize() {
            var deferred = $q.defer();
            $http.get(gameUrl)
                .then(function (response) {
                    deferred.resolve(response.data);
                }, function() {
                    deferred.reject();
                });
            return deferred.promise;
        };

        initialize().then(function (games) {

            for(var index in games) {
                games[index].date_time = new Date(games[index].date_time);
                games[index].gamePassed = new Date() > new Date(games[index].date_time);
                games[index].isOwner = $window.currentUser._id === games[index].createdBy;
            }

            // remove passed games, sort by date
            games = _.sortBy(_.filter(games, { gamePassed: false }), 'date_time');
            $scope.games = games;

            $scope.isLoading = false;
        }, function err() {
            return notificationManager.error('GENERAL ERROR');
        });
    }];
});
