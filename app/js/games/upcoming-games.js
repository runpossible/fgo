define([], function () {
    'use strict';

    return ['$scope', '$stateParams', 'appConfig', '$state', '$http', '$q', '$window', 'notificationManager',
        'imageService', 'gameService', function ($scope, $stateParams, appConfig, $state, $http, $q, $window, notificationManager, imageService, gameService) {
        var gameUrl = appConfig.baseApiUrl + 'game';
        $scope.isLoading = true;

        var initialize = function initialize() {
            var deferred = $q.defer();
            $http.get(gameUrl)
                .then(function (response) {
                    deferred.resolve(response.data);
                }, function() {
                    deferred.reject();
                });
            return deferred.promise;
        };

        initialize().then(function (games) {

            for (var index in games) {
                games[index].date_time = new Date(games[index].date_time);
                games[index].gamePassed = new Date() > new Date(games[index].date_time);
                games[index].isOwner = $window.currentUser._id === games[index].createdBy;
            }

            // remove passed games, sort by date
            games = _.sortBy(_.filter(games, {gamePassed: false}), 'date_time');
            _.each(games, function (g) {
                g.location.imageUrl = imageService.getLocationImageUrl(g.location);

                if(g.guests) {
                    g.guests = JSON.parse(g.guests);
                }
            });

            $scope.games = games;

            $scope.isLoading = false;
        }, function err() {
            return notificationManager.error('GENERAL ERROR');
        });

        $scope.gameService = gameService;
        $scope.getBookedCount = function(game) {
            return gameService.getPlayersCountStat(game).booked;
            if(!game) {
                return 0;
            }

            return game.players.length + game.guests.length;
        };

        $scope.getFreeSlotsCount = function(game) {
            if(!game) {
                return 0;
            }

            return game.numberOfPlayers - game.players.length - game.guests.length;
        };
    }];
});
