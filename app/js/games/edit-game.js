define([], function () {
    'use strict';

    return ['$scope', '$stateParams', 'appConfig', '$state', '$http', '$translate', '$q', 'notificationManager', function($scope, $stateParams, appConfig, $state, $http, $translate, $q, notificationManager) {
        var gameId = $stateParams.gameId,
            gameUrl = appConfig.baseApiUrl + 'game';
        var originalLocation = null;
        var originalDate = null;
        var originalTime = null;

        if(!gameId){
            return $state.go('base.newgame');
        }

        // show some loader
        var setLoaderVisbility = function(isVisible) { $scope.isLoading = isVisible; };
        setLoaderVisbility(true);

        $scope.$watch('game.location', function(newValue, oldValue) {
            if(newValue && newValue.price) {
                $scope.game.price = newValue.price;
            }
        }, true);

        var initialize = function initialize() {
            var deferred = $q.defer();
            $http.get(gameUrl + '/' + gameId)
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function() {
                    deferred.reject();
                });
            return deferred.promise;
        };

        initialize().then(function(game) {
            gameId = game._id;
            $scope.game = game;
            $scope.game.time = new Date(game.date_time);
            $scope.game.date = new Date(game.date_time);
            $scope.game.hour = $scope.game.time.getHours();
            $scope.game.minutes = $scope.game.time.getMinutes();
            $scope.game.public = $scope.game.public ? "true" : "false";
            $scope.currentUserId = window.currentUser._id;
            $scope.userIsAdmin = game.group && game.group.admin_users && game.group.admin_users.indexOf($scope.currentUserId) > -1;
            $scope.gamePassed = new Date() > new Date($scope.game.date_time);
            $scope.game.locationId = $scope.game.location._id;

            $scope.groups = [];
            if(game.group){
                $scope.groups.push({id: game.group._id, name: game.group.name, urlName: game.group.urlName});
                $scope.game.groupId = game.group._id;
            }

            originalLocation = game.location ? game.location._id : null;
            originalDate = game.date;
            originalTime = game.time;

            setLoaderVisbility(false);
        }, function err() {
            return notificationManager.error('GENERAL ERROR');
        });

        $scope.minDate = $scope.minDate ? null : new Date();
        $scope.maxDate = new Date(2020, 5, 22);

        $scope.open = function($event) {
            $scope.status.opened = true;
        };

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };

        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];
        $scope.status = {
            opened: false
        };

        var tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 1);
        var afterTomorrow = new Date();
        afterTomorrow.setDate(tomorrow.getDate() + 2);
        $scope.events =
            [
                {
                    date: tomorrow,
                    status: 'full'
                },
                {
                    date: afterTomorrow,
                    status: 'partially'
                }
            ];

        $scope.getDayClass = function(date, mode) {
            if (mode === 'day') {
                var dayToCheck = new Date(date).setHours(0,0,0,0);

                for (var i=0;i<$scope.events.length;i++){
                    var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);

                    if (dayToCheck === currentDay) {
                        return $scope.events[i].status;
                    }
                }
            }

            return '';
        };

        $scope.updateGame = function(isValid) {
            if(isValid) {
                $scope.submitting = true;
                var locationChanged = originalLocation != ($scope.game.location ? $scope.game.location.id : null);
                var timeChanged = ($scope.game.hour != originalTime.getHours())
                    || ($scope.game.minutes != originalTime.getMinutes())
                    || ($scope.game.date != originalDate);

                if($scope.game.date != originalDate){
                    $scope.game.date.setHours($scope.game.date.getHours() + 12);
                }
                $scope.game.time.setHours($scope.game.hour, $scope.game.minutes, 0);
                var neededNotification = null;
                if(locationChanged && timeChanged){
                    neededNotification = 'GameTimeAndLocationChanged'
                }else if (locationChanged)
                {
                    neededNotification = 'GameLocationChanged'
                } else if (timeChanged){
                    neededNotification = 'GameTimeChanged'
                }
                var url = appConfig.baseApiUrl + 'game/' + $scope.game._id;
                $http.put(url, $scope.game).then(function ok(response) {
                    var updatedGame = response.data;
                    if(neededNotification){
                        notificationManager.addOfflineNotification(neededNotification, $scope.game);
                    }
                    return $state.go('base.gamedetails.game', { gameId: updatedGame.urlName || updatedGame._id });
                }, function error() {
                    $scope.submitting = false;
                    return notificationManager.error('GENERAL ERROR');
                });
            }
        };
    }];
});
