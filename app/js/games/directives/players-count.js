define([], function() {
    'use strict';

    return [function() {
        return {
            restrict: 'E',
            templateUrl: '/js/games/directives/players-count.html?t=' + window.requestSuffix,
            replace: true,
            scope: {
                playersCount: '='
            },
            link: function($scope) {
                $scope.playersCount = $scope.playersCount || 12;
                $scope.dec = function() {
                    if($scope.playersCount > 1) {
                        $scope.playersCount--;
                    }
                };

                $scope.inc = function() {
                    if($scope.playersCount < 24) {
                        $scope.playersCount++;
                    }
                };

                $scope.range = function() {
                    var r = [];
                    for(var i=1;i<=$scope.playersCount;i++) {
                        r.push(1);
                    }
                    return r;
                }
            }
        };
    }]
});