define(['lodash'], function(_) {
    'use strict';

    return ['inviteService', 'appConfig', 'notificationManager', '$http', '$translate', '$rootScope',
        function(inviteService, appConfig, notificationManager, $http, $translate, $rootScope) {
        return {
            restrict: 'E',
            templateUrl: '/js/games/directives/invited-list.html?t=' + window.requestSuffix,
            replace: true,
            scope: {
                game: '=',
                canAddRemove: '&'
            },
            link: function($scope) {
                var loadInvitations = function(gameId) {
                    inviteService.getInvitedForGame(gameId).then(function (invites) {
                        var notAcceptedInvites = _.filter(invites, function (invite) {
                            return invite.status !== 'Accepted';
                        });
                        
                        if($scope.game){
                            notAcceptedInvites = _.filter(notAcceptedInvites, function (invite) {
                                return !_.any($scope.game.players, function(player){
                                    return player._id == invite.player._id;
                                });
                            });
                        }

                        var canAddRemove = $scope.canAddRemove || function() {};
                        _.each(notAcceptedInvites, function(inv) {
                            inv.hasRejected = inv.status === 'Rejected';
                            inv.notAnswered = inv.status === 'Active';

                            inv.showControls = canAddRemove({ player: inv.player });
                        });

                        _.sortBy(notAcceptedInvites, 'notAnswered');
                        notAcceptedInvites.reverse();

                        $scope.invites = notAcceptedInvites;
                    }, function error(err) {
                        // TODO: handle error
                    });
                };

                $scope.$watch('game', function() {
                    if(!$scope.game) {
                        return;
                    }

                    loadInvitations($scope.game._id);
                });

                $rootScope.$on('invitationsChange', function() {
                    loadInvitations($scope.game._id);
                });

                $scope.inviteAction = function (invite, status) {
                    var url = appConfig.baseApiUrl + 'game/' + invite.game._id  + '/invite/' + invite._id;

                    $http.put(url, {
                        status: status
                    }).then(function () {
                        // no translation here yet.
                        if (status == 'Accepted') {
                            $translate('SUCCESSFULLY JOINED THE GAME').then(function (translatedMessage) {
                                notificationManager.success(translatedMessage, false);
                            });
                        }
                        else {
                            $translate('REJECTED JOINING TO GAME').then(function (translatedMessage) {
                                notificationManager.info(translatedMessage, false);
                            });
                        }

                        $rootScope.$emit('invitationsChange');
                    }, function err() {
                        notificationManager.error('GENERAL ERROR');

                        // should anyway load the invites again.
                        $rootScope.$emit('invitationsChange');
                    });
                };
            }
        };
    }]
});