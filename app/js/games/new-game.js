define(['lodash'], function (_) {
    'use strict';

    return ['$scope', 'appConfig', '$http', '$state', 'notificationManager', '$window', '$filter', '$q',
        function ($scope, appConfig, $http, $state, notificationManager, $window, $filter, $q) {
            $scope.map = {center: {latitude: 45, longitude: -73}, zoom: 8};

            $scope.loadLastGame = false;
            $scope.isLoading = true;

            var now = new Date();
            // Form bound game object
            $scope.game = {
                date: now,
                time: now,
                hour: 20,
                minutes: 0,
                numberOfPlayers: 10,
                public: "false",
                team1: {
                    name: $filter('translate')('TEAM 1')
                },
                team2: {
                    name: $filter('translate')('TEAM 2')
                }
            };
            var currentHour = $scope.game.time.getHours();
            if(currentHour > 20){
                $scope.game.date.setDate($scope.game.date.getDate() + 1);
            }

            //$scope.game.time.setHours(20, 0, 0);

            $scope.minDate = $scope.minDate ? null : new Date();
            $scope.maxDate = new Date(2020, 5, 22);

            // Disable weekend selection
            //$scope.disabled = function(date, mode) {
            //    return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
            //};

            $scope.open = function ($event) {
                $scope.status.opened = true;
            };

            $scope.dateOptions = {
                formatYear: 'yy',
                startingDay: 1
            };

            $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
            $scope.format = $scope.formats[0];
            $scope.status = {
                opened: false
            };


            var tomorrow = new Date();
            tomorrow.setDate(tomorrow.getDate() + 1);
            var afterTomorrow = new Date();
            afterTomorrow.setDate(tomorrow.getDate() + 2);
            $scope.events =
                [
                    {
                        date: tomorrow,
                        status: 'full'
                    },
                    {
                        date: afterTomorrow,
                        status: 'partially'
                    }
                ];

            $scope.getDayClass = function (date, mode) {
                if (mode === 'day') {
                    var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

                    for (var i = 0; i < $scope.events.length; i++) {
                        var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                        if (dayToCheck === currentDay) {
                            return $scope.events[i].status;
                        }
                    }
                }

                return '';
            };

            var initializeGroups = function() {
                var deferred = $q.defer();
                $http.get(appConfig.baseApiUrl + 'group')
                    .then(function(response) {
                        deferred.resolve(response.data);
                    }, function() {
                        deferred.reject();
                    });
                return deferred.promise;
            };

            $scope.$watch('game.location', function(newValue, oldValue) {
                if(newValue && newValue.price) {
                    $scope.game.price = newValue.price;
                }
            }, true);

            var initializeLastGame = function() {
                var deferred = $q.defer();
                if(!$scope.loadLastGame){
                    deferred.resolve(null);
                } else {
                    $http.get(appConfig.baseApiUrl + 'game/last')
                        .then(function (response) {
                            deferred.resolve(response.data);
                        }, function (response) {
                            if (response.status == 404) {
                                deferred.resolve(null);
                            } else {
                                deferred.reject();
                            }
                        });
                }
                return deferred.promise;
            };

            var initialize = function () {
                var promises = [ initializeGroups(), initializeLastGame() ];
                $q.all(promises).then(function(results) {
                    var groups = [
                        {
                            id: null,
                            name: $filter('translate')('GROUP') // empty, without a group
                        }
                    ];
                    var data = results[0];

                    for (var i = 0; i < data.length; i++) {
                        // filter out the groups, which the user has rights for
                        // TODO: Yosif removed it, as currently we cannot make users admins.
                        // Let user organize a game for a group
                        //var isAdmin = _.some(data[i].admin_users, {_id: $window.currentUser._id});
                        //if (isAdmin) {
                            groups.push({id: data[i]._id, name: data[i].name, urlName: data[i].urlName});
                        //}
                    }

                    $scope.groups = groups;
                    if ($scope.groups.length > 0) {
                        $scope.game.groupId = groups[0].id;
                    }

                    var lastGame = results[1];
                    if(lastGame){
                        //$scope.game.time.setHours(lastGame.time.getHours(), 0, 0);
                        $scope.game.numberOfPlayers = lastGame.numberOfPlayers;
                        $scope.game.public = (lastGame.public || false).toString();
                        if(lastGame.team1 && lastGame.team1.name) {
                            $scope.game.team1.name = lastGame.team1.name;
                        }
                        if(lastGame.team2 && lastGame.team2.name) {
                            $scope.game.team2.name = lastGame.team2.name;
                        }
                        if(lastGame.group){
                            $scope.game.groupId = lastGame.group._id;
                        }
                        $scope.game.locationId = lastGame.location._id;
                        $scope.game.location = lastGame.location;
                        //$scope.game.name = lastGame.name;
                        $scope.game.price = lastGame.price;

                        var lastGameTime = new Date(lastGame.date_time);

                        if(lastGameTime.getHours() > now.getHours()){
                            $scope.game.hour = lastGameTime.getHours();
                            $scope.game.minutes = lastGameTime.getMinutes();
                        }
                    }

                    $scope.isLoading = false;
                }, function err() {
                    return notificationManager.error('GENERAL ERROR');
                });
            };

            initialize();

            $scope.createGame = function (isValid) {
                if($scope.game.locationId === 'empty') {
                    return false;
                }

                if (isValid) {
                    var dateTime = $scope.game.date;
                    dateTime.setHours($scope.game.hour, $scope.game.minutes, 0);
                    var now = new Date();

                    if(dateTime < now) {
                        notificationManager.error('TIME IN THE PAST ERROR');
                        return; // it's not valid
                    }

                    $scope.submitting = true;
                    var url = appConfig.baseApiUrl + 'game';
                    $scope.game.group = _.findLast($scope.groups, function (group) {
                        return group.id = $scope.game.groupId
                    });
                    $scope.game.time.setHours($scope.game.hour, $scope.game.minutes, 0);

                    $http.post(url, $scope.game).then(function ok(response) {
                        var createdGame = response.data;
                        return $state.go('base.gamedetails.game', {gameId: createdGame.urlName || createdGame._id});
                    }, function error(err) {
                        $scope.submitting = false;
                        if(err && err.data && err.data.errorMessage && err.data.errorMessage.code == 11000) {
                            return notificationManager.error('DUPLICATE CREATE GAME ERROR');
                        } else {
                            return notificationManager.error('GENERAL ERROR');
                        }
                    });
                }
            };

        //$scope.$watch('game.location', function() {
        //    console.log('game location has changed');
        //});
    }];
});
