define([], function () {
    'use strict';

    return ['$scope', '$stateParams', 'appConfig', '$state', '$http', '$q', 'TableOptions', 'gameService', function ($scope, $stateParams, appConfig, $state, $http, $q, TableOptions, gameService) {
        var gameUrl = appConfig.baseApiUrl + 'game';
        $scope.isLoading = true;

        $scope.gameService = gameService;

        var loadData = function (page, count) {
            var deferred = $q.defer();

            $scope.isLoading = true;
            $http.get(gameUrl + "/paginated?page=" + (page - 1) + "&count=" + count)
                .then(function (response) {
                    $scope.isLoading = false;
                    $scope.options.setPage( page );
                    deferred.resolve(response.data);
                }, function() {
                    $scope.isLoading = false;
                    deferred.reject();
                });

            return deferred.promise;
        };

        var opts = new TableOptions(
            function goToPage(page, pageSz) {
                loadData(page, pageSz).then(updateResults);
            }
        );

        opts.hasPrev = true;
        opts.hasNext = true;

        $scope.options = opts;

        var updateResults = function(data) {
            $scope.isLoading = false;

            $scope.games = data.items;

            $scope.options.setTotal(data.total);
            $scope.total = data.total;

            $scope.hasItems = data.total > 0;
            $scope.showPaging = data.total > data.items.length;

            $scope.$evalAsync();
        };

        // initial load
        loadData(opts.getPage(), opts.getPageSize()).then(updateResults);
    }];
});
