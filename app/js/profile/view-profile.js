define(['lodash'], function (_) {
    'use strict';

    return ['$scope', 'appConfig', '$http', '$state', 'notificationManager', '$window', '$filter', '$stateParams', '$q', 'countryService', 'friendService',
        function($scope, appConfig, $http, $state, notificationManager, $window, $filter, $stateParams, $q, countryService, friendService) {
            var userUrl = appConfig.baseApiUrl + 'user',
                friendsUrl = appConfig.baseApiUrl + 'profile/friends';
            $scope.isLoading = true;
            $scope.isCurrentUser = (!$stateParams.userId || $stateParams.userId === $window.currentUser._id);
            $scope.userId = $stateParams.userId || $window.currentUser._id;
            $scope.areAlreadyFriends = false;


            var initializeUser = function() {
                var deferred = $q.defer();
                if($scope.isCurrentUser){
                    deferred.resolve(_.clone($window.currentUser));
                } else {
                    $http.get(userUrl + '/' + $scope.userId)
                        .then(function(response) {
                            var user = response.data;
                            friendService.getFriends().then(function(res2) {
                                // get the friends of the Current user.
                                var areAlreadyFriends = _.some(res2, {'_id': user._id});
                                $scope.areAlreadyFriends = areAlreadyFriends;
                                deferred.resolve(response.data);
                            });
                        }, function(response) {
                            deferred.reject(response);
                        });
                }

                return deferred.promise;
            };

            $scope.addFriend = function addFriend() {
                if(!$scope.areAlreadyFriends) {
                    $http.post(friendsUrl, {
                        users: [$scope.user._id]
                    }).then(function () {
                        $scope.areAlreadyFriends = true; // is a friend now
                        $window.currentUser.friendsCount = $window.currentUser.friendsCount + 1;
                        return notificationManager.success('FRIEND ADDED');
                    }, function err() {
                        return notificationManager.error('GENERAL ERROR');
                    });
                }
            };

            initializeUser().then(function(user) {
                if(typeof  user.public == 'string'){
                    user.public = user.public == 'true';
                }

                $scope.user = user;

                if($scope.user.countryId) {
                    countryService.getCountryName($scope.user.countryId, function(err, country){
                         $scope.user.countryName = country;
                    });
                }

                $scope.user.preferredPositionText = '';
                var preferedPosition = parseInt($scope.user.preferredPosition);
                switch(preferedPosition){
                    case 1: $scope.user.preferredPositionText = $filter('translate')('KEEPER');
                        break;
                    case 2: $scope.user.preferredPositionText = $filter('translate')('DEFENDER');
                        break;
                    case 3: $scope.user.preferredPositionText = $filter('translate')('MIDFIELDER');
                        break;
                    case 4: $scope.user.preferredPositionText = $filter('translate')('ATTACKER');
                        break;
                    default: $scope.user.preferredPositionText = $filter('translate')('NO PREFERRED POSITION');
                        break;
                }

                $scope.isLoading = false;
            }, function err(response) {
                if(response.status == 404){
                    return notificationManager.error('CANNOT FIND USER INFORMATION');
                } else {
                    return notificationManager.error('GENERAL ERROR');
                }
            });
    }];
});
