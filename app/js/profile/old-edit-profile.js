define(['lodash'], function (_) {
    'use strict';

    return ['$scope', 'appConfig', '$http', '$state', 'notificationManager', '$window', '$filter', 
        function($scope, appConfig, $http, $state, notificationManager, $window, $filter) {
            $scope.isLoading = true;
            $scope.user = $window.currentUser;
            $scope.user.preferredPosition = ($scope.user.preferredPosition || '0').toString();
            $scope.initialPasswordMissing = !$scope.user.hasLocal;
            $scope.isLoading = false;
            $scope.currentUser = $window.currentUser;

            if($state.params.linkError)
            {
                notificationManager.error($state.params.linkError == "1" ? "FACEBOOK ACCOUNT ALREADY CONNECTED WITH ANOTHER USER" : "GOOGLE ACCOUNT ALREADY CONNECTED WITH ANOTHER USER");
                return $state.transitionTo('base.profile.edit', {}, {reload: true});
            }
            else if($state.params.linkSuccess){
                notificationManager.success($state.params.provider == "facebook" ? "FACEBOOK ACCOUNT LINKED" : "GOOGLE ACCOUNT LINKED");
                return $state.transitionTo('base.profile.edit', {}, {reload: true});
            }

            $scope.saveProfile = function(isValid){
                if(isValid) {
                    var url = appConfig.baseApiUrl + 'profile';
                    var body = {
                        name: $scope.user.name,
                        preferredAvatar: $scope.user.preferredAvatar,
                        preferredPosition: $scope.user.preferredPosition,
                    };
                    if ($scope.changePassword){
                        body.currentPassword = $scope.currentPassword || 'first-password';
                        body.newPassword = $scope.newPassword;
                    }
                    $http.post(url, body).then(function ok(response) {
                        var updatedUser = response.data;
                        $window.currentUser = updatedUser;
                        $scope.user = $window.currentUser;
                        $scope.user.preferredPosition = ($scope.user.preferredPosition || '0').toString();

                        return notificationManager.success('PROFILE UPDATED');
                    }, function error(response) {
                        return notificationManager.error((response && response.data && response.data.errorMessage) ? response.data.errorMessage : 'GENERAL ERROR');
                    });
                }
            };

            var disconnectProfile = function(profile, successMessage, confirmationMessage){
                var confirmationMessageTranslated = $filter('translate')(confirmationMessage);
                if(confirm(confirmationMessageTranslated)){
                    var url = appConfig.baseApiUrl + 'user/' + profile;
                    $http.delete(url).then(function ok(response) {
                        var updatedUser = response.data;
                        $window.currentUser = updatedUser;
                        $scope.user = $window.currentUser;
                        return notificationManager.success('FACEBOOK DISCONNECTED');
                    }, function error(response) {
                        return notificationManager.error((response && response.data && response.data.errorMessage) ? response.data.errorMessage : 'GENERAL ERROR');
                    });
                }
            };

            $scope.disconnectFacebook = function(){
                disconnectProfile('facebook', 'FACEBOOK DISCONNECTED', 'CONFIRM FACEBOOK DISCONNECT');
            };

            $scope.disconnectGoogle = function(){
                disconnectProfile('google', 'GOOGLE DISCONNECTED', 'CONFIRM GOOGLE DISCONNECT');
            };
    }];
});
