define([], function() {
    'use strict';

    // we have small = 50px, normal = 100px and large = 200px
    // add userid or email if you want to get the avatar of another user
    return ['$stateParams', 'appConfig', '$state', '$http', '$translate', '$interval', '$q', 'notificationManager', 'timeService', '$window',
        function($stateParams, appConfig, $state, $http, $translate, $interval, $q, notificationManager, timeService, $window) {
        return {
            restrict: 'E',
            templateUrl: '/js/profile/player-reviews.html?t=' + window.requestSuffix,
            replace: true,
            link: function($scope, element, attrs) {
                var userId = $stateParams.userId || $window.currentUser._id;
                var commentsUrl = appConfig.baseApiUrl + 'comments/user';

                var loadComments = function loadComments(){
                    var deferred = $q.defer();
                    $http.get(commentsUrl + '/' + userId)
                        .then(function(response) {
                            deferred.resolve(response.data);
                        }, function() {
                            deferred.reject();
                        });
                    return deferred.promise;
                };

                $scope.refreshComments = function(){
                    loadComments().then(function(comments) {
                        $scope.comments = comments;
                    })
                };

                $scope.submitComment = function(){
                    if( $scope.newComment && $scope.newComment.trim() != '') {
                        var body = {
                            text: $scope.newComment
                        };
                        $http.post(commentsUrl + '/' + userId, body)
                            .then(function (response) {
                                response.data.createdBy = window.currentUser;
                                response.data.user = $scope.userId;
                                $scope.comments.push(response.data);
                                $scope.newComment = '';
                            }, function error() {
                                return notificationManager.error('GENERAL ERROR');
                            });
                    }
                };

                $scope.timeSince = function(date_string){
                    return timeService.timeSince(date_string);
                };

                $scope.refreshComments();
            }
        };
    }];
});