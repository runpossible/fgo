define(function(require) {
    var angular = require('angular');

    var profile = angular.module('profile', ['ui.router']);
    var viewProfileCtrl = require('/js/profile/view-profile.js');
    var editProfileCtrl = require('/js/profile/edit-profile.js');
    var contactCtrl = require('/js/profile/contact.js');
    var playerReviewsDir = require('/js/profile/player-reviews.js');

    profile.controller('editProfileCtrl', editProfileCtrl);
    profile.controller('viewProfileCtrl', viewProfileCtrl);
    profile.controller('contactCtrl', contactCtrl);

    profile.directive('playerReviews', playerReviewsDir);

    profile.config(['$stateProvider', function($stateProvider) {
        $stateProvider.state('base.profile', {
            url: 'profile',
            abstract: true,
            templateUrl: '/js/home/empty.html'
        });

        $stateProvider.state('base.profile.edit', {
            url: '/edit?provider&linkError&linkSuccess',
            templateUrl: '/js/profile/edit-profile.html',
            controller: 'editProfileCtrl',
            data:{
                pageTitle: 'EDIT USER PROFILE',
                requireSSL: true
            }
        });

        $stateProvider.state('base.profile.view', {
            url: '/view',
            abstract: true,
            templateUrl: '/js/home/empty.html'
        });

        $stateProvider.state('base.profile.view.current', {
            url: '',
            templateUrl: '/js/profile/view-profile.html',
            controller: 'viewProfileCtrl',
            data:{
                pageTitle: 'USER PROFILE'
            }
        });

        $stateProvider.state('base.profile.view.user', {
            url: '/:userId',
            templateUrl: '/js/profile/view-profile.html',
            controller: 'viewProfileCtrl',
            data:{
                pageTitle: 'USER PROFILE'
            }
        });

        $stateProvider.state('base.profile.contact', {
            url: '/contact',
            templateUrl: '/js/profile/contact.html',
            controller: 'contactCtrl',
            data:{
                pageTitle: 'CONTACT US'
            }
        });
    }]);

    return profile;
});
