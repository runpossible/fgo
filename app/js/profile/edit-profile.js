define(['lodash'], function (_) {
    'use strict';

    return ['$scope', 'appConfig', '$http', '$state', 'notificationManager', '$window', '$filter', '$q', 'FileUploader', 'Upload', '$timeout',
        function($scope, appConfig, $http, $state, notificationManager, $window, $filter, $q, FileUploader, Upload, $timeout) {
            var userUrl = appConfig.baseApiUrl + 'user';
            $scope.isLoading = true;
            $scope.userId = $window.currentUser._id;
            $scope.isMobile = appConfig.isMobile;

            var setupUploader = function(){
                var uploader = new FileUploader();
                uploader.filters.push({
                    name: 'imageFilter',
                    fn: function (item /*{File|FileLikeObject}*/, options) {
                        var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                        return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
                    }
                });
                
                //https://github.com/nervgh/angular-file-upload/blob/master/examples/image-preview/controllers.js
                uploader.onWhenAddingFileFailed = function (item /*{File|FileLikeObject}*/, filter, options) {
                    //console.info('onWhenAddingFileFailed', item, filter, options);
                };
                
                uploader.onSuccessItem = function(){
                    $timeout(function(){
                        reloadImage();
                    }, 1000);
                }

                // file upload
                uploader.onAfterAddingFile = function (fileItem) {
                    var fileSelector = $('#file-selector-id');
                    if(fileSelector && fileSelector[0] && fileSelector[0].files && fileSelector[0].files[0]) {
                        var szInMb = fileSelector[0].files[0].size / (1024.0 * 1024.0);
                        if(szInMb >= 5) {
                            return notificationManager.error('FILE SIZE IS BIGGER THAN 5MB');
                        }
                        
                        var imgUrl = appConfig.baseApiUrl + 'user/image';
                        $scope.localAvatarUrl = appConfig.defaultAvatarUrl;
                        $scope.uploadFile(imgUrl);                 
                    }
                };
                
                $scope.uploader = uploader;
            };
            
            setupUploader();
            
            $scope.reloadImage = function(){
                $scope.user.avatarUrl = appConfig.baseApiUrl + 'user/' + $scope.userId + '/image';
                $scope.localAvatarUrl = $scope.user.avatarUrl;
                $scope.facebookAvatarUrl = '';
                $window.currentUser.avatarUrl = $scope.localAvatarUrl;
                $scope.croppedDataUrl = '';
                $scope.imageSelected = false;
                //setupUploader();
            };

            $scope.cancelUpload = function(){
                $scope.reloadImage();
            };

            $scope.upload = function () {
                $scope.localAvatarUrl = appConfig.defaultAvatarUrl;
                Upload.upload({
                    url: appConfig.baseApiUrl + 'user/image',
                    data: {
                        file: Upload.dataUrltoBlob($scope.croppedDataUrl)
                    }
                }).then(function (response) {
                    $timeout(function () {
                        $scope.reloadImage();
                    });
                }, function (response) {
                    if (response.status > 0) {
                        var errorMsg = response.status + ': ' + response.data;
                        notificationManager.error(errorMsg);
                    }
                }, function (evt) {
                    var success = parseInt(100.0 * evt.loaded / evt.total);
                });
            };

            $scope.uploadFile = function uploadFile(url) {
                if($scope.uploader.queue.length) {
                    _.forEach($scope.uploader.queue, function(i){ i.url = url });
                    $scope.uploader.uploadAll();
                }
            };

            $scope.uploadBtnClicked = function() {
                $('#file-selector-id').click();
            };

            //$scope.user = $window.currentUser;
            //$scope.user.preferredPosition = ($scope.user.preferredPosition || '0').toString();
            //$scope.initialPasswordMissing = !$scope.user.hasLocal;
            //$scope.isLoading = false;
            //$scope.currentUser = $window.currentUser;

            var loadUser = function(){
                $scope.user = _.clone($window.currentUser);

                $scope.user.countryId = ($scope.user.countryId || 0).toString();
                $scope.user.preferredPosition = ($scope.user.preferredPosition != null) ? $scope.user.preferredPosition.toString() : '';
                if($scope.user.public == undefined){
                    $scope.user.public = true;
                }
                $scope.user.public = $scope.user.public ? 'true' : 'false';
                if($scope.user.birthDate){
                        $scope.user.birthDate = new Date($scope.user.birthDate);
                }

                $scope.currentPassword = '';
                $scope.newPassword = '';

                if($scope.user.avatarUrl){
                    if($scope.user.avatarUrl.indexOf('.facebook.com') > -1){
                        $scope.facebookAvatarUrl = $scope.user.avatarUrl;
                    } else {
                        $scope.localAvatarUrl = $scope.user.avatarUrl;
                    }
                }

                // set to true by default.
                $scope.user.shouldReceiveGameInviteMails = ($scope.user.shouldReceiveGameInviteMails !== false);
                $scope.isLoading = false;
            };

            loadUser();

            $scope.loadFacebookImage = function(){
                $http.get(userUrl + '/facebook-image')
                    .then(function(response) {
                        $scope.facebookAvatarUrl = response.data;
                        $scope.localAvatarUrl = '';
                    }, function(response) {
                        return notificationManager.error('GENERAL ERROR');
                    });
            };

            $(".md-datepicker-input").attr("placeholder", $filter("translate")("DATE OF BIRTH"));

            $scope.saveProfile = function(isValid){
                if(isValid) {
                    $scope.submitting = true;
                    var url = appConfig.baseApiUrl + 'profile';
                    var body = {
                        name: $scope.user.name,
                        birthDate: $scope.user.birthDate,
                        city: $scope.user.city,
                        height: $scope.user.height,
                        weight: $scope.user.weight,
                        strongFoot: $scope.user.strongFoot,
                        public: $scope.user.public == 'true',
                        shouldReceiveGameInviteMails: $scope.user.shouldReceiveGameInviteMails
                    };
                    if($scope.user.preferredPosition){
                        body.preferredPosition = parseInt($scope.user.preferredPosition || '0');
                    }
                    if($scope.user.countryId){
                        body.countryId = parseInt($scope.user.countryId || '0');
                    }
                    if($scope.newPassword){
                        body.currentPassword = $scope.currentPassword;
                        body.newPassword = $scope.newPassword;
                    }
                    if($scope.facebookAvatarUrl){
                        body.avatarUrl = $scope.facebookAvatarUrl;
                    }
                    if ($scope.currentPassword){
                        body.currentPassword = $scope.currentPassword || 'first-password';
                        body.newPassword = $scope.newPassword;
                    }
                    $http.post(url, body).then(function ok(response) {
                        var updatedUser = response.data;
                        $window.currentUser = updatedUser;
                        $scope.user = $window.currentUser;
                        $scope.user.preferredPosition = ($scope.user.preferredPosition || '0').toString();
                        loadUser();
                        $scope.submitting = false;
                        notificationManager.success('PROFILE UPDATED');
                        $state.go("base.profile.view.current");
                    }, function error(response) {
                        $scope.submitting = false;
                        return notificationManager.error((response && response.data && response.data.errorMessage) ? response.data.errorMessage : 'GENERAL ERROR');
                    });
                }
            };
    }];
});
