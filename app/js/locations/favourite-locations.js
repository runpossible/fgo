define([], function () {
    'use strict';
    // TODO: Refactor
    return ['$scope', 'appConfig', '$http', '$stateParams', 'locationService', 'imageService', function ($scope, appConfig, $http, $stateParams, locationService, imageService) {
        $scope.isLoading = true;

        $http.get(appConfig.baseApiUrl + 'favourite/location')
            .then(function (res) {
                $scope.locations = _.map(res.data, function(item) {
                    item.location.imageUrl = imageService.getLocationImageUrl(item.location);

                    return item.location;
                });

                $scope.isLoading = false;
            });
    }];
});
