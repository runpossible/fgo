define(function (require) {
    var angular = require('angular');

    var locations = angular.module('locations', ['ui.router']);
    var newLocationCtrl = require('/js/locations/new-location.js');
    var viewLocationCtrl = require('/js/locations/view-location.js');
    var searchLocationCtrl = require('/js/locations/search-location.js');
    var locationResultsDir = require('/js/locations/location-results.js');
    var favLocationCtrl = require('/js/locations/favourite-locations.js');

    locations.controller('newLocationCtrl', newLocationCtrl);
    locations.controller('viewLocationCtrl', viewLocationCtrl);
    locations.controller('searchLocationCtrl', searchLocationCtrl);
    locations.controller('favLocationCtrl', favLocationCtrl);

    locations.directive('locationResults', locationResultsDir);

    locations.config(['$stateProvider', function ($stateProvider) {
        $stateProvider.state('base.location', {
            url: 'location',
            abstract: true,
            templateUrl: '/js/home/empty.html'
        });

        $stateProvider.state('base.location.new', {
            url: '/new',
            templateUrl: '/js/locations/new-location.html',
            controller: 'newLocationCtrl',
            data:{
                pageTitle: 'NEW LOCATION'
            }
        });

        $stateProvider.state('base.location.view', {
            url: '/view/:id',
            templateUrl: '/js/locations/view-location.html',
            controller: 'viewLocationCtrl',
            data:{
                pageTitle: 'VIEW LOCATION'
            }
        });

        $stateProvider.state('base.location.find', {
            url: '/search',
            templateUrl: '/js/locations/search-location.html',
            controller: 'searchLocationCtrl',
            data:{
                pageTitle: 'FIND LOCATION'
            }
        });

        $stateProvider.state('base.location.favourites', {
            url: '/favourite-locations',
            templateUrl: '/js/locations/favourite-locations.html',
            controller: 'favLocationCtrl',
            data:{
                pageTitle: 'FAVOURITE LOCATIONS'
            }
        });
    }]);

    return locations;
});
