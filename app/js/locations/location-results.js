define(['lodash'], function (_) {
    'use strict';

    return ['$http', 'appConfig', 'notificationManager', 'locationService', '$timeout', function ($http, appConfig, notificationManager, locationService, $timeout) {

        return {
            restrict: 'E',
            templateUrl: '/js/locations/location-results.html?t=' + window.requestSuffix,
            scope: {
                locations: '=',
                favLocations: '=',
                view: '='
            },
            link: function ($scope) {
                $scope.isFavLocation = function (id) {
                    var isFav = _.some($scope.favLocations, function (loc) {
                        return loc.location._id === id;
                    });

                    return isFav;
                };
                
                $scope.range = function () {
                    if(!$scope.locations) return;

                    var r = [];
                    for (var i = 1; i <= 4 - $scope.locations.length; i++) {
                        r.push(1);
                    }
                    return r;
                };

                $scope.marker = {
                    options: {draggable: false}
                };

                locationService.getUserLocation(function (lat, long) {
                    $timeout(function () {
                        $scope.map = {
                            center: {
                                latitude: lat,
                                longitude: long
                            },
                            zoom: 14
                            /*,
                            events: {
                                dragend:function(circle, eventName, model, args){
                                    console.log('Event - dragend', eventName)
                                  },
                                  radius_changed:function(circle, eventName){
                                    console.log('Event - radius_changed', eventName)
                                  },
                                center_changed:function(circle, eventName) {
                                    console.log('Event - center_changed', eventName)
                                }
                            }*/
                        }
                    });
                });

                $scope.existingMarkers = [];
                $scope.$watch('locations', function () {
                    if(!$scope.locations) return;

                    var markers = _.map($scope.locations, locationService.locationToMarker);

                    // force $scope.digest loop
                    $scope.existingMarkers = markers;
                    $scope.$evalAsync();
                });

                $scope.$watch('view', function () {
                    $timeout(function() {
                        $scope.showMap = ($scope.view === 'map');
                    }, 100);
                });
            }
        };
    }];
});