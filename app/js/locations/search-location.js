define([], function () {
    'use strict';
    // TODO: Refactor
    return ['$scope', 'appConfig', '$http', '$stateParams', 'locationService', 'imageService', function($scope, appConfig, $http, $stateParams, locationService, imageService) {
        $scope.isLoading = true;
        $scope.isLocationResultsLoading = true;

        $scope.view = 'grid';

        locationService.getUserLocation(function(lat, long) {
            $scope.isLoading = false;
            $scope.latitude = lat;
            $scope.longitude = long;

            $scope.typeNames = {
                byNearest: 'by-nearest',
                byName: 'by-name',
                byAddress: 'by-address'
            };

            $scope.criteria = {
                type: $scope.typeNames.byNearest
            };

            var getSearchQueryString = function() {
                var searchText = $scope.searchText;
                var queryString = '';
                if(!searchText) {
                    // search by nearest
                    queryString += '?type=by-nearest&long=' + $scope.longitude + '&lat=' + $scope.latitude;
                } else {
                    queryString += '?type=all&txt=' + $scope.searchText;
                }

                return queryString;
            };

            $scope.search = function(done) {
                var queryString = getSearchQueryString();
                var url = appConfig.baseApiUrl + 'location/search' + queryString;

                $http.get(url).then(function(res) {
                    $scope.locations = res.data;

                    _.each($scope.locations, function(location) {
                        location.imageUrl = imageService.getLocationImageUrl(location);
                    });


                    if(done) {
                        return done();
                    }
                });
            };

            $http.get(appConfig.baseApiUrl + 'favourite/location')
                .then(function(res) {
                   $scope.favLocations = res.data;

                    $scope.search(function allLoaded() {
                        $scope.isLocationResultsLoading = false;
                        $scope.$evalAsync();
                    });

                    $scope.$evalAsync();
                });

            $scope.go = function(view) {
                $scope.view = view;
                //if(view === 'map') {
                //    $scope.reloadLocations();
                //}
            }
        });
    }];
});
