/***
 * Info
 * google maps example html https://github.com/angular-ui/angular-google-maps/blob/97e9649d565bad593831e969a5bb9d36c067a575/example/example.html
 * google maps example controller: https://github.com/angular-ui/angular-google-maps/blob/abce610a29649159363ce071829d7d1d9c8ec4d3/example/assets/scripts/controllers/example.js
 * google maps loaded event: //http://stackoverflow.com/questions/28247260/angular-google-maps-run-function-once-after-initial-map-load
 */
define(['lodash', 'angular'], function (_, angular) {
    'use strict';
    // TODO: Refactor
    return ['$scope', '$timeout', 'notificationManager', 'appConfig', '$http', 'locationService', 'utilityService', '$translate', '$state', 'uiGmapIsReady', function ($scope, $timeout, notificationManager, appConfig, $http, locationService, utilityService, $translate, $state, uiGmapIsReady) {
        // start the loader
        //$scope.isLoading = true;
        $scope.location = {};
        $scope.existingMarkers = [];

        locationService.getUserLocation(function (lat, long) {
            $timeout(function () {
                $scope.map = {
                    center: {
                        latitude: lat,
                        longitude: long
                    },
                    zoom: 14
                }
            });
        });

        // initialize the marker scope - set draggable, and etc.
        $scope.marker = {
            options: {draggable: true},
            events: {
                dragend: function () {
                    $scope.marker.options = {
                        draggable: true
                    };
                }
            }
        };

        var events = {
            places_changed: function (searchBox) {
                var place = searchBox.getPlaces();
                if (!place || place == 'undefined' || place.length == 0) {
                    //console.log('no place data :(');
                    return;
                }

                $scope.map = {
                    "center": {
                        "latitude": place[0].geometry.location.lat(),
                        "longitude": place[0].geometry.location.lng()
                    },
                    "zoom": 18
                };
                $scope.marker.id = utilityService.guid();
                $scope.marker.coords = {
                    latitude: place[0].geometry.location.lat(),
                    longitude: place[0].geometry.location.lng()
                };
                setCityInfo($scope.marker, place[0]);
                $scope.marker.formattedAddress = place[0].formatted_address;
            }
        };

        var setCityInfo = function(marker, googleApiResult){
            var getComponentByName = function(googleApiResult, componentName) {
                var element = _.findLast(googleApiResult.address_components, function (addressComponent) {
                    return addressComponent.types && addressComponent.types.indexOf(componentName) > -1;
                });
                if (element) {
                    return element.long_name || element.short_name;
                } else {
                    return undefined;
                }
            };

            marker.city = {};

            marker.city.country = getComponentByName(googleApiResult, "country");
            marker.city.name = getComponentByName(googleApiResult, "locality");
            marker.city.district = getComponentByName(googleApiResult, "administrative_area_level_1");
            marker.city.municipality = getComponentByName(googleApiResult, "administrative_area_level_2") || marker.city.district;
            marker.city.postalCode = getComponentByName(googleApiResult, "postal_code");
        };

        $scope.mapEvents = {
            click: function (mapModel, eventName, originalEventArgs) {
                // 'this' is the directive's scope
                //console.log("user defined event: " + eventName, mapModel, originalEventArgs);

                var e = originalEventArgs[0];
                var lat = e.latLng.lat(),
                    lon = e.latLng.lng();
                $scope.marker = {
                    id: utilityService.guid(),
                    coords: {
                        latitude: lat,
                        longitude: lon
                    }
                };

                // get the address of the place
                var url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat + "," + lon
                    + '&language=' + $translate.use();
                $http.get(url).then(function (response) {
                    var d = response.data;
                    if (d.results) {
                        var addr = d.results[0].formatted_address;

                        $scope.location.address = addr;
                        setCityInfo($scope.marker, d.results[0]);
                        $scope.marker.formattedAddress = addr;
                    }
                });

                //scope apply required because this event handler is outside of the angular domain
                $scope.$evalAsync();
            }
        };



        $scope.searchBox = {template: 'searchBox.template.html', events: events, parentdiv: 'searchBoxParent'};

        $scope.create = function (isValid) {
            if (isValid && $scope.marker && $scope.marker.coords) {
                var fileSelector = $('#file-selector-id');
                if(fileSelector && fileSelector[0] && fileSelector[0].files && fileSelector[0].files[0]) {
                    var szInMb = fileSelector[0].files[0].size / (1024.0 * 1024.0);
                    if(szInMb >= 5) {
                        return notificationManager.error('FILE SIZE IS BIGGER THAN 5MB');
                    }
                }

                var url = appConfig.baseApiUrl + 'location';

                var location = {
                    name: $scope.location.name,
                    price: $scope.location.price,
                    playersCount: $scope.location.playersCount,
                    phone: $scope.location.phone,
                    totalFieldsCount: $scope.location.totalFieldsCount,
                    coordinates: [
                        $scope.marker.coords.longitude,
                        $scope.marker.coords.latitude
                    ],
                    formattedAddress: $scope.marker.formattedAddress,
                    city: $scope.marker.city
                };

                $http.post(url, location).then(function ok(response) {
                    $scope.existingMarkers.push(locationService.locationToMarker(response.data));
                    $scope.marker.id = '';
                    $scope.marker.coords = {};

                    var locationId = response.data._id;

                    // now upload the file.
                    var imgUrl = appConfig.baseApiUrl + 'location/' + locationId + '/image';
                    $scope.uploadFile(imgUrl);

                    $timeout(function() {
                        return $state.go('base.location.view', { id: locationId });
                    }, 1000); // delay with a sec...

                }, function error() {
                    return notificationManager.error('GENERAL ERROR');
                });
            }
        };


        var loadExistingLocations = function () {
            var url = appConfig.baseApiUrl + 'location';
            $http.get(url).then(function ok(response) {
                var locations = response.data;
                if (locations) {
                    var markers = _.map(locations, locationService.locationToMarker);

                    // force $scope.digest loop
                    $scope.existingMarkers =  angular.copy( markers );
                    //$scope.$evalAsync();
                }
            });
        };

        $scope.$watch($scope.active, function() {
            $timeout(function() {
                uiGmapIsReady.promise().then(function () {
                    loadExistingLocations();
                });
            }, 0);
        });

        $scope.fileSelectorHolderText = 'LOCATION IMAGE';
    }];
});
