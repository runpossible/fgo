define(['lodash'], function (_) {
    'use strict';
    // TODO: Refactor
    return ['$scope', 'appConfig', '$http', '$stateParams', 'locationService', 'notificationManager', '$q', '$timeout', 'imageService',
        function($scope, appConfig, $http, $stateParams, locationService, notificationManager, $q, $timeout, imageService) {
        $scope.isLoading = true;

        var id = $stateParams.id;
        var locationUrl = appConfig.baseApiUrl + 'location/' + id;
        var favLocationsUrl = appConfig.baseApiUrl + 'favourite/location';

        $scope.render = false;

        $q.all([$http.get(locationUrl), $http.get(favLocationsUrl)]).then(function(values) {
            $scope.location = values[0].data;
            $scope.favLocations = values[1].data;

            $scope.location.imageUrl = imageService.getLocationImageUrl($scope.location);

            var marker = locationService.locationToMarker($scope.location);
            $scope.marker = marker;
            $scope.map = {
                center: {
                    longitude: marker.coords.longitude,
                    latitude: marker.coords.latitude
                },
                options: {
                    draggable: false
                },
                zoom: 14
            };

            $scope.isLoading = false;

            $timeout(function() {
                $scope.render = !$scope.render;
            }, 100);
        });

        $scope.isFavLocation = function(id) {
            $scope.favLocations = $scope.favLocations || [];
            var isFav = _.some($scope.favLocations, function(loc) {
                return loc.location._id  === id;
            });

            return isFav;
        };

        $scope.addFavLocation = function(id) {
            $http.post(appConfig.baseApiUrl + 'favourite/location/' + id)
                .then(function(response) {
                    // update the fav locations list
                    $scope.favLocations = $scope.favLocations || [];
                    $scope.favLocations.push({
                        location: {
                            _id: id
                        }
                    });

                    return notificationManager.success('LOCATION ADDED TO FAVOURITES SUCCESSFULLY')
                }, function err(response) {
                    if(response.data.errorMessage != 'LOCATION ALREADY ADDED'){
                        return notificationManager.error('GENERAL ERROR');
                    }
                });
        };
            $scope.removeFavLocation = function(id) {
                $http.delete(appConfig.baseApiUrl + 'favourite/location/' + id)
                    .then(function(response) {
                        // update the fav locations list
                        $scope.favLocations = $scope.favLocations || [];
                        _.remove($scope.favLocations, function(favL) {
                           return favL.location._id === id;
                        });

                        return notificationManager.success('LOCATION REMOVED FROM FAVOURITES SUCCESSFULLY')
                    }, function err(response) {
                        return notificationManager.error('GENERAL ERROR');
                    });
            };
    }];
});
