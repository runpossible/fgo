define(['lodash'], function (_) {
    'use strict';

    return ['appConfig', function (appConfig) {
        return {
            restrict: 'E',
            templateUrl: '/js/locations/directives/location-image.html?t=' + window.requestSuffix,
            replace: true,
            scope: {
                location: '='
            },
            link: function ($scope) {
                $scope.$watch('location', function() {
                    if($scope.location) {
                        $scope.locationImageUrl = appConfig.baseApiUrl + 'location/' + $scope.location._id + '/image';
                    }
                });
            }
        };
    }]
});