function showNav(){
    $("#main-menu").addClass("menu-show");
    $("#close-nav-btn").addClass("close-show");
    $("body").css("overflow-y","hidden");
}

function closeNav(){
    $(".menu-show").removeClass("menu-show");
    $("#notifications-list").removeClass("notification-show");
    $("#close-nav-btn").removeClass("close-show");
    $("body").css("overflow-y","auto");
}

function showNotifications(){
    if($("#notifications-list").hasClass("notification-show")){
        $("#notifications-list").removeClass("notification-show");
        $("#close-nav-btn").removeClass("close-show");
    }else{
        $("#notifications-list").addClass("notification-show");
        $("#close-nav-btn").addClass("close-show");
        $("body").css("overflow-y","hidden");
    }
}

function showSettings(){
    if($("#profile-settings").hasClass("settings-show")){
        $("#profile-settings").removeClass("settings-show");
    }else{
        $("#profile-settings").addClass("settings-show");
    }
}